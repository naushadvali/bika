<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Barcodes extends Model
{
  protected $table = 'm_barcodes';
  protected $fillable = ['year','code','codeA','codeB'];

}
