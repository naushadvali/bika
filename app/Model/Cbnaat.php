<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cbnaat extends Model
{
	protected $table = 't_cbnaat';
    protected $fillable = ['enroll_id','sample_id','nikshay_id','user_id','result_MTB','result_RIF',
		'next_step','status','test_date','created_by','updated_by','error','reason_edit','edit_microbiologist'];

    // public static function cbnaat_list(){
    //     try{
    //     	$name = Cbnaat::select('name as name')->first();
	   //      $user = $name->name;
	   //      $data['user'] = $user;
    //     	$cbnaat = Cbnaat::all();
    //   		$data['cbnaat'] = $cbnaat;


    //   		return view('admin.cbnaat.list',compact('data'));

    //     }catch(\Exception $e){

    //         return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
    //     }

    // }

}
