<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    //
    protected $table = 'M_facility_type';
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';
    
}
