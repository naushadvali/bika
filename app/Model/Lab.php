<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Lab extends Model
{
	protected $table = 'lab';
    protected $fillable = ['name', 'lab_code','location'];
    
    public static function lab_list(){
        try{
        	$name = User::select('name as name')->first();
	        $user = $name->name;
	        $data['user'] = $user;
        	$lab = Lab::select('id','name','lab_code','location')->get();    
      		$data['lab'] = $lab;
      		return view('admin.lab.list',compact('data'));
            
        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }

    }
}
