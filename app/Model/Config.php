<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
   protected $table = 'm_configuration';
  protected $fillable = ['lab_name','lab_code','city','address','details','logo','micro_name','micro_email','micro_number','barcode_offset','sink_schedule','sink_user','sink_user_id','sink_password','status'];
}
