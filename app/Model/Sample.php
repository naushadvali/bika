<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Http\Responce;
use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    protected $table = 'sample';
    protected $fillable = ['enroll_id', 'sample_label', 'nikshay_id', 'user_id', 'name','receive_date',
    'sample_quality', 'is_accepted', 'test_reason', 'sample_type', 'fu_month', 'no_of_samples',
    'visual','service_id','others_type'];

    public static function add(){
        try{
          $enroll_id = Enroll::create([]);
          $data['enroll_id'] = $enroll_id->id;
          return $data;

        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }

    }

    public static function store(Request $request){


          $count = count($request->sample_id);
          if($count){
            $enroll = Enroll::find($request->enroll_id);
            $enroll->label = substr_replace($request->sample_id[0] ,"",-1);
            $enroll->save();
          }

          for ($i=0; $i < $count; $i++) {


            if($request->sample_type[$i]=="Other"){
              $sample_type = $request->other_sample_type[$i];
            }else{
              $sample_type = $request->sample_type[$i];
            }

              if($request->rejection=='Other reason'){
                $request->rejection = $request->reason_reject;
              }

             $request->name =  ucfirst ($request->name);
             //dd($request->receive_date[$i]);
             if($request->fu_month[$i]=='Other'){
               $month=$request->followup_other[$i];
             }
             else {
               $month=$request->fu_month[$i];
             }

             if($request->service_id[$i] == '8F' || $request->service_id[$i] == '8S' ){
               $request->service_id[$i].set('8');
             }

             $data = Sample::insertGetId([
              'name' => $request->name,
              'nikshay_id' => $request->nikshay_id,
              'sample_label' => $request->sample_id[$i],
              'receive_date' => $request->receive_date[$i],
              'sample_quality' => $request->sample_quality[$i],
              'sample_type' => $sample_type,
              'is_accepted' => $request->is_accepted[$i],
              'rejection'  => $request->rejection,
              'test_reason' => $request->test_reason[$i],
              'fu_month' => $month,
              'enroll_id' => $request->enroll_id,
              'user_id' => $request->user()->id,
              'no_of_samples' => $request->no_of_samples,
              'service_id' => $request->service_id[$i],
              'others_type' => $request->others_type[$i],
              'created_at' => date('Y-m-d H:i:s'),

            ]);
            if($request->service_id[$i]==env('STORAGE_ID')){
              $status = 2;
            }else{
              $status = 1;
            }
            $type = '';
            if($request->service_id[$i] == '8F'){
              $type = 'LPA1';
              $request->service_id[$i].set('8');
            }else if($request->service_id[$i] == '8S'){
              $type = 'LPA2';
              $request->service_id[$i].set('8');
            }
            ServiceLog::create([
              'enroll_id' => $request->enroll_id,
              'sample_id' => $data,
              'service_id' => $request->service_id[$i],
              'enroll_label' => substr_replace($request->sample_id[0] ,"",-1),
              'created_by' => $request->user()->id,
              'updated_by' => $request->user()->id,
              'sample_label' => $request->sample_id[$i],
              'tag' => $type,
              'status' => $status
            ]);
          }
      		return true;



    }

     public static function edit($id){
        try{

          return $data;


        }catch(\Exception $e){

            return (['ret'=>[],'err'=>['code'=>1, 'msg'=>$e->getMessage()]]);
        }

    }
}
