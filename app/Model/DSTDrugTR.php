<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DSTDrugTR extends Model
{
  protected $table = 't_dst_drugs_tr';
  protected $fillable = ['enroll_id','sample_id','drug_ids','status','flag','created_by','updated_by'];
}
