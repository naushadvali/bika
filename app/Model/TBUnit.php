<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TBUnit extends Model
{
    //
    protected $table = 'M_tbunit';
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';
}
