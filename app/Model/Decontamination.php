<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Decontamination extends Model
{
    protected $table = 't_decontamination';
    protected $fillable = ['enroll_id','sample_id','status','test_date','created_by','updated_by','sent_for','other'];
}
