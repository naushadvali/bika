<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Http\Responce;
use Illuminate\Database\Eloquent\Model;

class ServiceLog extends Model
{
  protected $table = 't_service_log';
  protected $fillable = ['enroll_id','created_by','updated_by','sample_id','service_id', 'status', 'enroll_label', 'sample_label','tag'];

  public static function microscopyLog(Request $request)
  {
    $logdata = ServiceLog::find($request->service_log_id);
    $data = Microscopy::create([
      'sample_id' => $logdata->sample_id,
      'enroll_id' => $logdata->enroll_id,
      'result' => 'NA',
      'created_by' => $request->user()->id,
      'updated_by' => $request->user()->id,
      'status' => 2
    ]);
    return $data;
  }

  public static function dnaExtractionLog(Request $request)
  {
    $logdata = ServiceLog::find($request->service_log_id);
    $data = DNAextraction::create([
      'sample_id' => $logdata->sample_id,
      'enroll_id' => $logdata->enroll_id,
      'created_by' => $request->user()->id,
      'updated_by' => $request->user()->id,
      'status' => 2
    ]);
    return $data;
  }
}
