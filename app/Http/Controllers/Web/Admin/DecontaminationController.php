<?php

namespace App\Http\Controllers\Web\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Decontamination;
use App\Model\Sample;
use App\Model\Service;
use App\Model\Microscopy;
use App\Model\Enroll;
use App\Model\ServiceLog;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Microbio;

use Illuminate\Support\Facades\DB;

class DecontaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            $data = [];
            $data['today'] = date('Y-m-d H:i:s');

            // $data['services'] = ['1' => 'ZN Microscopy', '2' => 'FM Microscopy', '3' => 'Culture inoculation for LC', '4' => 'Culture inoculation for LJ', '5' => 'Culture inoculation for LC & LJ', '8' => 'DNA Extraction'];

            $data['services'] = ['1' => 'ZN Microscopy', '2' => 'FM Microscopy', '3' => 'Liquid Culture', '4' => 'Solid Culture', '5' => 'Solid and Liquid Culture', '8' => 'DNA Extraction', '7' => 'For storage'];

            // $data['services_copy'] = ['1' => 'ZN Microscopy', '2' => 'FM Microscopy', '5' => 'Liquid Culture', '4' => 'Solid Culture', '5' => 'Solid and Liquid Culture', '8' => 'DNA Extraction', '6' => 'Request for another sample', '7' => 'For storage', '9' => 'BWM'];

            $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','t_service_log.enroll_label',
            't_service_log.sent_to as sent_for_service',
            't_service_log.enroll_id','t_service_log.sample_label as samples',
            't_service_log.sample_id as sample_id','s.test_reason',
            's.sample_type','m.result as result','t_service_log.tag','s.fu_month',
            's.no_of_samples',
            't_service_log.status')
            //->join('t_decontamination as d','d.sample_id','t_service_log.sample_id')
             // ->leftjoin('t_decontamination as d',function($join)
             //            {

             //                  $join->on('t_service_log.sample_id','=','d.sample_id')
             //                        ->where('t_service_log.enroll_id','=','d.enroll_id');
             //            })
             ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
             // ->leftjoin('m_services as sr','sr.id','=','d.sent_for')
             ->leftjoin('t_microscopy as m',function($join)
                        {

                              $join->on('m.sample_id','=','t_service_log.sample_id')
                                    ;
                        })
            ->whereIn('t_service_log.status',[0,2])
            ->where('t_service_log.service_id','=',3)
            //->where('sr.name','!=',null)
            ->orderBy('t_service_log.enroll_id','desc')
            ->distinct()
            ->get();

            foreach ($data['sample'] as $key => $value) {
              $value->no_sample = ServiceLog::where('enroll_id',$value->enroll_id)->where('service_id',11)->count();
              $date = DB::table('t_decontamination as d')->select(DB::raw('date_format(d.test_date,"%d-%m-%y") as date'))
              ->where('d.sample_id',$value->sample_id)->first();
              $value->date = $date->date;
            }


            // $data['sample'] = ServiceLog::select('m.result')
            //             ->leftjoin('t_microscopy as m',function($join)
            //             {

            //                   $join->on('t_service_log.sample_id','=','m.sample_id')
            //                         ->where('t_service_log.enroll_id','=','m.enroll_id');
            //             })
            //             ->get();

            //dd($data['sample']);



            $data['decontamination_test'] = ServiceLog::select('id')->whereIn('status',[0,1,2])->where('service_id',3)->count();

            $data['decontamination_tested'] = ServiceLog::select('id')->where('status',1)->where('service_id',3)->count();


            $data['decontamination_review'] = ServiceLog::select('id')->where('status',2)->where('service_id',3)
                        ->count();


            return view('admin.decontamination.list',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $en_label = Enroll::select('label as l')->where('id',$request->enrollId)->first();
         $s_id = Sample::select('id as l')->where('sample_label',$request->sample_ids)->first();
         $enroll_label=$en_label->l;
         $sample_id=$s_id->l;
         $services_sent_to='';
         $data['services'] = ['1' => 'ZN Microscopy', '2' => 'FM Microscopy', '3' => 'Liquid Culture', '4' => 'Solid Culture', '5' => 'Solid and Liquid Culture', '8' => 'DNA Extraction', '6' => 'Request for another sample', '7' => 'For storage','Send to BWM' => 'BWM'];
         foreach($data['services'] as $key=>$value){
           if(in_array($key, $request->service_id)){
              $services_sent_to = $services_sent_to . " , " . $value;
           }
         }
         //$services_sent_to = DB::table('m_services')->whereIN('id',$request->service_id)->pluck('name')->toArray();
         $services_sent_to= substr($services_sent_to, 3);
         //$services_sent_to = implode($services_sent_to,',');
         $logservice = ServiceLog::where('enroll_id',$request->enrollId)->where('sample_id',$sample_id)->where('service_id',3)->first();
         $logservice->sent_to = $services_sent_to;
         $logservice->save();


         $log = ServiceLog::where('enroll_id',$request->enrollId)->where('sample_id',$sample_id)->first();
         if($log){
           $log->status = 0;
           $log->updated_by = $request->user()->id;
           $data = $log;
           $log->save();
         }

         if($request->request_another =='on'){
           $log = ServiceLog::where('enroll_id',$request->enrollId)->where('service_id',11)->first();
           if($log){
             $log->service_id = 3;
             $log->status = 1;
             $log->updated_by = $request->user()->id;
             $data = $log;
             $log->save();

           }
           else{

             return redirect('/decontamination');
           }
         }

        foreach($request->service_id as $service_id){
                if($service_id==3){
                    $request->tag = 'LC';
                }elseif($service_id==4){
                    $request->tag = 'LJ';
                }elseif($service_id==5){
                    $request->tag = 'LC and LJ Both';
                }

                if($service_id=='Send to BWM')
                {
                  $decon = Decontamination::select('id')->where('sample_id',$sample_id)->where('enroll_id',$request->enrollId)->first();
                  $decon_obj = Decontamination::find($decon->id);
                    $microbio = Microbio::create([
                      'enroll_id' => $request->enrollId,
                      'sample_id' => $sample_id,
                      'service_id' => 26,
                      'next_step' => '',
                      'detail' => '',
                      'remark' => '',
                      'bwm' => 1,
                      'status' => 0,
                      'created_by' => Auth::user()->id,
                       'updated_by' => Auth::user()->id
                    ]);
                    $decon_obj->status = 9;
                    $decon_obj->updated_by = $request->user()->id;
                    $decon_obj->save();
                    ServiceLog::create([
                       'enroll_id' => $request->enrollId,
                       'sample_id' => $sample_id,
                       'enroll_label' => $enroll_label,
                       'sample_label' => $request->sample_ids,
                       'service_id' => 26,
                       'status' => 1,
                       'tag' => $request->tag,
                       'test_date' => date('Y-m-d H:i:s'),
                       'created_by' => Auth::user()->id,
                       'updated_by' => Auth::user()->id
                     ]);
                  //break;
                }
                if($service_id==-1)
                {
                  return redirect('/decontamination');
                }
                if(!$request->other){
                    $request->other='';
                }



                ServiceLog::where('enroll_id', $request->enrollId)
                    ->where('sample_id', $sample_id)
                    ->where('sample_label', $request->sample_ids)
                    ->where('service_id',3)
                    ->update(['status' => 0 ,'created_by' => Auth::user()->id,'updated_by' => Auth::user()->id]);
                if($service_id == 3 || $service_id == 4 || $service_id == 5 ){
                  $service_id = 16;
                }
                if($service_id == 7){
                  $service_id = 11;
                }




               Decontamination::create([
                 'enroll_id' => $request->enrollId,
                  'sample_id' => $sample_id,
                  'sent_for' => $service_id,
                  'status' => 2,
                  'test_date' => date('Y-m-d H:i:s'),
                  'created_by' => Auth::user()->id,
                  'updated_by' => Auth::user()->id
                ]);

               // if($request->service_id=='micro'){
               //  $microbio = Microbio::create([
               //      'enroll_id' => $request->enrollId,
               //      'sample_id' => $sample_id,
               //      'service_id' => 3,
               //      'next_step' => '',
               //      'detail' => '',
               //      'remark' => '',
               //      'status' => 0,
               //      'created_by' => $request->user()->id,
               //       'updated_by' => $request->user()->id
               //    ]);
               //  return redirect('/decontamination');
               // }
               ServiceLog::create([
                  'enroll_id' => $request->enrollId,
                  'sample_id' => $sample_id,
                  'enroll_label' => $enroll_label,
                  'sample_label' => $request->sample_ids,
                  'service_id' => $service_id,
                  'status' => 1,
                  'tag' => $request->tag,
                  'test_date' => date('Y-m-d H:i:s'),
                  'created_by' => Auth::user()->id,
                  'updated_by' => Auth::user()->id
                ]);
              }

                // if($service_id=='Send to BWM'){
                //   $decon = Decontamination::select('id')->where('sample_id',$sample_id)->where('enroll_id',$request->enrollId)->first();
                //   $decon_obj = Decontamination::find($decon->id);
                //     $microbio = Microbio::create([
                //       'enroll_id' => $request->enrollId,
                //       'sample_id' => $sample_id,
                //       'service_id' => 3,
                //       'next_step' => '',
                //       'detail' => '',
                //       'remark' => '',
                //       'bwm' => 1,
                //       'status' => 0,
                //       'created_by' => Auth::user()->id,
                //        'updated_by' => Auth::user()->id
                //     ]);
                //     $decon_obj->status = 9;
                //     $decon_obj->updated_by = $request->user()->id;
                //     $decon_obj->save();
                //   }

        return redirect('/decontamination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function decontaminationPrint()
    {

            $data = [];
            $data['today'] = date('Y-m-d H:i:s');

            $data['services'] = ['1' => 'ZN Microscopy', '2' => 'FM Microscopy', '3' => 'Culture inoculation for LC', '4' => 'Culture inoculation for LJ', '5' => 'Culture inoculation for LC & LJ', '8' => 'DNA Extraction'];


            $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','t_service_log.enroll_label','t_service_log.enroll_id','t_service_log.sample_label as samples','t_service_log.sample_id','d.test_date','d.sent_for','s.test_reason','s.sample_type','m.result as result','t_service_log.tag',DB::raw('date_format(m.created_at,"%d-%m-%y") as date'))
             ->leftjoin('t_decontamination as d',function($join)
                        {

                              $join->on('t_service_log.sample_id','=','d.sample_id')
                                    ->where('t_service_log.enroll_id','=','d.enroll_id');
                        })
             ->join('sample as s','s.id','=','t_service_log.sample_id')
             ->leftjoin('t_microscopy as m',function($join)
                        {

                              $join->on('m.sample_id','=','t_service_log.sample_id')
                                    ;
                        })
            ->where('t_service_log.status',2)
            ->where('t_service_log.service_id',3)
            ->orderBy('t_service_log.enroll_id','desc')
            ->distinct()
            ->get();



            return view('admin.decontamination.print',compact('data'));

    }
}
