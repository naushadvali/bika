<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\Microscopy;
use App\Model\DNAextraction;
use App\Model\Decontamination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DNAextractionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $data = [];
        $data['today'] = date('d-m-Y H:i:s');
        $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id', 'm.receive_date as receive',
                    'm.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples',
                    't_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples',
                    't.status as dna_status','t_service_log.tag',DB::raw('date_format(d.test_date,"%d-%m-%y") as decontamination_date'),
                    't.created_at as extraction_date','t_service_log.status as STATUS')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_decontamination as d', function ($join) {
              $join->on('d.sample_id','=','t_service_log.sample_id')
                   ->where('d.status', 1);
          })
        ->where('t_service_log.service_id',8)
        ->whereIn('t_service_log.status',[0,1,2])
        ->orderBy('enroll_id','desc')
        ->distinct()
        ->get();
        foreach ($data['sample'] as $key => $value) {
          $value->no_sample = ServiceLog::where('enroll_id',$value->enroll_id)->where('service_id',11)->count();
        }
        $data['summaryTotal'] = ServiceLog::whereIn('status',[1,2])
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summaryDone'] = ServiceLog::where('status',1)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summarySent'] = ServiceLog::where('status',2)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['services'] = Service::select('id','name')->where('record_status',1)->get();
        return view('admin.DNAextraction.list',compact('data'));
      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $log = ServiceLog::find($request->log_id);
        $data = DNAextraction::create([
          'enroll_id' => $log->enroll_id,
          'sample_id' => $log->sample_id,
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'status' => 1
        ]);
        $log->status = 2;
        $log->save();
        return redirect('/DNAextraction');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function DNANext(Request $request)
    {
      $log = ServiceLog::find($request->service_log_id);
      $data = DNAextraction::create([
        'enroll_id' => $log->enroll_id,
        'sample_id' => $log->sample_id,
        'created_by' => $request->user()->id,
        'updated_by' => $request->user()->id,
        'status' => 1
      ]);
      //dd('ddd');
        if($request->service_id == 1 || $request->service_id == 2){
          $log = ServiceLog::find($request->service_log_id);
          $log->status = 0;
          $log->updated_by = $request->user()->id;
          $data = $log;
          $log->save();
          $log = ServiceLog::where('enroll_id',$request->enroll_id)->where('service_id',11)->first();
          if($log){
            $log->service_id = $request->service_id == 2?8:3;
            $log->updated_by = $request->user()->id;
            $data = $log;
            $log->save();

          }
          return redirect('/DNAextraction');
        }elseif($request->service_id == 3){
          $old_sample = Sample::select('sample_label')->where('id',$data->sample_id)->first();
          $new_sample = $old_sample->sample_label.'R';
          Sample::where('id',$data->sample_id)->update(['sample_label'=>$new_sample]);
          ServiceLog::where('sample_id',$data->sample_id)->where('status','!=',0)->update(['sample_label'=>$new_sample]);
          $log = ServiceLog::find($request->service_log_id);
          $log->status = 1;
          $log->sample_label = $new_sample;
          $log->updated_by = $request->user()->id;
          $data = $log;
          $log->save();
          DNAextraction::where('sample_id', $data->sample_id)
          ->where('enroll_id', $request->enroll_id)
          ->where('status',1)
          ->update(['status' => 0]);
          return redirect('/DNAextraction');
        }elseif($request->service_id == 4 || $request->service_id == 5 || $request->service_id == 6){
          if($request->service_log_id > 0){
            $service = ServiceLog::find($request->service_log_id);
            $service->status = 0;
            $service->updated_by = $request->user()->id;
            $service->save();
            if($request->service_id == 4){
              $tag = '1st line LPA';
            }elseif($request->service_id == 5){
              $tag = '2st line LPA';
            }else{
              $tag = '1st line LPA  and for 2nd line LPA';
            }
            $new_service = [
              'enroll_id' => $service->enroll_id,
              'sample_id' => $service->sample_id,
              'service_id' => 12,
              'status' => 1,
              'tag' => $tag,
              'created_by' => $request->user()->id,
              'updated_by' => $request->user()->id,
              'enroll_label' => $service->enroll_label,
              'sample_label' => $service->sample_label,
            ];

            $nwService = ServiceLog::create($new_service);
            //return $nwService;
            return redirect('/DNAextraction');
        }
      }
    }


    public function dnaextractprint()
    {

        $data = [];
        $data['today'] = date('d-m-Y H:i:s');
        $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id', DB::raw('date_format(m.receive_date,"%d-%m-%y") as receive'),'m.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples','t.status as dna_status','t_service_log.tag',DB::raw('date_format(d.test_date,"%d-%m-%y") as decontamination_date'),DB::raw('date_format(t.created_at,"%d-%m-%y") as extraction_date'))
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_decontamination as d', function ($join) {
              $join->on('d.sample_id','=','t_service_log.sample_id')
                   ->where('d.status', 1);
          })
        ->where('t_service_log.service_id',8)
        ->whereIn('t_service_log.status',[1,2])
        ->orderBy('enroll_id','desc')
        ->distinct()
        ->get();

        return view('admin.DNAextraction.print',compact('data'));

    }
}
