<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Patient;
use App\Model\State;
use App\Model\Enroll;
use App\Model\District;
use App\Model\Sample;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Model\Occupation;
use App\Model\Tbunits_master;
use App\Model\PHI_master;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         return Patient::patient_list();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data =  Patient::patient_form();
        $data['adhaar_no'] = '';
        return view('admin.patient.form',compact('data'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Patient::patient_add();
        return redirect('/admin/enroll/patient');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient =  Patient::find($id);
        $data['adhaar_no'] = Patient::where('id',$id)->select(DB::raw('RIGHT(adhar_no, 4) as adhar_no'))->first();
        if($data['adhaar_no']->adhar_no){
              $data['adhaar_no']->adhar_no = '********'.$data['adhaar_no']->adhar_no;
        }

        $enroll =  Enroll::select('id')->where('patient_id',$id)->first();
        //dd($id);


        //dd($patient);
        $label = Enroll::select('label as label')->where('id',$enroll->id)->first();
        $name = Sample::select('name as name')->where('enroll_id',$enroll->id)->first();

        if($name){
            $state = State::orderBy('name','ASC')->get();
            //$district = District::all();
            $occupation = Occupation::all();
            $data['label']=$label->label;

            // $data['phi']=DB::table('m_phi')->get();
            // $data['tb']=DB::table('m_tb')->get();

            // $data['phi']=PHI_master::select('m_dmcs_phi_relation.id','m_dmcs_phi_relation.DMC_PHI_Name','m_dmcs_phi_relation.DMC_PHI_Code')->leftjoin('patient as req','req.district','=','m_dmcs_phi_relation.DTOCode')->where('req.district',$patient->district)->get();
            // $data['tb']=Tbunits_master::select('m_tbunits_relation.id','m_tbunits_relation.TBUnitCode','m_tbunits_relation.TBUnitName')->join('patient as req','req.district','=','m_tbunits_relation.DTOCode')->where('req.district',$patient->district)->get();
            $data['district']=District::select('district.id','district.DTOCode','district.name')->where('district.STOCode',$patient->state)->orderBy('district.name','ASC')->get();
            $data['district2']=District::select('district.id','district.DTOCode','district.name')->where('district.STOCode',$patient->landmark_state)->orderBy('district.name','ASC')->get();
            $data['patient'] = $patient;

            $data['phi'] = PHI_master::select('m_dmcs_phi_relation.id','m_dmcs_phi_relation.DMC_PHI_Name','m_dmcs_phi_relation.DMC_PHI_Code')->where('m_dmcs_phi_relation.TBUCode',$data['patient']->tb)->where('m_dmcs_phi_relation.DTOCode',$data['patient']->district)->get();
            $data['tb'] = Tbunits_master::select('m_tbunits_relation.id','m_tbunits_relation.TBUnitCode','m_tbunits_relation.TBUnitName')->where('m_tbunits_relation.DTOCode',$data['patient']->district)->where('m_tbunits_relation.STOCode',$data['patient']->state)->get();
            //dd($data['patient']);
            //$data['nikshay_id'] = $nikshay_id;
            //dd($data['patient']);
            $data['state'] = $state;
            //$data['district'] = $district;
            $data['name'] = $name->name;
            $data['today'] = date('dd/mm/yyyy');
            //dd($data['today']);

            $data['occupation'] = $occupation;
          }
        return view('admin.patient.form',compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

                //dd($request->all());
                // $validator = Validator::make($request->all(), [
                // 'pincode' => 'digits_between:6,6',
                // 'adhar_no' => 'max:12',
                // 'mobile_number' => 'max:10',
                // ]);
                // if ($validator->fails()) {
                //     return redirect('/enroll')
                //                 ->withErrors($validator)
                //                 ->withInput();
                // }

                $patient = Patient::find($id);
                $data['adhaar_no'] = '';
                $regr_date = strtotime($request->regr_date);
                $request->regr_date = date('Y-m-d H:i:s',$regr_date);
                //dd($request->regr_date);
                $request->request->add(['reg_by' => Auth::user()->name]);

                $val = $patient->update($request->all());
                //dd($val);
                return redirect('/enroll');

        }catch(\Exception $e){
            $error = $e->getMessage();
            return view('admin.layout.error',$error);   // insert query
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function district_collect($state)
    {
        try{


            $district_val = District::orderBy('name')->select('DTOCode','name')->where('STOCode',$state)->get();
            //$district_val = District::all();

            return response()->json([
              "district" => $district_val
            ]);



        }catch(\Exception $e){
            $error = $e->getMessage();
            return view('admin.layout.error',$error);   // insert query
        }
    }
}
