<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SampleQuality;
use App\Model\Sample;
use App\Model\State;
use App\Model\District;
use App\Model\Facility;
use App\Model\FacilityType;
use App\Model\ServiceLog;
use App\Model\Service;
use App\Model\Enroll;
use App\Model\TBUnit;
use App\Model\TBDiagnosis;
use App\Model\LCDSTDrugs;
use App\Model\DSTDrugTR;
use App\Model\TBPredominanSymptom;
use App\Model\TestServices;
use App\Model\RequestServices;
use App\Model\Designations;
use App\User;
use App\Model\TestRequest;
use App\Model\Tbunits_master;
use App\Model\PHI_master;
use Illuminate\Support\Facades\DB;

class TestRequestController extends Controller
{
    public $test_request_type = ['N/A','Diagnosis TB', 'Follow Up (smear)','DSTB','DRTB'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];
        $data['sample'] = Sample::select(DB::raw('e.patient_id,sample.enroll_id,e.label, ifnull(max(tr.id),0) as tr_id, receive_date as receive,group_concat(sample_label) as samples,group_concat(test_reason) as reason,group_concat(fu_month) as fu_month'))
                          ->leftjoin('enrolls as e','e.id','=','enroll_id')
                          ->leftjoin('req_test as tr','sample.enroll_id','=','tr.enroll_id')
                          ->groupBy('enroll_id')
                          ->where('is_accepted','Accepted')
                          ->where('sample.test_reason','!=','EQA')
                          ->orderBy('enroll_id','desc')
                          ->get();
        //dd($data['sample']);
        return view('admin.test_request.list',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($enrollId)
    {
      $data['enroll_id'] = $enrollId;
      $data['date'] = date('Y-m-d H:i:s');
      $data['testrequest'] = [];
      $reqservices = RequestServices::all();
      $data['testrequestservices'] = new RequestServices();
      //$data['diagnosis_value'] = new TBDiagnosis();
      $data['prsmptv_xdrtv']=[];
      $data['type_of_prsmptv_drtb']=[];
      $data['regimen']=[];
      $data['diagnosis_value'] = [];
      $diagnosis = TBDiagnosis::select('diagnosis_id','name')->where('record_status',1)->get();
      $state = State::orderBy('name')->get();
      $facility_type = FacilityType::select('facility_type_id','name')->where('record_status',1)->get();
      $predominan_symptom = TBPredominanSymptom::select('symptom_id','name')->where('record_status',1)->get();
      $services = DB::table('m_test_request')->select('id','name')->where('status',1)->get();
      $services = json_decode($services, true);
      $designations = Designations::select('designation_id','name')->where('record_status',1)->get();
      $dstdrugs = LCDSTDrugs::select('id','name')->where('status',1)->get();
      $test_reason = Sample::select('test_reason as reason')->where('enroll_id',$enrollId)->first();
      $enroll_label = Enroll::select('label as enroll_label')->where('id',$enrollId)->first();
      $data['reason'] = $test_reason->reason;
      $data['state'] = $state;
      $data['reqservices'] = $reqservices;
      $data['enroll_label'] = $enroll_label->enroll_label;
      $data['dstdrugs'] = $dstdrugs;
      $data['district'] = [];
      $data['facility_types'] = Facility::all();
      $data['facility'] = [];
      $data['facility_type'] = $facility_type;
      $data['tbunit'] = [];
      $data['diagnosis'] = $diagnosis;
      $data['predominan_symptom'] = $predominan_symptom;
      $data['services'] = $services;
      $data['designations'] = $designations;
      $data['dst_id'] = env("DST_ID", "21,22,23");
      $data['lpa_id'] = env("LPA_ID", "4,5,6");
      $data['lpa_id_not'] = [];
      return view('admin.test_request.form1',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $test_req = TestRequest::updateOrCreate(['enroll_id'=>$request->enroll_id],$request->except(['services']));

        RequestServices::where('enroll_id', $request->enroll_id)->delete();
        #RequestServices::
        $en_label = Enroll::select('label as label')->where('id',$request->enroll_id)->first();
        $s_id = Sample::select('id as sample_id')->where('sample_label',$request->samplelable)->first();
        // dd($request->diagnosis?  implode(',',$request->diagnosis) : '');

        $services = $request->services;
        if($services){
            foreach($services as $service){
              $data = [
                'enroll_id' => $request->enroll_id,
                'test_req_id' => $test_req->id,
                'service_id' => $service,
                'requestor_name' => $request->requestor_name,
                'designation' => $request->designation,
                'month_week' => $request->month_week,
                'duration' => $request->duration,
                'diagnosis' => $request->diagnosis ?  implode(',',$request->diagnosis) : '',
                'contact_no' => $request->contact_no,
                'email_id' => $request->email_id,
                'created_by' => $request->user()->id,
                'request_date' => $request->request_date,
                'type_of_prsmptv_drtb' => $request->type_of_prsmptv_drtb?  implode(',',$request->type_of_prsmptv_drtb) : '',
                'presumptive_h' => $request->presumptive_h,
                'prsmptv_xdrtv' => $request->prsmptv_xdrtv ?  implode(',',$request->prsmptv_xdrtv) : '',
                'post_treatment' => $request->post_treatment,
                'other_post_treatment' => $request->other_post_treatment,
                'regimen_fu' => $request->regimen_fu,
                'rntcp_reg_no' => $request->rntcp_reg_no,
                'regimen' => $request->regimen ?  implode(',',$request->regimen) : '',
                'reason' => $request->reason,
                'post_treatment' => $request->post_treatment,
                'pmdt_tb_no' => $request->pmdt_tb_no,
                'month_week' => $request->month_week,
                'treatment' => $request->treatment,
                'ho_anti_tb' => $request->ho_anti_tb,
                'regimen_fu' => $request->regimen_fu,
                'fudrtb_regimen_other' => $request->fudrtb_regimen_other,
                'facility_type_other' => $request->facility_type_other,

              ];

              //  $servicelog = [
              //   'enroll_id' => $request->enroll_id,
              //   'sample_id' => $s_id->sample_id,
              //   'enroll_label' => $en_label->label,
              //   'sample_label' => $request->samplelable,
              //   'service_id' => $service,
              //   'status' => 1,
              //   'tag' => '',
              //   'test_date' => date('Y-m-d H:i:s'),
              //   'created_by' => $request->user()->id,
              //   'updated_by' => $request->user()->id
              // ];

              RequestServices::create($data);

          //    ServiceLog::create($servicelog);
            }
          }else{
            $tmp_regimen = '';
            if(gettype($request->regimen)=='string'){
              $tmp_regimen = $request->regimen;
            }
            else if(gettype($request->regimen)=='array'){
              $tmp_regimen = implode(',',$request->regimen);
            }
            //if($request->regimen)
            $data = [
                'enroll_id' => $request->enroll_id,
                'test_req_id' => $test_req->id,
                'requestor_name' => $request->requestor_name,
                'designation' => $request->designation,
                'duration' => $request->duration,
                'month_week' => $request->month_week,
                'diagnosis' => $request->diagnosis?  implode(',',$request->diagnosis) : '',
                'contact_no' => $request->contact_no,
                'email_id' => $request->email_id,
                'created_by' => $request->user()->id,
                'request_date' => $request->request_date,
                'type_of_prsmptv_drtb' => $request->type_of_prsmptv_drtb ? implode(',',$request->type_of_prsmptv_drtb) : '',
                'presumptive_h' => $request->presumptive_h,
                'prsmptv_xdrtv' => $request->prsmptv_xdrtv  ? implode(',',$request->prsmptv_xdrtv): '',
                'post_treatment' => $request->post_treatment,
                'other_post_treatment' => $request->other_post_treatment,
                'regimen_fu' => $request->regimen_fu,
                'rntcp_reg_no' => $request->rntcp_reg_no,
                'regimen' => $tmp_regimen,
                'reason' => $request->reason,
                'post_treatment' => $request->post_treatment,
                'pmdt_tb_no' => $request->pmdt_tb_no,
                'month_week' => $request->month_week,
                'treatment' => $request->treatment,
                'ho_anti_tb' => $request->ho_anti_tb,
                'regimen_fu' => $request->regimen_fu,
                'fudrtb_regimen_other' => $request->fudrtb_regimen_other,
                'facility_type_other' => $request->facility_type_other,


              ];
              RequestServices::create($data);
          }
        if($request->drugs){
          $drug_str = implode(',',$request->drugs);
          DSTDrugTR::create([
            'enroll_id' => $request->enroll_id,
            'sample_id' => '0',
            'drug_ids' => $drug_str,
            'status' => 1,
            'flag' => 1,
            'created_by'=>$request->user()->id,
            'updated_by'=>$request->user()->id,
          ]);
        }


        return redirect('/test_request');
    }

    /**
     * Display the specified resource.'month_week' => $request->month_week,
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testrequest = TestRequest::where('id',$id)->first();
        $data['testrequest'] = $testrequest;
        $state = State::select('name')->where('STOCode', $testrequest->state)->first();
        $district = District::select('name')->where('id', $testrequest->district)->first();
        $facility = PHI_master::all();

        $data['state'] = $state;
        $data['district'] = $district;
        $data['facility'] = $facility;
        $data['test_type'] = $this->test_request_type[$testrequest->req_test_type];

        //dd($data);
        return view('admin.test_request.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testrequest = TestRequest::where('id',$id)->first();
        $data['testrequest'] = $testrequest;
        $data['testrequestservices']=RequestServices::where('enroll_id',$testrequest->enroll_id)->first();
        $data['prsmptv_xdrtv'] = $data['testrequestservices']->prsmptv_xdrtv != null ?  explode(',',$data['testrequestservices']->prsmptv_xdrtv) : [];
        $data['type_of_prsmptv_drtb'] = $data['testrequestservices']->type_of_prsmptv_drtb != null ?  explode(',',$data['testrequestservices']->type_of_prsmptv_drtb) : [];
        $data['regimen'] = $data['testrequestservices']->regimen != null ?  explode(',',$data['testrequestservices']->regimen) : [];
        $state = State::orderBy('name')->get();
        $facility_type = FacilityType::select('facility_type_id','name')->where('record_status',1)->get();
        $diagnosis = TBDiagnosis::select('diagnosis_id','name')->where('record_status',1)->get();
        $data['diagnosis'] = TBDiagnosis::select('diagnosis_id','name')->where('record_status',1)->get();
        $diagnosis_id = $data['testrequestservices']->diagnosis != null ?  explode(',',$data['testrequestservices']->diagnosis) : [];
        $data['diagnosis_value'] = TBDiagnosis::where('record_status',1)->whereIn('diagnosis_id',$diagnosis_id)->pluck('diagnosis_id')->toArray();
        $predominan_symptom = TBPredominanSymptom::select('symptom_id','name')->where('record_status',1)->get();
        $services = DB::table('m_test_request')->select('id','name')->where('status',1)->get();
        $services = json_decode($services, true);
        $reqservices = RequestServices::where('enroll_id',$testrequest->enroll_id)->get();
        $designations = Designations::select('designation_id','name')->where('record_status',1)->get();
        $dstdrugs = LCDSTDrugs::select('id','name')->where('status',1)->get();
        $enroll_label = Enroll::select('label as enroll_label')->where('id',$testrequest->enroll_id)->first();
        $test_reason = Sample::select('test_reason as reason')->where('enroll_id',$testrequest->enroll_id)->first();
        $data['reason'] = $test_reason->reason;
        $data['dstdrugs'] = $dstdrugs;
        $data['enroll_label'] = $enroll_label->enroll_label;
        $data['enroll_id'] = $testrequest->enroll_id;
        $data['state'] = $state;
        $district= District::where('district.STOCode',$data['testrequest']->state)->distinct('district.name')->get();
        $data['district'] = $district;
        $facility = PHI_master::select('m_dmcs_phi_relation.id','m_dmcs_phi_relation.DMC_PHI_Name','m_dmcs_phi_relation.DMC_PHI_Code')->where('m_dmcs_phi_relation.TBUCode',$data['testrequest']->tbu)->where('m_dmcs_phi_relation.DTOCode',$data['testrequest']->district)->get();
        $data['facility'] = $facility;
        $data['facility_type'] = $facility_type;
        $tbunit = Tbunits_master::select('m_tbunits_relation.id','m_tbunits_relation.TBUnitCode','m_tbunits_relation.TBUnitName')->where('m_tbunits_relation.DTOCode',$data['testrequest']->district)->where('m_tbunits_relation.STOCode',$data['testrequest']->state)->get();
        $data['tbunit'] = $tbunit;
        $data['diagnosis'] = $diagnosis;
        $data['facility_types'] = Facility::all();
        $data['predominan_symptom'] = $predominan_symptom;
        $data['services'] = $services;
        $_reqservices = [];
        foreach ($reqservices as $value) {
          $_reqservices[] = $value->service_id;
        }
        $data['reqservices'] = $reqservices;
        $data['_reqservices'] = $_reqservices;
        //dd($data['reqservices']);
        $data['designations'] = $designations;
        $data['dst_id'] = env("DST_ID", "21,22,23");
        $data['lpa_id'] = env("LPA_ID", "4,5,6");
        $data['lpa_id_not'] = [];

        // return SampleQuality::sample_quality_edit($id);
        return view('admin.test_request.form1',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function testrequestPrint()
    {
        $data['sample'] = Sample::select(DB::raw('e.patient_id,sample.enroll_id,e.label, ifnull(max(tr.id),0) as tr_id, group_concat(date_format(receive_date,"%d-%m-%y")) as receive,group_concat(sample_label) as samples,group_concat(test_reason) as reason'))
                          ->leftjoin('enrolls as e','e.id','=','enroll_id')
                          ->leftjoin('req_test as tr','sample.enroll_id','=','tr.enroll_id')
                          ->groupBy('enroll_id')
                          ->orderBy('enroll_id','desc')
                          ->get();
        //dd($data['sample']);
        return view('admin.test_request.print',compact('data'));
    }

    public function phi_collect($state,$tbu,$district)
    {
        try{


            $phi_val = DB::table('m_dmcs_phi_relation')->orderBy('DMC_PHI_Name')->select('id','DMC_PHI_Name')
                        ->where('STOCode',$state)->where('TBUCode',$tbu)->where('DTOCode',$district)->distinct()->get();
            //$district_val = District::all();

            return response()->json([
              "phi" => $phi_val
            ]);



        }catch(\Exception $e){

            $error = $e->getMessage();
            return view('admin.layout.error',$error);   // insert query
        }
    }

    public function tbunit_collect($state,$district)
    {
        try{


            $tbunit_val = DB::table('m_tbunits_relation')->orderBy('TBUnitName')->select('id','TBUnitCode','TBUnitName')->where('STOCode',$state)->where('DTOCode',$district)->distinct()->get();
            //$district_val = District::all();

            return response()->json([
              "tbunit" => $tbunit_val
            ]);



        }catch(\Exception $e){
            $error = $e->getMessage();
            return view('admin.layout.error',$error);   // insert query
        }
    }
}
