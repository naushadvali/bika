<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Decontamination;
use App\Model\Sample;
use App\Model\Enroll;
use App\Model\Service;
use App\Model\ServiceLog;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class DashboardDecontaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         try{
            $data = [];
            $data['today'] = date('Y-m-d H:i:s');
            $data['services'] = Service::select('name')->get();


             $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','t_service_log.enroll_label','t_service_log.enroll_id','t_service_log.sample_label as samples','t_service_log.status',DB::raw('date_format(d.test_date,"%d-%m-%y") as date'),'s.test_reason','m.result','s.fu_month')
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_decontamination as d','d.sample_id','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','t_service_log.sample_id')
                       ->whereIn('t_service_log.status',[1,2,0])
                       ->where('t_service_log.service_id',3)
                       ->orderBy('t_service_log.enroll_id','desc')
                       ->distinct()
                       ->get();


                       //dd($data['sample']);


           $data['decontamination_test'] = ServiceLog::select('id')->whereIn('status',[0,1,2])->where('service_id',3)->count();

            $data['decontamination_tested'] = ServiceLog::select('id')->where('status',1)->where('service_id',3)->count();


            $data['decontamination_review'] = ServiceLog::select('id')->where('status',2)->where('service_id',3)
                        ->count();




            return view('admin.decontamination.dashboard_list',compact('data'));
        }catch(\Exception $e){
              $error = $e->getMessage();
              return view('admin.layout.error',$error);   // insert query
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        $enroll_id = $request->enrollId;
        $sample_label = $request->sample_ids;
           // dd($request->all());
        $en_label = Enroll::select('label as l')->where('id',$enroll_id)->first();
        $s_id = Sample::select('id as l')->where('sample_label',$sample_label)->first();


        $enroll_label=$en_label->l;
        $sample_id=$s_id->l;
       // dd($en_id,$sample_id);



             Decontamination::where('enroll_id', $enroll_id)->where('sample_id', $sample_label)->where('status',1)->delete();
            $decontamination = Decontamination::create([
            'enroll_id' => $enroll_id,
            'sample_id' => $sample_id,
            'status' => 1,
            'test_date' => $request->test_date,
            'created_by' => Auth::user()->id,
             'updated_by' => Auth::user()->id
          ]);



            ServiceLog::where('enroll_id', $enroll_id)
            ->where('sample_label', $sample_label)
            ->where('service_id',3)
            ->update(['status' => 2 ,'created_by' => Auth::user()->id,'updated_by' => Auth::user()->id]);
        return redirect('/dash_decontamination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $ids = explode(',', $id);
        $enroll_id = $ids[0];
        $sample_label = $ids[1];
        $en_label = Enroll::select('label as l')->where('id',$enroll_id)->first();
        $s_id = Sample::select('id as l')->where('sample_label',$sample_label)->first();

        $enroll_label=$en_label->l;
        $sample_id=$s_id->l;
       // dd($en_id,$sample_id);
        try{
             Decontamination::where('enroll_id', $enroll_id)->where('sample_id', $sample_label)->where('status',1)->delete();
            $decontamination = Decontamination::create([
            'enroll_id' => $enroll_id,
            'sample_id' => $sample_id,
            'status' => 1,
            'test_date' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id,
             'updated_by' => Auth::user()->id
          ]);



            ServiceLog::where('enroll_id', $enroll_id)
            ->where('sample_id', $sample_id)
            ->where('service_id',3)
            ->update(['status' => 2 ,'created_by' => Auth::user()->id,'updated_by' => Auth::user()->id]);
        // dd($s_id->l);
          //   $decontamination = ServiceLog::create([
          //   'enroll_id' => $enroll_id,
          //   'sample_id' => $sample_id,
          //   'enroll_label' => $enroll_label,
          //   'sample_label' => $sample_label,
          //   'service_id' => 3,
          //   'status' => 2,
          //   'test_date' => date('Y-m-d H:i:s'),
          //   'created_by' => Auth::user()->id,
          //    'updated_by' => Auth::user()->id
          // ]);
        return redirect('/dash_decontamination');


      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function dashdecontPrint()
    {
        $data = [];
            $data['today'] = date('Y-m-d H:i:s');
            $data['services'] = Service::select('name')->get();


             $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','t_service_log.enroll_label','t_service_log.enroll_id','t_service_log.sample_label as samples','t_service_log.status',DB::raw('date_format(d.test_date,"%d-%m-%y") as date'),'s.test_reason','m.result')
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_decontamination as d','d.sample_id','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','t_service_log.sample_id')
                       ->whereIn('t_service_log.status',[1,2,0])
                       ->where('t_service_log.service_id',3)
                       ->orderBy('t_service_log.enroll_id','desc')
                       ->distinct()
                       ->get();


                       //dd($data['sample']);


           $data['decontamination_test'] = ServiceLog::select('id')->where('status',1)->where('service_id',3)->count();

            $data['decontamination_tested'] = ServiceLog::select('id')->where('status',2)->where('service_id',3)->count();


            $data['decontamination_review'] = ServiceLog::select('id')->where('status',2)->where('service_id',3)
                        ->count();




            return view('admin.decontamination.dashprint',compact('data'));
    }
}
