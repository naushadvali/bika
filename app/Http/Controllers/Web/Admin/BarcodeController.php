<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Barcodes;
use App\Model\Config;
use App\Model\PrintLog;
use \Milon\Barcode\DNS1D;

class BarcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $years = Barcodes::distinct()->get(['year']);

        return view('admin.barcodes.list',compact('years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $year = Barcodes::distinct()->get(['year'])->last();

        $last = Barcodes::orderBy('id','DESC')->first();
        $_next = $request->_next;
        $lab_name = Config::where('status',1)->select('lab_name','barcode_offset')->first();

        $barcode_offset = Config::where('status',1)->select('barcode_offset')->first();
        $offset=substr($barcode_offset->barcode_offset,-5);
        #$offset_int= (int) $offset + 1;
        #$offset_int = intval(strval($offset+1));

        if(!$year){
          $year = date("Y");
          $startIndex = $offset;
        }else{
          if($_next == 0){
            $year = $year->year + 1;
            $startIndex = 1;
          }else{
            $year = $year->year;
            $startIndex = $last->code +1;
          }
        }



        $length = 0;
        for($i = $startIndex;$i <= $startIndex + 19; $i++){
          $length = 5 - strlen((string) $i);
          $strA = substr($year,-2).substr($lab_name->barcode_offset, 3, 4).str_pad($i, 5, "0", STR_PAD_LEFT)."A";
          $strB = substr($year,-2).substr($lab_name->barcode_offset, 3, 4).str_pad($i, 5, "0", STR_PAD_LEFT)."B";
          // $strA = substr($year,-2).str_pad($i, 5, "0", STR_PAD_LEFT)."A";
          // $strB = substr($year,-2).str_pad($i, 5, "0", STR_PAD_LEFT)."B";


          Barcodes::create([
            'year'=>$year,
            'code'=>$i,
            'codeA'=>$strA,
            'codeB'=>$strB
          ]);
        }
        return response()->json([
            'count' => $i,
            'year' => $year
        ]);
        //return redirect('/barcodes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function printBarcodes(Request $request)
    {
      $year = $request->year;
      $seqFrm = $request->seqFrm;
      $seqTo = $request->seqTo;

      PrintLog::create([
        'r_year' => $year ,
        'r_from' => $seqFrm,
        'r_to' => $seqTo,
        'created_by' => $request->user()->id
      ]);

      $barcodes = Barcodes::where('year',$year)
      ->whereBetween('code',[$seqFrm, $seqTo])
      ->get();

      //dd($year,$seqFrm,$seqTo);

      echo "<style>
          @media print {
            @page {
              margin:10px 0 0 0;
              size: 100mm 25mm;
              #size: 210mm 297mm;
            }
          }
          body {
            font-size: 8pt;
            margin: 0 12px;
          }
          .page-break {
              page-break-after: always;
          }
          </style>";
      foreach ($barcodes as $key => $value) {
        #echo '<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('22', 'MSI+')}}" alt="barcode" />'

        // echo DNS1D::getBarcodeSVG($value->codeA, "C93",0.6,20);//C93
        // echo "&emsp;&emsp;&emsp;&nbsp;&nbsp;".DNS1D::getBarcodeSVG($value->codeA, "C93",0.6,20);//C93
        // echo "<br/>".strtoupper($value->codeA);
        // echo "&emsp;&emsp;&emsp;&nbsp;&nbsp;".strtoupper($value->codeA);
        // echo "<br/><br/>".DNS1D::getBarcodeSVG($value->codeB, "C93",0.6,20);//C93
        // echo "&emsp;&emsp;&emsp;&nbsp;&nbsp;".DNS1D::getBarcodeSVG($value->codeB, "C93",0.6,20);//C93
        // echo "<br/>".strtoupper($value->codeB);
        // echo "&emsp;&emsp;&emsp;&nbsp;&nbsp;".strtoupper($value->codeB);
        // echo "<div class='page-break'></div>";
        $barcode_valueA="";
        $barcode_valueB="";
        $barcode_valueA = substr($value->codeA,0,2).substr($value->codeA,7,11);
        $barcode_valueB = substr($value->codeB,0,2).substr($value->codeB,7,11);
        //dd($barcode_valueA);
        echo DNS1D::getBarcodeSVG($barcode_valueA, "C128B",0.6,50);//C93
        echo "&emsp;&nbsp;".DNS1D::getBarcodeSVG($barcode_valueA, "C128B",0.6,50);//C93
        echo "&emsp;&nbsp;".DNS1D::getBarcodeSVG($barcode_valueB, "C128B",0.6,50);//"C93",1,15
        echo "&emsp;&nbsp;".DNS1D::getBarcodeSVG($barcode_valueB, "C128B",0.6,50);//C93
        echo "<br/>".strtoupper($value->codeA);
        echo "&nbsp;&nbsp;".strtoupper($value->codeA);
        // echo "<div class='page-break'></div>";

        echo "&nbsp;&nbsp;&nbsp;".strtoupper($value->codeB);
        echo "&nbsp;&emsp;".strtoupper($value->codeB);
        echo "<div class='page-break'></div>";

      }


    }
}
