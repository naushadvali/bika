<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BioWaste;
use Illuminate\Support\Facades\DB;

class BioWasteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                   // $data['today'] = date('Y-m-d H:i:s');


            $data['today_date']=DB::select('select date_format(now(),"%d-%m-%y %H:%i:%s") as date');
            $data['today']=$data['today_date'][0]->date;
             $data['sample'] = BioWaste::orderBy('id','desc')
                        ->distinct()
                        ->get();
            // foreach($data['sample'] as $key=>$value){
            //   if($value->quantity_status==1){
            //     collected_date
            //   }
            //   else{
            //     $value->today_end='';
            //   }
            // }


            return view('admin.biowaste.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['today_date']=DB::select('select date_format(now(),"%d-%m-%y %H:%i:%s") as date');
      $data['today']=$data['today_date'][0]->date;
        $cbnaat = BioWaste::create([
            'generated_date' =>   $data['today'],


          ]);
        return redirect('/bioWaste');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->quantity){
          $waste = BioWaste::find($request->waste_id);
          $waste->quantity = $request->quantity;
          $waste->save();
        }
        elseif($request->collected_date) {
          $waste = BioWaste::find($request->waste_id_col);
          $waste->collected_date = $request->collected_date;
          $waste->save();
        }
        return redirect('/bioWaste');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $waste = BioWaste::find($id);



        $data['today_date']=DB::select('select date_format(now(),"%d-%m-%y %H:%i:%s") as date');
        $data['today_end']=$data['today_date'][0]->date;

        //dd($data['today_end']);
        //$waste->collected_date = $data['today_end'];
        $waste->quantity_status=1;
        $waste->status=1;
        $waste->save();
        return redirect('/bioWaste');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function biowasteprint()
    {

        $data['waste'] = BioWaste::all();
        return view('admin.biowaste.print',compact('data'));

    }
}
