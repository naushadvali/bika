<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Equipment;
use Illuminate\Support\Facades\DB;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sample'] = Equipment::orderBy('id','desc')->where('flag',1)->get();
        return view('admin.equipment.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['equipment'] = new Equipment;
        $data['category'] = DB::table('m_equipment_category')->select('name')->get();

         return view('admin.equipment.form',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
         $eqp = Equipment::create([
          'name_cat' => $request->name_cat,
          'name' => $request->name,
          'tool' => $request->tool,
          'status' => $request->status,
          'supplier' => $request->supplier,
          'make' => $request->make,
          'serial_no' => $request->serial_no,
          'model_no' => $request->model_no,
          'date_installation' => $request->date_installation,
          'location' => $request->location,
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'provider' => $request->provider,
          'nameProvider' => $request->nameProvider,
          'waranty_status' => $request->waranty_status,
          'date_last_maintain' => $request->date_last_maintain,
          'maintainance_report' => $request->maintainance_report,
          'due_date' => $request->due_date,
          'contact_name' => $request->contact_name,
          'contact_no' => $request->contact_no,
          'responsible_person' => $request->responsible_person,
          'date_decommission' => $request->date_decommission,
          'next_calibration' => $request->next_calibration,
          'breakdown_eqp' => $request->breakdown_eqp,
          'return_function_status' => $request->return_function_status,
          'record_instrument' => $request->record_instrument,
          'eqp_id' => $request->eqp_id,
          'org' => $request->org,
          'curr_warrenty' => $request->curr_warrenty,
          'contact_email' => $request->contact_email,
          'company_name' => $request->company_name,
          'eqp_maintain' => $request->eqp_maintain,
          'records_inst' => $request->records_inst,
          'date_last_caliberation' => $request->date_last_caliberation
        ]);

        DB::table('m_equip_breakdown_date')->insert([
         'equipment_id' => $eqp->id,
         'breakdown_date' => $request->breakdown_eqp,
         'recovery_date' => $request->return_function_status,
         'created_at' => date('d-m-Y H:i:s'),
       ]);
        return redirect('/equipment');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data['equipment'] = Equipment::find($id);
         $data['category'] = DB::table('m_equipment_category')->select('name')->get();
         //dd($data['category']);
        return view('admin.equipment.form',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equipment=Equipment::find($id);
        //dd($request->all());
        $equipment->update([
          'name_cat' => $request->name_cat,
          'name' => $request->name,
          'tool' => $request->tool,
          'status' => $request->status,
          'supplier' => $request->supplier,
          'make' => $request->make,
          'serial_no' => $request->serial_no,
          'model_no' => $request->model_no,
          'date_installation' => $request->date_installation,
          'location' => $request->location,
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'provider' => $request->provider,
          'nameProvider' => $request->nameProvider,
          'waranty_status' => $request->waranty_status,
          'date_last_maintain' => $request->date_last_maintain,
          'maintainance_report' => $request->maintainance_report,
          'due_date' => $request->due_date,
          'contact_name' => $request->contact_name,
          'contact_no' => $request->contact_no,
          'responsible_person' => $request->responsible_person,
          'date_decommission' => $request->date_decommission,
          'next_calibration' => $request->next_calibration,
          'breakdown_eqp' => $request->breakdown_eqp,
          'return_function_status' => $request->return_function_status,
          'record_instrument' => $request->record_instrument,
          'eqp_id' => $request->eqp_id,
          'org' => $request->org,
          'curr_warrenty' => $request->curr_warrenty,
          'contact_email' => $request->contact_email,
          'company_name' => $request->company_name,
          'eqp_maintain' => $request->eqp_maintain,
          'records_inst' => $request->records_inst,
          'date_last_caliberation' => $request->date_last_caliberation

        ]);

        return redirect('/equipment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function equipmentprint()
    {
        $data['sample'] = Equipment::orderBy('id','desc')->where('flag',1)->get();
        return view('admin.equipment.print',compact('data'));
    }
    public function delete_equipment($id)
    {
      $equipment=Equipment::find($id);
      $equipment->update([
        'flag' => 0,
      ]);
      return redirect('/equipment');
    }
    public function downtimeAnalysis()
    {
      $data['fromdate']='';
      $data['todate']='';
      $data['sample']= Equipment::where('flag',1)->get();
      if($data['sample']){
        foreach($data['sample'] as $key=>$value){
    			$date1 = strtotime($value->return_function_status);
    			$date2 = strtotime($value->breakdown_eqp);
    			$datediff = $date1 - $date2;
    			$value->days = $datediff / (60 * 60 * 24);
          //$value->days = $value->return_function_status - $value->breakdown_eqp;
        }
      }
      return view('admin.equipment.downtimereport',compact('data'));
    }
    public function freqdowntimeAnalysis(Request $request)
    {
      if($request->from_date && $request->to_date){
        $data['sample']=Equipment::select(DB::raw("count('b.breakdown_date') as count"),'m_equipment.name_cat','m_equipment.name')
        ->join('m_equip_breakdown_date as b','b.equipment_id','=','m_equipment.id')
        ->where('m_equipment.flag',1)
        ->where('b.breakdown_date','>=',$request->from_date)
        ->where('b.breakdown_date','<=',$request->to_date)
        ->where('m_equipment.flag',1)->get();

      }else{
          $data['sample']=Equipment::select(DB::raw("count('b.breakdown_date') as count"),'m_equipment.name_cat','m_equipment.name')
          ->join('m_equip_breakdown_date as b','b.equipment_id','=','m_equipment.id')
          ->where('m_equipment.flag',1)->get();
      }
      return view('admin.equipment.freqdowntimereport',compact('data'));
    }

    public function downtimeAnalysisDatefilfer(Request $request)
    {
      $fromdate=[];
      $todate=[];
      $data['todate']='';
      $data['fromdate']='';

      if($request->from_date && $request->to_date){

        $time = strtotime($request->from_date);
        $from_date = date('Y-m-d',$time);
        $time = strtotime($request->to_date);
        $to_date = date('Y-m-d',$time);

        $data['sample']=Equipment::where('m_equipment.flag',1)
        //->where('breakdown_eqp','>=',$from_date)
        // ->where(DB::raw('DATE_FORMAT(breakdown_eqp, "%Y %M %d")'),'>=',$from_date)
        // ->where(DB::raw('DATE_FORMAT(return_function_status, "%Y %M %d")'),'<=',$to_date)
      //  ->where('return_function_status','<=',$to_date)
        ->get();

        foreach($data['sample'] as $key => $value){
          $time = strtotime($value->breakdown_eqp);
          $breakdown_eqp = date('Y-m-d',$time);
          $time = strtotime($value->return_function_status);
          $return_function_status = date('Y-m-d',$time);
          if($breakdown_eqp >= $from_date && $return_function_status<= $to_date)
              $value->query=1;
          else {
              $value->query=0;
          }
        }

      }else{
          $data['sample']=Equipment::where('m_equipment.flag',1)->get();
          foreach($data['sample'] as $key => $value){
                $value->query=1;
          }
      }

      if($data['sample']){

        foreach($data['sample'] as $key=>$value){
          $date1 = strtotime($value->return_function_status);
          $date2 = strtotime($value->breakdown_eqp);
          $datediff = $date1 - $date2;
          $value->days = $datediff / (60 * 60 * 24);
        }
      }
      $todate=$request->to_date;
      $fromdate=$request->from_date;
      $data['todate']=$request->to_date;
      $data['fromdate']=$request->from_date;

      return view('admin.equipment.downtimereport',compact('data','todate','fromdate'));
    }


    public function addbreakdown(Request $request)
    {
      DB::table('m_equip_breakdown_date')->insert([
       'equipment_id' => $request->equipId,
       'breakdown_date' => $request->break_date,
       'recovery_date' => $request->recovery_date,
       'created_at' => date('d-m-Y H:i:s'),
     ]);
      return redirect('/equipment');
    }

}
