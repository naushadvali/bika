<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\Microscopy;
use App\Model\LCFlaggedMGIT;
use App\Model\LCFlaggedMGITFurther;
use App\Model\CultureInoculation;
use App\Model\Microbio;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class LCResultReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $data = [];
          $data['sample'] = ServiceLog::select('m.enroll_id','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','t_service_log.tag as lpa_type','m.no_of_samples','t.status as dna_status','t.created_at as date_of_extraction','ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date', 'lfm.gu','lfm.flagging_date','m.sample_type','lfmf.result', 'lfmf.result_date','m.fu_month','lfmf.status as lcstatus')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        //->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_microscopy as s', function ($join) {
              $join->on('s.sample_id','=','t_service_log.sample_id')
                   ->where('s.status', 1);
          })
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lc_flagged_mgit as lfm','lfm.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',18)
        //->where('s.status',1)
        ->whereIn('t_service_log.status',[0,2])
        ->orderBy('enroll_id','desc')
        ->distinct()
        ->get();
       // dd($data['sample']);
        // foreach ($data['sample'] as $key => $value) {
        //   $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
        //   if($lpa->service_id==6){
        //     $value->lpa_type = "LPA 1st line";
        //     $value->lpa_method = "LC";
        //   }elseif($lpa->service_id==7){
        //     $value->lpa_type = "LPA 2nd line";
        //     $value->lpa_method = "LJ";
        //   }elseif($lpa->service_id==13){
        //     $value->lpa_type = "LPA Both Line";
        //     $value->lpa_method = "Both";
        //   }else{
        //     $value->lpa_type = "NA";
        //     $value->lpa_method = "NA";
        //   }
        // }
        foreach ($data['sample'] as $key => $value) {
          $value->no_sample = ServiceLog::where('enroll_id',$value->enroll_id)->where('service_id',11)->count();
          $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          if($lpa->service_id==13){
            $value->lpa_method = "Both";
          }elseif($lpa->service_id==7){
            $value->lpa_method = "LPA 2nd line";
          }elseif($lpa->service_id==6){
            $value->lpa_method = "LPA 1st line";
          }
          // else{
          //   $value->lpa_type = "NA";
          // }

          $culture = ServiceLog::select('tag')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->where('service_id',16)->first();
          if($culture->tag=='LC & LJ Both'){
            $value->lpa_type = "LC & LJ Both";
            //$value->lpa_method = "Both";
          }else{
            $value->lpa_type = "LC";
            //$value->lpa_method = "LC";
          }
        }
        return view('admin.lc_result_review.dashboard',compact('data'));
      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->service_id == 1 || $request->service_id == 2 || $request->service_id == 3){
          if($request->service_log_id > 0){
            $service = ServiceLog::find($request->service_log_id);
            $service->status = 0;
            $service->updated_by = $request->user()->id;
            $service->save();
            if($request->service_id == 1){
              $tag = '1st line LPA';
            }elseif($request->service_id == 2){
              $tag = '2st line LPA';
            }else{
              $tag = '1st line LPA  and for 2nd line LPA';
            }
            $new_service = [
              'enroll_id' => $service->enroll_id,
              'sample_id' => $service->sample_id,
              'service_id' => 8,
              'status' => 1,
              'tag' => $tag,
              'created_by' => $request->user()->id,
              'updated_by' => $request->user()->id,
              'enroll_label' => $service->enroll_label,
              'sample_label' => $service->sample_label,
            ];

            $nwService = ServiceLog::create($new_service);
            //return $nwService;
        }
      }elseif($request->service_id == 4){
        $service = ServiceLog::find($request->service_log_id);
        $service->status = 0;
        $service->updated_by = $request->user()->id;
        $service->save();
        $new_service = [
          'enroll_id' => $service->enroll_id,
          'sample_id' => $service->sample_id,
          'service_id' => 21,
          'status' => 1,
          'tag' => '',
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'enroll_label' => $service->enroll_label,
          'sample_label' => $service->sample_label,
        ];
        $nwService = ServiceLog::create($new_service);
        //return $nwService;
      }



      elseif($request->service_id == 5 || $request->service_id == 7){
               $service = ServiceLog::find($request->service_log_id);
              $service->status = 0;
              $service->updated_by = $request->user()->id;
              $service->save();
             $microbio = Microbio::create([
                  'enroll_id' => $service->enroll_id,
                  'sample_id' => $service->sample_id,
                  'service_id' => 17,
                  'next_step' => '',
                  'detail' => '',
                  'remark' => '',
                  'status' => 0,
                  'created_by' => $request->user()->id,
                  'updated_by' => $request->user()->id
                ]);
              //return $microbio;
      }
      elseif($request->service_id == 6){

              $service = ServiceLog::find($request->service_log_id);
              $service->status = 0;
              $service->updated_by = $request->user()->id;
              $service->save();
              $another_sample = ServiceLog::select('sample_id','sample_label')->where('enroll_id',$service->enroll_id)->where('service_id',11)->first();
              //dd($another_sample);
              $another_sample_obj = ServiceLog::where('enroll_id',$service->enroll_id)->where('service_id',11);
              if($another_sample){
                    $is_decontamin = ServiceLog::where('sample_id',$service->sample_id)->where('service_id',3)->first();
                    if($is_decontamin){
                      $new_service = [
                        'enroll_id' => $service->enroll_id,
                        'sample_id' => $another_sample->sample_id,
                        'service_id' => 16,
                        'status' => 1,
                        'tag' => '',
                        'created_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'enroll_label' => $service->enroll_label,
                        'sample_label' => $another_sample->sample_label,
                      ];

                    }
                    else{
                        $new_service = [
                        'enroll_id' => $service->enroll_id,
                        'sample_id' => $another_sample->sample_id,
                        'service_id' => 3,
                        'status' => 1,
                        'tag' => '',
                        'created_by' => $request->user()->id,
                        'updated_by' => $request->user()->id,
                        'enroll_label' => $service->enroll_label,
                        'sample_label' => $another_sample->sample_label,
                      ];
                    }
                    $nwService = ServiceLog::create($new_service);
                    $another_sample_obj->service_id = 0;
              }
              else
              {
                return redirect('/lc_result_review');
              }


      }
      return redirect('/lc_result_review');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function lcflagprint()
    {

        $data = [];
          $data['sample'] = ServiceLog::select('m.enroll_id','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples','t.status as dna_status','t.created_at as date_of_extraction','ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date', 'lfm.gu','lfm.flagging_date','m.sample_type','lfmf.result', 'lfmf.result_date')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lc_flagged_mgit as lfm','lfm.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',18)
        ->where('s.status',1)
        ->whereIn('t_service_log.status',[1,2])
        ->orderBy('enroll_id','desc')
        ->distinct()
        ->get();
        foreach ($data['sample'] as $key => $value) {
          $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          if($lpa->service_id==6){
            $value->lpa_type = "LPA 1st line";
            $value->lpa_method = "LC";
          }elseif($lpa->service_id==7){
            $value->lpa_type = "LPA 2nd line";
            $value->lpa_method = "LJ";
          }elseif($lpa->service_id==13){
            $value->lpa_type = "LPA Both Line";
            $value->lpa_method = "Both";
          }else{
            $value->lpa_type = "NA";
            $value->lpa_method = "NA";
          }
        }
        return view('admin.lc_result_review.print',compact('data'));

    }
}
