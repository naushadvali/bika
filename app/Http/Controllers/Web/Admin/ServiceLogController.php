<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\Microscopy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Microbio;

use App\Http\Controllers\Controller;

class ServiceLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       //dd($request->service_id);
        if($request->service_log_id > 0){
          //updating the current serice status to 0 and creating new service
          $service = ServiceLog::find($request->service_log_id);
          $pre_service_id =  $service->service_id;
          $service->status = 0;
          $service->updated_by = $request->user()->id;
          $service->save();

         //dd($pre_service_id);
          if($request->service_id=='12' && ($pre_service_id==1 || $pre_service_id==2)){
            $microbio = Microbio::create([
            'enroll_id' => $service->enroll_id,
            'sample_id' => $service->sample_id,
            'service_id' => $pre_service_id,
            'next_step' => '',
            'detail' => '',
            'remark' => '',
            'status' => 0,
            'created_by' => $request->user()->id,
             'updated_by' => $request->user()->id
          ]);
            return $microbio;
        }

          $new_service = [
            'enroll_id' => $service->enroll_id,
            'sample_id' => $service->sample_id,
            'service_id' => $request->service_id,
            'status' => 1,
            'tag' => $request->tag,
            'created_by' => $request->user()->id,
            'updated_by' => $request->user()->id,
            'enroll_label' => $service->enroll_label,
            'sample_label' => $service->sample_label,
          ];

          $nwService = ServiceLog::create($new_service);
          // if($pre_service_id==1 || $pre_service_id==2){
          //   ServiceLog::microscopyLog($request);
          // }elseif($pre_service_id==8){
          //   ServiceLog::dnaExtractionLog($request);
          // }
          return $nwService;
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
