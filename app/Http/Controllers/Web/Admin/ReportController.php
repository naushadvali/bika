<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Dashboard;
use App\User;
use App\Model\Service;
use App\Model\Sample;
use App\Model\ServiceLog;
use App\Model\LCDST;
use App\Model\LjDstReading;
use App\Model\TestRequest;
use DateTime;
use Illuminate\Support\Facades\DB;
use App\Model\ResultEdit;
use App\Model\Cbnaat_lab_details;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = Service::select('id','name')->whereIn('id',[1,2,6,7,21,22,23,4])->get();
      $diagnosis_sum = 0;
      $follow_up_sum = 0;
      foreach ($data as $key => $value) {
        $value->diagnosis = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
        ->where('t_service_log.service_id',$value->id)
        ->where('t_service_log.status',1)
        ->where('s.test_reason','DX')
        ->count();
        $value->follow_up = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
        ->where('t_service_log.service_id',$value->id)
        ->where('t_service_log.status',1)
        ->where('s.test_reason','FU')
        ->count();
        $diagnosis_sum += $value->diagnosis;
        $follow_up_sum += $value->follow_up;
      }
      $data->diagnosis_sum = $diagnosis_sum;
      $data->follow_up_sum = $follow_up_sum;
      $to_date='';
      $from_date='';
      //dd($data);
      return view('admin.report.dashboard',compact('data','to_date','from_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Service::select('id','name')->whereIn('id',[1,2,6,7,21,22,23,4])->get();
        $diagnosis_sum = 0;
        $follow_up_sum = 0;
        foreach ($data as $key => $value) {
          $value->diagnosis = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
          ->where('t_service_log.service_id',$value->id)
          ->where('t_service_log.status',1)
          ->where('s.test_reason','DX')
          ->where('t_service_log.created_at','>=',date('Y-m-d',strtotime($request->from_date)))
          ->where('t_service_log.created_at','<=',date('Y-m-d',strtotime($request->to_date)))
          ->count();
          $value->follow_up = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
          ->where('t_service_log.service_id',$value->id)
          ->where('t_service_log.status',1)
          ->where('s.test_reason','FU')
          ->where('t_service_log.created_at','>=',date('Y-m-d',strtotime($request->from_date)))
          ->where('t_service_log.created_at','<=',date('Y-m-d',strtotime($request->to_date)))
          ->count();
          $diagnosis_sum += $value->diagnosis;
          $follow_up_sum += $value->follow_up;
        }
        $data->diagnosis_sum = $diagnosis_sum;
        $data->follow_up_sum = $follow_up_sum;

        $to_date = $request->to_date;
        $from_date = $request->from_date;
        //dd($data);
        return view('admin.report.dashboard',compact('data','to_date','from_date'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function lqc_indicator(Request $request)
    {
      // dd($request->all());
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['t_service_log.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['t_service_log.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['t_service_log.id', '!=', '']];
      }
      $data['m_positive_dg'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[1,2])
      ->whereIn('m.result',['1+positive','2+positive','3+positive'])
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();
      $data['m_positive_fu'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[1,2])
      ->whereIn('m.result',['1+positive','2+positive','3+positive'])
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['m_negative_dg'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[1,2])
      ->where('m.result','Negative')
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();
      $data['m_negative_fu'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[1,2])
      ->where('m.result','Negative')
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['cbnaat_diagnosis'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'),DB::raw('COUNT(s.id) as total'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
      ->where('t_service_log.service_id',4)
      ->where('s.test_reason','DX')
      ->where($con)
      ->first();
      $data['cbnaat_fu'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'),DB::raw('COUNT(s.id) as total'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
      ->where('t_service_log.service_id',4)
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['lpa1_diagnosis'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','!=','Invalid')
      ->where('t_service_log.tag','1st line LPA')
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();
      $data['lpa1_fu'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','!=','Invalid')
      ->where('t_service_log.tag','1st line LPA')
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['lpa1_total'] = $data['lpa1_diagnosis']+$data['lpa1_fu'];
      //dd($data);
      $data['lpa1_d_invalid'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','1st line LPA')
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();

      $data['lpa1_fu_invalid'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','1st line LPA')
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['lpa1_d_invalid_total'] = $data['lpa1_d_invalid'] + $data['lpa1_fu_invalid'];

      $data['lpa2_diagnosis'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','!=','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();
      $data['lpa2_fu'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_2stlinelpa as lpa2','lpa2.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','!=','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['lpa2_total'] = $data['lpa2_diagnosis']+$data['lpa2_fu'];
      //dd($data);
      $data['lpa2_d_invalid'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_2stlinelpa as lpa2','lpa2.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where('s.test_reason','DX')
      ->where($con)
      ->count();

      $data['lpa2_fu_invalid'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_2stlinelpa as lpa2','lpa2.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where('s.test_reason','FU')
      ->where($con)
      ->count();
      $data['lpa2_d_invalid_total'] = $data['lpa2_d_invalid'] + $data['lpa2_fu_invalid'];
      $data['lpa_fld_retest'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_2stlinelpa as lpa2','lpa2.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_microbiologist as mb','mb.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->whereIn('mb.service_id',[6,7,13,15])
      ->where('t_service_log.tag','1st line LPA')
      ->where('mb.status',1)
      ->where($con)
      ->first();
      $data['lpa_sld_retest'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_2stlinelpa as lpa2','lpa2.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_microbiologist as mb','mb.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->whereIn('mb.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where('mb.status',1)
      ->where($con)
      ->first();
      //dd($data);
      $data['lc_dst_fld_d'] = ServiceLog::select(DB::raw('sum(ld.result = "Contaminated ( C)" OR ld.result = "Error ( E)")  invalid'),DB::raw('sum(ld.result = "Sensitive (S)" OR ld.result = "Resistance (R)" OR ld.result = "Not Done (-)")  valid'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->where('s.test_reason','DX')
      ->whereIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where($con)
      ->first();
      $data['lc_dst_fld_fu'] = ServiceLog::select(DB::raw('sum(ld.result = "Contaminated ( C)" OR ld.result = "Error ( E)")  invalid'),DB::raw('sum(ld.result = "Sensitive (S)" OR ld.result = "Resistance (R)" OR ld.result = "Not Done (-)")  valid'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['lc_dst_fld_total'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where($con)
      ->first();
      $data['lc_dst_fld_retest'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->leftjoin('t_microbiologist as mb','mb.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where('mb.service_id',21)
      ->where('mb.status',1)
      ->where($con)
      ->first();
      //dd($data);
      $data['lc_dst_sld_retest'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->leftjoin('t_microbiologist as mb','mb.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereNotIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where('mb.service_id',21)
      ->where('mb.status',1)
      ->where($con)
      ->first();
      $data['lc_dst_sld_d'] = ServiceLog::select(DB::raw('sum(ld.result = "Contaminated ( C)" OR ld.result = "Error ( E)")  invalid'),DB::raw('sum(ld.result = "Sensitive (S)" OR ld.result = "Resistance (R)" OR ld.result = "Not Done (-)")  valid'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->where('s.test_reason','DX')
      ->whereNotIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where($con)
      ->first();
      $data['lc_dst_sld_fu'] = ServiceLog::select(DB::raw('sum(ld.result = "Contaminated ( C)" OR ld.result = "Error ( E)")  invalid'),DB::raw('sum(ld.result = "Sensitive (S)" OR ld.result = "Resistance (R)" OR ld.result = "Not Done (-)")  valid'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereNotIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['lc_dst_sld_total'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_dst as ld','t_service_log.sample_id','=','ld.sample_id')
      ->where('t_service_log.service_id',21)
      ->whereNotIn('ld.drug_name',['S','H1', 'H2', 'R', 'E', 'Z'])
      ->where($con)
      ->first();
      $data['afb_lc_dx'] = ServiceLog::select(DB::raw('sum(lfmf.result = "Negative")  neg'),DB::raw('sum(lfmf.result = "Positive")  post'),DB::raw('sum(lfmf.result = "Positive" AND lfmf.result = "NTM")  pos_ntm'),DB::raw('sum(lfmf.result = "Contamination")  con'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',18)
      ->where('s.test_reason','DX')
      ->where($con)
      ->first();
      $data['afb_lc_fu'] = ServiceLog::select(DB::raw('sum(lfmf.result = "Negative")  neg'),DB::raw('sum(lfmf.result = "Positive")  post'),DB::raw('sum(lfmf.result = "Positive" AND lfmf.result = "NTM")  pos_ntm'),DB::raw('sum(lfmf.result = "Contamination")  con'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',18)
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['afb_total'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',18)
      ->whereIn('s.test_reason',['FU','DX'])
      ->where($con)
      ->count();
      $data['afb_lj_dx'] = ServiceLog::select(DB::raw('sum(lj.final_result = "Positive") post'), DB::raw('sum(lj.final_result = "Negative") neg'), DB::raw('sum(lj.final_result = "Contamination") con'), DB::raw('sum(lj.final_result = "Positive" AND lj.final_result = "NTM") pos_ntm'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
      ->where('t_service_log.service_id',20)
      ->where('s.test_reason','DX')
      ->where($con)
      ->first();
      $data['afb_lj_fu'] = ServiceLog::select(DB::raw('sum(lj.final_result = "Positive") post'), DB::raw('sum(lj.final_result = "Negative") neg'), DB::raw('sum(lj.final_result = "Contamination") con'), DB::raw('sum(lj.final_result = "Positive" AND lj.final_result = "NTM") pos_ntm'))->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
      ->where('t_service_log.service_id',20)
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['afb_lj_total'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
      ->where('t_service_log.service_id',20)
      ->whereIn('s.test_reason',['FU','DX'])
      ->where($con)
      ->count();
      $data['afb_lc_lj_dx'] = ServiceLog::select(DB::raw('sum(lfmf.result = "Negative" AND lj.final_result = "Negative")  neg'),DB::raw('sum(lfmf.result = "Positive" AND lj.final_result = "Positive")  post'),DB::raw('sum(lfmf.result = "Positive" AND lfmf.result = "NTM" AND lj.final_result = "Positive" AND lj.final_result = "NTM")  pos_ntm'),DB::raw('sum(lfmf.result = "Contamination" AND lj.final_result = "Contamination")  con'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
      ->where('t_service_log.service_id',18)
      ->where('s.test_reason','DX')
      ->where($con)
      ->first();
      $data['afb_lc_lj_fu'] = ServiceLog::select(DB::raw('sum(lfmf.result = "Negative" AND lj.final_result = "Negative")  neg'),DB::raw('sum(lfmf.result = "Positive" AND lj.final_result = "Positive")  post'),DB::raw('sum(lfmf.result = "Positive" AND lfmf.result = "NTM" AND lj.final_result = "Positive" AND lj.final_result = "NTM")  pos_ntm'),DB::raw('sum(lfmf.result = "Contamination" AND lj.final_result = "Contamination")  con'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
      ->where('t_service_log.service_id',18)
      ->where('s.test_reason','FU')
      ->where($con)
      ->first();
      $data['afb_lj_lc_total'] = $data['afb_total']+$data['afb_lj_total'];
      //dd($data);
      $data['lpa_total'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','1st line LPA')
      ->where($con)
      ->first();
      $data['lpa_sld_total'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where('t_service_log.tag','2st line LPA')
      ->where($con)
      ->first();
      $data['cbnaat_retest'] = ServiceLog::select(DB::raw('sum(s.test_reason = "FU")  fu'),DB::raw('sum(s.test_reason = "DX")  dx'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_microbiologist as mb','mb.sample_id','=','t_service_log.sample_id')
      ->where('t_service_log.service_id',4)
      ->where('mb.service_id',4)
      ->where('mb.status',1)
      ->where($con)
      ->first();
      $sum = $data['m_positive_dg']+$data['m_positive_fu']+$data['m_negative_dg']+$data['m_negative_fu'];
      $data['m_positive_dg_per'] = $sum?($data['m_positive_dg']/$sum)*100:0;
      $data['m_positive_fu_per'] = $sum?($data['m_positive_fu']/$sum)*100:0;
      $data['m_negative_dg_per'] = $sum?($data['m_negative_dg']/$sum)*100:0;
      $data['m_negative_fu_per'] = $sum?($data['m_negative_fu']/$sum)*100:0;
      //dd($data);
      $data['to_date'] = $request->to_date;
      $data['from_date'] = $request->from_date;
      //dd($data['to_date']);
        return view('admin.report.lqc_indicator',compact('data'));
    }

    public function lqc_indicator_tat(Request $request)
    {
      $data['lpa1_in_tat'] = 0;
      $data['lpa2_in_tat'] = 0;
      $data['lpa1_out_tat'] = 0;
      $data['lpa2_out_tat'] = 0;
      $data['lj1_in_tat'] = 0;
      $data['lj1_out_tat'] = 0;
      $data['lj2_in_tat'] = 0;
      $data['lj2_out_tat'] = 0;
      $data['cbnaat_in_tat'] = 0;
      $data['cbnaat_out_tat'] = 0;
      $data['p_culture_in_tat'] = 0;
      $data['p_culture_out_tat'] = 0;
      $data['p_lj_in_tat'] = 0;
      $data['p_lj_out_tat'] = 0;
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['t_service_log.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['t_service_log.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['t_service_log.id', '!=', '']];
      }
      $data['lpa1'] = ServiceLog::select('s.receive_date as date_submitted','lpa1.test_date as result_date')
                        ->where('t_service_log.service_id',6)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_1stlinelpa as lpa1','t_service_log.sample_id','=','lpa1.sample_id')
                        ->where($con)
                        ->get();
      foreach ($data['lpa1'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=3){
          $data['lpa1_in_tat'] += 1;
        }else{
          $data['lpa1_out_tat'] += 1;
        }
      }
      $data['lpa1_total']=$data['lpa1_in_tat']+$data['lpa1_out_tat'];
      $data['lpa2'] = ServiceLog::select('s.receive_date as date_submitted','lpa1.test_date as result_date')
                        ->where('t_service_log.service_id',7)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_1stlinelpa as lpa1','t_service_log.sample_id','=','lpa1.sample_id')
                        ->where($con)
                        ->get();
      foreach ($data['lpa2'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=3){
          $data['lpa2_in_tat'] += 1;
        }else{
          $data['lpa2_out_tat'] += 1;
        }
      }
      $data['lpa2_total']=$data['lpa2_in_tat']+$data['lpa2_out_tat'];
      $data['lj1'] = ServiceLog::select('s.receive_date as date_submitted','lj1.created_at as result_date')
                        ->where('t_service_log.service_id',22)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_lj_dst_reading as lj1','t_service_log.sample_id','=','lj1.sample_id')
                        ->leftjoin('t_dst_drugs_tr as ddt','ddt.enroll_id','=','t_service_log.enroll_id')
                        ->where($con)
                        ->get();
      foreach ($data['lj1'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=84){
          $data['lj1_in_tat'] += 1;
        }else{
          $data['lj1_out_tat'] += 1;
        }
      }
      $data['lj1_total'] = $data['lj1_in_tat']+$data['lj1_out_tat'];

      $data['lj2'] = ServiceLog::select('s.receive_date as date_submitted','lj2.created_at as result_date')
                        ->where('t_service_log.service_id',23)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_lj_dst_reading as lj2','t_service_log.sample_id','=','lj2.sample_id')
                        ->leftjoin('t_dst_drugs_tr as ddt','ddt.enroll_id','=','t_service_log.enroll_id')
                        ->where($con)
                        ->get();
      foreach ($data['lj2'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=84){
          $data['lj2_in_tat'] += 1;
        }else{
          $data['lj2_out_tat'] += 1;
        }
      }
      $data['lj2_total'] = $data['lj2_in_tat']+$data['lj2_out_tat'];
      $data['cbnaat'] = ServiceLog::select('s.receive_date as date_submitted','cbn.test_date as result_date')
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where($con)
                        ->get();
      foreach ($data['cbnaat'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=1){
          $data['cbnaat_in_tat'] += 1;
        }else{
          $data['cbnaat_out_tat'] += 1;
        }
      }
      $data['cbnaat_total'] = $data['cbnaat_in_tat']+$data['cbnaat_out_tat'];

      $data['p_culture'] = ServiceLog::select('ci.inoculation_date as date_submitted','fmf.result_date as result_date')
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('t_culture_inoculation as ci','t_service_log.sample_id','=','ci.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as fmf','t_service_log.sample_id','=','fmf.sample_id')
                        ->where($con)
                        ->get();
      foreach ($data['p_culture'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=1){
          $data['p_culture_in_tat'] += 1;
        }else{
          $data['p_culture_out_tat'] += 1;
        }
      }
      $data['p_culture_total'] = $data['p_culture_in_tat'] + $data['p_culture_out_tat'];

      $data['p_lj'] = ServiceLog::select('li.inoculation_date as date_submitted','ld.lj_result_date as result_date')
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('t_lj_dst_inoculation as li','t_service_log.sample_id','=','li.sample_id')
                        ->leftjoin('t_lj_detail as ld','t_service_log.sample_id','=','ld.sample_id')
                        ->where($con)
                        ->get();
      foreach ($data['p_lj'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=1){
          $data['p_lj_in_tat'] += 1;
        }else{
          $data['p_lj_out_tat'] += 1;
        }
      }
      $data['p_lj_total'] = $data['p_lj_in_tat']+$data['p_lj_out_tat'];
      $data['lc_dst_fld_in_tat'] = 0;
      $data['lc_dst_fld_out_tat'] = 0;
      $data['lc_dst_fld'] = ServiceLog::select('s.receive_date as date_submitted','ld.result_date')
                            ->where('t_service_log.service_id',21)
                            ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                            ->leftjoin('t_lc_dst as ld','ld.sample_id','=','t_service_log.sample_id')
                            ->where($con)
                            ->whereIn('ld.drug_name',['S','H1','H2','R','E','Z'])
                            ->groupBy('ld.sample_id')
                            ->get();
                            //dd($data);
      foreach ($data['lc_dst_fld'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=28){
          $data['lc_dst_fld_in_tat'] += 1;
        }else{
          $data['lc_dst_fld_out_tat'] += 1;
        }
      }
      $data['lc_dst_fld_total'] = $data['lc_dst_fld_in_tat']+$data['lc_dst_fld_out_tat'];
      $data['lc_dst_sld_in_tat'] = 0;
      $data['lc_dst_sld_out_tat'] = 0;
      $data['lc_dst_sld']=ServiceLog::select('s.receive_date as date_submitted','ld.result_date as result_date')
                        ->where('t_service_log.service_id',21)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_lc_dst as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where($con)
                        ->whereIn('ld.drug_name',['C_M','K_M','A_M'])
                        ->groupBy('ld.sample_id')
                        ->get();
      foreach ($data['lc_dst_sld'] as $key => $value) {
        //dd($value);
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($interval->d<=28){
          $data['lc_dst_sld_in_tat'] += 1;
        }else{
          $data['lc_dst_sld_out_tat'] += 1;
        }
      }
      $data['to_date'] = $request->to_date;
      $data['from_date'] = $request->from_date;
      $data['lc_dst_sld_total'] = $data['lc_dst_sld_in_tat']+$data['lc_dst_sld_out_tat'];
      return view('admin.report.lqc_indicator_tat',compact('data'));
    }

    public function quality_indicator(Request $request)
    {
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['t_service_log.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['t_service_log.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
        $con1 = [['created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['t_service_log.id', '!=', '']];
        $con1 = [['id', '!=', '']];
      }
      $data['lc_m_pos'] = ServiceLog::select(DB::raw('sum(fmf.result = "Positive") fmf_pos'), DB::raw('sum(fmf.result = "Negative") fmf_neg'), DB::raw('sum(fmf.result = "Contamination") fmf_con'))
                        ->where('t_service_log.service_id',18)
                        ->leftjoin('t_microscopy as m','t_service_log.sample_id','=','m.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as fmf','t_service_log.sample_id','=','fmf.sample_id')
                        ->whereIn('m.result',['1+positive','2+positive','3+positive'])
                        ->where($con)
                        ->first();
      $data['lc_m_neg'] = ServiceLog::select(DB::raw('sum(fmf.result = "Positive") fmf_pos'), DB::raw('sum(fmf.result = "Negative") fmf_neg'), DB::raw('sum(fmf.result = "Contamination") fmf_con'))
                        ->where('t_service_log.service_id',18)
                        ->leftjoin('t_microscopy as m','t_service_log.sample_id','=','m.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as fmf','t_service_log.sample_id','=','fmf.sample_id')
                        ->where('m.result','Negative')
                        ->where($con)
                        ->first();
      $data['lc_m_pos_total'] = $data['lc_m_pos']->fmf_pos + $data['lc_m_pos']->fmf_neg + $data['lc_m_pos']->fmf_con;
      $data['lc_m_neg_total'] = $data['lc_m_neg']->fmf_pos + $data['lc_m_neg']->fmf_neg + $data['lc_m_neg']->fmf_con;

      $data['lj_m_pos'] = ServiceLog::select(DB::raw('sum(lj.final_result = "Positive") lj_pos'), DB::raw('sum(lj.final_result = "Negative") lj_neg'), DB::raw('sum(lj.final_result = "Contamination") lj_con'))
                        ->where('t_service_log.service_id',18)
                        ->leftjoin('t_microscopy as m','t_service_log.sample_id','=','m.sample_id')
                        ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
                        ->whereIn('m.result',['1+positive','2+positive','3+positive'])
                        ->where($con)
                        ->first();
      $data['lj_m_neg'] = ServiceLog::select(DB::raw('sum(lj.final_result = "Positive") lj_pos'), DB::raw('sum(lj.final_result = "Negative") lj_neg'), DB::raw('sum(lj.final_result = "Contamination") lj_con'))
                        ->where('t_service_log.service_id',18)
                        ->leftjoin('t_microscopy as m','t_service_log.sample_id','=','m.sample_id')
                        ->leftjoin('t_lj_detail as lj','t_service_log.sample_id','=','lj.sample_id')
                        ->where('m.result','Negative')
                        ->where($con)
                        ->first();
      $data['lj_m_pos_total'] = $data['lj_m_pos']->lj_pos + $data['lj_m_pos']->lj_neg + $data['lj_m_pos']->lj_con;
      $data['lj_m_neg_total'] = $data['lj_m_neg']->lj_pos + $data['lj_m_neg']->lj_neg + $data['lj_m_neg']->lj_con;

      $data['lpa_fld'] = ServiceLog::select(DB::raw('sum(lf.rif = "Sensitive" AND lf.inh ="Sensitive") h_r_sens'), DB::raw('sum(lf.rif = "Resistant" AND lf.inh ="Resistant") h_r_res'),DB::raw('sum(lf.inh = "Resistant") h_res'),DB::raw('sum(lf.rif = "Resistant") r_res'))
                        ->where('t_service_log.service_id',15)
                        ->leftjoin('t_lpa_final as lf','t_service_log.sample_id','=','lf.sample_id')
                        ->where($con)
                        ->first();

      $data['lpa_sld'] = ServiceLog::select(DB::raw('sum(lf.quinolone = "Sensitive" AND lf.slid ="Sensitive" AND lpa2.eis = 1) q_s_lkm_sens'),DB::raw('sum(lf.quinolone = "Resistant" AND lf.slid ="Resistant") q_s_res'), DB::raw('sum(lf.quinolone = "Resistant") q_res'),DB::raw('sum(lf.slid = "Resistant") s_res'),DB::raw('sum(lf.slid ="Sensitive" AND lpa2.eis = 1) s_lkm_sens'),DB::raw('sum(lf.slid ="Resistant" AND lpa2.eis = 0) s_lkm_res'),DB::raw('sum(lpa2.eis = 0) lkm_res'))
                        ->where('t_service_log.service_id',15)
                        ->leftjoin('t_lpa_final as lf','t_service_log.sample_id','=','lf.sample_id')
                        ->leftjoin('t_2stlinelpa as lpa2','t_service_log.sample_id','=','lpa2.sample_id')
                        ->where($con)
                        ->first();
      //dd($data);
      $data['cbn_sputum'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','Sputum')
                        ->where($con)
                        ->first();
      $data['cbn_csf'] = ServiceLog::select(DB::raw('count(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','CSF')
                        ->where($con)
                        ->first();
      $data['cbn_ga'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','GA')
                        ->where($con)
                        ->first();
      $data['cbn_pus'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','PUS')
                        ->where($con)
                        ->first();
      $data['cbn_bal'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','BAL')
                        ->where($con)
                        ->first();
      $data['cbn_pf'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','PF')
                        ->where($con)
                        ->first();

      $data['cbn_hiv'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','HIV positive (All sample type)')
                        ->where($con)
                        ->first();
      $data['cbn_others'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','Others')
                        ->where($con)
                        ->first();
      $data['e_id'] = ServiceLog::where('service_id',21)->where($con)->pluck('sample_id')->toArray();
      $data['r_h_s'] = 0;
      $data['r_h_r'] = 0;
      $data['h_r'] = 0;
      $data['mx_am_sens'] = 0;
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Sensitive (S)')
        ->whereIn('drug_name',['R','H1','H2'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['r_h_s'] += 1;
        }
      }
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Resistance (R)')
        ->whereIn('drug_name',['R','H1','H2'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['r_h_r'] += 1;
        }
      }
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Resistance (R)')
        ->whereIn('drug_name',['H1','H2'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=2')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['h_r'] += 1;
        }
      }
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Sensitive (S)')
        ->whereIn('drug_name',['H1','H2'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=2')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['h_r'] += 1;
        }
      }
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Sensitive (S)')
        ->whereIn('drug_name',['MFX(0_5)','MFX (2)', 'A_M'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['mx_am_sens'] += 1;
        }
      }
      $data['mx_am_res'] = 0;
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Resistance (R)')
        ->whereIn('drug_name',['MFX(0_5)','MFX (2)', 'A_M'])
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['mx_am_res'] += 1;
        }
      }
      $data['flq_flx_res'] = 0;
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Resistance (R)')
        ->whereIn('drug_name',['MFX(0_5)','MFX (2)', 'A_M'])
        ->whereOr('drug_name','Lf_x')
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['flq_flx_res'] += 1;
        }
      }
      $data['lc_dst_sld_all'] = 0;
      foreach ($data['e_id'] as $key => $value) {
        $data['r_h'] = LCDST::selectRaw('COUNT(*) as grp_cnt')
        ->where('sample_id',$value)
        ->where('result', 'Resistance (R)')
        ->whereOr('drug_name','MFX (2)')
        ->whereOr('drug_name','A_M')
        ->whereOr('drug_name','K_M')
        ->whereOr('drug_name','C_M')
        ->whereOr('drug_name','Lf_x')
        ->whereOr('drug_name','PAS')
        ->whereOr('drug_name','Lzd')
        ->whereOr('drug_name','Cfz')
        ->whereOr('drug_name','Eto')
        ->whereOr('drug_name','Cla')
        ->whereOr('drug_name','Azi')
        ->whereOr('drug_name','Others')
        ->whereOr('drug_name','MFX(0_5)')
        ->groupBy('sample_id')
        ->havingRaw('grp_cnt>=3')
        ->get()->toArray();
        if(COUNT($data['r_h'])){
          $data['lc_dst_sld_all'] += 1;
        }
      }
      $data['lc_dst_fld']=ServiceLog::select(DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="R") r_res'), DB::raw('sum(ld.result = "Resistance (R)") all_res'), DB::raw('sum(ld.result = "Sensitive (S)") all_sens'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Z") z_res'))
                        ->where('t_service_log.service_id',21)
                        ->leftjoin('t_lc_dst as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where($con)
                        ->first();
      $data['lc_dst_sld']=ServiceLog::select(DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="PAS") pas_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Lzd") lzd_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Cfz") cfz_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Eto") eto_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Cla") cla_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Azi") azi_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Azi") azi_res'), DB::raw('sum(ld.result = "Resistance (R)" AND ld.drug_name ="Others") others_res'))
                        ->where('t_service_log.service_id',21)
                        ->leftjoin('t_lc_dst as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where($con)
                        ->first();
      //dd($data['lc_dst_sld']);
      $data['lj_h_r_sens'] = 0;
      $data['lj_h_r_res'] = 0;
      $data['lj_r_res'] = 0;
      $data['lj_h_res'] = 0;
      $data['lj_z_res'] = 0;
      $data['lj_pas_res'] = 0;
      $data['lj_lzd_res'] = 0;
      $data['lj_cfz_res'] = 0;
      $data['lj_eto_res'] = 0;
      $data['lj_cla_res'] = 0;
      $data['lj_azi_res'] = 0;
      $data['lj_others_res'] = 0;
      $data['lj_sld_1'] = 0;
      $data['lj_sld_2'] = 0;
      $data['lj_sld_3'] = 0;
      $data['lj_sld_3'] = 0;
      $data['lj_dst_fld'] = LjDstReading::where($con1)->pluck('drug_reading')->toArray();
      foreach ($data['lj_dst_fld'] as $key => $value) {
        $lj_red = json_decode($value);
        $temp_count = 0;
        $temp_count1 = 0;
        $h_count = 0;
        $sld_count = 0;
        $sld_count1 = 0;
        $sld_count2 = 0;
        foreach ($lj_red->dil_2 as $key1 => $value1) {
          if(in_array($value1->name,['H1', 'H2', 'R'])){
            if($value1->value=='Sensitive'){
              $temp_count += 1;
            }elseif($value1->value=='Resistance'){
              $temp_count1 += 1;
            }else{}
          }
          if(in_array($value1->name,['H1', 'H2'])){
            if($value1->value=='Resistance'){
              $h_count += 1;
            }
          }
          if($value1->name == 'R'){
            if($value1->value=='Resistance'){
              $data['lj_r_res'] += 1;
            }
          }
          if($value1->name == 'Z'){
            if($value1->value=='Resistance'){
              $data['lj_z_res'] += 1;
            }
          }
          if(in_array($value1->name,['MFX(0.5)', 'MFX (2)', 'K M', 'A M'])){
            if($value1->value=='Sensitive'){
              $sld_count += 1;
            }elseif($value1->value=='Resistance'){
              $sld_count1 += 1;
            }else{}
          }
          if(in_array($value1->name,['MFX(0.5)', 'MFX (2)', 'Lf x'])){
            if($value1->value=='Resistance'){
              $sld_count2 += 1;
            }
          }
          if($value1->name == 'Cfz'){
            if($value1->value=='Resistance'){
              $data['lj_cfz_res'] += 1;
            }
          }
          if($value1->name == 'PAS'){
            if($value1->value=='Resistance'){
              $data['lj_pas_res'] += 1;
            }
          }
          if($value1->name == 'Lzd'){
            if($value1->value=='Resistance'){
              $data['lj_lzd_res'] += 1;
            }
          }
          if($value1->name == 'Eto'){
            if($value1->value=='Resistance'){
              $data['lj_eto_res'] += 1;
            }
          }
          if($value1->name == 'Cla'){
            if($value1->value=='Resistance'){
              $data['lj_cla_res'] += 1;
            }
          }
          if($value1->name == 'Azi'){
            if($value1->value=='Resistance'){
              $data['lj_azi_res'] += 1;
            }
          }
          if($value1->name == 'Others'){
            if($value1->value=='Resistance'){
              $data['lj_others_res'] += 1;
            }
          }
        }
        if($temp_count==3){
          $data['lj_h_r_sens'] += 1;
        }
        if($temp_count1==3){
          $data['lj_h_r_res'] += 1;
        }
        if($h_count==2){
          $data['lj_h_res'] += 1;
        }
        if($sld_count==4){
          $data['lj_sld_1'] += 1;
        }
        if($sld_count1==4){
          $data['lj_sld_2'] += 1;
        }
        if($sld_count2==3){
          $data['lj_sld_3'] += 1;
        }
      }
      $data['to_date'] = $request->to_date;
      $data['from_date'] = $request->from_date;
      //dd($data);
        return view('admin.report.quality_indicator',compact('data'));
    }
    public function referral_indicator(Request $request)
    {
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['req_test.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['req_test.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['req_test.id', '!=', '']];
      }

      $data = TestRequest::select('dpr.DMC_PHI_Name',DB::raw('sum(m.print_15a = 1) declared'),DB::raw('COUNT(DISTINCT s.id) as received'))
              ->leftjoin('sample as s','s.enroll_id','=','req_test.enroll_id')
              ->leftjoin('t_microbiologist as m','m.sample_id','=','s.id')
              ->leftjoin('m_dmcs_phi_relation as dpr','dpr.id','=','req_test.facility_id')
              ->groupBy('facility_id')
              ->where($con)
              ->get();
      //dd($data);
      $to_date = $request->to_date;
      $from_date = $request->from_date;

        return view('admin.report.referral_indicator',compact('data','to_date','from_date'));
    }
    public function cbnaat_indicator(Request $request)
    {
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['t_service_log.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['t_service_log.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['t_service_log.id', '!=', '']];
      }
      $data['cbn_sputum'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','Sputum')
                        ->first();
      $data['cbn_csf'] = ServiceLog::select(DB::raw('count(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','CSF')
                        ->first();
      $data['cbn_ga'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','GA')
                        ->first();
      $data['cbn_pus'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','PUS')
                        ->first();
      $data['cbn_bal'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','BAL')
                        ->first();
      $data['cbn_pf'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','PF')
                        ->first();

      $data['cbn_hiv'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','HIV positive (All sample type)')
                        ->first();
      $data['cbn_others'] = ServiceLog::select(DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Detected") mtb_ref_det'), DB::raw('sum(cbn.result_MTB = "MTB Detected" AND cbn.result_RIF ="RIF Not Detected") mtb_det_ref_ndet'), DB::raw('sum(cbn.result_MTB = "MTB Not Detected") mtb_ndet'), DB::raw('sum(cbn.result_MTB = "Invalid") mtb_invalid'), DB::raw('sum(cbn.result_MTB = "RIF Indeterminate") rif_indeterminate'),DB::raw('COUNT(s.id) as total'))
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_cbnaat as cbn','t_service_log.sample_id','=','cbn.sample_id')
                        ->where('s.sample_type','Others')
                        ->first();
        return view('admin.report.cbnaat_indicator',compact('data'));
    }
    public function performance_indicator(Request $request)
    {
      if ($request->has('from_date') && $request->has('to_date')) {
        $con = [['t_service_log.created_at', '>=', date('Y-m-d',strtotime($request->from_date))],['t_service_log.created_at', '<=', date('Y-m-d',strtotime($request->to_date))]];
      }else{
        $con = [['t_service_log.id', '!=', '']];
      }
      $data['c_workload'] = ServiceLog::select(DB::raw('sum(s.test_reason = "DX" AND s.sample_type = "Sputum") d_s_s_inoculated'), DB::raw('sum(s.test_reason = "FU" AND s.sample_type = "Sputum") f_s_inoculated'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->where('t_service_log.service_id',16)
      ->where($con)
      ->first();
      $data['solid_dst_processed'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->whereIn('t_service_log.service_id',[22,23])
      ->where($con)
      ->count();
      $data['lpa_done'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where($con)
      ->count();
      $data['lc_dst_done'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->where('t_service_log.service_id',21)
      ->where($con)
      ->count();
      $data['lpa_fld'] = ServiceLog::select(DB::raw('sum(lf.rif = "Sensitive" AND lf.inh ="Sensitive") h_r_sens'), DB::raw('sum(lf.rif = "Resistant" AND lf.inh ="Resistant") h_r_res'),DB::raw('sum(lf.inh = "Resistant") h_res'),DB::raw('sum(lf.rif = "Resistant") r_res'))
                        ->where('t_service_log.service_id',15)
                        ->leftjoin('t_lpa_final as lf','t_service_log.sample_id','=','lf.sample_id')
                        ->where($con)
                        ->first();
      $data['lpa_invalid'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->where('lpaf.mtb_result','Invalid')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where($con)
      ->count();
      $data['lpa_total'] = ServiceLog::leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->leftjoin('t_1stlinelpa as lpa1','lpa1.sample_id','=','t_service_log.sample_id')
      ->leftjoin('t_lpa_final as lpaf','lpaf.sample_id','=','t_service_log.sample_id')
      ->whereIn('t_service_log.service_id',[6,7,13,15])
      ->where($con)
      ->count();
      $data['lpa1'] = ServiceLog::select('s.receive_date as date_submitted','lpa1.test_date as result_date')
                        ->where('t_service_log.service_id',6)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_1stlinelpa as lpa1','t_service_log.sample_id','=','lpa1.sample_id')
                        ->where($con)
                        ->get();
      $data['lpa1_in_tat'] = 0;
      $data['lpa1_out_tat'] = 0;
      foreach ($data['lpa1'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['lpa1_in_tat'] += 1;
        }else{
          $data['lpa1_out_tat'] += 1;
        }
      }
      $data['t_total'] = $data['lpa1_out_tat']+$data['lpa1_in_tat'];

      $data['sample_rejected'] = ServiceLog::select(DB::raw('sum(s.is_accepted = "Rejected") rej'),DB::raw('count(*) as alle'))
      ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
      ->where($con)
      ->first();
      $data['culture_smear_positive'] = ServiceLog::where('t_service_log.service_id',18)
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
                        ->where('lfmf.result','Positive')
                        ->where('s.test_reason','DX')
                        ->whereIn('m.result',['1+positive', '2+positive', '3+positive'])
                        ->where($con)
                        ->count();
      $data['culture_positive_total'] = ServiceLog::where('t_service_log.service_id',18)
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
                        ->where('lfmf.result','Positive')
                        ->where('s.test_reason','DX')
                        ->where($con)
                        ->count();
      $data['culture_lj_contaminated'] = ServiceLog::where('t_service_log.service_id',20)
                        ->leftjoin('t_lj_detail as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where('ld.final_result','Contamination')
                        ->where($con)
                        ->count();
      $data['culture_lj_total'] = ServiceLog::where('t_service_log.service_id',20)
                        ->leftjoin('t_lj_detail as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where($con)
                        ->count();
      $data['culture_lc_contaminated'] = ServiceLog::where('t_service_log.service_id',18)
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
                        ->where('lfmf.result','Contamination')
                        ->where($con)
                        ->count();
      $data['culture_lc_total'] = ServiceLog::where('t_service_log.service_id',18)
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
                        ->where($con)
                        ->count();
      $data['culture_lj_ntm'] = ServiceLog::where('t_service_log.service_id',20)
                        ->leftjoin('t_lj_detail as ld','ld.sample_id','=','t_service_log.sample_id')
                        ->where('ld.final_result','NTM')
                        ->where($con)
                        ->count();
      $data['culture_lc_ntm'] = ServiceLog::where('t_service_log.service_id',18)
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','t_service_log.sample_id')
                        ->where('lfmf.result','NTM')
                        ->where($con)
                        ->count();
      $data['culture_all_ntm'] = $data['culture_lj_ntm']+$data['culture_lc_ntm'];
      $data['p_culture_in_tat'] = 0;
      $data['p_culture_out_tat'] = 0;
      $data['p_lj_in_tat'] = 0;
      $data['p_lj_out_tat'] = 0;
      $data['p_culture'] = ServiceLog::select('ci.inoculation_date as date_submitted','fmf.result_date as result_date')
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('t_culture_inoculation as ci','t_service_log.sample_id','=','ci.sample_id')
                        ->leftjoin('t_lc_flagged_mgit_further as fmf','t_service_log.sample_id','=','fmf.sample_id')
                        ->where($con)
                        ->get();
      $data['p_culture_total'] = COUNT($data['p_culture']);
      foreach ($data['p_culture'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['p_culture_in_tat'] += 1;
        }else{
          $data['p_culture_out_tat'] += 1;
        }
      }
      $data['p_lj'] = ServiceLog::select('li.inoculation_date as date_submitted','ld.lj_result_date as result_date')
                        ->where('t_service_log.service_id',4)
                        ->leftjoin('t_lj_dst_inoculation as li','t_service_log.sample_id','=','li.sample_id')
                        ->leftjoin('t_lj_detail as ld','t_service_log.sample_id','=','ld.sample_id')
                        ->where($con)
                        ->get();
      $data['p_lj_total'] = COUNT($data['p_lj']);
      foreach ($data['p_lj'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['p_lj_in_tat'] += 1;
        }else{
          $data['p_lj_out_tat'] += 1;
        }
      }
      $data['lpa2_in_tat'] = 0;
      $data['lpa2_out_tat'] = 0;
      $data['lpa2'] = ServiceLog::select('s.receive_date as date_submitted','lpa1.test_date as result_date')
                        ->where('t_service_log.service_id',7)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_1stlinelpa as lpa1','t_service_log.sample_id','=','lpa1.sample_id')
                        ->where($con)
                        ->get();
      $data['lpa2_total'] = COUNT($data['lpa2']);
      foreach ($data['lpa2'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['lpa2_in_tat'] += 1;
        }else{
          $data['lpa2_out_tat'] += 1;
        }
      }
      $data['lj1_in_tat'] = 0;
      $data['lj1_out_tat'] = 0;
      $data['lj2_in_tat'] = 0;
      $data['lj2_out_tat'] = 0;
      $data['lj1'] = ServiceLog::select('s.receive_date as date_submitted','lj1.created_at as result_date')
                        ->where('t_service_log.service_id',22)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_lj_dst_reading as lj1','t_service_log.sample_id','=','lj1.sample_id')
                        ->leftjoin('t_dst_drugs_tr as ddt','ddt.enroll_id','=','t_service_log.enroll_id')
                        ->where($con)
                        ->get();
      foreach ($data['lj1'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['lj1_in_tat'] += 1;
        }else{
          $data['lj1_out_tat'] += 1;
        }
      }

      $data['lj2'] = ServiceLog::select('s.receive_date as date_submitted','lj2.created_at as result_date')
                        ->where('t_service_log.service_id',23)
                        ->leftjoin('sample as s','t_service_log.sample_id','=','s.id')
                        ->leftjoin('t_lj_dst_reading as lj2','t_service_log.sample_id','=','lj2.sample_id')
                        ->leftjoin('t_dst_drugs_tr as ddt','ddt.enroll_id','=','t_service_log.enroll_id')
                        ->where($con)
                        ->get();
      $data['lj_total'] = COUNT($data['lj1']) + COUNT($data['lj2']);
      foreach ($data['lj2'] as $key => $value) {
        $day = new DateTime(date($value->result_date));
        $dayAfter3 = (new DateTime($value->date_submitted))->modify('+3 day')->format('Y-m-d h:i:s');
        $dayA3 = new DateTime(date($dayAfter3));
        $interval = $dayA3->diff($day);
        $days = $interval->format('%a');
        if($days<=3){
          $data['lj2_in_tat'] += 1;
        }else{
          $data['lj2_out_tat'] += 1;
        }
      }

      $data['to_date'] = $request->to_date;
      $data['from_date'] = $request->from_date;
      return view('admin.report.performance_indicator',compact('data'));
    }
    public function cbnaat_monthly_report_submit(Request $request)
    {
      $id = Cbnaat_lab_details::select('id as ID')->where('status',1)->first();
      Cbnaat_lab_details::whereId($id->ID)->update(['status'=>0]);
      Cbnaat_lab_details::create([
        'lab_name' => $request->lab_name,
        'lab_addr' => $request->lab_addr,
        'contact_name' => $request->contact_name,
        'contact_no' => $request->contact_no,
        'contact_email' => $request->contact_email,
        'sr_no' => $request->sr_no,
        'date_caliberation' => $request->date_caliberation,
        'reporting_year' => $request->reporting_year,
        'status' => 1,
      ]);
      return redirect("/report/cbnaat_monthly_report");

    }
    public function cbnaat_monthly_report(Request $request)
    {
      $data['cbnaat_lab_details'] = new Cbnaat_lab_details;
      $data['cbnaat_lab_details'] = Cbnaat_lab_details::where('status',1)->first();

      $data['cbnaat_total'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as cbnaat_total_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as cbnaat_total_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as cbnaat_total_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as cbnaat_total_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as cbnaat_total_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as cbnaat_total_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as cbnaat_total_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as cbnaat_total_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as cbnaat_total_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as cbnaat_total_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as cbnaat_total_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as cbnaat_total_12'))
                        ->where('t_service_log.service_id',4)
                        ->first();

      $data['hiv_pos'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as hiv_pos_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as hiv_pos_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as hiv_pos_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as hiv_pos_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as hiv_pos_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as hiv_pos_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as hiv_pos_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as hiv_pos_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as hiv_pos_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as hiv_pos_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as hiv_pos_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as cbnaat_total_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->where('p.hiv_test','Pos')
                        ->first();
      $data['presumptive_DRDT'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_DRDT_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_DRDT_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_DRDT_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_DRDT_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_DRDT_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_DRDT_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_DRDT_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_DRDT_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_DRDT_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_DRDT_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_DRDT_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_DRDT_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->where('t_service_log.service_id',4)
                        ->where('req_ser.type_of_prsmptv_drtb','!=','')
                        ->where('req_ser.prsmptv_xdrtv','!=','')
                        ->first();

    $data['hiv_neg_and_non_paediatric'] = ServiceLog::select(
                      DB::raw('sum(MONTH(t_service_log.created_at)="01") as hiv_neg_and_non_paediatric_01'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="02") as hiv_neg_and_non_paediatric_02'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="03") as hiv_neg_and_non_paediatric_03'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="04") as hiv_neg_and_non_paediatric_04'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="05") as hiv_neg_and_non_paediatric_05'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="06") as hiv_neg_and_non_paediatric_06'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="07") as hiv_neg_and_non_paediatric_07'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="08") as hiv_neg_and_non_paediatric_08'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="09") as hiv_neg_and_non_paediatric_09'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="10") as hiv_neg_and_non_paediatric_10'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="11") as hiv_neg_and_non_paediatric_11'),
                      DB::raw('sum(MONTH(t_service_log.created_at)="12") as hiv_neg_and_non_paediatric_12'))
                      ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                      ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                      ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                      ->leftjoin('patient as p','p.id','=','e.patient_id')
                      ->where('t_service_log.service_id',4)
                      ->where('cbn.result_MTB','MTB Not Detected')
                      ->where('p.hiv_test','!=','Pos')
                      ->where('p.age','>','15')
                      ->first();
      $data['hiv_neg_and_non_paediatric_mtb_rif_not'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as hiv_neg_and_non_paediatric_mtb_rif_not_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as hiv_neg_and_non_paediatric_mtb_rif_not_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as hiv_neg_and_non_paediatric_mtb_rif_not_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as hiv_neg_and_non_paediatric_mtb_rif_not_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as hiv_neg_and_non_paediatric_mtb_rif_not_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as hiv_neg_and_non_paediatric_mtb_rif_not_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as hiv_neg_and_non_paediatric_mtb_rif_not_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as hiv_neg_and_non_paediatric_mtb_rif_not_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as hiv_neg_and_non_paediatric_mtb_rif_not_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as hiv_neg_and_non_paediatric_mtb_rif_not_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as hiv_neg_and_non_paediatric_mtb_rif_not_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as hiv_neg_and_non_paediatric_mtb_rif_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','>','15')
                        ->first();
      $data['hiv_neg_and_non_paediatric_mtb_not_rif_not'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as hiv_neg_and_non_paediatric_mtb_not_rif_not_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as hiv_neg_and_non_paediatric_mtb_not_rif_not_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as hiv_neg_and_non_paediatric_mtb_not_rif_not_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as hiv_neg_and_non_paediatric_mtb_not_rif_not_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as hiv_neg_and_non_paediatric_mtb_not_rif_not_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as hiv_neg_and_non_paediatric_mtb_not_rif_not_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as hiv_neg_and_non_paediatric_mtb_not_rif_not_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as hiv_neg_and_non_paediatric_mtb_not_rif_not_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as hiv_neg_and_non_paediatric_mtb_not_rif_not_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as hiv_neg_and_non_paediatric_mtb_not_rif_not_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as hiv_neg_and_non_paediatric_mtb_not_rif_not_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as hiv_neg_and_non_paediatric_mtb_not_rif_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','>','15')
                        ->first();

      $data['presumptive_hiv_mtb_not'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_hiv_mtb_not_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_hiv_mtb_not_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_hiv_mtb_not_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_hiv_mtb_not_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_hiv_mtb_not_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_hiv_mtb_not_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_hiv_mtb_not_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_hiv_mtb_not_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_hiv_mtb_not_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_hiv_mtb_not_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_hiv_mtb_not_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_hiv_mtb_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('req_ser.diagnosis','%1%')
                        //->where('cbn.result_RIF','RIF Not Detected')
                        ->where('p.hiv_test','Pos')
                        ->where('p.age','>','15')
                        ->first();
      $data['presumptive_dr_current_mtb_not'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_dr_current_mtb_not_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_dr_current_mtb_not_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_dr_current_mtb_not_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_dr_current_mtb_not_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_dr_current_mtb_not_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_dr_current_mtb_not_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_dr_current_mtb_not_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_dr_current_mtb_not_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_dr_current_mtb_not_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_dr_current_mtb_not_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_dr_current_mtb_not_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_dr_current_mtb_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('req_ser.type_of_prsmptv_drtb','!=','')
                        ->where('req_ser.prsmptv_xdrtv','!=','')
                        //->where('cbn.result_RIF','RIF Not Detected')
                        ->where('p.hiv_test','Pos')
                        ->where('p.age','>','15')
                        ->first();
      $data['presumptive_dr_current_mtb_rif_not'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_dr_current_mtb_rif_not_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_dr_current_mtb_rif_not_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_dr_current_mtb_rif_not_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_dr_current_mtb_rif_not_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_dr_current_mtb_rif_not_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_dr_current_mtb_rif_not_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_dr_current_mtb_rif_not_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_dr_current_mtb_rif_not_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_dr_current_mtb_rif_not_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_dr_current_mtb_rif_not_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_dr_current_mtb_rif_not_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_dr_current_mtb_rif_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('req_ser.type_of_prsmptv_drtb','!=','')
                        ->where('req_ser.prsmptv_xdrtv','!=','')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','<=','15')
                        ->first();
      $data['presumptive_dr_current_mtb_rif'] = ServiceLog::select(
                        DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_dr_current_mtb_rif_01'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_dr_current_mtb_rif_02'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_dr_current_mtb_rif_03'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_dr_current_mtb_rif_04'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_dr_current_mtb_rif_05'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_dr_current_mtb_rif_06'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_dr_current_mtb_rif_07'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_dr_current_mtb_rif_08'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_dr_current_mtb_rif_09'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_dr_current_mtb_rif_10'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_dr_current_mtb_rif_11'),
                        DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_dr_current_mtb_rif_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('req_ser.type_of_prsmptv_drtb','!=','')
                        ->where('req_ser.prsmptv_xdrtv','!=','')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','>','15')
                        ->first();

      $data['paediatric'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as paediatric_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as paediatric_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as paediatric_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as paediatric_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as paediatric_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as paediatric_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as paediatric_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as paediatric_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as paediatric_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as paediatric_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as paediatric_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as cbnaat_total_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('t_service_log.service_id',4)
                        // ->where('p.hiv_test','!=','Pos')
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->where('p.age','<=','15')
                        ->first();
      $data['mtb_not_detected'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_not_detected_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_not_detected_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_not_detected_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_not_detected_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_not_detected_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_not_detected_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_not_detected_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_not_detected_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_not_detected_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_not_detected_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_not_detected_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_not_detected_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->first();

      $data['presumptive_tb'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as presumptive_tb_01'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="02") as presumptive_tb_02'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="03") as presumptive_tb_03'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="04") as presumptive_tb_04'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="05") as presumptive_tb_05'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="06") as presumptive_tb_06'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="07") as presumptive_tb_07'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="08") as presumptive_tb_08'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="09") as presumptive_tb_09'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="10") as presumptive_tb_10'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="11") as presumptive_tb_11'),
                          DB::raw('sum(MONTH(t_service_log.created_at)="12") as presumptive_tb_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as test_req','test_req.enroll_id','=','t_service_log.enroll_id')
                        ->where('t_service_log.service_id',4)
                        //->where(DB::raw("FIND_IN_SET('1','test_req.diagnosis')"))
                        ->first();
      //dd($data['presumptive_tb']);

      $data['EP'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as EP_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as EP_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as EP_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as EP_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as EP_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as EP_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as EP_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as EP_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as EP_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as EP_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as EP_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as EP_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->whereIn('s.sample_type',['Pus','CSF','GA','Pericardial fluid','EB tissue','Urine','Pleural fluid','FNAC','Others'])
                        ->first();

      $data['EP_mtb_not'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as EP_mtb_not_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as EP_mtb_not_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as EP_mtb_not_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as EP_mtb_not_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as EP_mtb_not_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as EP_mtb_not_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as EP_mtb_not_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as EP_mtb_not_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as EP_mtb_not_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as EP_mtb_not_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as EP_mtb_not_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as EP_mtb_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->whereIn('s.sample_type',['Pus','CSF','GA','Pericardial fluid','EB tissue','Urine','Pleural fluid','FNAC','Others'])
                        //->where('cbn.result_RIF','RIF Not Detected')
                        ->first();
      $data['EP_mtb_rif_not'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as EP_mtb_rif_not_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as EP_mtb_rif_not_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as EP_mtb_rif_not_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as EP_mtb_rif_not_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as EP_mtb_rif_not_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as EP_mtb_rif_not_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as EP_mtb_rif_not_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as EP_mtb_rif_not_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as EP_mtb_rif_not_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as EP_mtb_rif_not_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as EP_mtb_rif_not_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as EP_mtb_rif_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->whereIn('s.sample_type',['Pus','CSF','GA','Pericardial fluid','EB tissue','Urine','Pleural fluid','FNAC','Others'])
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->first();
      $data['EP_mtb_rif'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as EP_mtb_rif_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as EP_mtb_rif_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as EP_mtb_rif_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as EP_mtb_rif_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as EP_mtb_rif_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as EP_mtb_rif_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as EP_mtb_rif_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as EP_mtb_rif_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as EP_mtb_rif_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as EP_mtb_rif_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as EP_mtb_rif_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as EP_mtb_rif_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->whereIn('s.sample_type',['Pus','CSF','GA','Pericardial fluid','EB tissue','Urine','Pleural fluid','FNAC','Others'])
                        ->first();

      $data['mtb_detected_rif_not'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_detected_rif_not_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_detected_rif_not_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_detected_rif_not_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_detected_rif_not_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_detected_rif_not_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_detected_rif_not_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_detected_rif_not_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_detected_rif_not_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_detected_rif_not_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_detected_rif_not_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_detected_rif_not_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_detected_rif_not_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->first();

      $data['mtb_detected_rif_not_hiv'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_detected_rif_not_hiv_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_detected_rif_not_hiv_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_detected_rif_not_hiv_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_detected_rif_not_hiv_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_detected_rif_not_hiv_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_detected_rif_not_hiv_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_detected_rif_not_hiv_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_detected_rif_not_hiv_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_detected_rif_not_hiv_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_detected_rif_not_hiv_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_detected_rif_not_hiv_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_detected_rif_not_hiv_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('p.hiv_test','Pos')
                        ->first();
      $data['mtb_detected_rif_not_paediatric'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_detected_rif_not_paediatric_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_detected_rif_not_paediatric_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_detected_rif_not_paediatric_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_detected_rif_not_paediatric_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_detected_rif_not_paediatric_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_detected_rif_not_paediatric_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_detected_rif_not_paediatric_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_detected_rif_not_paediatric_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_detected_rif_not_paediatric_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_detected_rif_not_paediatric_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_detected_rif_not_paediatric_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_detected_rif_not_paediatric_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','<=','15')
                        ->first();
      $data['mtb_rif_detected'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_detected_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_detected_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_detected_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_detected_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_detected_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_detected_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_detected_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_detected_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_detected_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_detected_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_detected_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_detected_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->first();
      $data['mtb_rif_detected_hiv'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_detected_hiv_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_detected_hiv_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_detected_hiv_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_detected_hiv_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_detected_hiv_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_detected_hiv_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_detected_hiv_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_detected_hiv_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_detected_hiv_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_detected_hiv_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_detected_hiv_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_detected_hiv_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('p.hiv_test','Pos')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->first();
      $data['mtb_rif_detected_paediatric'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_detected_paediatric_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_detected_paediatric_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_detected_paediatric_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_detected_paediatric_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_detected_paediatric_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_detected_paediatric_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_detected_paediatric_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_detected_paediatric_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_detected_paediatric_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_detected_paediatric_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_detected_paediatric_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_detected_paediatric_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('enrolls as e','e.id','=','s.enroll_id')
                        ->leftjoin('patient as p','p.id','=','e.patient_id')
                        ->where('p.hiv_test','!=','Pos')
                        ->where('p.age','<=','15')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->first();
      $data['mtb_rif_indeterminate'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_indeterminate_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_indeterminate_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_indeterminate_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_indeterminate_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_indeterminate_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_indeterminate_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_indeterminate_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_indeterminate_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_indeterminate_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_indeterminate_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_indeterminate_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_indeterminate_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Indeterminate')
                        ->first();
      $data['mtb_rif_indeterminate_sens'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_indeterminate_sens_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_indeterminate_sens_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_indeterminate_sens_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_indeterminate_sens_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_indeterminate_sens_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_indeterminate_sens_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_indeterminate_sens_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_indeterminate_sens_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_indeterminate_sens_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_indeterminate_sens_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_indeterminate_sens_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_indeterminate_sens_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Indeterminate')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->first();
      $data['mtb_rif_indeterminate_res'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_rif_indeterminate_res_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_rif_indeterminate_res_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_rif_indeterminate_res_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_rif_indeterminate_res_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_rif_indeterminate_res_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_rif_indeterminate_res_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_rif_indeterminate_res_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_rif_indeterminate_res_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_rif_indeterminate_res_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_rif_indeterminate_res_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_rif_indeterminate_res_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_rif_indeterminate_res_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Indeterminate')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->first();
      $data['mtb_error'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_error_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Error')
                        ->first();
      $data['mtb_no_result'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as no_result_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','No Result')
                        ->first();
      $data['mtb_error_code_2008'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_error_code_2008_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_code_2008_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_code_2008_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_code_2008_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_code_2008_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_code_2008_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_code_2008_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_code_2008_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_code_2008_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_code_2008_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_code_2008_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_code_2008_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Error')
                        ->where('cbn.error',2008)
                        ->first();
      $data['mtb_error_code_5'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_error_code_5_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_code_5_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_code_5_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_code_5_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_code_5_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_code_5_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_code_5_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_code_5_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_code_5_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_code_5_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_code_5_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_code_5_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Error')
                        ->whereIn('cbn.error',[5006,5007,5008])
                        ->first();
      $data['mtb_error_code_3'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_error_code_3_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_code_3_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_code_3_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_code_3_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_code_3_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_code_3_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_code_3_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_code_3_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_code_3_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_code_3_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_code_3_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_code_3_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Error')
                        ->whereIn('cbn.error',[3074,3075])
                        ->first();
      $data['mtb_error_code_others'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_error_code_others_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_error_code_others_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_error_code_others_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_error_code_others_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_error_code_others_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_error_code_others_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_error_code_others_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_error_code_others_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_error_code_others_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_error_code_others_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_error_code_others_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_error_code_others_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Error')
                        ->whereNotIn('cbn.error',[2008,5006,5007,5008,3074,3075])
                        ->first();
      $data['mtb_invalid'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_invalid_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_invalid_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_invalid_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_invalid_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_invalid_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_invalid_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_invalid_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_invalid_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_invalid_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_invalid_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_invalid_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_invalid_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','Invalid')
                        ->first();
      $data['mtb_neg_microscopy_pos'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_neg_microscopy_pos_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_neg_microscopy_pos_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_neg_microscopy_pos_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_neg_microscopy_pos_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_neg_microscopy_pos_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_neg_microscopy_pos_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_neg_microscopy_pos_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_neg_microscopy_pos_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_neg_microscopy_pos_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_neg_microscopy_pos_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_neg_microscopy_pos_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_neg_microscopy_pos_12'))
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->whereIn('m.result',['1+positive','2+positive','3+positive'])
                        ->first();
      $data['mtb_pos_microscopy_neg'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as mtb_pos_microscopy_neg_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as mtb_pos_microscopy_neg_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as mtb_pos_microscopy_neg_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as mtb_pos_microscopy_neg_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as mtb_pos_microscopy_neg_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as mtb_pos_microscopy_neg_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as mtb_pos_microscopy_neg_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as mtb_pos_microscopy_neg_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as mtb_pos_microscopy_neg_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as mtb_pos_microscopy_neg_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as mtb_pos_microscopy_neg_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as mtb_pos_microscopy_neg_12'))
                        ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('m.result','Negative')
                        ->first();
      $data['referrals_ptivate_sec'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as referrals_ptivate_sec_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as referrals_ptivate_sec_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as referrals_ptivate_sec_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as referrals_ptivate_sec_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as referrals_ptivate_sec_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as referrals_ptivate_sec_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as referrals_ptivate_sec_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as referrals_ptivate_sec_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as referrals_ptivate_sec_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as referrals_ptivate_sec_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as referrals_ptivate_sec_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as referrals_ptivate_sec_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('req_test as rt','rt.enroll_id','=','s.enroll_id')
                        ->where('t_service_log.service_id',4)
                        ->where('rt.facility_type',13)
                        ->first();

      $data['referrals_ptivate_sec_mtb_not'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as referrals_ptivate_sec_mtb_not_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as referrals_ptivate_sec_mtb_not_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as referrals_ptivate_sec_mtb_not_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as referrals_ptivate_sec_mtb_not_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as referrals_ptivate_sec_mtb_not_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as referrals_ptivate_sec_mtb_not_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as referrals_ptivate_sec_mtb_not_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as referrals_ptivate_sec_mtb_not_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as referrals_ptivate_sec_mtb_not_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as referrals_ptivate_sec_mtb_not_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as referrals_ptivate_sec_mtb_not_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as referrals_ptivate_sec_mtb_not_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('req_test as rt','rt.enroll_id','=','s.enroll_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('rt.facility_type',13)
                        ->where('cbn.result_MTB','MTB Not Detected')
                        ->first();

      $data['referrals_ptivate_sec_mtb_rif_not'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as referrals_ptivate_sec_mtb_rif_not_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as referrals_ptivate_sec_mtb_rif_not_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as referrals_ptivate_sec_mtb_rif_not_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as referrals_ptivate_sec_mtb_rif_not_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as referrals_ptivate_sec_mtb_rif_not_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as referrals_ptivate_sec_mtb_rif_not_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as referrals_ptivate_sec_mtb_rif_not_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as referrals_ptivate_sec_mtb_rif_not_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as referrals_ptivate_sec_mtb_rif_not_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as referrals_ptivate_sec_mtb_rif_not_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as referrals_ptivate_sec_mtb_rif_not_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as referrals_ptivate_sec_mtb_rif_not_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('req_test as rt','rt.enroll_id','=','s.enroll_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('rt.facility_type',13)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->first();

      $data['referrals_ptivate_sec_mtb_rif_pos'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as referrals_ptivate_sec_mtb_rif_pos_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as referrals_ptivate_sec_mtb_rif_pos_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as referrals_ptivate_sec_mtb_rif_pos_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as referrals_ptivate_sec_mtb_rif_pos_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as referrals_ptivate_sec_mtb_rif_pos_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as referrals_ptivate_sec_mtb_rif_pos_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as referrals_ptivate_sec_mtb_rif_pos_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as referrals_ptivate_sec_mtb_rif_pos_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as referrals_ptivate_sec_mtb_rif_pos_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as referrals_ptivate_sec_mtb_rif_pos_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as referrals_ptivate_sec_mtb_rif_pos_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as referrals_ptivate_sec_mtb_rif_pos_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('req_test as rt','rt.enroll_id','=','s.enroll_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('rt.facility_type',13)
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->first();
      $data['ep_tb'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as ep_tb_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as ep_tb_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as ep_tb_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as ep_tb_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as ep_tb_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as ep_tb_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as ep_tb_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as ep_tb_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as ep_tb_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as ep_tb_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as ep_tb_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as ep_tb_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->where('t_service_log.service_id',4)
                        ->where('s.sample_type','Others')
                        ->first();
      $data['ep_tb_mtb_rif_neg'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as ep_tb_mtb_rif_neg_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as ep_tb_mtb_rif_neg_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as ep_tb_mtb_rif_neg_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as ep_tb_mtb_rif_neg_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as ep_tb_mtb_rif_neg_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as ep_tb_mtb_rif_neg_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as ep_tb_mtb_rif_neg_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as ep_tb_mtb_rif_neg_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as ep_tb_mtb_rif_neg_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as ep_tb_mtb_rif_neg_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as ep_tb_mtb_rif_neg_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as ep_tb_mtb_rif_neg_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Not Detected')
                        ->where('t_service_log.service_id',4)
                        ->where('s.sample_type','Others')
                        ->first();
      $data['ep_tb_mtb_rif_pos'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as ep_tb_mtb_rif_pos_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as ep_tb_mtb_rif_pos_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as ep_tb_mtb_rif_pos_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as ep_tb_mtb_rif_pos_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as ep_tb_mtb_rif_pos_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as ep_tb_mtb_rif_pos_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as ep_tb_mtb_rif_pos_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as ep_tb_mtb_rif_pos_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as ep_tb_mtb_rif_pos_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as ep_tb_mtb_rif_pos_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as ep_tb_mtb_rif_pos_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as ep_tb_mtb_rif_pos_12'))
                        ->leftjoin('sample as s','s.id','=','t_service_log.sample_id')
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->where('cbn.result_MTB','MTB Detected')
                        ->where('cbn.result_RIF','RIF Detected')
                        ->where('t_service_log.service_id',4)
                        ->where('s.sample_type','Others')
                        ->first();
      $data['secondLineDST'] = ServiceLog::select(DB::raw('sum(MONTH(t_service_log.created_at)="01") as secondLineDST_01'),DB::raw('sum(MONTH(t_service_log.created_at)="02") as secondLineDST_02'),DB::raw('sum(MONTH(t_service_log.created_at)="03") as secondLineDST_03'),DB::raw('sum(MONTH(t_service_log.created_at)="04") as secondLineDST_04'),DB::raw('sum(MONTH(t_service_log.created_at)="05") as secondLineDST_05'),DB::raw('sum(MONTH(t_service_log.created_at)="06") as secondLineDST_06'),DB::raw('sum(MONTH(t_service_log.created_at)="07") as secondLineDST_07'),DB::raw('sum(MONTH(t_service_log.created_at)="08") as secondLineDST_08'),DB::raw('sum(MONTH(t_service_log.created_at)="09") as secondLineDST_09'),DB::raw('sum(MONTH(t_service_log.created_at)="10") as secondLineDST_10'),DB::raw('sum(MONTH(t_service_log.created_at)="11") as secondLineDST_11'),DB::raw('sum(MONTH(t_service_log.created_at)="12") as secondLineDST_12'))
                        ->leftjoin('t_cbnaat as cbn','cbn.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('t_request_services as req_ser','req_ser.enroll_id','=','t_service_log.enroll_id')
                        ->where('t_service_log.service_id',4)
                        ->where('t_service_log.service_id',23)
                        ->first();
      $data['lab_details'] =DB::table('m_configuration')->select('lab_name','address')->where('status',1)->first();
                        //dd($data);
      return view('admin.report.cbnaat_monthly_new',compact('data'));
    }
    public function result_edit(Request $request)
    {
      $data['sample'] = ResultEdit::select('s.sample_label as sample','t_result_edit.id',
                            DB::raw('DATE_FORMAT(t_result_edit.created_at, "%d-%m-%Y") as created'),'t_result_edit.previous_result','t_result_edit.updated_result',
                            't_result_edit.reason','service.name as service','u.name as updatedBy')
                             ->leftjoin('sample as s','t_result_edit.sample_id','=','s.id')
                             ->leftjoin('users as u','u.id','=','t_result_edit.updated_by')
                             ->leftjoin('m_services as service','service.id','=','t_result_edit.service_id')
                             ->get();
      if($request->from_date || $request->to_date){
        $data['sample'] = ResultEdit::select('s.sample_label as sample','t_result_edit.id',
                              DB::raw('DATE_FORMAT(t_result_edit.created_at, "%d-%m-%Y") as created'),'t_result_edit.previous_result','t_result_edit.updated_result',
                              't_result_edit.reason','service.name as service','u.name as updatedBy')
                               ->leftjoin('sample as s','t_result_edit.sample_id','=','s.id')
                               ->leftjoin('users as u','u.id','=','t_result_edit.updated_by')
                               ->leftjoin('m_services as service','service.id','=','t_result_edit.service_id')
                               ->where(DB::raw('DATE_FORMAT(t_result_edit.created_at, "%d-%m-%Y")'),'>',$request->from_date)
                               ->where(DB::raw('DATE_FORMAT(t_result_edit.created_at, "%d-%m-%Y")'),'<=',$request->to_date)
                               ->get();
      }
      $data['from_date'] = $request->from_date;
      $data['to_date'] = $request->to_date;
      return view('admin.report.result_edit',compact('data'));
    }


}
