<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Config;
use App\Model\User;
use App\Model\Enroll;

class LaboratoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data=[];
        // $data['config'] = Config::where('status',1)->get();
        // return view('admin.laboratory.index',compact('data'));

        $data['config'] = Config::where('status',1)->first();
        $data['user'] = User::select('name')->get();
       // dd($data['config']);
        return view('admin.laboratory.view',compact('data'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[];
        //$data['config'] = new Config;
        $data['config'] = Config::where('status',1)->first();
        $data['user'] = User::select('name')->get();
        return view('admin.laboratory.form',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $offset = $request->barcode_offset;
        preg_match_all('!\d+!', $offset, $matches);
        print_r($matches);
        $year = $matches[0][0];
        $offset_val= $matches[0][1];
        if($offset_val > 0){
            $enroll = Enroll::create([
                'id' => $offset_val,
                'patient_id' => 0,

               ]);
         }

      //  dd($request->all());

        $img_name = $request->img->getClientOriginalName();
        $destinationPath = public_path().'/files/';
        if($request->img->move($destinationPath,$img_name))
        {

             $url= \URL::to('/').'/files/'.$img_name;
             $request->request->add(['logo' => $url]);

        }


        $user_id = User::select('id as id')->where('email',$request->sink_user)->first();
        if($user_id){
            $request->request->add(['sink_user_id' => $user_id->id]);
        }

        $ispresent = Config::where('status',1)->get();
        if(count($ispresent)){
            Config::where('status', 1)
                     ->update(['status' => 0]);
        }
        $config = new Config;
        //dd($request->all());
        $config->fill($request->all());
        $config->save();
         return redirect('/laboratory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['config'] = Config::find($id);
        $data['user'] = User::select('name')->get();
        return view('admin.laboratory.view',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view_form()
    {
        dd('ddd');
        $data['config'] = Config::where('status',1)->first();
        dd($data['config']);
        return view('admin.laboratory.view',compact('data'));
    }
}
