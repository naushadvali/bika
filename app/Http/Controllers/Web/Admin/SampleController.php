<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Model\Sample;
use App\Model\Cbnaat;
use App\Model\Enroll;
use App\Model\Service;
use App\Model\LJDetail;
use App\Model\FirstLineLpa;
use App\Model\SecondLineLpa;
use App\Model\FinalInterpretation;
use App\Model\LCFlaggedMGITFurther;
use App\Model\LjDstReading;
use App\Model\LCDST;
use App\Model\ServiceLog;
use App\Model\RequestServices;
use Illuminate\Support\Facades\DB;
use App\Model\Microbio;
use App\Model\Microscopy;
use App\User;
use \Milon\Barcode\DNS1D;
use Illuminate\Support\Facades\Auth;
use PDF;

class SampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $data['sample'] = Sample::select(DB::raw('sample.id, sample.others_type,e.patient_id,sample.enroll_id,e.label,group_concat(receive_date) as receive,group_concat(sample.sample_label) as samples,
        group_concat(test_reason) as reason,group_concat(fu_month) as fu_month,
        group_concat(sample_type) as sample_type, group_concat(sample_quality) as sample_quality,
        group_concat(is_accepted) as is_accepted, count(sample_quality) as no_of_samples, group_concat(s.name) as sname'))


                          ->leftjoin('m_services as s','s.id','=','sample.service_id')
                          ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                          ->groupBy('sample.enroll_id')
                          ->orderBy('sample.enroll_id','desc')
                          ->distinct()
                          ->get();


        return view('admin.sample.list',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($enroll_id)
    {
      $data['enroll_id'] = $enroll_id;
      $data['today_date']=DB::select('select date_format(now(),"%d-%m-%y %H:%i:%s") as date');
        $data['today']=$data['today_date'][0]->date;
       // dd($data);
      $data['services'] = Service::select('id','name')->get();
        return view('admin.sample.form1',compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       //dd($request->all());

        $data1 = Sample::store($request);
        return redirect('/sample');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data =  Sample::edit($id);
       return view('admin.sample.form',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $sample_string
     * @return \Illuminate\Http\Response
     */
    public function barCodePrint($sample_string)
    {
        //echo $sample_string;
        $samples = explode(',', $sample_string);
        foreach ($samples as $key => $value) {
          echo DNS1D::getBarcodeSVG($value,"C93",0.6,20); //C39
          echo "<br/>".$value;
          echo "<br/><br/><br/>";
        }
        //echo DNS1D::getBarcodeSVG("0000000004A", "C39");
    }

    public function sample_preview(request $request, $id)
    {
        $data['enroll']=DB::table('enrolls')->select('label as enroll')->where('id',$id)->first();
        $data['sample_detail']=Sample::select('name','no_of_samples')->where('enroll_id',$id)->first();
        $data['sample'] = Sample::where('enroll_id',$id)->get();
        return view('admin.sample.preview',compact('data'));
    }

    public function pdfview(Request $request, $id, $pdf=null)
    {

      $data['today'] = date('d-m-Y');
      $final=0;
      // if($final==1){
      //   $data['report_type'] = 'End of Report';
      // }
      // else {
      //   $data['report_type'] = 'Interim Report';
      // }
      $data['report_type'] = 'Interim Report';
      $reportType = Microbio::select('report_type')->where('sample_id',$id)->where('status',1)->first();
      $data['date_receipt'] = Sample::select('receive_date')->where('id',$id)->first();
      if($reportType){
        $data['report_type'] = $reportType->report_type;
      }

      $data['user'] = Auth::user()->name;
      $data['personal'] = Sample::select('sample.*','sample.id as smp_id','p.name as userName','p.*','e.*','d.name as district_name',
      's.name as state_name','r.rntcp_reg_no', 'r.regimen','r.reason as test_reason','r.requestor_name','r.designation',
      'r.contact_no as requestor_cno','r.email_id as requestor_email','ps.name as ps_name','r.duration','r.type_of_prsmptv_drtb',
      'r.prsmptv_xdrtv','r.presumptive_h','r.pmdt_tb_no','rs.diagnosis','p.population_other',
      'r.month_week','r.treatment','rs.regimen_fu','rs.fudrtb_regimen_other','rs.facility_type_other','r.req_test_type',
      'phi.name as phi_name','mtb.name as mtb','ft.name as f_name','p.collection_date as collection_dates')
                          ->leftjoin('req_test as r','r.enroll_id','=','sample.enroll_id')
                          ->leftjoin('m_predominan_symptom as ps','ps.symptom_id','=','r.predmnnt_symptoms')
                          ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                          ->leftjoin('t_request_services as rs','rs.enroll_id','=','sample.enroll_id')
                          ->leftjoin('patient as p','p.id','=','e.patient_id')
                          ->leftjoin('m_phi as phi','phi.id','=','p.phi')
                          ->leftjoin('m_tb as mtb','mtb.id','=','p.tb')
                          ->leftjoin('district as d','d.id','=','p.district')
                          ->leftjoin('state as s','s.STOCode','=','p.state')
                          ->leftjoin('facility_master as ft','ft.id','=','r.facility_type')
                          ->where('sample.id', $id)
                          ->first();
      $data['microscopy_data'] = ServiceLog::select('m.*','t_service_log.service_id','t_service_log.sample_label')
                            ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                            ->where('t_service_log.enroll_id',$data['personal']->enroll_id)
                            ->whereIn('t_service_log.service_id', [1,2])
                            //->where('t_service_log.status',0)
                            ->first();
      $data['microscopy']=0;
      $data['microscopyA'] = new Microscopy;
      $data['microscopyB'] = new Microscopy;
      if($data['microscopy_data']){
        $data['microscopy'] = $data['microscopy_data']->service_id;
        $sample_type = substr($data['microscopy_data']->sample_label, -1);
        if($sample_type=='A'){
          $data['microscopyA'] = $data['microscopy_data'];
        }
        if($sample_type=='B'){
          $data['microscopyB'] = $data['microscopy_data'];
        }
      }
      $data['culturelj'] = LJDetail::leftjoin('sample as s','s.id','=','t_lj_detail.sample_id')
                          ->where('t_lj_detail.enroll_id',$data['personal']->enroll_id)
                          ->where('t_lj_detail.status', 1)
                          ->first();
      $data['culturelc'] = LCFlaggedMGITFurther::leftjoin('sample as s','s.id','=','t_lc_flagged_mgit_further.sample_id')
                          ->where('t_lc_flagged_mgit_further.enroll_id',$data['personal']->enroll_id)
                          ->where('t_lc_flagged_mgit_further.status', 1)
                          ->first();
      $data['culture'] = "";
      if($data['culturelj']){
        $data['culture'] = 1;
      }
      if($data['culturelc']){
        $data['culture'] = 2;
      }

      $data['cbnaat'] = Cbnaat::leftjoin('sample as s','s.id','=','t_cbnaat.sample_id')
                        ->where('t_cbnaat.enroll_id', $data['personal']->enroll_id)
                        //->where('t_cbnaat.status', 1)
                        ->first();
      $data['lpa1'] = FirstLineLpa::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
      $data['lpa2'] = SecondLineLpa::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
      $data['lpaf'] = FinalInterpretation::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
      $data['test_requested'] = RequestServices::where('enroll_id', $data['personal']->enroll_id)->pluck('service_id')->toArray();
      //dd($data['test_requested'] );
      $data['microbio'] = Microbio::where('enroll_id',$data['personal']->enroll_id)->get()->toArray();
      //dd($data['microbio']);
      $data['lj_dst_fld'] = LjDstReading::select('drug_reading')->where('enroll_id',$data['personal']->enroll_id)->orderBy('id','DESC')->first();

      $data['lc_dst'] = LCDST::select('drug_name as name','result as value')->where('enroll_id',$data['personal']->enroll_id)->orderBy('id','DESC')->get();
      $data['lab_sr'] = DB::table('sample')->select('sample_label')->where('sample.enroll_id',$data['personal']->enroll_id)->first();
      //$dil = json_decode($data['lj_dst_fld']);
      $data['s'] = "";
      $data['h1'] = "";
      $data['h2'] = "";
      $data['r'] = "";
      $data['e'] = "";
      $data['z'] = "";
      $data['km'] = "";
      $data['cm'] = "";
      $data['am'] = "";
      $data['lfx'] = "";
      $data['mfx1'] = "";
      $data['mfx2'] = "";
      $data['pas'] = "";
      $data['lzd'] = "";
      $data['cfz'] = "";
      $data['eto'] = "";
      $data['cla'] = "";
      $data['azi'] = "";
      foreach ($data['lc_dst'] as $key => $value) {
        if($value->name=="S"){
          $data['s'] = $value->value;
        }
        if($value->name=="H1"){
          $data['h1'] = $value->value;
        }
        if($value->name=="H2"){
          $data['h2'] = $value->value;
        }
        if($value->name=="R"){
          $data['r'] = $value->value;
        }
        if($value->name=="E"){
          $data['e'] = $value->value;
        }
        if($value->name=="Z"){
          $data['z'] = $value->value;
        }
        if($value->name=="K_M"){
          $data['km'] = $value->value;
        }
        if($value->name=="C_M"){
          $data['cm'] = $value->value;
        }
        if($value->name=="A_M"){
          $data['am'] = $value->value;
        }
        if($value->name=="Lf_x"){
          $data['lfx'] = $value->value;
        }
        if($value->name=="Mfx(0.5)"){
          $data['mfx1'] = $value->value;
        }
        if($value->name=="Mfx_(2)"){
          $data['mfx2'] = $value->value;
        }
        if($value->name=="PAS"){
          $data['pas'] = $value->value;
        }
        if($value->name=="Lzd"){
          $data['lzd'] = $value->value;
        }
        if($value->name=="Cfz"){
          $data['cfz'] = $value->value;
        }
        if($value->name=="Eto"){
          $data['eto'] = $value->value;
        }
        if($value->name=="Cla"){
          $data['cla'] = $value->value;
        }
        if($value->name=="Azi"){
          $data['azi'] = $value->value;

        }
      }
      if($data['lj_dst_fld']){
        $dil = json_decode($data['lj_dst_fld']->drug_reading);
        foreach ($dil->dil_2 as $key => $value) {
          if($value->name=="S"){
            $data['s'] = $value->value;
          }
          if($value->name=="H1"){
            $data['h1'] = $value->value;
          }
          if($value->name=="H2"){
            $data['h2'] = $value->value;
          }
          if($value->name=="R"){
            $data['r'] = $value->value;
          }
          if($value->name=="E"){
            $data['e'] = $value->value;
          }
          if($value->name=="Z"){
            $data['z'] = $value->value;
          }
          if($value->name=="K M"){
            $data['km'] = $value->value;
          }
          if($value->name=="C M"){
            $data['cm'] = $value->value;
          }
          if($value->name=="A M"){
            $data['am'] = $value->value;
          }
          if($value->name=="Lf x"){
            $data['lfx'] = $value->value;
          }
          if($value->name=="Mfx(0.5)"){
            $data['mfx1'] = $value->value;
          }
          if($value->name=="Mfx (2)"){
            $data['mfx2'] = $value->value;
          }
          if($value->name=="PAS"){
            $data['pas'] = $value->value;
          }
          if($value->name=="Lzd"){
            $data['lzd'] = $value->value;
          }
          if($value->name=="Cfz"){
            $data['cfz'] = $value->value;
          }
          if($value->name=="Eto"){
            $data['eto'] = $value->value;
          }
          if($value->name=="Cla"){
            $data['cla'] = $value->value;
          }
          if($value->name=="Azi"){
            $data['azi'] = $value->value;

          }
        }
      }


      if($pdf=="pdf"){
        $pdfname = $data['personal']?$data['personal']->userName:"pdfview";
        $elabel = $data['personal']?$data['personal']->label:"";
        $pdffilename = $elabel."".substr($pdfname, 0, 5).".pdf";
          $pdf = PDF::loadView('admin.sample.pdfview',compact('data'));
          return $pdf->download($pdffilename);
      }

      return view('admin.sample.pdfview',compact('data'));
    }
    public function annexurek(Request $request)
    {
      $data['lab'] = Sample::select('sample.sample_label','sample.id', 'sample.receive_date', 'sample.name', 'p.gender', 'p.age', 's.name as state', 'd.name as district', 'p.house_no', 'p.pincode', 'p.key_population', 'sample.test_reason', 'r.prsmptv_xdrtv', 'r.predmnnt_symptoms', 'r.ho_anti_tb','sample.nikshay_id','r.regimen','r.post_treatment as fu_month', 'sample.sample_type', 'sample.sample_quality','mft.name as facility_name')
                          ->leftjoin('req_test as r','r.enroll_id','=','sample.enroll_id')
                          ->leftjoin('m_facility_type as mft','mft.facility_type_id','=','r.facility_type')
                          ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                          ->leftjoin('patient as p','p.id','=','e.patient_id')
                          ->leftjoin('district as d','d.id','=','p.district')
                          ->leftjoin('state as s','s.STOCode','=','p.state')
                          ->get();
      $data['result'] = Sample::select('ld.id as ld_id', 'p.hiv_test', 'ld.lj_result_date as ld_date', 'sample.nikshay_id',  'mt.name as tb', 'sample.sample_label', 'sample.receive_date')
                          ->leftjoin('req_test as r','r.enroll_id','=','sample.enroll_id')
                          ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                          ->leftjoin('patient as p','p.id','=','e.patient_id')
                          ->leftjoin('t_lj_detail as ld','ld.sample_id','=','sample.id')
                          ->leftjoin('m_tb as mt','mt.id','=','p.tb')
                          ->get();
                          //dd($data);
      return view('admin.sample.annexurek',compact('data'));
    }
    public function annexurel(Request $request)
    {
      $data['lab'] = Sample::select('sample.sample_label','sample.id', 'sample.receive_date', 'sample.name', 'p.gender',
      'p.age', 's.name as state', 'd.name as district', 'p.house_no', 'p.pincode', 'p.key_population', 'sample.test_reason',
      'r.prsmptv_xdrtv', 'r.predmnnt_symptoms', 'r.ho_anti_tb','sample.nikshay_id','r.regimen','r.post_treatment as fu_month',
      'sample.sample_type','sample.sample_quality','mft.name as facility_name', 'p.nikshay_id as nikshay', 'p.mobile_number as mobile','rq.regimen as regimenname'
      ,'rq.type_of_prsmptv_drtb as mdrtb','rq.duration','ps.name as psymptom','rq.pmdt_tb_no',
      'rq.treatment','m.result as mresult','p.collection_date','p.regr_date')
                          ->leftjoin('req_test as r','r.enroll_id','=','sample.enroll_id')
                          ->leftjoin('m_predominan_symptom as ps','r.predmnnt_symptoms','ps.symptom_id')
                          ->leftjoin('t_request_services as rq','rq.enroll_id','=','sample.enroll_id')
                          ->leftjoin('m_facility_type as mft','mft.facility_type_id','=','r.facility_type')
                          ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                          ->leftjoin('t_microscopy as m','m.sample_id','sample.id')
                          ->leftjoin('patient as p','p.id','=','e.patient_id')
                          ->leftjoin('district as d','d.id','=','p.district')
                          ->leftjoin('state as s','s.STOCode','=','p.state')
                          ->get();

      $data['result'] = Sample::select('ld.id as ld_id', 'lfmf.id as lfmf_id', 'lfmf.result as lfmf_result',
       'lfmf.result_date as lfmf_date', 'ld.final_result as ld_result', 'ld.lj_result_date as ld_date','sample.id as sampleId',
       'sample.nikshay_id','sample.receive_date','c.result_MTB','c.result_RIF', 'c.id as c_id', 'lf.id as lf_id',
       'lf.mtb_result','sample.service_id',
       'lf.rif', 'lf.inh','lf.type_direct','lf.quinolone','lf.slid','c.result_MTB as mtb')
                        ->leftjoin('t_lj_detail as ld','ld.sample_id','=','sample.id')
                        ->leftjoin('t_lc_flagged_mgit_further as lfmf','lfmf.sample_id','=','sample.id')
                        ->leftjoin('t_cbnaat as c','c.sample_id','=','sample.id')
                        ->leftjoin('t_lpa_final as lf','lf.sample_id','=','sample.id')
                        // ->leftjoin('t_dst_drugs_tr as drugs','sample.enroll_id','=','drugs.enroll_id')
                        // ->leftjoin('t_lc_dst as lcdrug','sample.id','=','lcdrug.sample_id')
                        // ->where('sample.id','!=','')
                        // ->whereOr('ld.id','!=','')
                        // ->whereOr('lfmf.id','!=','')
                        ->get();

      foreach($data['result'] as $key=>$value){
        $value->drug_name_lj = [];
        $value->ljdrugresult = [];
        $value->lcdrugresult = [];
        $value->drug_name = [];
        $drugs_result = DB::table('t_lc_dst as lcdrug')
                          ->where('sample_id',$value->sampleId)
                          ->where('status',1)
                          ->pluck('lcdrug.result')
                          ->toArray();
        $drugs_name = DB::table('t_lc_dst as lcdrug')
                          ->where('sample_id',$value->sampleId)
                          ->where('status',1)
                          ->pluck('lcdrug.drug_name')
                          ->toArray();
        $drugs_result_lj = DB::table('t_lj_dst_reading as ljdrug')
                          ->where('sample_id',$value->sampleId)
                          ->where('status',1)
                          ->where('flag',1)
                          ->select('ljdrug.drug_reading')
                          ->first();
        if($drugs_result){
          $value->lcdrugresult = $drugs_result;
          $value->drug_name = $drugs_name;
        }
        if($drugs_result_lj)
        {
          $arrname=[];
          $arrdrug=[];

          $result_data=(array) json_decode($drugs_result_lj->drug_reading);
          foreach($result_data['dil_4'] as $key1=>$value1){
            array_push($arrname,$value1->name);
            array_push($arrdrug,$value1->value);
          }
          $value->drug_name_lj = $arrname;
          $value->ljdrugresult = $arrdrug;

        }
      }
      //dd($data['result']);
      return view('admin.sample.annexurel',compact('data'));
    }
}
