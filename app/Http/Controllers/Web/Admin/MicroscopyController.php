<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\Microscopy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Microbio;
use App\Model\ResultEdit;

class MicroscopyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $data = [];
        $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id', 'e.label', 'm.receive_date as receive',
        'm.test_reason as reason','m.sample_quality','is_accepted','s.result','t_service_log.sample_label',
        't_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.fu_month','m.service_id as serviceID')
        ->leftjoin('enrolls as e','e.id','=','t_service_log.enroll_id')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->whereIn('t_service_log.service_id',[1,2])
        ->whereIn('t_service_log.status',[0,1,2])
        ->orderBy('enroll_id','desc')
        ->get();
        $data['summaryTotal'] = ServiceLog::whereIn('status',[0,1,2])
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summaryDone'] = ServiceLog::where('status',1)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summarySent'] = ServiceLog::where('status',2)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['services'] = Service::select('id','name')->where('record_status',1)->get();
        return view('admin.microscopy.list',compact('data'));
      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->service_id);

        if($request->service_id=='Sent for microbiologist review'){
            $microbio = Microbio::create([
            'enroll_id' => $request->enrollId,
            'sample_id' => $sample_id,
            'service_id' => $request->serviceID,
            'next_step' => '',
            'detail' => '',
            'remark' => '',
            'status' => 0,
            'created_by' => Auth::user()->id,
             'updated_by' => Auth::user()->id
          ]);
        }


        if($request->editresult){
          $sample = Sample::select('id','enroll_id')->where('sample_label',$request->sample_id)->first();
          $microscopyObj = Microscopy::select('id','enroll_id','result')->where('sample_id',$sample->id)->first();
          if($microscopyObj){

            $edit = ResultEdit::create([
              'enroll_id' => $sample->enroll_id,
              'sample_id' => $sample->id,
              'service_id' => $request->service,
              'previous_result' => $microscopyObj->result,
              'updated_result' => $request->result,
              'updated_by' => Auth::user()->id,
              'status' => 1,
              'reason' => $request->reason_edit,
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => ''
            ]);

            $data = Microscopy::updateOrCreate(['sample_id' => $sample->id],[
              'sample_id' => $sample->id,
              'enroll_id' => $microscopyObj->enroll_id,
              'result' => $request->result,
              'reason_edit_result' => $request->reason_edit,
              'created_by' => $request->user()->id,
              'updated_by' => $request->user()->id,
              'status' => 1,
            ]);

            $microscopy = $microscopyObj->id;
            $microscopy = Microscopy::find($microscopyObj->id);
            $microscopy->reason_edit = $request->reason_edit;
            $microscopy->is_moved = 0;
            $microscopy->save();
          }
          return redirect('/microbiologist');
        }

      $logdata = ServiceLog::find($request->service_log_id);
        if($request->type=='review'){
          $sample = Sample::select('id')->where('sample_label',$request->sample_id)->first();
          $microscopyObj = Microscopy::select('id','enroll_id','result')->where('sample_id',$sample->id)->first();
          if($microscopyObj){
            $edit = ResultEdit::create([
            'enroll_id' => $request->enrollId,
            'sample_id' => $sample->id,
            'service_id' => $request->serviceID,
            'previous_result' => $microscopyObj->result,
            'updated_result' => $request->result,
            'updated_by' => Auth::user()->id,
            'status' => 1,
            'reason' => $request->reason_other,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => ''
          ]);
        }
      }
      $data = Microscopy::updateOrCreate(['sample_id' => $logdata->sample_id],[
        'sample_id' => $logdata->sample_id,
        'enroll_id' => $logdata->enroll_id,
        'result' => $request->result,
        'reason_edit_result' => $request->reason_other,
        'created_by' => $request->user()->id,
        'updated_by' => $request->user()->id,
        'status' => 1,
      ]);

      $logdata->status = 2;
      $logdata->save();
      //ServiceLog::where('enroll_id',$enroll_id->enroll_id)
      if($request->type=='review'){
        return redirect('/review_microscopy');
      }else{
        return redirect('/microscopy');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function microscopyPrint()
    {
        $data = [];
        $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id', 'e.label', 'm.receive_date as receive','m.test_reason as reason','m.sample_quality','is_accepted','s.result','t_service_log.sample_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status')
        ->leftjoin('enrolls as e','e.id','=','t_service_log.enroll_id')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->whereIn('t_service_log.service_id',[1,2])
        ->whereIn('t_service_log.status',[1,2])
        ->orderBy('enroll_id','desc')
        ->get();
        $data['summaryTotal'] = ServiceLog::whereIn('status',[1,2])
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summaryDone'] = ServiceLog::where('status',1)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['summarySent'] = ServiceLog::where('status',2)
                    ->whereIn('service_id',[1,2])
                    ->count();
        $data['services'] = Service::select('id','name')->where('record_status',1)->get();
        return view('admin.microscopy.print',compact('data'));
    }
}
