<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Model\ServiceLog;
use App\Model\Sample;
use App\Model\Cbnaat;
use App\Model\Microbio;
use App\Model\Service;
use App\Model\Enroll;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\LJDetail;
use App\Model\LCDST;
use App\Model\LCDSTDrugs;
use App\Model\DSTDrugTR;
use App\Model\LCDSTInoculation;
use App\Model\CultureInoculation;
use App\Model\FirstLineLpa;
use App\Model\SecondLineLpa;
use App\Model\FinalInterpretation;
use App\Model\LCFlaggedMGITFurther;
use App\Model\LjDstReading;
use App\Model\RequestServices;
use PDF;



class MicroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
            $data['services'] = "fddg,gdg,gdg";

            //dd($data['services']);
            $data['summary']['todo']=DB::select('select m.id as ID,  s.name,  ifnull(count(m.id),0) as cnt from t_microbiologist m join m_services as s on m.service_id = s.id where m.status = 0 and s.id in (1,2,3,4,8,14,15,16,17,20,21) group by m.service_id');
            $data['summary']['done']=DB::select('select m.id as ID, s.name, ifnull(count(m.id),0) as cnt from t_microbiologist m join m_services as s on m.service_id = s.id where m.status = 1 and s.id in (1,2,3,4,8,14,15,16,17,20,21) group by m.service_id');

            $ret = [];
            foreach($data['summary']['todo'] as $sample){
                $data1 = $sample;
                $data2 = [];
                $data3 = [];

                foreach($data['summary']['done'] as $test){
                    if($sample->name == $test->name){
                        $data2 = $test;
                        break;
                    }
                }



                $ret[] = [
                    'todo'=>$data1,
                    'done'=>$data2,

                ];
            }
            $data['ret']=$ret;

           // dd($data['ret']);


            //dd($data['summary']);
             $data['sample'] = ServiceLog::select('s.id as ID','s.tag as tag','f.DMC_PHI_Name as facility_id',
             'sample.id as sampleID','s.enroll_id as enroll','enrolls.label as enroll_l',
             'sample.sample_label as samples','s.sample_id as sample','sample.name as p_name',
             's.created_at as date','sample.sample_type','s.service_id as service','s.next_step as next','s.detail as m_detail',
             's.remark as m_remark','m.name as service_name','m.url','s.bwm as bwm_status',
             'ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date','ddt.id as lc_dst_tr_id','ddt.drug_ids as drug_ids',
             'ldi.mgit_seq_id', 'ldi.dst_c_id1', 'ldi.dst_c_id2','ldi.dst_c_id3',
              DB::raw('date_format(ldi.inoculation_date,"%d-%m-%y")')
             )
                        ->leftjoin('t_microbiologist as s','s.sample_id','=','t_service_log.sample_id')
                        ->leftjoin('sample as sample','s.sample_id','=','sample.id')
                        ->leftjoin('enrolls as enrolls','enrolls.id','=','sample.enroll_id')
                        ->leftjoin('t_cbnaat as cbnaat','s.sample_id','=','cbnaat.sample_id')
                        ->leftjoin('t_decontamination as d','s.sample_id','=','d.sample_id')
                        ->leftjoin('req_test as rq','rq.enroll_id','=','s.enroll_id')
                        ->leftjoin('m_dmcs_phi_relation as f','rq.facility_id','=','f.id')
                        ->leftjoin('m_services as m','m.id','=','s.service_id')
                        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','s.sample_id')
                        // ->leftjoin('t_dst_drugs_tr as ddt','ddt.enroll_id','=','t_service_log.enroll_id')
                        ->leftjoin('t_lc_dst_inoculation as ldi','ldi.sample_id','=','s.sample_id')
                        //->where('ddt.status',1)
                        ->leftjoin('t_dst_drugs_tr as ddt', function ($join) {
                             $join->on('ddt.enroll_id','=','s.enroll_id')
                                  ->where('ddt.status', 1);
                         })
                        ->groupby('sample.id')
                        ->where('s.status',0)
                        ->get();

              $data['drugs'] = [];
              foreach ($data['sample'] as $key => $value) {
              if($value->drug_ids != ''){
                $drugids = explode(',',$value->drug_ids);
                $druglist = LCDSTDrugs::whereIn('id',$drugids)->pluck('name')->toArray();
                $value->druglist = implode(',',$druglist);
                $data['drugs'] = LCDSTDrugs::whereIn('id',$drugids)->get();
                }
              }

              $data['dp_result'] = ["Sensitive (S)","Resistance (R)", "Not Done (-)", "Contaminated ( C)", "Error ( E)"];
              $data['dp_result_value'] = ["Sensitive","Resistance", "Not Done", "Contaminated", "Error"];

              foreach ($data['sample'] as $key => $value) {
                $value->no_sample = ServiceLog::where('enroll_id',$value->enroll)->where('service_id',11)->count();
              }

            $data['done'] = DB::table('t_microbiologist as s')->select('s.id as ID','f.DMC_PHI_Name as facility_id','s.enroll_id as enroll',
            'enrolls.label as enroll_l','sample.sample_label as samples','sample.id as sample',
            'sample.name as p_name','s.created_at as date','sample.sample_type','s.service_id as service','s.next_step as next',
            's.detail','s.remark','m.name as service_name','m.url','s.sample_label as sampleLabel')
                        ->join('sample as sample','s.sample_id','=','sample.id')
                        ->join('enrolls as enrolls','enrolls.id','=','sample.enroll_id')
                        //->leftjoin('t_microbiologist as s','sample.id','=','s.sample_id')
                        ->join('m_services as m','m.id','=','s.service_id')
                        ->leftjoin('req_test as rq','rq.enroll_id','=','s.enroll_id')
                        ->leftjoin('m_dmcs_phi_relation as f','rq.facility_id','=','f.id')
                        ->where('s.status',1)
                      //  ->orderBy('t_service_log.enroll_id','desc')
                        ->distinct()
                        ->get();

                    //  dd($data['done']);

            $services = DB::table('m_test_request')->select('id','name')->whereIn('name',['LCDST','LJ DST','LJ DST 2st line'])->where('status',1)->get();
            $services = json_decode($services, true);
            $data['services'] = $services;
            $dstdrugs = LCDSTDrugs::select('id','name')->where('status',1)->get();
            $data['dstdrugs'] = $dstdrugs;

            return view('admin.microbiologist.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          // dd($request->service1);

          $en_label = Enroll::select('label as l')->where('id',$request->enrollId1)->first();
          $s_id = Sample::select('id as l')->where('sample_label',$request->sampleid1)->first();
          $enroll_label=$en_label->l;
          $sample_id=$s_id->l;
          $data['report_type'] = Microbio::select('report_type')->where('sample_id',$sample_id)->where('status',1)->first();
          if($request->print15A==true)
          {
            $id = $sample_id;
            $pdf=null;
            $data['today'] = date('d-m-Y');
            $data['user'] = Auth::user()->name;
            $data['personal'] = Sample::select('sample.*','sample.id as smp_id','p.name as userName','p.*','e.*','d.name as district_name',
            's.name as state_name','r.rntcp_reg_no', 'r.regimen','r.reason as test_reason','r.requestor_name','r.designation',
            'r.contact_no as requestor_cno','r.email_id as requestor_email','ps.name as ps_name','r.duration','r.type_of_prsmptv_drtb',
            'r.prsmptv_xdrtv','r.presumptive_h','r.pmdt_tb_no','rs.diagnosis','p.population_other',
            'r.month_week','r.treatment','rs.regimen_fu','rs.fudrtb_regimen_other','rs.facility_type_other','r.req_test_type',
            'phi.name as phi_name','mtb.name as mtb','ft.name as f_name')
                                ->leftjoin('req_test as r','r.enroll_id','=','sample.enroll_id')
                                ->leftjoin('m_predominan_symptom as ps','ps.symptom_id','=','r.predmnnt_symptoms')
                                ->leftjoin('enrolls as e','e.id','=','sample.enroll_id')
                                ->leftjoin('t_request_services as rs','rs.enroll_id','=','sample.enroll_id')
                                ->leftjoin('patient as p','p.id','=','e.patient_id')
                                ->leftjoin('m_phi as phi','phi.id','=','p.phi')
                                ->leftjoin('m_tb as mtb','mtb.id','=','p.tb')
                                ->leftjoin('district as d','d.id','=','p.district')
                                ->leftjoin('state as s','s.STOCode','=','p.state')
                                ->leftjoin('facility_master as ft','ft.id','=','r.facility_type')
                                ->where('sample.id', $id)
                                ->first();
            $data['microscopy_data'] = ServiceLog::select('m.*','t_service_log.service_id','t_service_log.sample_label')
                                  ->leftjoin('t_microscopy as m','m.sample_id','=','t_service_log.sample_id')
                                  ->where('t_service_log.enroll_id',$data['personal']->enroll_id)
                                  ->whereIn('t_service_log.service_id', [1,2])
                                  //->where('t_service_log.status',0)
                                  ->first();
            $data['microscopy']=0;
            $data['microscopyA'] = '';
            $data['microscopyB'] = '';
            if($data['microscopy_data']){
              $data['microscopy'] = $data['microscopy_data']->service_id;
              $sample_type = substr($data['microscopy_data']->sample_label, -1);
              if($sample_type=='A'){
                $data['microscopyA'] = $data['microscopy_data'];
              }
              if($sample_type=='B'){
                $data['microscopyB'] = $data['microscopy_data'];
              }
            }
            $data['culturelj'] = LJDetail::leftjoin('sample as s','s.id','=','t_lj_detail.sample_id')
                                ->where('t_lj_detail.enroll_id',$data['personal']->enroll_id)
                                ->where('t_lj_detail.status', 1)
                                ->first();
            $data['culturelc'] = LCFlaggedMGITFurther::leftjoin('sample as s','s.id','=','t_lc_flagged_mgit_further.sample_id')
                                ->where('t_lc_flagged_mgit_further.enroll_id',$data['personal']->enroll_id)
                                ->where('t_lc_flagged_mgit_further.status', 1)
                                ->first();
            $data['culture'] = "";
            if($data['culturelj']){
              $data['culture'] = 1;
            }
            if($data['culturelc']){
              $data['culture'] = 2;
            }

            $data['cbnaat'] = Cbnaat::leftjoin('sample as s','s.id','=','t_cbnaat.sample_id')
                              ->where('t_cbnaat.enroll_id', $data['personal']->enroll_id)
                              //->where('t_cbnaat.status', 1)
                              ->first();
            $data['lpa1'] = FirstLineLpa::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
            $data['lpa2'] = SecondLineLpa::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
            $data['lpaf'] = FinalInterpretation::where('enroll_id', $data['personal']->enroll_id)->where('status', 1)->first();
            $data['test_requested'] = RequestServices::where('enroll_id', $data['personal']->enroll_id)->pluck('service_id')->toArray();
            //dd($data['test_requested'] );
            $data['microbio'] = Microbio::where('enroll_id',$data['personal']->enroll_id)->get()->toArray();
            //dd($data['microbio']);
            $data['lj_dst_fld'] = LjDstReading::select('drug_reading')->where('enroll_id',$data['personal']->enroll_id)->orderBy('id','DESC')->first();

            $data['lc_dst'] = LCDST::select('drug_name as name','result as value')->where('enroll_id',$data['personal']->enroll_id)->orderBy('id','DESC')->get();

            //$dil = json_decode($data['lj_dst_fld']);
            $data['s'] = "";
            $data['h1'] = "";
            $data['h2'] = "";
            $data['r'] = "";
            $data['e'] = "";
            $data['z'] = "";
            $data['km'] = "";
            $data['cm'] = "";
            $data['am'] = "";
            $data['lfx'] = "";
            $data['mfx1'] = "";
            $data['mfx2'] = "";
            $data['pas'] = "";
            $data['lzd'] = "";
            $data['cfz'] = "";
            $data['eto'] = "";
            $data['cla'] = "";
            $data['azi'] = "";
            foreach ($data['lc_dst'] as $key => $value) {
              if($value->name=="S"){
                $data['s'] = $value->value;
              }
              if($value->name=="H1"){
                $data['h1'] = $value->value;
              }
              if($value->name=="H2"){
                $data['h2'] = $value->value;
              }
              if($value->name=="R"){
                $data['r'] = $value->value;
              }
              if($value->name=="E"){
                $data['e'] = $value->value;
              }
              if($value->name=="Z"){
                $data['z'] = $value->value;
              }
              if($value->name=="K_M"){
                $data['km'] = $value->value;
              }
              if($value->name=="C_M"){
                $data['cm'] = $value->value;
              }
              if($value->name=="A_M"){
                $data['am'] = $value->value;
              }
              if($value->name=="Lf_x"){
                $data['lfx'] = $value->value;
              }
              if($value->name=="Mfx(0.5)"){
                $data['mfx1'] = $value->value;
              }
              if($value->name=="Mfx_(2)"){
                $data['mfx2'] = $value->value;
              }
              if($value->name=="PAS"){
                $data['pas'] = $value->value;
              }
              if($value->name=="Lzd"){
                $data['lzd'] = $value->value;
              }
              if($value->name=="Cfz"){
                $data['cfz'] = $value->value;
              }
              if($value->name=="Eto"){
                $data['eto'] = $value->value;
              }
              if($value->name=="Cla"){
                $data['cla'] = $value->value;
              }
              if($value->name=="Azi"){
                $data['azi'] = $value->value;

              }
            }
            if($data['lj_dst_fld']){
              $dil = json_decode($data['lj_dst_fld']->drug_reading);
              foreach ($dil->dil_2 as $key => $value) {
                if($value->name=="S"){
                  $data['s'] = $value->value;
                }
                if($value->name=="H1"){
                  $data['h1'] = $value->value;
                }
                if($value->name=="H2"){
                  $data['h2'] = $value->value;
                }
                if($value->name=="R"){
                  $data['r'] = $value->value;
                }
                if($value->name=="E"){
                  $data['e'] = $value->value;
                }
                if($value->name=="Z"){
                  $data['z'] = $value->value;
                }
                if($value->name=="K M"){
                  $data['km'] = $value->value;
                }
                if($value->name=="C M"){
                  $data['cm'] = $value->value;
                }
                if($value->name=="A M"){
                  $data['am'] = $value->value;
                }
                if($value->name=="Lf x"){
                  $data['lfx'] = $value->value;
                }
                if($value->name=="Mfx(0.5)"){
                  $data['mfx1'] = $value->value;
                }
                if($value->name=="Mfx (2)"){
                  $data['mfx2'] = $value->value;
                }
                if($value->name=="PAS"){
                  $data['pas'] = $value->value;
                }
                if($value->name=="Lzd"){
                  $data['lzd'] = $value->value;
                }
                if($value->name=="Cfz"){
                  $data['cfz'] = $value->value;
                }
                if($value->name=="Eto"){
                  $data['eto'] = $value->value;
                }
                if($value->name=="Cla"){
                  $data['cla'] = $value->value;
                }
                if($value->name=="Azi"){
                  $data['azi'] = $value->value;

                }
              }
            }


            if($pdf=="pdf"){
              $pdfname = $data['personal']?$data['personal']->userName:"pdfview";
              $elabel = $data['personal']?$data['personal']->label:"";
              $pdffilename = $elabel."".substr($pdfname, 0, 5).".pdf";
                $pdf = PDF::loadView('admin.sample.pdfview',compact('data'));
                return $pdf->download($pdffilename);
            }

            //$data_decoded = json_decode($data);
          // /  dd($data);

            $lab_id = DB::table('m_configuration')->select('lab_code as ID')->where('status',1)->first();
          //  dd($lab_id);
            if($lab_id){
              DB::table('t_15AFormData')->insert(
                ['sample_id' => $id,
                'enroll_id' => $request->enrollId1,
                'lab_id' => $lab_id->ID,
                'ismoved'=> 0,
                '15Aresult'=> json_encode($data),
                'patient_name' => $data['personal']->userName,
                'created_at' => date('Y-m-d')
                ]
              );
            }

            // dd(json_encode($data));
            $print15A=1;
            $report_type = 'End of Report';
          }
          else {
            $print15A=0;
            $report_type = 'Interim Report';
          }

          if($request->type=='drug'){
            if($request->drugs){
              $drug_str = implode(',',$request->drugs);
              DSTDrugTR::create([
                'enroll_id' => $request->enrollId1,
                'sample_id' => '0',
                'drug_ids' => $drug_str,
                'status' => 1,
                'flag' => 1,
                'created_by'=>$request->user()->id,
                'updated_by'=>$request->user()->id,
              ]);
            }
            return redirect('/microbiologist');
          }



          if($request->type==1){
              return redirect('/microbiologist');
           }

            //dd($request->all());
            //$next_step = Service::select('id as id')->where('name',$request->nextStep)->first();
            Microbio::where('enroll_id', $request->enrollId1)->where('sample_id', $sample_id)->delete();
            $microbio = Microbio::create([
                'enroll_id' => $request->enrollId1,
                'sample_id' => $sample_id,
                'service_id' => $request->service1,
                'next_step' => $request->nextStep,
                'detail' => $request->detail,
                'remark' => $request->remark,
                'reason_bwm' => $request->reason_bwm,
                'reason_other' => $request->reason_other,
                'status' => 1,
                'print_15a' => $print15A,
                'report_type' => $report_type,
                'created_by' => Auth::user()->id,
                 'updated_by' => Auth::user()->id
              ]);


              if($request->nextStep == 'Request for Retest'){

                  if($request->service1 == 15){
                    $nextService=8;
                  }if($request->service1 == 21){
                    $nextService=16;
                  }
                  else{
                    $nextService=$request->service1;
                  }

                  $log_serviceLog = ServiceLog::where('enroll_id',$request->enrollId1)->
                                    where('sample_id',$sample_id)->
                                    where('service_id',$nextService)->first();

                  $log = ServiceLog::where('enroll_id',$request->enrollId1)->where('service_id',11)->first();
                  if($log && $request->service1==26 && $request->service1!=21){
                    $log->service_id = $nextService;
                    $log->status = 1;
                    $log->updated_by = $request->user()->id;
                    $data = $log;
                    $log->save();

                  }
                  // elseif(!$log && $request->service1!=26 && $request->service1!=21){
                  //     ServiceLog::create([
                  //                 'enroll_id' => $request->enrollId1,
                  //                 'sample_id' => $sample_id,
                  //                 'enroll_label' => $enroll_label,
                  //                 'sample_label' => $request->sampleid1,
                  //                 'service_id' => $nextService,
                  //                 'status' => 1,
                  //                 'tag' => '',
                  //                 'test_date' => date('Y-m-d H:i:s'),
                  //                 'created_by' => Auth::user()->id,
                  //                 'updated_by' => Auth::user()->id
                  //         ]);
                  //       }
                  elseif(!$log && $request->service1!=26){

                    $old_sample = Sample::select('sample_label')->where('id',$sample_id)->first();
                    $new_sample = $old_sample->sample_label.'R';
                    Sample::where('id',$sample_id)->update(['sample_label'=>$new_sample]);
                    //ServiceLog::where('sample_id',$sample_id)->update(['sample_label'=>$new_sample]);

                    // $sample = Sample::find($sample_id);
                    // $sample->sample_label = $request->sampleid1.'R';
                    // $sample->save();
                    ServiceLog::create([
                                        'enroll_id' => $request->enrollId1,
                                        'sample_id' => $sample_id,
                                        'enroll_label' => $enroll_label,
                                        'sample_label' => $request->sampleid1.'R',
                                        'service_id' => $nextService,
                                        'status' => 1,
                                        'tag' => '',
                                        'test_date' => date('Y-m-d H:i:s'),
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                          ]);
                    // $log_serviceLog->status=1;
                    // $log_serviceLog->sample_label=$request->sampleid1.'R';
                    // $log_serviceLog->save();
                  }
              }else if($request->nextStep == 'Print 15A form'){

                            //Redirect::away('pdfview/1');
                            //return redirect::away("pdfview/$sample_id");

              }else{
                 ServiceLog::create([
                                  'enroll_id' => $request->enrollId1,
                                  'sample_id' => $sample_id,
                                  'enroll_label' => $enroll_label,
                                  'sample_label' => $request->sampleid1,
                                  'service_id' => $request->nextStep,
                                  'status' => 1,
                                  'tag' => '',
                                  'test_date' => date('Y-m-d H:i:s'),
                                  'created_by' => Auth::user()->id,
                                  'updated_by' => Auth::user()->id
                          ]);

              }


        return redirect('/microbiologist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function result($serviceId, $sampleId, $enrollmentId)
    {
        // try{

                $service = Service::select('table as table_name')->where('id',$serviceId)->first();
                //$result = ServiceLog::where('sample_id',$sample)->where('enroll_id',$enroll)->get();
                //print_r($service->table_name);
                switch($service->table_name){
                   case 't_lpa_final': $data = $this->lpaFinal($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_culture_inoculation': $data = $this->cultureInno($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_microscopy': $data = $this->microscopy($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_decontamination': $data = $this->decontamination($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_dnaextraction': $data = $this->dna($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_lc_flagged_mgit': $data = $this->flagged_mgit($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_lj_detail': $data = $this->lj($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_cbnaat': $data = $this->cbnaat($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_cbnaat': $data = $this->cbnaat($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   // case 't_cbnaat': $data = $this->cbnaat($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_lc_dst_inoculation': $data = $this->lcdst_inno($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_lj_dst_reading': $data = $this->dstreading($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                   case 't_hybridization': $data = $this->hybridization($service->table_name, $serviceId, $sampleId, $enrollmentId); break;
                }

                return ($data);

            // }catch(\Exception $e){
            // $error = $e->getMessage();
            // return view('admin.layout.error',$error);   // insert query
            // }

    }
    public function hybridization($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->where('status',1)
                ->select('result')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Result : $value->result </p>";

        }

        return $html;
    }
    public function dstreading($table_name, $serviceId, $sampleId, $enrollmentId){

        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->select('drug_reading')
                ->where('status',1)
                ->get();


        $html = '';
        $i=0;
        foreach ($result as $key => $value) {
            $data=json_decode($value->drug_reading, true);
            //dd($data['dil_4']);
            //$len=count($data);
            foreach($data['dil_4'] as $per_data){
              //dd($per_data);
              //$html .= "<p> $per_data </p>";
              //foreach($per_data as $per_sub_data){
                //dd($per_sub_data['name']);

                  $html .= "<p>$per_data[name] : $per_data[value] </p>";
              //}
            }
        }

        return $html;
    }

    public function lpaFinal($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->select('rif', 'inh','quinolone', 'slid','mtb_result')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>MTB Result : $value->mtb_result </p>";
            $html .= "<p>RIF : $value->rif </p>";
            $html .= "<p>INH : $value->inh </p>";
            $html .= "<p>quinolone : $value->quinolone </p>";
            $html .= "<p>SLID : $value->slid </p>";
        }

        return $html;
    }

    public function microscopy($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->select('result')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Result : $value->result </p>";

        }

        return $html;
    }

    public function cultureInno($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->select('inoculation_date','status')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Innoculation Date : $value->inoculation_date </p>";
            if($value->status==9)
            {
                $html .= "<p>Please send another sample</p>";
            }

        }

        return $html;
    }

    public function decontamination($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                //->where('status',1)
                ->select('test_date','status')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Tested On : $value->test_date </p>";
            if($value->status==9)
            {
                $html .= "<p>Please send another sample</p>";
            }

        }

        return $html;
    }

    public function dna($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table($table_name)
                ->where('sample_id',$sampleId)
                ->where('enroll_id',$enrollmentId)
                ->select('created_at')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Tested On : $value->created_at </p>";

        }

        return $html;
    }

    public function flagged_mgit($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table('t_lc_flagged_mgit as b')
                ->join('t_lc_flagged_mgit_further as a','a.sample_id','=','b.sample_id')
                ->where('b.sample_id',$sampleId)
                ->where('b.enroll_id',$enrollmentId)
                ->select('b.flagging_date','b.gu','a.result','a.result_date')
                ->distinct()
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Flagged On : $value->flagging_date </p>";
            $html .= "<p>GU : $value->gu </p>";
            $html .= "<p>Result : $value->result </p>";
            $html .= "<p>Further Tested on : $value->result_date </p>";
        }

        return $html;
    }

    public function lj($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table('t_lj_detail as b')
                ->where('b.sample_id',$sampleId)
                ->where('b.enroll_id',$enrollmentId)
                ->select('b.final_result')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>Final Result : $value->final_result </p>";

        }

        return $html;
    }

    public function cbnaat($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table('t_cbnaat as b')
                ->where('b.sample_id',$sampleId)
                ->where('b.enroll_id',$enrollmentId)
                ->select('b.result_MTB','b.result_RIF','b.status','b.error')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>MTB Result : $value->result_MTB </p>";
            if($value->error){
              $html .= "<p>Error: $value->error </p>";
            }
            if($value->result_RIF){
              $html .= "<p>RIF Result (if any) : $value->result_RIF </p>";
            }
            if($value->status==10 || $value->status==9)
            {
                $html .= "<p>Please send another sample</p>";
            }

        }

        return $html;
    }

    public function lcdst_inno($table_name, $serviceId, $sampleId, $enrollmentId){
        $result = DB::table('t_lc_dst_inoculation as b')
                ->where('b.sample_id',$sampleId)
                ->where('b.enroll_id',$enrollmentId)
                ->select('b.mgit_seq_id','b.dst_c_id1','b.dst_c_id2')
                ->get();
        $result2 = DB::table('t_lc_dst as b')
                ->where('b.sample_id',$sampleId)
                ->where('b.status',1)
                ->select('b.drug_name','b.result')
                ->get();
        $html = '';
        foreach ($result as $key => $value) {
            //print_r($value);
            $html .= "<p>MGIT Sequence ID : $value->mgit_seq_id </p>";
            $html .= "<p>DST ID1 : $value->dst_c_id1 </p>";
            $html .= "<p>DST ID2 : $value->dst_c_id2 </p>";

        }
        foreach ($result2 as $key => $value) {
            $html .= "<p>Drugs : $value->drug_name </p>";
            $html .= "<p>Results : $value->result </p>";

        }

        return $html;
    }

    public function microbiologistprint()
    {
        $data = [];
            $data['services'] = "fddg,gdg,gdg";

            //dd($data['services']);
            $data['summary']['todo']=DB::select('select s.name, ifnull(count(m.id),0) as cnt from t_microbiologist m join m_services as s on m.service_id = s.id where m.status = 0 group by m.service_id');
            $data['summary']['done']=DB::select('select s.name, ifnull(count(m.id),0) as cnt from t_microbiologist m join m_services as s on m.service_id = s.id where m.status = 1 group by m.service_id');


            //dd($data['summary']);
             $data['sample'] = ServiceLog::select('t_service_log.enroll_id as enroll','t_service_log.enroll_label as enroll_l','sample.sample_label as samples','t_service_log.sample_id as sample','sample.name as p_name','s.created_at as date','sample.sample_type','s.service_id as service','s.next_step as next','s.detail','s.remark','m.name as service_name','m.url')
                        ->leftjoin('sample as sample','t_service_log.sample_id','=','sample.id')
                        ->leftjoin('t_microbiologist as s','t_service_log.sample_id','=','s.sample_id')
                        ->leftjoin('m_services as m','m.id','=','s.service_id')
                        ->where('s.status',0)
                        ->orderBy('t_service_log.enroll_id','desc')
                        ->distinct()
                        ->get();

            $data['done'] = ServiceLog::select('t_service_log.enroll_id as enroll','t_service_log.enroll_label as enroll_l','sample.sample_label as samples','t_service_log.sample_id as sample','sample.name as p_name','s.created_at as date','sample.sample_type','s.service_id as service','s.next_step as next','s.detail','s.remark','m.name as service_name','m.url')
                        ->join('sample as sample','t_service_log.sample_id','=','sample.id')
                        ->leftjoin('t_microbiologist as s','t_service_log.sample_id','=','s.sample_id')
                        ->join('m_services as m','m.id','=','s.service_id')
                        ->where('s.status',1)
                        ->orderBy('t_service_log.enroll_id','desc')
                        ->distinct()
                        ->get();


            return view('admin.microbiologist.print',compact('data'));
    }

}
