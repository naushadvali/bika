<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\LJ;
use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\LJWeekLog;
use App\Model\LJDetail;
use App\Model\Microscopy;
use App\Model\LCFlaggedMGIT;
use App\Model\Microbio;
use App\Model\CultureInoculation;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class LJReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $data = [];
          $data['sample'] = ServiceLog::select('m.enroll_id','t_service_log.status as week_status','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples', 'ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date','m.sample_type','ljd.final_result','ljd.lj_result_date','lc.result as lc_result','lc.result_date as lc_result_date','w.week as week','m.fu_month')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
       // ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_microscopy as s', function ($join) {
              $join->on('s.sample_id','=','t_service_log.sample_id')
                   ->where('s.status', 1);
          })
        ->leftjoin('t_lj_week_log as w', function ($join) {
                $join->on('w.sample_id','=','m.id')
                     ->where('w.status', 1);
            })
        ->leftjoin('t_lc_flagged_mgit_further as lc','lc.sample_id','=','t_service_log.sample_id')
        // ->leftjoin('t_lj_week_log as w','w.sample_id','=','m.id')
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lj_detail as ljd','ljd.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',20)
        //->where('w.status',1)
        ->whereIn('t_service_log.status',[0,-1])
        ->orderBy('enroll_id','desc')
        ->distinct('w.sample_id')
        ->get();
      //  dd($data['sample']);
        //dd($data['sample']);
        foreach ($data['sample'] as $key => $value) {
          // $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          // if($lpa->service_id==6){
          //   $value->culture_method = "Solid Culture";
          //   $value->lpa_type = "LPA 1st line";
          // }elseif($lpa->service_id==7){
          //   $value->culture_method = "Liquid Culture";
          //   $value->lpa_type = "LPA 2nd line";
          // }elseif($lpa->service_id==13){
          //   $value->culture_method = "Both";
          //   $value->lpa_type = "LPA Both Line";
          // }else{
          //   $value->culture_method = "NA";
          //   $value->lpa_type = "NA";
          // }

          $culture = ServiceLog::select('tag')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->where('service_id',16)->first();
          $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          if($lpa->service_id==13){
            $value->lpa_type = "Both";
          }elseif($lpa->service_id==7){
            $value->lpa_type = "LPA 2nd line";
          }elseif($lpa->service_id==6){
            $value->lpa_type = "LPA 1st line";
          }
          // else{
          //   $value->lpa_type = "NA";
          // }

          if($culture->tag=='LC & LJ Both'){
            $value->culture_method = "Both";
          }else{
            $value->culture_method = "LJ";
          }
        }
        return view('admin.ljreview.list',compact('data'));
      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->service_id == 1 || $request->service_id == 2 || $request->service_id == 3){
          if($request->service_log_id > 0){
            $service = ServiceLog::find($request->service_log_id);
            $service->status = 0;
            $service->updated_by = $request->user()->id;
            $service->save();
            if($request->service_id == 1){
              $tag = '1st line LPA';
            }elseif($request->service_id == 2){
              $tag = '2st line LPA';
            }else{
              $tag = '1st line LPA  and for 2nd line LPA';
            }
            $new_service = [
              'enroll_id' => $service->enroll_id,
              'sample_id' => $service->sample_id,
              'service_id' => 8,
              'status' => 1,
              'tag' => $tag,
              'created_by' => $request->user()->id,
              'updated_by' => $request->user()->id,
              'enroll_label' => $service->enroll_label,
              'sample_label' => $service->sample_label,
            ];

            $nwService = ServiceLog::create($new_service);
            //return $nwService;
        }
      }elseif($request->service_id == 4 || $request->service_id == 5){
        $service = ServiceLog::find($request->service_log_id);
        $service->status = 0;
        $service->updated_by = $request->user()->id;
        $service->save();
        if($request->service_id == 4){
          $s_id = 22;
        }else{
          $s_id = 23;
        }
        $new_service = [
          'enroll_id' => $service->enroll_id,
          'sample_id' => $service->sample_id,
          'service_id' => $s_id,
          'status' => 1,
          'tag' => '',
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'enroll_label' => $service->enroll_label,
          'sample_label' => $service->sample_label,
        ];

        $nwService = ServiceLog::create($new_service);
        //return $nwService;
      }
      elseif($request->service_id == 6 || $request->service_id == 19){
        $service = ServiceLog::find($request->service_log_id);
        $service->status = 0;
        $service->updated_by = $request->user()->id;
        $service->save();
        $microbio = Microbio::create([
            'enroll_id' => $service->enroll_id,
            'sample_id' => $service->sample_id,
            'service_id' => 20,
            'next_step' => '',
            'detail' => '',
            'remark' => '',
            'status' => 0,
            'created_by' => $request->user()->id,
             'updated_by' => $request->user()->id
          ]);

        //return $microbio;
      }

      return redirect('/reviewlj');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ljreviewprint()
    {

        $data = [];
          $data['sample'] = ServiceLog::select('m.enroll_id','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples', 'ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date','m.sample_type','ljd.final_result','ljd.lj_result_date')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_lj_detail as ljd','ljd.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',20)
        ->where('s.status',1)
        ->where('t_service_log.status',9)
        ->orderBy('enroll_id','desc')
        ->get();
        foreach ($data['sample'] as $key => $value) {
          $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          if($lpa->service_id==6){
            $value->culture_method = "Solid Culture";
            $value->lpa_type = "LPA 1st line";
          }elseif($lpa->service_id==7){
            $value->culture_method = "Liquid Culture";
            $value->lpa_type = "LPA 2nd line";
          }elseif($lpa->service_id==13){
            $value->culture_method = "Both";
            $value->lpa_type = "LPA Both Line";
          }else{
            $value->culture_method = "NA";
            $value->lpa_type = "NA";
          }
        }
        return view('admin.ljreview.print',compact('data'));

    }
}
