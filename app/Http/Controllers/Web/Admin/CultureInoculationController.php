<?php

namespace App\Http\Controllers\Web\Admin;

use App\Model\Sample;
use App\Model\Service;
use App\Model\ServiceLog;
use App\Model\Microscopy;
use App\Model\CultureInoculation;
use App\Model\Microbio;
use App\Model\Barcodes;
use Illuminate\Support\Facades\Auth;
use \Milon\Barcode\DNS1D;
use PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class CultureInoculationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $data = [];
          $year = Barcodes::distinct()->get(['year'])->last();
          $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples','t.status as dna_status','t.created_at as date_of_extraction','ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date', 't_service_log.tag', 't_service_log.tag as lpa_type','m.fu_month')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        ->leftjoin('t_microscopy as s', function ($join) {
              $join->on('s.sample_id','=','t_service_log.sample_id')
                   ->where('s.status', 1);
          })
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',16)
        ->whereIn('t_service_log.status',[0,1,2])
        ->orderBy('enroll_id','desc')
        ->get();
        //dd($data['sample']);
        // foreach ($data['sample'] as $key => $value) {
        //   $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
        //   if($lpa->service_id==6){
        //     $value->lpa_type = "LJ";
        //   }elseif($lpa->service_id==7){
        //     $value->lpa_type = "LC";
        //   }elseif($lpa->service_id==13){
        //     $value->lpa_type = "Both";
        //   }else{
        //     $value->lpa_type = "NA";
        //   }
        // }
        //dd($data['sample']);
        return view('admin.culture_inoculation.list',compact('data','year'));
      }catch(\Exception $e){
          $error = $e->getMessage();
          return view('admin.layout.error',$error);   // insert query
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $logdata = ServiceLog::find($request->log_id);
      CultureInoculation::where('sample_id',$logdata->sample_id)->delete();
      $data = CultureInoculation::create([
        'sample_id' => $logdata->sample_id,
        'enroll_id' => $logdata->enroll_id,
        'mgit_id' => $request->mgit_id,
        'tube_id_lj' => $request->tube_id_lj,
        'tube_id_lc' => $request->tube_id_lc,
        'inoculation_date' => $request->inoculation_date,
        'created_by' => $request->user()->id,
        'updated_by' => $request->user()->id
      ]);
      $logdata->status = 0;
      $logdata->save();
      if($request->service_id == 1 || $request->service_id == 2){
        if($request->service_id == 1){
          $serviceId = 17;
        }else{
          $serviceId = 20;
        }
        $new_service = [
          'enroll_id' => $logdata->enroll_id,
          'sample_id' => $logdata->sample_id,
          'service_id' => $serviceId,
          'status' => 1,
          'created_by' => $request->user()->id,
          'updated_by' => $request->user()->id,
          'enroll_label' => $logdata->enroll_label,
          'sample_label' => $logdata->sample_label,
        ];

        $nwService = ServiceLog::create($new_service);
      }else{
        if($request->service_id == 3){
          $new_service = [
            'enroll_id' => $logdata->enroll_id,
            'sample_id' => $logdata->sample_id,
            'service_id' => 17,
            'status' => 1,
            'created_by' => $request->user()->id,
            'updated_by' => $request->user()->id,
            'enroll_label' => $logdata->enroll_label,
            'sample_label' => $logdata->sample_label,
          ];
          $new_service1 = [
            'enroll_id' => $logdata->enroll_id,
            'sample_id' => $logdata->sample_id,
            'service_id' => 20,
            'status' => 1,
            'created_by' => $request->user()->id,
            'updated_by' => $request->user()->id,
            'enroll_label' => $logdata->enroll_label,
            'sample_label' => $logdata->sample_label,
          ];
          $nwService = ServiceLog::create($new_service);
          $nwService1 = ServiceLog::create($new_service1);
        }
      }

      if($request->service_id=='Send to BWM'){
        $culture = CultureInoculation::select('id')->where('sample_id',$logdata->sample_id)->where('enroll_id',$logdata->enroll_id)->first();
        $culture_obj = CultureInoculation::find($culture->id);
          $microbio = Microbio::create([
            'enroll_id' => $logdata->enroll_id,
            'sample_id' => $logdata->sample_id,
            'service_id' => 26,
            'next_step' => '',
            'detail' => '',
            'remark' => '',
            'bwm' => 1,
            'status' => 0,
            'created_by' => Auth::user()->id,
             'updated_by' => Auth::user()->id
          ]);
          $culture_obj->status = 9;
          $culture_obj->updated_by = $request->user()->id;
          $culture_obj->save();

          ServiceLog::create([
             'enroll_id' => $logdata->enroll_id,
             'sample_id' => $logdata->sample_id,
             'enroll_label' => $logdata->enroll_label,
             'sample_label' => $logdata->sample_label,
             'service_id' => 26,
             'status' => 1,
             'tag' => '',
             'test_date' => date('Y-m-d H:i:s'),
             'created_by' => Auth::user()->id,
             'updated_by' => Auth::user()->id
           ]);
        }


      return redirect('/culture_inoculation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cultureInnoprint()
    {

        $data = [];
          $data['sample'] = ServiceLog::select('t_service_log.updated_at as ID','m.enroll_id','m.id as sample_id', 'm.receive_date as receive','m.test_reason as reason','is_accepted','s.result','t_service_log.sample_label as samples','t_service_log.enroll_label as enroll_label','t_service_log.service_id','t_service_log.id as log_id', 't_service_log.status','m.no_of_samples','t.status as dna_status','t.created_at as date_of_extraction','ci.mgit_id','ci.tube_id_lj','ci.tube_id_lc','ci.inoculation_date', 't_service_log.tag')
        ->leftjoin('sample as m','m.id','=','t_service_log.sample_id')
        ->leftjoin('t_dnaextraction as t', function ($join) {
              $join->on('t.sample_id','=','t_service_log.sample_id')
                   ->where('t.status', 1);
          })
        ->leftjoin('t_microscopy as s','s.sample_id','=','t_service_log.sample_id')
        ->leftjoin('t_culture_inoculation as ci','ci.sample_id','=','t_service_log.sample_id')
        ->where('t_service_log.service_id',16)
        //->where('s.status',1)
        ->whereIn('t_service_log.status',[1,2])
        ->orderBy('enroll_id','desc')
        ->get();
        foreach ($data['sample'] as $key => $value) {
          $lpa = ServiceLog::select('service_id')->where('enroll_id',$value->enroll_id)->where('sample_id',$value->sample_id)->first();
          if($lpa->service_id==6){
            $value->lpa_type = "LJ";
          }elseif($lpa->service_id==7){
            $value->lpa_type = "LC";
          }elseif($lpa->service_id==13){
            $value->lpa_type = "Both";
          }else{
            $value->lpa_type = "NA";
          }
        }
        //dd($data['sample']);
        return view('admin.culture_inoculation.print',compact('data'));

    }

    public function printBarcode(Request $request)
    {

        // dd($request->all());
        echo "<style>
            @media print {
              @page {
                size: 100mm 50mm;
                #size: 210mm 297mm;
              }
            }
            body {
              font-size: 10pt;
            }
            .page-break {
                page-break-after: always;
            }
            </style>";
        $sample_string = $request->print;
        foreach ($sample_string as $key => $value) {
          echo "<div style='width:25%; float:left;'>";
          echo DNS1D::getBarcodeSVG($value,"C93",0.6,20); //C39
          echo "<br/>".$value;
          echo "</div>";
        }

    }
}
