@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Hybridization</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/hybrid/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
              @include('admin/hybridization/nextsteppopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                          <tr>
                                            <th class="hide">ID</th>
                                            <th>Sample ID</th>
                                            <th>Samples submitted</th>
                                            <th>Date of Decontamination</th>
                                            <th>Microscopy result</th>
                                            <th>Date of Extraction</th>
                                            <th>LPA test type</th>
                                            <th>PCR completed</th>
                                            <th>Results/ Next Step (Drop down)</th>
                                          </tr>
                                        </thead>
                                        <tbody>

                                          @foreach ($data['sample'] as $key=> $samples)
                                          <tr>
                                            <td class="hide">{{$samples->ID}}</td>
                                            <td>{{$samples->samples}}</td>
                                            <td>{{$samples->no_of_samples}}</td>
                                            <td>
                                              @if($samples->decontamination_date)
                                              <?php echo date('d/m/Y', strtotime($samples->decontamination_date)); ?>
                                              @endif
                                            </td>
                                            <td>{{$samples->result}}</td>
                                            <td>
                                              @if($samples->date_of_extraction)
                                                <?php echo date('d/m/Y', strtotime($samples->date_of_extraction)); ?>
                                              @endif
                                            </td>
                                            <td>{{$samples->tag}}</td>
                                            <td>
                                              @if($samples->pcr_completed==1)
                                              yes
                                              @else
                                              no
                                              @endif
                                            </td>
                                            <td>
                                              @if($samples->STATUS == 0)
                                              Done
                                              @else
                                              <button onclick="openNextForm('{{$samples->samples}}', {{$samples->log_id}}, {{$samples->enroll_id}},'{{$samples->tag}}','{{$samples->no_sample}}')" type="button" class = "btn btn-default btn-sm  nextbtn">Next</button>
                                              @endif
                                            </td>
                                          </tr>
                                          @endforeach

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>

<script>
  $(function(){


});
function openNextForm(sample_label, log_id, enroll_id, tag, no){
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#enroll_id').val(enroll_id);
  $('#spantag').text(tag);
  $('#no_sample').val(no);
  $('#tag').val(tag);
  $('#nextpopupDiv').modal('toggle');
}
</script>


@endsection
