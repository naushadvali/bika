@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Microscopy Next Step</h3>

                  </div>
                   <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/review_microscopy/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
              @include('admin/microscopy/resultpopup')
              @include('admin/microscopy/nextpopup')
              <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >

                                    <!-- <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%"> -->
                                      <style>
                                      table, th, td {
                                          border: 1px solid black;
                                      }
                                      </style>
                                      <table style="width:100%">
                                        <thead>
                                            <tr>

                                              <th>Test to be performed</th>
                                              <th>Tests in process</th>
                                              <th>Tests for review</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                              <td>{{$data['microscopy_test']}}</td>
                                              <td>{{$data['microscopy_tested']}}</td>
                                              <td>{{$data['microscopy_review']}}</td>
                                         </tr>


                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Lab Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Visual appearance</th>
                                              <th>Microscopy method (ZN OR FM)</th>
                                              <th>Reason for test (DX/FU/EQA)</th>
                                               <th>Follow up month</th>
                                              <th>Date of Receipt</th>
                                              <th>Microscopy result</th>
                                              <th>Next Step </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                          @foreach ($data['sample'] as $key=> $samples)
                                          <tr>
                                            <td class="hide">{{$samples->ID}}</td>
                                            <td>{{$samples->label}}</td>
                                            <td>{{$samples->sample_label}}</td>
                                            <td>{{$samples->sample_quality}}</td>

                                            <td>
                                              {{$samples->service_id == 1? "ZN Microscopy" : "FM Microscopy"}}
                                            </td>
                                            <td>
                                              @if($samples->reason=='Diagnosis')
                                              DX
                                              @elseif($samples->reason=='Follow up')
                                              FU
                                              @else
                                              {{$samples->reason}}
                                              @endif
                                            </td>
                                            <td>{{$samples->fu_month}}</td>
                                            <td>{{$samples->receive}}</td>
                                             <td>
                                               @if($samples->status==0)
                                               {{$samples->result}}
                                               @else
                                               <a href="#" onclick="openResultForm('{{$samples->sample_label}}', {{$samples->log_id}}, '{{$samples->result}}','{{$samples->serviceID}}' )">{{$samples->result}}</a>
                                               @endif
                                             </td>
                                            <td>
                                              @if($samples->status==2)
                                              <button onclick="openNextForm('{{$samples->sample_label}}', {{$samples->log_id}})" type="button" class = "btn btn-default btn-sm  nextbtn">Next</button>
                                              @elseif($samples->status==0)
                                              Done
                                              @else
                                              Storage
                                              @endif

                                            </td>
                                          </tr>
                                          @endforeach

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>





<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Barcodes</h4>
      </div>
      <div class="modal-body" id="printCode">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
$(function(){


  var confirmDelete = true;
  $(".resultbtn").click(function(){
    $('#resultpopupDiv').modal('toggle');
    $('#sample_id').val($(this).val());
  });
  // $(".nextbtn").click(function(){
  //   $('#nextpopupDiv').modal('toggle');
  //   $('#next_sample_id').val($(this).val());
  // });
});

function openResultForm(sample_label, log_id, result, serviceID){
  $('#sample_id').val(sample_label);
  $('#result_log_id').val(log_id);
  $('#type').val('review');
  $('#result').val(result);
  $('#serviceID').val(serviceID);

  if(serviceID=='1')
  {
    $('#allresults option').remove();
    $('#allresults').append($('<option>', {
        text : '--Select--',
        value : ''
    }));
    $('#allresults').append($('<option>', {
        text : 'Negative',
        value : 'Negative'
    }));
    $('#allresults').append($('<option>', {
          text : 'Sc 1',
          value : 'Sc 1'
      }));

    $('#allresults').append($('<option>', {
          text : 'Sc 2',
          value : 'Sc 2'
      }));
    $('#allresults').append($('<option>', {
          text : 'Sc 3',
          value : 'Sc 3'
      }));
    $('#allresults').append($('<option>', {
          text : 'Sc 4',
          value : 'Sc 4'
    }));
    $('#allresults').append($('<option>', {
          text : 'Sc 5',
          value : 'Sc 5'
      }));

    $('#allresults').append($('<option>', {
          text : 'Sc 6',
          value : 'Sc 6'
      }));
    $('#allresults').append($('<option>', {
          text : 'Sc 7',
          value : 'Sc 7'
      }));
     $('#allresults').append($('<option>', {
          text : 'Sc 8',
          value : 'Sc 8'
      }));
      $('#allresults').append($('<option>', {
            text : 'Sc 9',
            value : 'Sc 9'
        }));

      $('#allresults').append($('<option>', {
            text : '1+positive',
            value : '1+positive'
        }));
      $('#allresults').append($('<option>', {
            text : '2+positive',
            value : '2+positive'
        }));
      $('#allresults').append($('<option>', {
            text : '3+positive',
            value : '3+positive'
      }));
      $('#allresults').append($('<option>', {
            text : '4+positive',
            value : '4+positive'
      }));
  }
  else
  {
    $('#allresults option').remove();
    $('#allresults').append($('<option>', {
        text : '--Select--',
        value : ''
    }));
    $('#allresults').append($('<option>', {
        text : 'Negative',
        value : 'Negative'
    }));
    $('#allresults').append($('<option>', {
          text : '1+positive',
          value : '1+positive'
      }));
    $('#allresults').append($('<option>', {
          text : '2+positive',
          value : '2+positive'
      }));
     $('#allresults').append($('<option>', {
          text : '3+positive',
          value : '3+positive'
      }));
      $('#allresults').append($('<option>', {
           text : 'NA',
           value : 'NA'
       }));
  }

  var _sample = $("#result").val();
  if(_sample==''){
    $("#reason_other").addClass("hide");
  }else{
    $("#reason_other").removeClass("hide");
  }
  $('#resultpopupDiv').modal('toggle');
}

function openNextForm(sample_label, log_id){
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#nextpopupDiv').modal('toggle');
}
function openPrintModal(obj){
  //console.log(obj.attr('data-sample'));
  var samples = obj.attr('data-sample');
  $.ajax({
    method: "GET",
    url: "{{url('sample/print/')}}"+'/'+samples,
    data: { samples: samples }
  }).done(function( msg ) {
    $("#printCode").html(msg)
    $('#myModal').modal('toggle');
  });

}
</script>


@endsection
