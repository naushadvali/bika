
@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">PCR</h3>

                  </div>

                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/pcr/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>

              </div>
                              <div class="row">



                </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                                <div class="table-scroll" >

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Date of Decontamination </th>
                                              <th>Microscopy result</th>
                                              <th>Date of Extraction</th>
                                              <th>LPA test type</th>
                                              <th>PCR completed</th>
                                              <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          @if($data['sample'])
                                            @foreach ($data['sample'] as $key=> $samples)

                                                  <tr>
                                                    <td class="hide">{{$samples->ID}}</td>
                                                    <td>{{$samples->enroll_label}}</td>
                                                    <td>{{$samples->samples}}</td>
                                                    <td>
                                                      @if($samples->test_date!=null)
                                                      <?php echo date('d/m/Y', strtotime($samples->test_date)); ?>
                                                      @endif
                                                    </td>
                                                    <td>{{$samples->result}}</td>
                                                     <td><?php echo date('d/m/Y', strtotime($samples->created_extraction)); ?></td>
                                                    <td>{{$samples->tag}}</td>
                                                    <td>
                                                      @if($samples->completed==1)
                                                      yes
                                                      @else
                                                      no
                                                      @endif
                                                    </td>
                                                    <td>
                                                      @if($samples->STATUS==0)
                                                      Done
                                                      @else
                                                    <button type="button" onclick="openCbnaatForm({{$samples->enroll_id}},'{{$samples->samples}}','{{$samples->tag}}')" class="btn btn-info btn-sm resultbtn" >Submit</button>
                                                    @endif
                                                    </td>


                                                </tr>

                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>
<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">PCR Result</h4>
        </div>

         <form class="form-horizontal form-material" action="{{ url('/PCR') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
            <div class="modal-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="enrollId" id="enrollId" value="">
                <input type="hidden" name="tag" id="tag" value="">

                <label class="col-md-12"><h5>Sample ID:</h5></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="sampleid" id="sampleid">

                       </select>
                   </div>
                <br>

                <label class="col-md-12"><h5>PCR Completed:</h5></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="completed" value="" id="completed" required>
                        <option value="">select</option>
                        <option value="1">Yes</option>
                        <option value="0">Repeat PCR</option>
                       </select>
                   </div>
                <br>





            </div>
            <div class="modal-footer">
              <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
              <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
              <button type="submit" class="pull-right btn btn-primary btn-md" id="confirm" >Ok</button>
            </div>

      </form>
      </div>
    </div>
 </div>
<script>
$(function(){







  $(".resultbtn").click(function(){
      $('#sample_id').val($(this).val());
    });

    $('#confirmDelete').on('show.bs.modal', function (e) {

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
  });

});





 function openCbnaatForm(enroll_id, sample_ids, tag){
  //console.log("sample_ids", sample_ids.split(','));
  $("#enrollId").val(enroll_id);
  $("#tag").val(tag);

  var sampleArray = sample_ids.split(',');
  $('#sampleid option').remove();
  $.each(sampleArray, function (i, item) {
      $('#sampleid').append($('<option>', {
          text : item
      }));
  });

  $('#myModal').modal('toggle');
 }

</script>


@endsection
