@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">AFB Culture inoculation</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/cultureInno/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                    <form action="{{ url('/cultureInno/printBarcode') }}" method="post" id="formprint">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                 </div>
              </div>

              @include('admin/culture_inoculation/cipopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                  <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th class="hide">ID</th>
                                          <th>Lab Enrolment ID</th>
                                          <th>Sample ID</th>
                                          <th>Results</th>
                                          <th>Microscopy result</th>
                                          <th>MGIT  sequence ID (LC)</th>
                                          <th>Date of Inoculation</th>
                                          <th>DX/FU/EQA</th>
                                          <th>Follow up month</th>
                                          <th>Culture method (LJ,LC,both)</th>
                                          <th>TUBE 1 sequence ID (LJ)</th>
                                          <th>TUBE 2 sequence ID (LJ)</th>
                                          <th>
                                              <button type="submit" class="pull-right btn-sm btn-info" form="formprint" >Barcode Print</a>
                                          </th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <td class="hide">{{$samples->ID}}</td>
                                          <td>{{$samples->enroll_label}}</td>
                                          <td>{{$samples->samples}}</td>
                                          <td>
                                            @if($samples->status==1)
                                            <button onclick="openForm('{{$samples->samples}}', {{$samples->log_id}}, '{{$samples->lpa_type}}','{{$samples->tag}}')",  value="" type="button" class = "btn btn-default btn-sm resultbtn">Submit</button>
                                            @else
                                            Done
                                            @endif
                                          </td>
                                          <td>{{$samples->result}}</td>
                                          <td>{{$samples->mgit_id}}</td>
                                          <td>
                                            @if($samples->inoculation_date)
                                            {{$samples->inoculation_date}}
                                            @else
                                            pending
                                            @endif
                                          </td>
                                          <td>{{$samples->reason}}</td>
                                          <td>{{$samples->fu_month}}</td>
                                          <td>{{$samples->lpa_type}}</td>
                                          <td>{{$samples->tube_id_lj}}</td>
                                          <td>{{$samples->tube_id_lc}}</td>
                                          <td>
                                            <label class="hide"><span id="caterralert" style="color:red;"></span></label>
                                            <input type="checkbox" value="{{$samples->samples}}" class="single-checkbox" name="print[]" form="formprint" class="btn btn btn-sm">
                                          </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->

            <form action="{{ url('/barcodes/print') }}" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Barcodes</h4>
              </div>
              <div class="modal-body" id="printCode">
                <input type="hidden" value="" name="print_type" id="print_type" />
                <input type="hidden" value="" name="year" id="year" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <p>Year: <span id="yeartpl"></span> </p>
                <p>From: <input type="text" name="seqFrm" id="seqFrm"> </p>
                <p>To: &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="seqTo" id="seqTo"> </p>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-default btn-info" >Submit</button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </form>

          </div>
        </div>
<script>
$(function(){
  var limit = 4;
  $('input.single-checkbox').on('change', function(evt) {
     if($('[name="print[]"]:checked').length > limit) {
         this.checked = false;
         $("#caterralert").text("You can select only 4 samples!");
     }else{
        $("#caterralert").text("");
     }
  });
});
function openForm(sample_label, log_id, lpa_type, tag){
  $('#sample_id').val(sample_label);
  $('#log_id').val(log_id);
  $('#ssentfor').text(tag);
  $('#extractionpopupDiv').modal('toggle');
  $(".datep").datepicker({
      dateFormat: "dd/mm/yyyy"
  }).datepicker("setDate", "0");

  if(lpa_type == 'LJ'){
    $("#mgit_id").attr("disabled", "disabled");
    $("#tube_id_lj"). removeAttr("disabled");
    $("#tube_id_lc").removeAttr("disabled","disabled" );

  }
  if(lpa_type == 'LC'){
    $("#tube_id_lj").attr("disabled", "disabled");
    $("#tube_id_lc"). attr("disabled", "disabled");
    $("#mgit_id"). removeAttr("disabled");

  }
}
function openNextForm(sample_label, log_id, enroll_id){
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#enroll_id').val(enroll_id);
  $('#nextpopupDiv').modal('toggle');
}

function openPrintModal(year,print_type){
  //console.log(obj.attr('data-sample'));
    $("#year").val(year);
    $("#print_type").val(print_type);
    $("#yeartpl").html(year);
    $('#myModal').modal('toggle');

}


</script>


@endsection
