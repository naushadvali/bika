<!-- Modal Dialog -->
<div class="modal fade" id="extractionpopupDiv" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
        <h4 class="modal-title">AFB Culture Inoculation</h4>
      </div>
      <div class="modal-body">
        <p></p>
        <form class="form-horizontal form-material" action="{{ url('/culture_inoculation') }}" method="post" id="extractionpopup">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="log_id" id="log_id" value="">
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">Sample ID</label>
                   <div class="col-md-12">
                      <input type="text" name="sample_id" class="form-control form-control-line sampleId" value="" id="sample_id" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">MGIT  sequence ID (LC)</label>
                   <div class="col-md-12">
                      <input type="text" name="mgit_id" class="form-control form-control-line" value="" id="mgit_id" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">Date of Inoculation</label>
                   <div class="col-md-12">
                      <input type="text" name="inoculation_date" max="<?php echo date("Y-m-d");?>" class="form-control form-control-line datepicker" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">TUBE 1 sequence ID (LJ)</label>
                   <div class="col-md-12">
                      <input type="text" name="tube_id_lj" class="form-control form-control-line" value="" id="tube_id_lj" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">TUBE 2 sequence ID (LJ)</label>
                   <div class="col-md-12">
                      <input type="text" name="tube_id_lc" class="form-control form-control-line" value="" id="tube_id_lc" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
              <div class="col">
                  <label class="col-md-12">Sample Sent for:(<span id="ssentfor"></span>)</label>
                  <div class="col-md-12">
                     <select name="service_id" class="form-control form-control-line test_reason" id="service_id" required>
                       <option value="">--Select--</option>
                       <option value="1">LC</option>
                       <option value="2">LJ</option>
                       <option value="3">LC & LJ Both</option>
                       <option value="Send to BWM">Send to BWM</option>
                     </select>
                 </div>
              </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Cancel</button>
        <button type="submit" class="pull-right btn btn-primary btn-md" id="submit">Ok</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
$(function(){


});


$('#extractionpopupDiv').on('show.bs.modal', function (e) {

     // Pass form reference to modal for submission on yes/ok
     var form = $(e.relatedTarget).closest('form');
     $(this).find('.modal-footer #confirm').data('form', form);
 });

</script>
