@extends('admin.layout.app')
@section('content')
<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Equipments</h3>

                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                       <p  class="pull-right "></p>
                   </div>
                </div>
                   @if($data['equipment']->id>0)
                   <form id="createForm" action="{{ url('/equipment/'.$data['equipment']->id) }}" method="post">
                     <input name="_method" type="hidden" value="patch">
                   @else
                   <form id="createForm" action="{{ url('/equipment') }}" method="post" role="alert">
                 @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-block">

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Equiment by Category <span class="red">*</span></label>
                                                <div class="col-sm-12">

                                                    <select name="name_cat" class="form-control form-control-line" id="name_cat" value="{{ old('name_cat', $data['equipment']->name_cat) }}" required>
                                                         <option value="">--Select--</option>
                                                          @foreach ($data['category'] as $key=> $district)
                                                                  @if($data['equipment']->name_cat==$district->name)
                                                                  <option value="{{$district->name}}"  selected="selected">{{$district->name}}</option>
                                                                  @else
                                                                   <option value="{{$district->name}}">{{$district->name}}</option>
                                                                  @endif

                                                          @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">Equiment by Name <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <!-- <select name="name" class="form-control form-control-line" id="Item Name (Equiment by Name)" required="required" >
                                                        <option value="Alcohol Thermometer"
                                                        @if($data['equipment']->name=="Alcohol Thermometer")
                                                            selected="selected"
                                                        @endif
                                                        >Alcohol Thermometer</option>
                                                        <option value="Mercury Thermometer"
                                                        @if($data['equipment']->name=="Mercury Thermometer")
                                                            selected="selected"
                                                        @endif
                                                        >Mercury Thermometer</option>
                                                        <option value="Thermometer"
                                                        @if($data['equipment']->name=="Thermometer")
                                                            selected="selected"
                                                        @endif
                                                        >Thermometer</option>
                                                        <option value="Digital Thermometer"
                                                        @if($data['equipment']->name=="Digital Thermometer")
                                                            selected="selected"
                                                        @endif
                                                        >Digital Thermometer</option>
                                                        <option value="PCR-Workstation"
                                                        @if($data['equipment']->name=="PCR-Workstation")
                                                            selected="selected"
                                                        @endif
                                                        >PCR-Workstation</option>
                                                        <option value="UV Light"
                                                        @if($data['equipment']->name=="UV Light")
                                                            selected="selected"
                                                        @endif
                                                        >UV Light</option>
                                                        <option value="Magnetic stirrer with heating plate etc."
                                                        @if($data['equipment']->name=="Magnetic stirrer with heating plate etc.")
                                                            selected="selected"
                                                        @endif
                                                        >Magnetic stirrer with heating plate etc.</option>
                                                    </select> -->
                                                    <input type="text"  name="name" value="{{ old('name', $data['equipment']->name) }}" class="form-control form-control-line" required="required">
                                                </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Instrument/Tool <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="tool" class="form-control form-control-line" id="Instrument/Tool" required="required">
                                                        <option value="">--Select--</option>
                                                        <option value="Tool"
                                                        @if($data['equipment']->tool=="Tool")
                                                            selected="selected"
                                                        @endif
                                                        >Tool</option>
                                                        <option value="Instrument"
                                                        @if($data['equipment']->tool=="Instrument")
                                                            selected="selected"
                                                        @endif
                                                        >Instrument</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-sm-12">Contact Email <span class="red">*</span></label>
                                              <div class="col-sm-12">

                                                   <input type="email"  name="contact_email" value="{{ old('contact_email', $data['equipment']->contact_email) }}" class="form-control form-control-line" required="required">
                                            </div>
                                            <!-- <label class="col-sm-12">Status of the Equiment <span class="red">*</span></label>
                                                <div class="col-sm-12">

                                                     <input type="text"  name="status" value="{{ old('status', $data['equipment']->status) }}" class="form-control form-control-line" required="required">
                                                </div> -->
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Name of the Supplier <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="supplier" value="{{ old('supplier', $data['equipment']->supplier) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                             <label class="col-md-12">Date of Decommissioning the equipment</label>
                                            <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                              <input type="text" placeholder="dd-mm-yy" name="date_decommission" value="{{ old('date_decommission', $data['equipment']->date_decommission) }}" class="form-control datepicker" >

                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Equipment Make <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="make" value="{{ old('make', $data['equipment']->make) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Equipment Serial No. <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="serial_no" value="{{ old('serial_no', $data['equipment']->serial_no) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Equipment Model No. <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="model_no" value="{{ old('model_no', $data['equipment']->model_no) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Date of Installation <span class="red">*</span></label>
                                            <div id="5" class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                  <input type="text" placeholder="dd-mm-yy" name="date_installation" value="{{ old('date_installation', $data['equipment']->date_installation) }}" class="form-control datepicker" required="required">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Location of the Equiment <span class="red">*</span></label>
                                                <div class="col-sm-12">

                                                    <input type="text"  name="location" value="{{ old('location', $data['equipment']->location) }}" class="form-control form-control-line" required="required">
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">Name of the Provider <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="provider" class="form-control form-control-line" id="NameProvider" required="required">
                                                        <option value="">--Select--</option>
                                                        <option value="Central"
                                                        @if($data['equipment']->provider=="Central")
                                                            selected="selected"
                                                        @endif
                                                        >Central</option>
                                                        <option value="State"
                                                        @if($data['equipment']->provider=="State")
                                                            selected="selected"
                                                        @endif
                                                        >State</option>
                                                        <option value="Donor"
                                                        @if($data['equipment']->provider=="Donor")
                                                            selected="selected"
                                                        @endif
                                                        >Donor</option>
                                                        <option value="Hospital"
                                                        @if($data['equipment']->provider=="Hospital")
                                                            selected="selected"
                                                        @endif
                                                        >Hospital</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col hide" id="provider_name">
                                            <label class="col-sm-12">Name</label>
                                                <div class="col-sm-12">

                                                    <input type="text"  name="nameProvider" value="{{ old('nameProvider', $data['equipment']->nameProvider) }}" maxlength="13" class="form-control form-control-line">
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Warranty Status (till applicable date) <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text"  placeholder="dd-mm-yy" name="waranty_status" value="{{ old('waranty_status', $data['equipment']->waranty_status) }}" class="form-control datepicker_due" required="required">
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Date of Last Maintenance<span class="red">*</span></label>
                                            <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                              <input type="text" placeholder="dd-mm-yy" name="date_last_maintain" value="{{ old('date_last_maintain', $data['equipment']->date_last_maintain) }}" class="form-control datepicker" required="required">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Date of Last Calibration <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text"  placeholder="dd-mm-yy" name="date_last_caliberation" value="{{ old('date_last_caliberation', $data['equipment']->date_last_caliberation) }}" class="form-control datepicker" required="required">
                                                </div>
                                        </div>
                                        <div class="col">

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Maintenance Contract (till applicable date) <span class="red">*</span></label>
                                                <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <!-- <select name="maintainance_report" class="form-control form-control-line" id="Maintainence Report" required="required">
                                                        <option value="Completed"
                                                        @if($data['equipment']->maintainance_report=="Completed")
                                                            selected="selected"
                                                        @endif
                                                        >Completed</option>
                                                        <option value="Incompleted"
                                                        @if($data['equipment']->maintainance_report=="Incompleted")
                                                            selected="selected"
                                                        @endif
                                                        >Incompleted</option>

                                                    </select> -->
                                                      <input type="text" placeholder="dd-mm-yy" name="maintainance_report" value="{{ old('maintainance_report', $data['equipment']->maintainance_report) }}" class="form-control datepicker" required="required">
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Due Date of Next Maintenance<span class="red">*</span></label>
                                            <div id=due_date class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                              <input type="text" placeholder="dd-mm-yy" name="due_date" value="{{ old('due_date', $data['equipment']->due_date) }}" class="form-control datepicker_due " required="required">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Name Contact Person in the company for maintainance <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="contact_name" value="{{ old('contact_name', $data['equipment']->contact_name) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Contact No. <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="number"  name="contact_no"  pattern="^[0-9]$" onKeyPress="if(this.value.length==12) return false;" value="{{ old('contact_no', $data['equipment']->contact_no) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Lab Responsible Person<span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="responsible_person" value="{{ old('responsible_person', $data['equipment']->responsible_person) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Name of the company maintaining of equipment <span class="red">*</span></label>
                                          <div class="col-md-12">
                                             <input type="text"  name="company_name" value="{{ old('company_name', $data['equipment']->company_name) }}" class="form-control form-control-line" required="required">
                                         </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Due date of Next Calibration <span class="red">*</span></label>
                                                <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                      <input type="text" placeholder="dd-mm-yy" name="next_calibration" value="{{ old('next_calibration', $data['equipment']->next_calibration) }}" class="form-control datepicker_due" required="required">

                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Date of Breakdown of the equipment <span class="red">*</span></label>
                                            <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                              <input type="text" placeholder="dd-mm-yy" name="breakdown_eqp" value="{{ old('breakdown_eqp', $data['equipment']->breakdown_eqp) }}" class="form-control datepicker" required="required">


                                           </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Date of equipment returning back to functional status</label>
                                                <div id=5 class="date_class col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                      <input type="text" placeholder="dd-mm-yy" name="return_function_status" value="{{ old('return_function_status', $data['equipment']->return_function_status) }}" class="form-control datepicker" >

                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Records of instrument kept in <span class="red">*</span></label>
                                             <div class="col-md-12">
                                               <input type="text"  name="record_instrument" value="{{ old('record_instrument', $data['equipment']->record_instrument) }}" class="form-control form-control-line" required="required">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Equipment ID <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="eqp_id" value="{{ old('eqp_id', $data['equipment']->eqp_id) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Organization/Agency funding for equipment along with project name <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="org" value="{{ old('org', $data['equipment']->org) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-sm-12">Current warranty status <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="curr_warrenty" class="form-control form-control-line" id="Instrument/Tool" required="required">
                                                        <option value="">--Select--</option>
                                                        <option value="Under Warranty"
                                                        @if($data['equipment']->curr_warrenty=="Under Warranty")
                                                            selected="selected"
                                                        @endif
                                                        >Under Warranty</option>
                                                        <option value="Not Under Warranty"
                                                        @if($data['equipment']->curr_warrenty=="Not Under Warranty")
                                                            selected="selected"
                                                        @endif
                                                        >Not Under Warranty</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">Equipment under Maintenance<span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="eqp_maintain" class="form-control form-control-line" id="Status of the Equiment" required="required">
                                                        <option value="">--Select--</option>
                                                        <option value="Yes"
                                                        @if($data['equipment']->eqp_maintain=="Yes")
                                                            selected="selected"
                                                        @endif
                                                        >Yes</option>
                                                        <option value="No"
                                                        @if($data['equipment']->eqp_maintain=="No")
                                                            selected="selected"
                                                        @endif
                                                        >No</option>
                                                    </select>

                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Records of Instrument kept in<span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="records_inst" value="{{ old('records_inst', $data['equipment']->records_inst) }}" class="form-control form-control-line" required="required">
                                           </div>
                                        </div>
                                        <div class="col">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="col-12">

                            <button class="btn btn-info">Save</button>
                            <a class="btn btn-warning" href="{{url('/equipment')}}">Cancel</a>
                            <!-- <button class="btn btn-success">Preview</button>
                             <button class="btn ">Print</button> -->

                        </div>

                    </div>
                </form>


                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
</div>

<script type="text/javascript">
  var _sample = $("#NameProvider").find(":selected").val();
  if(_sample=='Donor'){
    $("#provider_name").removeClass("hide");
  }else{
    $("#provider_name").addClass("hide");
  }
  $(function () {
        var today = new Date();

        $("#NameProvider").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample=='Donor'){
              $("#provider_name").removeClass("hide");
            }else{
              $("#provider_name").addClass("hide");
            }
        });
        //alert(new Date());
        // $('#sandbox-container1').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container2').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container3').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container4').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container5').datepicker({
        //             format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container6').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container7').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });
        // $('#sandbox-container8').datepicker({
        //              format: 'dd-mm-yyyy',
        //              startDate: '-0d',
        //              autoclose: true
        // });

        // $("#sandbox-container1").on("dp.change", function (e) {
        //     $('#sandbox-container1').data("DatePicker").minDate(today);
        // });


  });
</script>
@endsection
