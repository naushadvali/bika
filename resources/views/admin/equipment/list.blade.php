@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Equipments</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">

                    <form action="{{ url('/equipment/print') }}" method="post" >

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="btn-group pull-right">
                        <a class="btn btn-sm btn-primary" href="{{ url('/equipment/create') }}">Add New</a>
                        <a class="btn btn-sm btn-warning" href="{{ url('/calendar') }}">Calendar</a>
                        <button type="submit" class="pull-right btn-sm btn-info" >Print</button>
                      </div>
                    </form>
                    <form action="{{ url('/equipment/downtimeAnalysis') }}" method="post" >

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="btn-group pull-right">

                        <button type="submit" class="pull-right btn-sm btn-info" >Downtime Report</button>
                      </div>
                    </form>
                    <form action="{{ url('/equipment/freqdowntimeAnalysis') }}" method="post" >

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="btn-group pull-right">

                        <button type="submit" class="pull-right btn-sm btn-info" >Frequent Breakdown Report</button>
                      </div>
                    </form>
                 </div>

              </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12 " >

                                  <div class="table-scroll">
                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12 " cellspacing="0" >
                                      <thead>
                                          <tr>
                                            <th>Item Name (Equipment by category)</th>
                                            <th>Item Name (Equipment by name)</th>
                                            <th>Instrument/Tool</th>
                                            <th>Equipment Make</th>
                                            <th>Equipment Model No</th>
                                            <th>Equipment Serial No. </th>
                                            <th>Organization/Agency funding for equipment along with project name </th>
                                            <th>Supplier Name</th>
                                            <th>Date of Installation </th>
                                            <th>Equipment ID </th>
                                            <th>Location of the equipment </th>
                                            <th>Warranty Status till applicable date </th>
                                            <th>Current warranty status </th>
                                            <th>Equipment under maintainance contract</th>
                                            <th>Maintainance contract (till applicable date)</th>
                                            <th>Name of the company maintaining of equipment</th>
                                            <th>Name Contact Person in the company for maintainance</th>
                                            <th>Contact No.</th>
                                            <th>Contact Email</th>
                                            <th>Date of Last Maintenance </th>
                                            <th>Date of Last Calibration </th>
                                            <th>Due date of Next Maintenance </th>
                                            <th>Due date of Next Calibration </th>
                                            <th>Lab Responsble person</th>
                                            <th>Date of Breakdown of the equipment </th>
                                            <th>Date of equipment returning back to functional status</th>
                                            <th>Date of Decommission the equipment </th>
                                            <th>Records of Instrument kept in</th>
                                            <th>Add Breakdown Date</th>
                                            <th>Action</th>
                                            <th>Delete</th>

                                          </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <th>{{$samples->name_cat}}</th>
                                          <th>{{$samples->name}}</th>
                                          <th>{{$samples->tool}}</th>
                                          <th>{{$samples->make}}</th>
                                          <th>{{$samples->model_no}}</th>
                                          <th>{{$samples->serial_no}}</th>
                                          <th>{{$samples->org}}</th>
                                          <th>{{$samples->supplier}}</th>
                                          <th>{{$samples->date_installation}}</th>
                                          <th>{{$samples->eqp_id}}</th>
                                          <th>{{$samples->location}}</th>
                                          <th>{{$samples->waranty_status}}</th>
                                          <th>{{$samples->curr_warrenty}}</th>
                                          <th></th>
                                          <th>{{$samples->maintainance_report}}</th>
                                          <th>{{$samples->company_name}}</th>
                                          <th>{{$samples->contact_name}}</th>
                                          <th>{{$samples->contact_no}}</th>
                                          <th>{{$samples->contact_email}}</th>
                                          <th>{{$samples->date_last_maintain}}</th>
                                          <th>{{$samples->date_last_caliberation}}</th>
                                          <th>{{$samples->due_date}}</th>
                                          <th>{{$samples->next_calibration}}</th>
                                          <th>{{$samples->responsible_person}}</th>
                                          <th>{{$samples->breakdown_eqp}}</th>
                                          <th>{{$samples->return_function_status}}</th>
                                          <th>{{$samples->date_decommission}}</th>
                                          <th>{{$samples->records_inst}}</th>
                                          <th>
                                            <button type="button" onclick="openCbnaatForm({{$samples->id}})"  class="btn btn-info btn-sm resultbtn" >Add</button>
                                            <!-- <form action="{{ url('/equipment/addbreakdown/'.$samples->id) }}" method="post" >
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              Breakdown date: <input type="text" placeholder="dd-mm-yy" name="break_date"  class="form-control datepicker" required="required">
                                              Recovery Date: <input type="text" placeholder="dd-mm-yy" name="recovery_date"  class="form-control datepicker" required="required">
                                              <button type="submit" class="pull-right btn-sm btn-info" >Add</button>
                                            </form> -->
                                          </th>

                                          <th><a href="{{ url('/equipment/'.$samples->id.'/edit') }}">Edit</a></th>
                                          <th><a href="{{ url('/equipment/'.$samples->id.'/delete_equipment') }}">Delete</a></th>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
</div>

<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add breakdown Date</h4>
        </div>
         <form class="form-horizontal form-material" action="{{ url('/equipment/addbreakdown') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
		        <div class="modal-body">

		           	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		           	<input type="hidden" name="equipId" id="equipId" value="">

		            <label class="col-md-12">Date of Breakdown of the equipment</label>
                <div class="col-md-12">
                   <input type="text" placeholder="dd-mm-yy" name="break_date"  class="form-control datepicker" required="required">
               </div>
               <br>
               <label class="col-md-12">Date of equipment returning back to functional status</label>
               <div class="col-md-12">
                  <input type="text" placeholder="dd-mm-yy" name="recovery_date"  class="form-control datepicker" required="required">
              </div>

		        </div>
		        <div class="modal-footer">
		          <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
		          <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
        		  <button type="submit" class="pull-right btn btn-primary btn-md" id="confirm">Ok</button>
		        </div>

		  </form>
      </div>
    </div>
 </div>

<script>

$(function(){


});
function openCbnaatForm(id){
 $("#equipId").val(id);
 $('#myModal').modal('toggle');
}
</script>






@endsection
