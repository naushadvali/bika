
@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0" id="dash">Decontamination</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/dashboardDecontamination/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>

              </div>
                              <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >

                                    <!-- <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%"> -->
                                      <style>
                                      table, th, td {
                                          border: 1px solid black;
                                      }
                                      </style>
                                      <table style="width:100%">
                                        <thead>
                                            <tr>

                                              <th>Test to be performed</th>
                                              <th>Tests in process</th>
                                              <th>Tests for review</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>



                                              <td>{{$data['decontamination_test']}}</td>
                                              <td>{{$data['decontamination_tested']}}</td>
                                              <td>{{$data['decontamination_review']}}</td>
                                         </tr>


                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                          <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                              <div class="table-scroll" >
                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Date of Decontamination</th>
                                              <th>DX/FU/EQA</th>
                                              <th>Follow up month</th>
                                              <th>Microscopy Result</th>
                                              <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                     @if($data['sample'])
											                 @foreach ($data['sample'] as $key=> $samples)
                                          	<tr>
                                              <td class="hide">{{$samples->ID}}</td>
	                                            <td>{{$samples->enroll_label}}</td>
                                              <td>{{$samples->samples}}</td>
                                              <td>
                                                @if($samples->date=='')
                                                pending
                                                @else
                                                {{$samples->date}}
                                                @endif
                                              </td>
                                              <td>{{$samples->test_reason}}</td>
                                              <td>{{$samples->fu_month}}</td>
                                              <td>{{$samples->result}}</td>
	                                            @if($samples->status && $samples->status==1)
                                                  <td>
                                                  <button type="button" onclick="openCbnaatForm({{$samples->enroll_id}},'{{$samples->samples}}','{{$samples->sent_for}}')"  class="btn btn-info btn-sm resultbtn" >Send for review</button>
                                                </td>
                                                @else
                                                  <td>sent</td>
                                              @endif

                                          </tr>
                                          @endforeach
                                      @endif

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>

<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sending sample for review...</h4>
        </div>

         <form class="form-horizontal form-material" action="{{ url('/dash_decontamination') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
		        <div class="modal-body">

		           	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		           	<input type="hidden" name="enrollId" id="enrollId" value="">

		            <label class="col-md-12"><h5>Sample ID:</h5></label>
                    <div class="col-md-12">
                      <input type="text" name="sample_ids" class="form-control form-control-line sample_ids"  id="sample_ids" readonly>

                   </div>
                <br>
                <label class="col-md-12"><h5>Date of Decontamination</h5></label>
                    <div class="col-md-12">
                       <input type="text" name="test_date" class="form-control form-control-line sampleId datepicker" value="<?php echo date("Y-m-d");?>"  max="<?php echo date("Y-m-d");?>" id="test_date" required>
                   </div>




		        </div>
		        <div class="modal-footer">
		          <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
		          <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
        		  <button type="button" class="pull-right btn btn-primary btn-md" id="confirm" data-dismiss="modal">Ok</button>
		        </div>

		  </form>
      </div>
    </div>
 </div>

<script>
$(function(){

  $('input.single-checkbox').on('change', function(evt) {
     if($('[name="category[]"]:checked').length > 2) {
         this.checked = false;
         $("#caterralert").text("You can select only 2 services!");
     }else{
        $("#caterralert").text("");
     }
  });

    // $("#sent_for").change(function(){

    //   if($( "#sent_for option:selected" ).text()=='Other'){
    //     $('#other').removeClass('hide');
    //   }
    //   else{
    //     $('#other').addClass('hide');
    //   }
    // });

	$(".resultbtn").click(function(){
    	$('#sample_id').val($(this).val());
  	});

  	$('#confirmDelete').on('show.bs.modal', function (e) {

		// Pass form reference to modal for submission on yes/ok
		var form = $(e.relatedTarget).closest('form');
		$(this).find('.modal-footer #confirm').data('form', form);
	});

	/* Form confirm (yes/ok) handler, submits form*/
	$('#confirm').click( function(){
		//var form = $(document).find('form#resultpopup');
    var form = $(document).find('form#cbnaat_result');
      form.submit();
		// console.log( $('#cbnaat_result').serialize() );
		// var data = $('#cbnaat_result').serialize();
		// $.post(window.location.replace("{{ url('/dash_decontamination') }}"), data);
		// //form.submit();
	});

});





 function openCbnaatForm(enroll_id, sample_ids, sent_for){
 	//console.log("sample_ids", sample_ids.split(','));
   $("#sample_ids").val(sample_ids);
 	$("#enrollId").val(enroll_id);
  $("#sent_for").val(sent_for);
 // 	var sampleArray = sample_ids.split(',');
 // 	$('#sampleid option').remove();
 // 	$.each(sampleArray, function (i, item) {
	//     $('#sampleid').append($('<option>', {
	//         text : item
	//     }));
	// });

 	$('#myModal').modal('toggle');
 }
</script>






@endsection
