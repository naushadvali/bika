
@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Decontamination Next Step</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/Decontamination/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>

              </div>
              <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                      <style>
                                      table, th, td {
                                          border: 1px solid black;
                                      }
                                      </style>
                                      <table style="width:100%">
                                        <thead>
                                            <tr>

                                              <th>Test to be performed</th>
                                              <th>Tests in process</th>
                                              <th>Tests for review</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>



                                              <td>{{$data['decontamination_test']}}</td>
                                              <td>{{$data['decontamination_tested']}}</td>
                                              <td>{{$data['decontamination_review']}}</td>
                                         </tr>


                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                          <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                              <div class="table-scroll" >
                                    <p>Filter by DX/FU/EQA : <input type="text" id="filter_by" name="filter_by">
                                      <button id="filter_button">Apply</button>
                                    </p>
                                    <br>
                                    <table id="table_decontamin" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Date of Decontamination</th>
                                              <th>DX/FU/EQA</th>
                                              <th>Follow up month</th>
                                              <th>Microscopy Result</th>
                                              <th>Next Step</th>
                                              <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                     @if($data['sample'])
                                       @foreach ($data['sample'] as $key=> $samples)
                                            <tr>
                                              <td class="hide">{{$samples->ID}}</td>
                                              <td>{{$samples->enroll_label}}</td>
                                              <td>{{$samples->samples}}</td>
                                              <td>{{$samples->date}}</td>
                                              <td>{{$samples->test_reason}}</td>
                                              <td>{{$samples->fu_month}}</td>
                                              <td>{{$samples->result}}</td>
                                              <td>{{$samples->sent_for_service}}</td>

                                              <td>
                                                @if($samples->sent_for_service=='' && $samples->status!=0)
                                                <button type="button" onclick="openCbnaatForm({{$samples->enroll_id}},'{{$samples->samples}}','{{$samples->sent_for}}','{{$samples->tag}}','{{$samples->date}}','{{$samples->no_sample}}')"  class="btn btn-info btn-sm resultbtn" >Submit</button>
                                                @elseif($samples->status==0)
                                                Done
                                                @else
                                                reviewed

                                                @endif
                                              </td>


                                          </tr>


                                          @endforeach
                                      @endif

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> Â© Copyright Reserved 2017-2018, LIMS </footer>
        </div>
 @if($data['sample'])
<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Decontamination Next Step</h4>
        </div>
         <form class="form-horizontal form-material" action="{{ url('/decontamination') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
            <div class="modal-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="enrollId" id="enrollId" value="">
                <input type="hidden" name="tag" id="tag" value="">
                 <input type="hidden" name="no_sample" class="form-control form-control-line sampleId" value="" id="no_sample">

                <label class="col-md-12"><h5>Sample ID:</h5></label>
                    <div class="col-md-12">
                      <input type="text" name="sample_ids" class="form-control form-control-line sample_ids"  id="sample_ids" readonly>

                   </div>
                <br>
                <label class="col-md-12"><h5>Sample sent for:<span id="ssentfor"></span></br><span id="ssentforreq" class="red"></span></h5></label>
                <div class="col-md-12">
                   <select name="service_id[]" class="form-control form-control-line test_reason" id="service_id" multiple required>
                     <!-- <option value="">--Select--</option> -->
                     @foreach ($data['services'] as $key=> $category)
                     <option value="{{$key}}" id="select{{$key}}">{{$category}}</option>
                     @endforeach
                    <option value="Send to BWM">Send to BWM</option>
                     <!-- <option value="micro" id="micro">Sent for Microbiologist review</option> -->
                   </select>
               </div>
               <label class="col-md-12"><h5>Request for another Sample : </h5></label>
                   <div class="col-md-12">
                      <input type="checkbox"  name="request_another" id="request_another">

                      </input>
                  </div>
               <br>
               <div id="other" class="hide">
                   <label class="col-md-12"><h5>Sample sent for:</h5></label>
                  <div class="col-md-12">
                         <input type="text" id="other" name="other" value="" class="form-control form-control-line">
                  </div>
              </div>
              <br>
               <label class="col-md-12"><h5>Date Of Decontamination</h5></label>
                    <div class="col-md-12">
                       <input type="text" id="test_date" name="test_date" value="" class="form-control form-control-line datepicker" disabled>
                   </div>


            </div>
            <div class="modal-footer">
              <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
              <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
              <button type="submit" class="pull-right btn btn-primary btn-md" id="confirm">Ok</button>
            </div>

      </form>
      </div>
    </div>
 </div>

 @endif

<script>
$(function(){


   $.fn.dataTableExt.afnFiltering.push(

    function( oSettings, aData, iDataIndex ) {
        var reason_test = document.getElementById('filter_by').value;

        var filter_col = 4;

        console.log(reason_test);

        var filter_val=aData[filter_col];
        if ( reason_test == "" )
        {
            return true;
        }
        else if ( reason_test == filter_val)
        {
            return true;
        }

        return false;
    }
);

    $('#request_another').on('click', function (e) {
      var service = $("#request_another:checked").val();
      var no_sample = $("#no_sample").val();
      console.log(service);
      if(service=="on" && no_sample=='0'){
        alert("standby sample not available");
        $('#request_another').prop('checked', false);
      }

     });


  var table=$('#table_decontamin').DataTable( {
       "order": [[ 0, "desc" ]],
       dom: 'Bfrtip',
        buttons: [
           // 'excel', 'pdf',
           'excel'
        ]
   } );






  $("#service_id").change(function() {
    var id = $(this).children(":selected").attr("id");
    if(id=='select3'){
      $('#tag').val('LC');
    }else if(id=='select4'){
      $('#tag').val('LJ');
    }else if(id=='select5'){
      $('#tag').val('LC & LJ Both');
    }else{
      $('#tag').val('');
    }
  });

  $(".resultbtn").click(function(){
      $('#sample_id').val($(this).val());
    });

    $('#confirmDelete').on('show.bs.modal', function (e) {

      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
    });


   $('#filter_button').click( function() {
        table.draw();
    } );
});





 function openCbnaatForm(enroll_id, sample_ids, sent_for, tag, test_date, no){
  //console.log("sample_ids", sample_ids.split(','));
  //alert(no);
  $("#enrollId").val(enroll_id);
  $('#no_sample').val(no);
  $("#sent_for").val(sent_for);
  $("#test_date").val(test_date);
  $("#ssentfor").text("("+tag+")");
   $("#sample_ids").val(sample_ids);


  $('#myModal').modal('toggle');
 }
</script>






@endsection
