
@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">LPA interpretation</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/lpa/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
            <div class="row">



                </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                          <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                              <div class="scroll-table " >
                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>LPA Type</th>
                                              <!-- <th>Test Date</th> -->

                                              <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          @if($data['sample'])

                                            @foreach ($data['sample'] as $key=> $samples)

                                                  <tr>

                                                    <td class="hide">{{$samples->ID}}</td>
                                                    <td>{{$samples->enroll_label}}</td>
                                                    <td>{{$samples->samples}}</td>
                                                    <td>{{$samples->tag}}</td>
                                                   <!--  <td></td> -->
                                                    <td>
                                                    @if($samples->status==0)
                                                    Done
                                                    @else
                                                    <button type="button" onclick="openCbnaatForm({{$samples->enroll_id}},'{{$samples->samples}}','{{$samples->tag}}')" class="btn btn-info btn-sm resultbtn" >Submit</button>
                                                    @endif
                                                    </td>


                                                </tr>

                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>
<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="text-themecolor m-b-0 m-t-0 modal-title">Line Probe Assay (LPA)</h4>

        </div>

         <form class="form-horizontal form-material" action="{{ url('/lpa_interpretation') }}" method="post" enctype='multipart/form-data' id="cbnaat_result" >
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
            <div class="modal-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="enrollId" id="enrollId" value="">
                <input type="hidden" name="tag" id="tag" value="">

                <label class="col-md-12"><h6>Sample ID:</h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="sampleid" id="sampleid">

                       </select>
                   </div>

                <label class="col-md-12"><h6>Lab Serial  <span class="red">*</span>  </h6></label>
                    <div class="col-md-6">
                       <!-- <select class="form-control form-control-line sampleId" name="type" value="" id="type" required >
                        <option value="">select</option>
                        <option value="Direct">Direct</option>
                        <option value="Indirect">Indirect</option>
                       </select> -->
                        <input type="text" class="form-control form-control-line sampleId" name="type" required>
                       <input type="checkbox"  name="type_direct" value="Direct" checked> Direct<br>
                       <input type="checkbox" name="type_indirect" value="Indirect"> Indirect<br>

                   </div>
                   <br>

                <div id="firstLPA">
                 <label class="col-md-12 text-center"><h6  style="font-weight: bold;">First Line LPA</h6></label>

                  <label class="col-md-12"><h6>RpoB :- Locus Control : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="RpoB" value="" id="RpoB" required>
                        <option value="">select</option>
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                   <br>
                  <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>WT1 : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt1" value="" id="wt1" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT2 : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt2" value="" id="wt2" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT3 : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt3" value="" id="wt3" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                 </div>
                 <div class="row">
                   <div class="col">
                     <label class="col-md-12"><h6>WT4 : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="wt4" value="" id="wt4" required>
                         <!-- <option value="">select</option> -->
                         <option value="1">Present</option>
                         <option value="0">Absent</option>
                        </select>
                    </div>
                   </div>
                    <div class="col">
                    <label class="col-md-12"><h6>WT5 : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt5" value="" id="wt5" required>
                       <!--  <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT6 : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt6" value="" id="wt6" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                 </div>

                 <div class="row">

                   <div class="col">
                     <label class="col-md-12"><h6>WT7 : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="wt7" value="" id="wt7" required>
                        <!--  <option value="">select</option> -->
                         <option value="1">Present</option>
                         <option value="0">Absent</option>
                        </select>
                    </div>
                   </div>
                   <div class="col">
                     <label class="col-md-12"><h6>WT8 : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="wt8" value="" id="wt8" required>
                        <!--  <option value="">select</option> -->
                         <option value="1">Present</option>
                         <option value="0">Absent</option>
                        </select>
                    </div>
                   </div>
                    <div class="col">
                    <label class="col-md-12"><h6>MUT1(D516V) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut1DS16V" value="" id="mut1DS16V" required>
                        <!-- <option value="">select</option> -->

                        <option value="0">Absent</option>
                         <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                 </div>

                 <div class="row">
                   <div class="col">
                     <label class="col-md-12"><h6>MUT2A(H526Y) : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="mut2aH526Y" value="" id="mut2aH526Y" required>
                        <!--  <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                   <div class="col">
                     <label class="col-md-12"><h6>MUT2B(H526D) : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="mut2bH526D" value="" id="mut2bH526D" required>
                        <!--  <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                   <div class="col">
                     <label class="col-md-12"><h6>MUT3(S531L) : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="mut3S531L" value="" id="mut3S531L" required>
                        <!--  <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                 </div>

                 <label class="col-md-12"><h6>KatG :- Locus Control : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="katg" value="" id="katg" required>
                        <option value="">select</option>
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                   <br>
                  <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>WT1(315) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt1315" value="" id="wt1315" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>MUT1(S315T1) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut1S315T1" value="" id="mut1S315T1" required>
                        <!-- <option value="">select</option> -->

                        <option value="0">Absent</option>
                        <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>MUT2(S315T2) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut2S315T2" value="" id="mut2S315T2" required>
                       <!--  <option value="">select</option> -->

                        <option value="0">Absent</option>
                         <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                 </div>


                 <label class="col-md-12"><h6>InhA :- Locus Control : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="inha" value="" id="inha" required>
                        <option value="">select</option>
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                   <br>
                  <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>WT1(-15,-16) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt1516" value="" id="wt1516" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT2(-8) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="wt28" value="" id="wt28" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>MUT1(C15T) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut1C15T" value="" id="mut1C15T" required>
                       <!--  <option value="">select</option> -->

                        <option value="0">Absent</option>
                         <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                 </div>
                 <div class="row">
                   <div class="col">
                     <label class="col-md-12"><h6>MUT2(A16G) : <span class="red">*</span> </h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId flpa" name="mut2A16G" value="" id="mut2A16G" required>
                         <!-- <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                    <div class="col">
                    <label class="col-md-12"><h6>MUT3A(T8C) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut3aT8C" value="" id="mut3aT8C" required>
                       <!--  <option value="">select</option> -->

                        <option value="0">Absent</option>
                         <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>MUT3B(T8A) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId flpa" name="mut3bT8A" value="" id="mut3bT8A" required>
                       <!--  <option value="">select</option> -->

                        <option value="0">Absent</option>
                        <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                 </div>
              </div>



                 <br>
                <div id="secondLPA">
                 <label class="col-md-12 text-center"><h6 style="font-weight: bold;">Second Line LPA</h6></label>

                  <label class="col-md-12"><h6>gyrA :- Locus Control : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="gyra" value="" id="gyra" required>
                        <option value="">select</option>
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                   <br>
                  <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>WT1(85-90) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="wt18590" value="" id="wt18590" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT2(89-93) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="wt28993" value="" id="wt28993" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>WT3(92-97) : <span class="red">*</span> </h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="wt39297" value="" id="wt39297" required>
                        <!-- <option value="">select</option> -->
                        <option value="1">Present</option>
                        <option value="0">Absent</option>
                       </select>
                   </div>
                  </div>
                 </div>
                 <div class="row">
                   <div class="col">
                     <label class="col-md-12"><h6>MUT1(A90V) : <span class="red">*</span></h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId slpa" name="mut1A90V" value="" id="mut1A90V" required>
                        <!--  <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                    <div class="col">
                    <label class="col-md-12"><h6>MUT2(S91P) : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="mut2S91P" value="" id="mut2S91P" required>
                        <!-- <option value="">select</option> -->

                        <option value="0">Absent</option>
                        <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>MUT3A(D94A) : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="mut3aD94A" value="" id="mut3aD94A" required>
                        <!-- <option value="">select</option> -->

                        <option value="0">Absent</option>
                        <option value="1">Present</option>
                       </select>
                   </div>
                  </div>

                 </div>

                 <div class="row">

                   <div class="col">
                     <label class="col-md-12"><h6>MUT3B(D94N/Y) : <span class="red">*</span></h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId slpa" name="mut3bD94N" value="" id="mut3bD94N" required>
                         <!-- <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                   <div class="col">
                     <label class="col-md-12"><h6>MUT3C(D94G) : <span class="red">*</span></h6></label>
                     <div class="col-md-12">
                        <select class="form-control form-control-line sampleId slpa" name="mut3cD94G" value="" id="mut3cD94G" required>
                         <!-- <option value="">select</option> -->

                         <option value="0">Absent</option>
                          <option value="1">Present</option>
                        </select>
                    </div>
                   </div>
                    <div class="col">
                    <label class="col-md-12"><h6>MUT3D(D94H) : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId slpa" name="mut3dD94H" value="" id="mut3dD94H" required>
                       <!--  <option value="">select</option> -->

                        <option value="0">Absent</option>
                         <option value="1">Present</option>
                       </select>
                   </div>
                  </div>
                 </div>


                <label class="col-md-12"><h6>gyrB :- Locus Control : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="gyrb" value="" id="gyrb" required>
                    <option value="">select</option>
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
               <br>
              <div class="row">
                <div class="col">
                <label class="col-md-12"><h6>WT1(536-541) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt1536541" value="" id="wt1536541" required>
                    <!-- <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>MUT1(N538D) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="mut1N538D" value="" id="mut1N538D" required>
                   <!--  <option value="">select</option> -->

                    <option value="0">Absent</option>
                     <option value="1">Present</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>MUT2(E540V) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="mut2E540V" value="" id="mut2E540V" required>
                    <!-- <option value="">select</option> -->

                    <option value="0">Absent</option>
                     <option value="1">Present</option>
                   </select>
               </div>
              </div>


             </div>

              <label class="col-md-12"><h6>rrs :- Locus Control : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="rrs" value="" id="rrs" required>
                    <option value="">select</option>
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
               <br>
              <div class="row">

              <div class="col">
                <label class="col-md-12"><h6>WT1(1401-02) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt1140102" value="" id="wt1140102" required>
                    <!-- <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>WT2(1484) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt21484" value="" id="wt21484" required>
                    <!-- <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>MUT1(A1401G) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="mut1A1401G" value="" id="mut1A1401G" required>
                   <!--  <option value="">select</option> -->

                    <option value="0">Absent</option>
                     <option value="1">Present</option>
                   </select>
               </div>
              </div>
             </div>
             <div class="row">
               <div class="col">
                 <label class="col-md-12"><h6>MUT2(G1484T) : <span class="red">*</span></h6></label>
                 <div class="col-md-12">
                    <select class="form-control form-control-line sampleId slpa" name="mut2G1484T" value="" id="mut2G1484T" required>
                    <!--  <option value="">select</option> -->

                     <option value="0">Absent</option>
                     <option value="1">Present</option>
                    </select>
                </div>
               </div>
               <div class="col">

               </div>
               <div class="col">

               </div>
             </div>

             <label class="col-md-12"><h6>eis :- Locus Control : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="eis" value="" id="eis" required>
                    <option value="">select</option>
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
               <br>
              <div class="row">

              <div class="col">
                <label class="col-md-12"><h6>WT1(37) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt137" value="" id="wt137" required>
                   <!--  <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>WT2(14,12,10) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt2141210" value="" id="wt2141210" required>
                   <!--  <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
              <div class="col">
                <label class="col-md-12"><h6>WT3(2) : <span class="red">*</span></h6></label>
                <div class="col-md-12">
                   <select class="form-control form-control-line sampleId slpa" name="wt32" value="" id="wt32" required>
                   <!--  <option value="">select</option> -->
                    <option value="1">Present</option>
                    <option value="0">Absent</option>
                   </select>
               </div>
              </div>
             </div>

             <div class="row">
               <div class="col">
                 <label class="col-md-12"><h6>MUT1(C-14T) : <span class="red">*</span></h6></label>
                 <div class="col-md-12">
                    <select class="form-control form-control-line sampleId slpa" name="mut1c14t" value="" id="mut1c14t" required>
                     <!-- <option value=" ">select</option> -->

                     <option value="0">Absent</option>
                     <option value="1">Present</option>
                    </select>
                </div>
               </div>
               <div class="col">
               </div>
               <div class="col">
               </div>
             </div>
            </div>

             <br>
             <label class="col-md-12 text-center"><h6 style="font-weight: bold;">Final Interpretation</h6></label>


                   <br>
                  <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>MTB Result : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="mtb_result" value="" id="mtb_result" required>
                        <option value="">select</option>
                        <option value=" MTB detected"> MTB detected</option>
                        <option value="MTB not detected">MTB not detected</option>
                        <option value="Invalid">Invalid</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>RIF Resi : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="rif" value="" id="rif" required>
                        <option value="">select</option>
                        <option value="Not detected">Not detected</option>
                        <option value="Detected">Detected</option>
                        <!-- <option value="Indeterminate">Indeterminate</option> -->
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>H Resi : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="inh" value="" id="inh" required>
                        <option value="">select</option>
                        <option value="InhA Detected">InhA Detected</option>
                        <option value="KatG Detected">KatG Detected</option>
                        <option value="Not detected">Not detected</option>
                       </select>
                   </div>
                  </div>
                 </div>
                 <div class="row">
                    <div class="col">
                    <label class="col-md-12"><h6>SLID Resi : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="slid" value="" id="slid" required>
                        <option value="">select</option>
                        <option value="eis Detected">eis Detected</option>
                        <option value="rss Detected">rss Detected</option>
                        <option value="eis and rss Detected">eis and rss Detected</option>
                        <option value="Not detected">Not detected</option>
                       </select>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>FQ Resi : <span class="red">*</span></h6></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="quinolone" value="" id="quinolone" required>
                        <option value="">select</option>
                        <option value="Not detected">Not detected</option>
                        <option value="Detected">Detected</option>
                        <!-- <option value="Indeterminate">Indeterminate</option> -->
                       </select>
                   </div>
                  </div>
                  <div class="col">
                  </div>
                 </div>

                 <br>

                 <div class="row">

                  <div class="col">
                    <label class="col-md-12"><h6>Date Result :</h6></label>
                    <div class="col-md-12">
                       <div class="col-md-12">
                       <input type="text" name="test_date" value="{{$data['today']}}" class="form-control form-control-line" disable>
                   </div>
                   </div>

                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>Date Reported :</h6></label>
                    <div class="col-md-12">
                       <input type="text" name="test_date" value="{{$data['today']}}" class="form-control form-control-line" disable>
                   </div>
                  </div>
                  <div class="col">
                    <label class="col-md-12"><h6>Reported By :</h6></label>
                    <div class="col-md-12">
                      <input type="text" name="created_by" value="{{$data['user']}}" class="form-control form-control-line" disable>
                   </div>
                  </div>
                 </div>

            </div>
            <div class="modal-footer">
              <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
              <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal" id="canceled">Cancel</button>
              <button type="submit" class="pull-right btn btn-primary btn-md" id="confirm" >Ok</button>
            </div>

      </form>
      </div>
    </div>
 </div>
<script>

$(function(){

  $(".resultbtn").click(function(){
      $('#sample_id').val($(this).val());
    });

    $('#confirmDelete').on('show.bs.modal', function (e) {


    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
  });

  /* Form confirm (yes/ok) handler, submits form*/
  $('#confirm').click( function(){
    //var form = $(document).find('form#resultpopup');

    console.log( $('#cbnaat_result').serialize() );
    var data = $('#cbnaat_result').serialize();
    if($('#type_direct').val()!="" || $('#type_indirect').val()!=""){

      // $.post("{{ url('/lpa_interpretation') }}", data, function(ret){
      //   location.reload();
      // });
      form.submit();
    }
  });

  $('#canceled').click( function(){

    console.log( $('#cbnaat_result').serialize() );
    var data = $('#cbnaat_result').serialize();

      $.get("{{ url('/lpa_interpretation') }}", data, function(ret){
        location.reload();
      });
    //form.submit();
  });

});





 function openCbnaatForm(enroll_id, sample_ids, tag){
  //console.log("sample_ids", sample_ids.split(','));
  $("#enrollId").val(enroll_id);
  $("#tag").val(tag);
  var sampleArray = sample_ids.split(',');
  $('#sampleid option').remove();
  $.each(sampleArray, function (i, item) {
      $('#sampleid').append($('<option>', {
          text : item
      }));
  });

  $('#myModal').modal('toggle');

            var _sample = $("#tag").val();

            if(_sample=='1st line LPA'){

              $("#secondLPA").addClass("hide");
              $(".slpa"). removeAttr("required");
              document.getElementById("quinolone").value = "";
              document.getElementById("quinolone").setAttribute("disabled","disabled");
              document.getElementById("slid").value = "";
              document.getElementById("slid").setAttribute("disabled","disabled");
              $("#mtb_result").change(function(){
                      var _sample = $("#mtb_result").val();
                      if(_sample=='Invalid'){
                        document.getElementById("inh").value = "";
                        document.getElementById("inh").setAttribute("disabled","disabled");
                        document.getElementById("rif").value = "";
                        document.getElementById("rif").setAttribute("disabled","disabled");
                      }else{
                              document.getElementById("inh").removeAttribute("disabled","disabled");
                              document.getElementById("rif").removeAttribute("disabled","disabled");
                      }
                });
            }
            else if(_sample=='2st line LPA')
            {
              $("#firstLPA").addClass("hide");
              $(".flpa"). removeAttr("required");
              document.getElementById("rif").value = "";
              document.getElementById("rif").removeAttribute("required");
              document.getElementById("rif").setAttribute("disabled","disabled");
              document.getElementById("inh").value = "";
              document.getElementById("inh").removeAttribute("required");
              document.getElementById("inh").setAttribute("disabled","disabled");
              $("#mtb_result").change(function(){
                      var _sample = $("#mtb_result").val();
                      if(_sample=='Invalid'){
                        document.getElementById("quinolone").value = "";
                        document.getElementById("quinolone").setAttribute("disabled","disabled");
                        document.getElementById("slid").value = "";
                        document.getElementById("slid").setAttribute("disabled","disabled");
                      }else{
                              document.getElementById("slid").removeAttribute("disabled","disabled");
                              document.getElementById("quinolone").removeAttribute("disabled","disabled");
                      }
                });
            }
            else {
              $("#mtb_result").change(function(){
                      var _sample = $("#mtb_result").val();
                      if(_sample=='Invalid'){

                        document.getElementById("quinolone").value = "";
                        document.getElementById("slid").value = "";
                        document.getElementById("inh").value = "";
                        document.getElementById("quinolone").setAttribute("disabled","disabled");
                        document.getElementById("inh").setAttribute("disabled","disabled");
                        document.getElementById("slid").setAttribute("disabled","disabled");
                        document.getElementById("rif").value = "";
                        document.getElementById("rif").setAttribute("disabled","disabled");


                      }else{
                              document.getElementById("quinolone").removeAttribute("disabled","disabled");
                              document.getElementById("inh").removeAttribute("disabled","disabled");
                              document.getElementById("slid").removeAttribute("disabled","disabled");
                              document.getElementById("rif").removeAttribute("disabled","disabled");
                      }
                  });
            }

 }

</script>


@endsection
