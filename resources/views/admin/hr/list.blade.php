@extends('admin.layout.app')
@section('content')

 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">HR</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">

                    <a class="pull-right btn-go go-button add-button" href="{{ url('/hr/create') }}">Add New</a>
                    <br>

                    <form action="{{ url('/hr/print') }}" method="post" >

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</button>
                    </form>
                 </div>

              </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12 " >
                                  <form action="{{ url('/hr/yearDesignationFilter') }}" method="post">
                                  <div class="row">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="col-sm-2">Designation:
                                        <!-- <input type="text" name="desig" class="form-control form-control-line"> -->
                                        <select name="desig" class="form-control form-control-line">
                                            <option value="">Select</option>
                                            @foreach ($data['designation'] as $key => $designation)
                                                    <option value="{{$designation->designation}}"
                                                    >{{$designation->designation}}</option>

                                            @endforeach
                                        </select>
                                      </div>
                                      <div class="col-sm-2">Year: <input type="number" name="year" class="form-control form-control-line "></div>
                                      <div class="col-sm-2">&nbsp;<input type="submit" class="btn btn-primary btn-sm form-control form-control-line text-white" value="Filter"></div>
                                  </div>
                                </form>
                                <form action="{{ url('/hr/yearOrganizationFilter') }}" method="post">
                                  <div class="row">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="col-sm-2">Organization:
                                        <select name="org" class="form-control form-control-line" >
                                            <option value="">--Select--</option>
                                            <option value="Central Govt."
                                            >Central Govt.</option>
                                            <option value="State Govt."
                                            >State Govt.</option>
                                            <option value="Institutional"
                                            >Institutional</option>
                                            <option value="Contractual"
                                            >Contractual</option>
                                            <option value="RNTCP"
                                            >RNTCP</option>
                                            <option value="Private"
                                            >Private</option>
                                            <option value="Project"
                                            >Project</option>
                                            <option value="Others"
                                            >Others</option>
                                        </select>
                                      </div>
                                      <div class="col-sm-2">Year: <input type="number" name="year_org" class="form-control form-control-line "></div>
                                      <div class="col-sm-2">&nbsp;<input type="submit" class="btn btn-primary btn-sm form-control form-control-line text-white" value="Filter"></div>
                                  </div>
                                </form>
                                <form action="{{ url('/hr/yearTypeFilter') }}" method="post">
                                  <div class="row">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="col-sm-2">Type of Employment:
                                        <select name="type" class="form-control form-control-line" required="required">
                                            <option value="">--Select--</option>
                                            <option value="Partime"
                                            >Partime</option>
                                            <option value="Fulltime"
                                            >Fulltime</option>
                                        </select>
                                      </div>
                                      <div class="col-sm-2">Year: <input type="number" name="year_type" class="form-control form-control-line "></div>
                                      <div class="col-sm-2">&nbsp;<input type="submit" class="btn btn-primary btn-sm form-control form-control-line text-white" value="Filter"></div>
                                  </div>
                                </form>
                                  <hr/>
                                  <div class="table-scroll">
                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12 " cellspacing="0" >
                                      <thead>
                                          <tr>
                                            <th>Name</th>
                                            <th>Adhaar ID</th>
                                            <th>Designation</th>
                                            <th>Qualifiaction</th>
                                            <th>Part time/Full time</th>
                                            <th>Organization/Source of funding for position</th>
                                            <th>Date of Joining</th>
                                            <th>Annual Health Checkup</th>
                                            <th>Vaccination</th>
                                            <th>Orientation Training</th>
                                            <th>Microscopy</th>
                                            <th>Liquid Culture Training (MGIT 960)</th>
                                            <th>Solid Culture (LJ) training</th>
                                            <th>DST (LC) First Line</th>
                                            <th>DST (LC) Second Line</th>
                                            <th>DST (LJ) First Line</th>
                                            <th>DST (LJ) Second Line</th>
                                            <th>LPA 1st Line</th>
                                            <th>LPA 2nd Line</th>
                                            <th>GeneXpert</th>
                                            <th>QMS Training</th>
                                            <th>Bio safety training</th>
                                            <th>Fire safety training</th>
                                            <th>Bio waste management training</th>
                                            <th>Others</th>
                                            <th>Date of releving from current post</th>

                                            <th>Action</th>
                                            <th>Delete</th>

                                          </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <th>{{$samples->name}}</th>
                                          <th>{{$samples->adhaar}}</th>
                                          <th>{{$samples->designation}}</th>
                                          <th>{{$samples->qualification}}</th>
                                          <th>{{$samples->type_qualification}}</th>
                                          <th>{{$samples->org_source}}</th>
                                          <th>{{$samples->date_joining}}</th>
                                          <th>{{$samples->health_check}}</th>
                                          <th>{{$samples->vaccination}}</th>
                                          <th>{{$samples->orientation_training}}</th>
                                          <th>{{$samples->microscopy}}</th>
                                          <th>{{$samples->lc}}</th>
                                          <th>{{$samples->lj}}</th>
                                          <th>{{$samples->dst}}</th>
                                          <th>{{$samples->dst_lc_2}}</th>
                                          <th>{{$samples->dst_lj_1}}</th>
                                          <th>{{$samples->dst_lj_2}}</th>
                                          <th>{{$samples->lpa}}</th>
                                          <th>{{$samples->lpa_2}}</th>
                                          <th>{{$samples->geneXpert}}</th>
                                          <th>{{$samples->qms}}</th>
                                          <th>{{$samples->bio_safe_t}}</th>
                                          <th>{{$samples->fire_safe_t}}</th>
                                          <th>{{$samples->bio_waste_man}}</th>
                                          <th>{{$samples->name_other}}</th>
                                          <th>{{$samples->date_reliving_curr}}</th>

                                          <th><a href="{{ url('/hr/'.$samples->id.'/edit') }}">Edit</a></th>
                                          <th><a href="{{ url('/hr/'.$samples->id.'/delete_hr') }}">Delete</a></th>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
</div>


<script>

$(function(){

});
</script>






@endsection
