@extends('admin.layout.app')
@section('content')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">HR</h3>

                    </div>

                </div>

               <!--  <form action="{{ url('/enroll') }}" class="form-horizontal form-material" method="post" enctype='multipart/form-data'>
 -->
                @if($data['hr']->id>0)
                   <form id="createForm" action="{{ url('/hr/'.$data['hr']->id) }}" method="post">
                     <input name="_method" type="hidden" value="patch">
                   @else
                   <form id="createForm" action="{{ url('/hr') }}" method="post" role="alert">
                 @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-block">


                                	<div class="row">
                                		<div class="col ">
	                                        <label class="col-md-12">Name <span class="red">*</span></label>
	                                        <div class="col-md-12">
	                                            <input type="text" name="name" class="form-control form-control-line" value="{{ old('name', $data['hr']->name) }}" required="required">
	                                        </div>
                                    	</div>
                                        <div class="col ">
                                             <label class="col-md-12">Adhaar </label>
                                            <div class="col-md-12">
                                                <input type="text" name="adhaar" value="{{ old('adhaar', $data['hr']->adhaar) }}" class="form-control form-control-line" maxlength="12" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                            <label class="col-sm-12">Designation <span class="red">*</span></label>
	                                        <div class="col-sm-12">

                                              <select name="designation" class="form-control form-control-line" id="designation" required>
                                                  <option value="">Select</option>
                                                  @foreach ($data['designation'] as $key => $designation)
                                                          <option value="{{$designation->designation}}"
                                                            @if($data['hr']->designation == $designation->designation)
                                                              selected
                                                            @endif
                                                          >{{$designation->designation}}</option>

                                                  @endforeach
                                              </select>
                                          </div>
                                        </div>

                                        <div class="col hide" id="other_designation">
                                            <label class="col-sm-12">Other Designation <span class="red">*</span> </label>
  	                                        <div class="col-md-12">
  	                                            <input type="text" name="other_designation" class="form-control form-control-line" value="{{ old('other_designation', $data['hr']->other_designation) }}" >
  	                                        </div>
	                                        <div class="col-sm-12">

                                          </div>
                                        </div>

                                        <div class="col ">
                                             <label class="col-md-12">Type of Employment <span class="red">*</span></label>
	                                        <div class="col-md-12">

                                                <select name="type_qualification" class="form-control form-control-line" required="required">
                                                    <option value="">--Select--</option>
                                                    <option value="Partime"
                                                    @if($data['hr']->type_qualification=="Partime")
                                                            selected="selected"
                                                    @endif
                                                    >Partime</option>
                                                    <option value="Fulltime"
                                                    @if($data['hr']->type_qualification=="Fulltime")
                                                            selected="selected"
                                                    @endif
                                                    >Fulltime</option>
                                                </select>
	                                        </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">Qualification </label>
                                            <div class="col-md-12">
                                                <input type="text" name="qualification" value="{{ old('qualification', $data['hr']->qualification) }}" class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="col ">

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col">
                                           <label class="col-sm-12">Mode of Employment <span class="red">*</span></label>
	                                        <div class="col-sm-12">
	                                            <select  name="mode" class="form-control form-control-line" required="required">
                                                    <option value="">--Select--</option>
	                                                <option value="Central Govt."
                                                    @if($data['hr']->mode=="Central Govt.")
                                                            selected="selected"
                                                    @endif
                                                    >Central Govt.</option>
	                                                <option value="State Govt."
                                                    @if($data['hr']->mode=="State Govt.")
                                                            selected="selected"
                                                    @endif
                                                    >State Govt.</option>
	                                                <option value="Institutional"
                                                    @if($data['hr']->mode=="Institutional")
                                                            selected="selected"
                                                    @endif
                                                    >Institutional</option>
	                                                <option value="Contractual(RNTCP,Private,Project,Others)"
                                                    @if($data['hr']->mode=="Contractual(RNTCP,Private,Project,Others)")
                                                            selected="selected"
                                                    @endif
                                                    >Contractual(RNTCP,Private,Project,Others)</option>
	                                            </select>
	                                        </div>
                                        </div>
                                        <div class="col">
                                           <label class="col-md-12">Date of Joining <span class="red">*</span></label>
                                         <!-- <div class="col-md-12" id="sandbox-container1"> -->
                                        <div id=5 class="date_class col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                               <input type="text" placeholder="dd-mm-yy" name="date_joining" value="{{ old('date_joining', $data['hr']->date_joining) }}" class="form-control datepicker" required="required">
                                         </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                      <div class="col">
                                         <label class="col-md-12">Date of Reliving </label>
                                          <div class="col-md-12">
                                            <input type="text" placeholder="dd-mm-yy" name="date_reliving" value="{{ old('date_reliving', $data['hr']->date_reliving) }}" class="form-control datepicker" >
                                          </div>

                                      </div>
                                        <div class="col">
                                            <label class="col-sm-12">Annual Health Checkup </label>
	                                        <div class="col-sm-12">
	                                            <select name="health_check" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
	                                                <option value="Yes"
                                                    @if($data['hr']->health_check=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
	                                                <option value="No"
                                                    @if($data['hr']->health_check=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
	                                                <option value="Ongoing"
                                                    @if($data['hr']->health_check=="Ongoing")
                                                            selected="selected"
                                                    @endif
                                                    >Ongoing</option>
	                                            </select>
	                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <label class="col-sm-12">Vaccination (Hep B) </label>
	                                        <div class="col-sm-12">
	                                            <select name="vaccination" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
	                                                <option value="Yes"
                                                    @if($data['hr']->vaccination=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
	                                                <option value="No"
                                                    @if($data['hr']->vaccination=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
	                                                <option value="N/A"
                                                    @if($data['hr']->vaccination=="N/A")
                                                            selected="selected"
                                                    @endif
                                                    >N/A</option>
	                                            </select>
	                                        </div>
                                        </div>
                                        <div class="col">
                                           <label class="col-md-12">Organization/Source of funding for position </label>

                                           <div class="col-sm-12">
                                                <select name="org_source" id="org_source" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Central Govt."
                                                    @if($data['hr']->org_source=="Central Govt.")
                                                            selected="selected"
                                                    @endif
                                                    >Central Govt.</option>
                                                    <option value="State Govt."
                                                    @if($data['hr']->org_source=="State Govt.")
                                                            selected="selected"
                                                    @endif
                                                    >State Govt.</option>
                                                    <option value="Institutional"
                                                    @if($data['hr']->org_source=="Institutional")
                                                            selected="selected"
                                                    @endif
                                                    >Institutional</option>

                                                    <option value="Contractual"
                                                    @if($data['hr']->org_source=="Contractual")
                                                            selected="selected"
                                                    @endif
                                                    >Contractual</option>
                                                    <option value="RNTCP"
                                                    @if($data['hr']->org_source=="RNTCP")
                                                            selected="selected"
                                                    @endif
                                                    >RNTCP</option>
                                                    <option value="Private"
                                                    @if($data['hr']->org_source=="Private")
                                                            selected="selected"
                                                    @endif
                                                    >Private</option>

                                                    <option value="Project"
                                                    @if($data['hr']->org_source=="Project")
                                                            selected="selected"
                                                    @endif
                                                    >Project</option>
                                                    <option value="Others"
                                                    @if($data['hr']->org_source=="Others")
                                                            selected="selected"
                                                    @endif
                                                    >Others</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col hide" id="org_source_other">
                                          <label class="col-sm-6">Organization/Source of funding for position Other</label>
                                              <div class="col-sm-6">
                                                   <input type="text" class="form-control form-control-line" value="{{ old('org_source_other', $data['hr']->org_source_other) }}" name="org_source_other">
                                              </div>
                                        </div>

                                    </div>

                                     <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">Date of Reliving from current post </label>
                                            <div class="col-md-12">
                                                <input type="text" name="date_reliving_curr" value="{{ old('date_reliving_curr', $data['hr']->date_reliving_curr) }}" class="form-control form-control-line datepicker" >
                                            </div>
                                        </div>

                                        <div class="col ">
                                             <label class="col-md-12">Bio safety training </label>
                                            <div class="col-md-12">

                                                <select name="bio_safe_t" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->bio_safe_t=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->bio_safe_t=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col ">
                                           <label class="col-md-12">Date for Bio safety training </label>
                                          <div class="col-md-12">
                                            <input type="text" placeholder="dd-mm-yy" name="date_biosafty" value="{{ old('date_biosafty', $data['hr']->date_biosafty) }}" class="form-control datepicker" >

                                          </div>
                                      </div>
                                        <div class="col">
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col ">
                                        <label class="col-md-12">Orientation Training </label>
                                       <div class="col-md-12">

                                           <select name="orientation_training" class="form-control form-control-line" >
                                               <option value="">--Select--</option>
                                               <option value="Yes"
                                               @if($data['hr']->orientation_training=="Yes")
                                                       selected="selected"
                                               @endif
                                               >Yes</option>
                                               <option value="No"
                                               @if($data['hr']->orientation_training=="No")
                                                       selected="selected"
                                               @endif
                                               >No</option>
                                           </select>
                                       </div>
                                      </div>
                                        <div class="col">
                                          <label class="col-md-12">Date for Orientation Training </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_orientation" value="{{ old('date_orientation', $data['hr']->date_orientation) }}" class="form-control datepicker" >

                                         </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="col">
                                           <label class="col-md-12">Fire Safety training </label>
                                           <div class="col-md-12">

                                               <select name="fire_safe_t" class="form-control form-control-line" >
                                                   <option value="">--Select--</option>
                                                   <option value="Yes"
                                                   @if($data['hr']->fire_safe_t=="Yes")
                                                           selected="selected"
                                                   @endif
                                                   >Yes</option>
                                                   <option value="No"
                                                   @if($data['hr']->fire_safe_t=="No")
                                                           selected="selected"
                                                   @endif
                                                   >No</option>
                                               </select>
                                           </div>


                                       </div>
                                       <div class="col">
                                         <label class="col-md-12">Date for Fire safety training </label>
                                        <div class="col-md-12">
                                          <input type="text" placeholder="dd-mm-yy" name="date_firesafty" value="{{ old('date_firesafty', $data['hr']->date_firesafty) }}" class="form-control datepicker" >

                                        </div>


                                       </div>
                                   </div>
                                   <div class="row">
                                     <div class="col ">
                                       <label class="col-md-12">QMS training </label>
                                       <div class="col-md-12">

                                            <select name="qms" class="form-control form-control-line" >
                                                <option value="">--Select--</option>
                                                <option value="Yes"
                                                @if($data['hr']->qms=="Yes")
                                                        selected="selected"
                                                @endif
                                                >Yes</option>
                                                <option value="No"
                                                @if($data['hr']->qms=="No")
                                                        selected="selected"
                                                @endif
                                                >No</option>
                                            </select>
                                        </div>

                                     </div>
                                       <div class="col">
                                         <label class="col-md-12">Date for QMS training </label>
                                        <div class="col-md-12">
                                          <input type="text" placeholder="dd-mm-yy" name="date_qms" value="{{ old('date_qms', $data['hr']->date_qms) }}" class="form-control datepicker" >

                                        </div>
                                       </div>
                                   </div>
                                   <div class="row">
                                     <div class="col ">
                                         <label class="col-md-12">Bio Waste management training</label>
                                        <div class="col-md-12">

                                             <select name="bio_waste_man" class="form-control form-control-line" >
                                                 <option value="">--Select--</option>
                                                 <option value="Yes"
                                                 @if($data['hr']->bio_waste_man=="Yes")
                                                         selected="selected"
                                                 @endif
                                                 >Yes</option>
                                                 <option value="No"
                                                 @if($data['hr']->bio_waste_man=="No")
                                                         selected="selected"
                                                 @endif
                                                 >No</option>
                                             </select>
                                         </div>
                                     </div>
                                      <div class="col">
                                        <label class="col-md-12">Date for Bio Waste management </label>
                                       <div class="col-md-12">
                                         <input type="text" placeholder="dd-mm-yy" name="date_biowaste" value="{{ old('date_biowaste', $data['hr']->date_biowaste) }}" class="form-control datepicker" >

                                       </div>


                                      </div>
                                  </div>

                                    <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">Liquid Culture Training (MGIT 960) </label>
                                           <div class="col-md-12">

                                                <select name="lc" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->lc=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->lc=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date for Liquid Culture Training </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_lc" value="{{ old('date_lc', $data['hr']->date_lc) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">Solid Culture (LJ) training</label>
                                           <div class="col-md-12">

                                                <select name="lj" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->lj=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->lj=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date for Liquid Culture Training </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_lj" value="{{ old('date_lj', $data['hr']->date_lj) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">GeneXpert </label>
                                            <div class="col-md-12">

                                                <select name="geneXpert" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->geneXpert=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->geneXpert=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of GeneXpert </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_GeneXpert" value="{{ old('date_GeneXpert', $data['hr']->date_GeneXpert) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">DST (LC) First Line </label>
                                           <div class="col-md-12">

                                                <select name="dst" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->dst=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->dst=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of DST (LC) First Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_dst" value="{{ old('date_dst', $data['hr']->date_dst) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">DST (LC) Second Line </label>
                                           <div class="col-md-12">

                                                <select name="dst_lc_2" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->dst_lc_2=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->dst_lc_2=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of DST (LC) Second Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_dst_lc_2" value="{{ old('date_dst_lc_2', $data['hr']->date_dst_lc_2) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">DST (LJ) First Line </label>
                                           <div class="col-md-12">

                                                <select name="dst_lj_1" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->dst_lj_1=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->dst_lj_1=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of DST (LJ) First Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_dst_lj_1" value="{{ old('date_dst_lj_1', $data['hr']->date_dst_lj_1) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">DST (LJ) Second Line </label>
                                           <div class="col-md-12">

                                                <select name="dst_lj_2" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->dst_lj_2=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->dst_lj_2=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of DST (LJ) Second Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_dst_lj_2" value="{{ old('date_dst_lj_2', $data['hr']->date_dst_lj_2) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">

                                        <div class="col ">
                                             <label class="col-md-12">LPA 1st Line </label>
                                            <div class="col-md-12">

                                                <select name="lpa" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->lpa=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->lpa=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of LPA 1st Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_lpa" value="{{ old('date_lpa', $data['hr']->date_lpa) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">LPA 2st Line </label>
                                            <div class="col-md-12">

                                                <select name="lpa_2" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->lpa_2=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->lpa_2=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of LPA 2st Line </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_lpa_2" value="{{ old('date_lpa_2', $data['hr']->date_lpa_2) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">Microscopy </label>
                                            <div class="col-md-12">
                                                <select name="microscopy" class="form-control form-control-line" >
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->microscopy=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->microscopy=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                          <label class="col-md-12">Date of Microscopy </label>
                                         <div class="col-md-12">
                                           <input type="text" placeholder="dd-mm-yy" name="date_microscopy" value="{{ old('date_microscopy', $data['hr']->date_microscopy) }}" class="form-control datepicker" >
                                         </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col ">
                                             <label class="col-md-12">Other </label>
                                            <div class="col-md-12">
                                                <select name="other" class="form-control form-control-line" id="other">
                                                    <option value="">--Select--</option>
                                                    <option value="Yes"
                                                    @if($data['hr']->other=="Yes")
                                                            selected="selected"
                                                    @endif
                                                    >Yes</option>
                                                    <option value="No"
                                                    @if($data['hr']->other=="No")
                                                            selected="selected"
                                                    @endif
                                                    >No</option>
                                                </select>
                                            </div>
                                        </div>

                                          <div class="col hide" id="other_view1">
                                            <label class="col-md-12">Other training name</label>
                                           <div class="col-md-12">
                                             <input type="text" name="name_other" value="{{ old('name_other', $data['hr']->name_other) }}" class="form-control" >
                                           </div>
                                          </div>
                                          <div class="col hide" id="other_view2">
                                            <label class="col-md-12">Date of other training </label>
                                           <div class="col-md-12">
                                             <input type="text" placeholder="dd-mm-yy" name="date_other" value="{{ old('date_other', $data['hr']->date_other) }}" class="form-control datepicker" >
                                           </div>
                                          </div>

                                    </div>






                                </div>
                            </div>
                        </div>





                        <div class="col-12">

                            <button class="btn btn-info">Save</button>
                            <a class="btn btn-warning" href="{{url('/hr')}}">Cancel</a>
                        </div>

                    </div>
                </form>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>

<script type="text/javascript">
  $(document).ready(function() {

    var _sample = $('#org_source').find(":selected").val();
    if(_sample=='Others'){
      $("#org_source_other").removeClass("hide");
    }else{
      $("#org_source_other").addClass("hide");
    }
  });
  $(function () {
        $("#org_source").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample=='Others'){
              $("#org_source_other").removeClass("hide");
              document.getElementById("org_source_other").setAttribute("required","required");
            }else{
              $("#org_source_other").addClass("hide");
              document.getElementById("org_source_other").removeAttribute("required","required");
            }
        });
        $("#other").change(function(){
          var _sample = $(this).val();
          console.log(_sample);
          if(_sample=='Yes'){
            $("#other_view1").removeClass("hide");
            $("#other_view2").removeClass("hide");
            //document.getElementById("name_other").setAttribute("required","required");
          }else{
            $("#other_view1").addClass("hide");
            $("#other_view2").addClass("hide");
          }
        });
        $(document).on("change", "#designation" , function() {
          var _sample = $(this).val();
          if(_sample=='Others'){
            $("#other_designation").removeClass("hide");
            document.getElementById("other_designation").setAttribute("required","required");
          }else{
            $("#other_designation").addClass("hide");
          }
        });

  });
</script>

@endsection
