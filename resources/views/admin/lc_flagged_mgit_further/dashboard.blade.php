@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">LC Reporting</h3>

                  </div>
              </div>
              @include('admin/lc_flagged_mgit_further/furtherpopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                  <table class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th>Sample ID</th>
                                          <th>MGIT Tube sequence ID</th>
                                          <th>Date of Inoculation</th>
                                          <th>Initial Smear  result</th>
                                          <th>DX/FU</th>
                                          <th>GU</th>
                                          <th>Date of flagging  by MGIT</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>{{$data['sample']->samples}}</td>
                                          <td>{{$data['sample']->mgit_id}}</td>
                                          <td>{{$data['sample']->inoculation_date}}</td>
                                          <td>{{$data['sample']->result}}</td>
                                          <td>{{$data['sample']->reason}}</td>
                                          <td>{{$data['sample']->gu}}</td>
                                          <td>{{$data['sample']->flagging_date}}</td>
                                        </tr>

                                    </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                  <table class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th>ICT</th>
                                          <th>Smear from culture</th>
                                          <th>BHI</th>
                                          <th>Final result</th>
                                          <th>Date of LC result</th>
                                          <th>Result</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <tr>
                                          <td>{{$data['sample']->ict}}</td>
                                          <td>{{$data['sample']->culture_smear}}</td>
                                          <td>{{$data['sample']->bhi}}</td>
                                          <td>{{$data['sample']->final_result}}</td>
                                          <td>{{$data['sample']->result_date}}</td>
                                          <td>
                                            @if($data['sample']->status==1)
                                            <button onclick="openForm('{{$data['sample']->samples}}', {{$data['sample']->log_id}}, '{{$data['sample']->lpa_type}}')",  value="" type="button" class = "btn btn-default btn-sm resultbtn">Submit</button>
                                            @elseif($data['sample']->status==4)
                                            Sent to decontamination
                                            @else
                                            Sent for review
                                            @endif
                                          </td>
                                        </tr>

                                    </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>

<script>
function openForm(sample_label, log_id, lpa_type){
  $('#sample_id').val(sample_label);
  $('#log_id').val(log_id);
  $('#extractionpopupDiv').modal('toggle');
  $(".datep").datepicker({
      dateFormat: "dd/mm/yyyy"
  }).datepicker("setDate", "0");
  if(lpa_type == 'LJ'){
    $("#tube_id_lc").attr("disabled", true);
  }
  if(lpa_type == 'LC'){
    $("#tube_id_lj").attr("disabled", true);
  }
}
function openNextForm(sample_label, log_id, enroll_id){
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#enroll_id').val(enroll_id);
  $('#nextpopupDiv').modal('toggle');
}
</script>


@endsection
