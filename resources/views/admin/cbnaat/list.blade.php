@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">CBNAAT </h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/cbnaat/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="no_sample" class="form-control form-control-line sampleId" value="" id="no_sample">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>

              </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12 " >

                                  <div class="table-scroll">
                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12 " cellspacing="0" >
                                      <thead>
                                          <tr>
                                            <th class="hide">ID</th>
                                            <th>Enrollment ID</th>
                                            <th>Sample ID</th>
                                            <th>Visual Appearance</th>
                                            <th>Date of Receipt</th>
                                            <th>Samples submitted</th>
                                            <th>Sample Type</th>
                                            <th>Result MTB</th>
                                            <th>Result RIF</th>
                                            <th>Date Tested</th>
                                            <th>Next Step</th>
                                            <!-- <th>Action</th> -->
                                          </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <td class="hide">{{$samples->ID}}</td>
                                          <td>{{$samples->enroll_label}}</td>
                                          <td>{{$samples->samples}}</td>
                                          <td>{{$samples->sample_quality}}</td>
                                          <td>{{$samples->receive}}</td>
                                          <td>{{$samples->no_of_samples}}</td>
                                          <td>{{$samples->sample_type}}</td>
                                          @if($samples->result_MTB)
                                          <td>{{$samples->result_MTB}}</td>
                                          @else
                                          <td>Pending</td>
                                          @endif
                                          @if($samples->result_RIF)
                                          <td>{{$samples->result_RIF}}</td>
                                          @else
                                          <td>Pending</td>
                                          @endif
                                          @if($samples->test_date)
                                          <td>{{$samples->test_date}}</td>
                                          @else
                                          <td>Pending</td>
                                          @endif

                                          <td>
                                            @if($samples->STATUS == 1)
                                            <button type="button" onclick="openCbnaatForm({{$samples->enroll_id}},'{{$samples->samples}}','{{$samples->result_MTB}}','{{$samples->result_RIF}}','{{$samples->next_step}}','{{$samples->error}}','{{$samples->no_sample}}')"  class="btn btn-info btn-sm resultbtn" >Submit</button>
                                            @else
                                            Done
                                            @endif
                                          </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
</div>

<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">CBNAAT Details</h4>
        </div>
         <form class="form-horizontal form-material" action="{{ url('/cbnaat') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
		        <div class="modal-body">

		           	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		           	<input type="hidden" name="enrollId" id="enrollId" value="">

		            <label class="col-md-12">Sample ID</label>
                    <div class="col-md-12">
                       <!-- <select class="form-control form-control-line sampleId" name="sampleid" id="sampleid">

                       </select> -->
                       <input type="text" name="sampleid" class="form-control form-control-line sampleid"  id="sampleid" readonly>
                   </div>

                   	<label class="col-md-12">Result of MTB:</label>
		            <div class="col-md-12">
		              <select name="mtb" id="mtb" class="form-control form-control-line" id="mtb" required>
		                <option>--Select--</option>
		                <option value="MTB Detected">MTB Detected</option>
		                <option value="MTB Not Detected">MTB Not Detected</option>
		                <option value="Invalid">Invalid</option>
		                <option value="Error">Error</option>
                    <option value="No Result">No Result</option>
                    <option value="NA">NA</option>
		              </select>
		           </div>

		           <div id="error" class="hide">
		           	 <label class="col-md-12">Error:</label>
		            <div class="col-md-12">
                       <input type="number" name="error" value="" class="form-control form-control-line">
                   </div>
               		</div>


		            <label class="col-md-12">Result of RIF:</label>
		            <div class="col-md-12">
		              <select name="rif" id="rif" class="form-control form-control-line" id="rif" required>
		                <option>--Select--</option>
		                <option value="RIF Detected">RIF Resistance Detected</option>
		                <option value="RIF Not Detected">RIF Resistance Not Detected</option>
		                <option value="RIF Indeterminate">RIF Indeterminate</option>
		                <option value="NA">NA</option>
		              </select>
		           </div>

		          <!--  <label class="col-md-12">Date Tested: {{$data['today']}}</label>
		           <div></div> -->
		           <label class="col-md-12">Date Tested</label>
                    <div class="col-md-12">
                       <input type="text" name="test_date" value="<?php echo date("Y-m-d");?>" max="<?php echo date("Y-m-d");?>" class="form-control form-control-line datepicker" >
                   </div>
		           <label class="col-md-12">Next Step: </label>
		            <div class="col-md-12">
		              <select name="next_step" class="form-control form-control-line" id="next_step" required>
		                <option value="">--Select--</option>
                    <option value="Interim Report Submit another sample">Interim Report Submit another sample</option>
                    <option value="Repeat Test with another sample">Repeat Test with another sample</option>
		                <option value="Repeat Test with same sample">Repeat Test with same sample</option>
		                <option value="Submit result for finalization">Submit result for finalization</option>
                    <option value="Send to BWM">Send to BWM</option>
		              </select>
		           </div>

		        </div>
		        <div class="modal-footer">
		          <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
		          <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
        		  <button type="submit" class="pull-right btn btn-primary btn-md" id="confirm">Ok</button>
		        </div>

		  </form>
      </div>
    </div>
 </div>

<script>
$(function(){

        $('#next_step').on('change', function (e) {
          var service = $("#next_step").val();
          var no_sample = $("#no_sample").val();
          if(service=='Repeat Test with another sample' && no_sample=='0'){
            alert("standby sample not available");
            $("#next_step").val('');
          }

         });
        $("#mtb").change(function(){
            var _sample = $("#mtb").val();

            if(_sample == 'Error' || _sample == 'No Result'){
                document.getElementById("rif").value = "";
                document.getElementById("rif").setAttribute("disabled","disabled");
            }else{
                document.getElementById("rif").removeAttribute("disabled","disabled");
            }


        });

        $("#confirm").change(function(){
            var _sample = $("#mtb").val();
            if(_sample == 'Error' || _sample == 'No Result'){
                document.getElementById("rif").removeAttribute("disabled","disabled");
            }
            else {
              document.getElementById("rif").addAttribute("disabled","disabled");
            }


        });




	$("#mtb").change(function(){

    	if($( "#mtb option:selected" ).text()=='Error'){
    		$('#error').removeClass('hide');
    	}
    	else{
    		$('#error').addClass('hide');
    	}
  	});

	$(".resultbtn").click(function(){
    	$('#sample_id').val($(this).val());
  	});

  	$('#confirmDelete').on('show.bs.modal', function (e) {

		// Pass form reference to modal for submission on yes/ok
		var form = $(e.relatedTarget).closest('form');
		$(this).find('.modal-footer #confirm').data('form', form);
	});

	/* Form confirm (yes/ok) handler, submits form*/

});
// function openPrintModal(obj){
//   //console.log(obj.attr('data-sample'));
//   var samples = obj.attr('data-sample');
//   $.ajax({
//     method: "GET",
//     url: "{{url('cbnaat/submit/')}}"+'/'+samples,
//     data: { samples: samples }
//   }).done(function( msg ) {
//     $("#printCode").html(msg)
//     $('#myModal').modal('toggle');
//   });

// }




 function openCbnaatForm(enroll_id, sample_ids, mtb, rif, next_step, error, no){
 	//console.log("sample_ids", sample_ids.split(','));
 	$("#enrollId").val(enroll_id);
 	$("#mtb").val(mtb);
 	$("#rif").val(rif);
 	$("#error").val(error);
 	$("#next_step").val(next_step);
  $('#no_sample').val(no);
     $("#sampleid").val(sample_ids);
 // 	var sampleArray = sample_ids.split(',');
 // 	$('#sampleid option').remove();
 // 	$.each(sampleArray, function (i, item) {
	//     $('#sampleid').append($('<option>', {
	//         text : item
	//     }));
	// });

 	$('#myModal').modal('toggle');
 }
</script>






@endsection
