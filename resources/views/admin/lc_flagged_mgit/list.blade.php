@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">LC Flagged MGIT Tube</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/lcflag/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
              @include('admin/lc_flagged_mgit/cipopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                  <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th class="hide">ID</th>
                                          <th>Sample ID</th>
                                          <th>MGIT Tube sequence ID</th>
                                          <th>Date of Inoculation</th>
                                          <th>Initial Smear  result</th>
                                          <th>DX/FU/EQA</th>
                                          <th>Follow up month</th>
                                          <th>GU</th>
                                          <th>Date of flagging  by MGIT</th>
                                          <th>Results</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <td class="hide">{{$samples->ID}}</td>
                                          <td>{{$samples->samples}}</td>
                                          <!-- <td>
                                            @if($samples->tube_id_lj)
                                            {{$samples->tube_id_lj}}
                                            @else
                                            {{$samples->tube_id_lc}}
                                            @endif
                                          </td> -->

                                          <td>{{$samples->mgit_id}}</td>
                                          <td>{{$samples->inoculation_date}}</td>
                                          <td>{{$samples->result}}</td>
                                          <td>{{$samples->reason}}</td>
                                          <td>{{$samples->fu_month}}</td>
                                          <td>{{$samples->gu}}</td>
                                          <td>{{$samples->flagging_date}}</td>
                                          <td>
                                            @if($samples->status==1)
                                            <button onclick="openForm('{{$samples->samples}}', {{$samples->log_id}}, '{{$samples->lpa_type}}','{{$samples->gu}}','{{$samples->flagging_date}}')",  value="" type="button" class = "btn btn-default btn-sm resultbtn">Submit</button>
                                            @else
                                            Done
                                            @endif
                                          </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>

<script>
  $(function(){



  });
  function openForm(sample_label, log_id, lpa_type, gu, flagging_date){
    $('#sample_id').val(sample_label);
    $('#log_id').val(log_id);
    $('#gu').val(gu);
    $('#flagging_date').val(flagging_date);
    $('#extractionpopupDiv').modal('toggle');
    $(".datep").datepicker({
        dateFormat: "dd/mm/yyyy"
    }).datepicker("setDate", "0");
    if(lpa_type == 'LJ'){
      $("#tube_id_lc").attr("disabled", true);
    }
    if(lpa_type == 'LC'){
      $("#tube_id_lj").attr("disabled", true);
    }
  }
  function openNextForm(sample_label, log_id, enroll_id){
    $('#next_sample_id').val(sample_label);
    $('#next_log_id').val(log_id);
    $('#enroll_id').val(enroll_id);
    $('#nextpopupDiv').modal('toggle');
  }
</script>


@endsection
