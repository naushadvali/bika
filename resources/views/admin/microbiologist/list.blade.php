
@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Microbiologist</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/microbiologist/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>

              </div>

              @include('admin/resultpopup/cbnaat_editResult')
              @include('admin/resultpopup/editresults')
              @include('admin/resultpopup/microscopy_editResult')
              @include('admin/resultpopup/lc_editResult')
              @include('admin/resultpopup/lj_editResult')
              @include('admin/resultpopup/lc_dst_editResult')
              @include('admin/resultpopup/ljdst1_editResult')
              @include('admin/resultpopup/ljdst2_editResult')
              <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                        <div class="card" >
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                      <style>
                                      table, th, td {
                                          border: 1px solid black;
                                      }
                                      </style>
                                      <table style="width:100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Test Name</th>
                                              <th>Tests to be reviewed</th>
                                              <th>Tests reviewed</th>

                                            </tr>

                                        </thead>
                                        <tbody>

                                         @foreach ($data['ret'] as $key=> $values)
                                          <tr>
                                            <td class="hide">{{$values['todo']->ID}}</td>
                                            <td>{{$values['todo']->name}}</td>

                                            <td>
                                                  @if($values['todo'])
                                                    {{$values['todo']->cnt}}
                                                  @else
                                                    0
                                                  @endif

                                            </td>
                                            <td>
                                                   @if($values['done'])
                                                      {{$values['done']->cnt}}
                                                   @else
                                                     0
                                                   @endif
                                            </td>
                                         </tr>
                                         @endforeach

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>




               <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">


                                    <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-expanded="true">CURRENT</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2" role="tab">HISTORY</a>
                                    </li>


                              </ul>
                          </div>
              </div>

                <div class="row tab-content" style="display:block;">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 tab-pane active" id="tab1" role="tabpanel" aria-expanded="true">
                        <div class="card" >
                            <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                                <div class="scroll-table scroll-table-micro" >

                                    <table id="example1" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Patient Name </th>
                                              <th>Referal Facility</th>
                                              <th>Date of Receipt</th>
                                              <th>Sample type</th>
                                              <th>Test To  Review</th>
                                              <!-- <th>Detail</th>
                                              <th>Remark</th> -->
                                              <th>Choose Drugs</th>
                                              <th>Result</th>
                                              <th>Edit Result</th>
                                              <th>Next Step</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                          @if($data['sample'])
                                            @foreach ($data['sample'] as $key=> $samples)
                                                  <tr>
                                                    <td class="hide">{{$samples->ID}}</td>
                                                    <td>{{$samples->enroll_l}}</td>
                                                    <td>{{$samples->samples}}</td>
                                                    <td>{{$samples->p_name}}</td>
                                                    <td>{{$samples->facility_id}}</td>
                                                    <td><?php echo date('d/m/Y', strtotime($samples->date)); ?></td>
                                                    <td>{{$samples->sample_type}}</td>
                                                    <!-- <td><a href={{$samples->url}}>{{$samples->service_name}}</a></td> -->
                                                    <td>{{$samples->service_name}}</td>
                                                    <!-- <td>{{$samples->m_detail}}</td>
                                                    <td>{{$samples->m_remark}}</td> -->
                                                    <td>

                                                    <button type="button" onclick="openCbnaatFormDrug({{$samples->enroll}},'{{$samples->sample}}','{{$samples->service}}','{{$samples->samples}}')" class="btn btn-info btn-sm resultbtn" >Choose Drugs</button>

                                                    </td>
                                                    <td>

                                                    <button type="button" onclick="openCbnaatForm1({{$samples->enroll}},'{{$samples->sample}}','{{$samples->service}}','{{$samples->samples}}')" class="btn btn-info btn-sm resultbtn" >View</button>

                                                    </td>
                                                    <td>
                                                    @if($samples->service_name!='BWM')
                                                    <a href="javascript:void(0);" onclick="editResultForm('{{$samples->sampleID}}',{{$samples->enroll}},'{{$samples->samples}}','{{$samples->service}}','{{$samples->tag}}','{{$samples->druglist}}','{{$samples->drug_ids}}')">Edit</a>
                                                    @endif
                                                    </td>
                                                    <td>

                                                    <button type="button" onclick="openCbnaatForm({{$samples->enroll}},'{{$samples->samples}}','{{$samples->service}}',{{$samples->sampleID}},'{{$samples->bwm_status}}','{{$samples->no_sample}}')" class="btn btn-info btn-sm resultbtn" >Submit</button>

                                                    </td>


                                                </tr>

                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 tab-pane" id="tab2" role="tabpanel" aria-expanded="true">
                        <div class="card" >
                            <div class="card-block col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12">
                                <div class="scroll-table" >

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Enrollment ID</th>
                                              <th>Sample ID</th>
                                              <th>Patient Name </th>
                                              <th>Referal Facility</th>
                                              <th>Date of Receipt</th>
                                              <th>Sample type</th>
                                              <!-- <th>Test To  Review</th> -->
                                              <th>Detail</th>
                                              <th>Remark</th>
                                              <!-- <th>Next Step</th> -->


                                            </tr>
                                        </thead>
                                        <tbody>
                                          @if($data['done'])
                                            @foreach ($data['done'] as $key=> $samples)
                                                  <tr>
                                                    <td class="hide">{{$samples->ID}}</td>
                                                    <td>{{$samples->enroll_l}}</td>
                                                    <td>{{$samples->samples}}</td>
                                                    <td>{{$samples->p_name}}</td>
                                                    <td>{{$samples->facility_id}}</td>
                                                    <td><?php echo date('d/m/Y', strtotime($samples->date)); ?></td>
                                                    <td>{{$samples->sample_type}}</td>
                                                    <!-- <td><a href={{$samples->url}}>{{$samples->service_name}}</a></td> -->
                                                    <td>{{$samples->detail}}</td>
                                                    <td>{{$samples->remark}}</td>
                                                    <!-- <td>{{$samples->next}}</td> -->




                                                </tr>

                                          @endforeach
                                        @endif
                                      </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>
        <div class="modal fade" id="myModal_drug" role="dialog"  id="confirmDelete">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Microbiologist</h4>
                </div>

                 <form class="form-horizontal form-material" action="{{ url('/microbiologist') }}" method="post" enctype='multipart/form-data' id="drugform">
                          @if(count($errors))
                            @foreach ($errors->all() as $error)
                               <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                           @endforeach
                         @endif
                    <div class="modal-body">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="enrollId1" id="enrollIddrug" value="">
                        <input type="hidden" name="service1" id="servicedrug" value="">
                        <input type="hidden" name="type" id="typedrug" value="drug">


                        <label class="col-md-12"><h5>Sample ID:</h5></label>
                            <div class="col-md-12">
                              <input type="text" name="sampleid1" class="form-control form-control-line sampleId"  id="sample-iddrugs">

                           </div>
                        <br>
                        <div class="row">
                          @foreach ($data['services'] as $key=> $services)
                          <div class="col-md-4 top5px">
                            <input class="service_array"
                              value="{{$services['id']}}"
                              @if(isset($data['_reqservices']) && is_array($data['_reqservices']) && in_array($services['id'], $data['_reqservices']))
                                checked=""
                              @endif
                              name="services[]"
                              type="checkbox">{{$services['name']}}
                          </div>
                          @endforeach
                        </div>
                        <div class="dst_drugs hide">
                          <div class="row  ">

                            <label class="col-md-12">DST Drugs</label>
                            @foreach ($data['dstdrugs'] as $key=> $drugs)
                            <div class="col-md-4 ">
                              <input class="drugs_array"
                                value="{{$drugs['id']}}"

                                name="drugs[]"
                                type="checkbox">{{$drugs['name']}}
                            </div>
                            @endforeach

                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="pull-right btn btn-primary btn-md" id="confirmdrugs" data-dismiss="modal">Ok</button>
                    </div>

              </form>
              </div>
            </div>
         </div>
<div class="modal fade" id="myModal1" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Microbiologist</h4>
        </div>

         <form class="form-horizontal form-material" action="{{ url('/microbiologist') }}" method="post" enctype='multipart/form-data' id="cbnaat_result1">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
            <div class="modal-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="enrollId" id="enrollId" value="">
                <input type="hidden" name="service" id="service" value="">
                <input type="hidden" name="type" id="type" value="1">


                <label class="col-md-12"><h5>Sample ID:</h5></label>
                    <div class="col-md-12">
                      <input type="text" name="sampleid" class="form-control form-control-line sampleId"  id="sample-id">

                   </div>
                <br>
                <label class="col-md-12"></label>
                    <div class="col-md-12">
                       <div id="resultData"></div>
                   </div>
                <br>
            </div>
            <div class="modal-footer">
              <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
              <!-- <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button> -->
              <button type="button" class="pull-right btn btn-primary btn-md hide" id="confirm1" data-dismiss="modal">Ok</button>
            </div>

      </form>
      </div>
    </div>
 </div>
<div class="modal fade" id="myModal" role="dialog"  id="confirmDelete">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Microbiologist</h4>
        </div>

         <form class="form-horizontal form-material" action="{{ url('/microbiologist') }}" method="post" enctype='multipart/form-data' id="cbnaat_result">
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
            <div class="modal-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="enrollId1" id="enrollId1" value="">
                 <input type="hidden" name="sample" id="sampleID" value="">
                <input type="hidden" name="service1" id="service1" value="">
                <input type="hidden" name="bwm_status" id="bwm_status" value="">
                 <input type="hidden" name="no_sample" class="form-control form-control-line sampleId" value="" id="no_sample">


                <label class="col-md-12"><h5>Sample ID:</h5></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="sampleid1" id="sampleid1" value="">

                       </select>
                   </div>
                <br>
                <!-- <label class="col-md-12"><h5>Service ID:</h5></label>
                    <div class="col-md-12">
                       <input type="number" class="form-control form-control-line sampleId" name="service" id="service" disabled>

                       </input>
                   </div>
                <br> -->
                <div id="nextStep_div">
                <label  class="col-md-12"><h5>Next Step:</h5></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="nextStep" value="" id="nextStep">
                        <option value="">--select--</option>
                        <!-- <option value="Push to  Nikshay">Push to  Nikshay</option> -->
                        <!-- <option value="Print Result">See Result</option> -->
                        <!-- <option value="Print 15A form">Print 15A form</option> -->
                        <option value="Request for Retest">Request for Retest</option>


                       </select>
                   </div>
                 </div>
                <br>
                <label class="col-md-12"><h5>Print 15A form : </h5></label>
                    <div class="col-md-12">
                       <input type="checkbox"  name="print15A" id="print15A">

                       </input>
                   </div>
                <br>
                <label class="col-md-12"><h5>Detail:</h5></label>
                    <div class="col-md-12">
                       <input type="text" class="form-control form-control-line sampleId" name="detail" id="detail">

                       </input>
                   </div>
                <br>
                <label class="col-md-12"><h5>Remark:</h5></label>
                    <div class="col-md-12">
                       <input type="text" class="form-control form-control-line sampleId" name="remark" id="remark">

                       </input>
                   </div>
                <br>
                <div id="reason" class="hide">
                <label class="col-md-12"><h5>Reason for sending sample to BWM</h5></label>
                    <div class="col-md-12">
                       <select class="form-control form-control-line sampleId" name="reason_bwm" id="reason_bwm">
                        <option value="">--select--</option>
                        <option value="Sample was drained during processes ">Sample was drained during processes </option>
                        <option value="Sample fall down ">Sample fall down </option>
                        <option value="Tube breakage during centrifugation"> Tube breakage during centrifugation</option>
                        <option value="Sample cross contaminated during processing ">Sample cross contaminated during processing </option>
                        <option value="Other">Other</option>
                       </select>
                   </div>

                 </div>
                 <div class="col hide" id="reason_other">
                     <label class="col-md-12"><h5>Reason for sending sample to BWM : Other </h5></label>
                     <div class="col-md-12">
                        <input type="text" class="form-control form-control-line" name="reason_other">
                    </div>
                 </div>



            </div>
            <div class="modal-footer">
              <!-- <button type="submit" class="btn btn-default" data-dismiss="modal">Save</button> -->
              <button type="button" class="btn btn-default add-button cancel btn-md" data-dismiss="modal">Cancel</button>
              <button type="button" class="pull-right btn btn-primary btn-md" id="confirmok" >Ok</button>
            </div>

      </form>
      </div>
    </div>
 </div>
<script>
function editResultForm(sample_id, enroll_id, sample_label,service,tag,druglist,drugid){
  $(function(){

    if(service==23){
      $("#enrollIdljdst2").val(enroll_id);
      $("#sampleidljdst2").val(sample_label);
      drugs=druglist.split(',');
      drugs_id=drugid.split(',');
      var text="";
       for(i=0;i<drugs.length;i++){
            text=text+" <div ><div class='col drug_names_ljdst'>"+
                "<label class='col-md-12'>"+drugs[i]+"</label>"+
                "<input type='hidden' name='drugname2[]' value='"+drugs[i]+"'' />"+
                "<div class='col-md-12'>"+
                   "<select  name='drugvalue2[]' id='drug"+i+"' class='form-control form-control-line' >"+
                     "<option value=''>--Select Result--</option>"+
                     "@foreach ($data['dp_result'] as $key => $result)"+
                      "<option value='{{$data['dp_result_value'][$key]}}'>{{$result}}</option>"+
                     "@endforeach"+
                   "</select>"+
               "</div>"+
            "</div>";
       }
      document.getElementById("drug_names_ljdst2").innerHTML=text;
      $(".datepi").datepicker({
          dateFormat: "dd/mm/yyyy"
      }).datepicker("setDate", "0");
      $.ajax({
            url: "{{url('edit_result_lj_dst2')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                //$("#ict").val(JSON.parse(data).ict);
            },
            error: function() {
              console.log("err")
          }
        });
        $.ajax({
              url: "{{url('edit_result_lj_dst2')}}"+'/'+sample_id,
              type:"GET",
              processData: false,
              contentType: false,
              dataType : "html",
              success: function(data) {
                    var result = JSON.parse(data);
                    var resultDrug = JSON.parse(result.drug_reading);
                    var drug = resultDrug.dil_4;
                    for(var i=0; i<drug.length; i++){
                      console.log(drug[i].value);
                      $("#drug"+i).val(drug[i].value);
                    }

              },
              error: function() {
                console.log("err");
            }
          });
      $('#ljdst2_editResult').modal('toggle');
    }
    else if(service==22){
      $("#enrollIdljdst1").val(enroll_id);
      $("#sampleidljdst1").val(sample_label);
      drugs=druglist.split(',');
      drugs_id=drugid.split(',');
      var text="";
       for(i=0;i<drugs.length;i++){
            text=text+" <div ><div class='col drug_names_ljdst'>"+
                "<label class='col-md-12'>"+drugs[i]+"</label>"+
                "<input type='hidden' name='drugname[]' value='"+drugs[i]+"'' />"+
                "<div class='col-md-12'>"+
                   "<select  name='drugvalue[]' id='"+i+"' class='form-control form-control-line' >"+
                     "<option value=''>--Select Result--</option>"+
                     "@foreach ($data['dp_result'] as $key => $result)"+
                      "<option value='{{$data['dp_result_value'][$key]}}'>{{$result}}</option>"+
                     "@endforeach"+
                   "</select>"+
               "</div>"+
            "</div>";
       }
      document.getElementById("drug_names_ljdst").innerHTML=text;
      $(".datepi").datepicker({
          dateFormat: "dd/mm/yyyy"
      }).datepicker("setDate", "0");
      $.ajax({
            url: "{{url('edit_result_lj_dst1')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                var result = JSON.parse(data);
                var resultDrug = JSON.parse(result.drug_reading);
                  var drug = resultDrug.dil_4;
                  for(var i=0; i<drug.length; i++){
                    console.log(drug[i].value);
                    //$("#drug"+i).val(drug[i].value);
                    $("#"+i).val(drug[i].value);
                  }

            },
            error: function() {
              console.log("err");
          }
        });
      $('#ljdst1_editResult').modal('toggle');
    }
    else if(service==21){

      $("#enrollIdlcdst").val(enroll_id);
      $("#sampleidlcdst").val(sample_label);
      drugs=druglist.split(',');
      drugs_id=drugid.split(',');
      var text="";
       for(i=0;i<drugs.length;i++){
            text=text+" <div ><div class='col'>"+
                "<label class='col-md-12'>"+drugs[i]+"</label>"+
                "<div class='col-md-12'>"+
                   "<select name='"+drugs[i]+"' id='"+i+"' class='form-control form-control-line' >"+
                     "<option value=''>--Select Result--</option>"+
                     "@foreach ($data['dp_result'] as $key => $result)"+
                      "<option value='{{$result}}'>{{$result}}</option>"+
                     "@endforeach"+
                   "</select>"+
               "</div>"+
            "</div>";
       }

      document.getElementById("drug_names").innerHTML=text;
      $(".datepi").datepicker({
          dateFormat: "dd/mm/yyyy"
      }).datepicker("setDate", "0");
      $.ajax({
            url: "{{url('edit_result_lc_dst')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                result = JSON.parse(data);
                console.log(result);
                for(var i=0; i<result.length; i++){
                  $("#"+i).val(result[i].result);
                }

            },
            error: function() {
              console.log("err")
          }
        });
      $('#lc_dst_editResult').modal('toggle');

    }
    else if(service==20){

      $("#enrollIdlj").val(enroll_id);
      $("#sampleidlj").val(sample_label);
      $.ajax({
            url: "{{url('edit_result_lj')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                $("#test_id").val(JSON.parse(data).test_id);
                $("#culture_smear_lj").val(JSON.parse(data).culture_smear);
                $("#final_result_lj").val(JSON.parse(data).final_result);
            },
            error: function() {
              console.log("err")
          }
        });
      $('#lj_editResult').modal('toggle');

    }
    else if(service==17){

      $("#enrollIdlc").val(enroll_id);
      $("#sampleidlc").val(sample_label);
      $(".datep").datepicker({
          dateFormat: "dd/mm/yyyy"
      }).datepicker("setDate", "0");

      $.ajax({
            url: "{{url('edit_result_lc')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                $("#ict").val(JSON.parse(data).ict);
                $("#culture_smear").val(JSON.parse(data).culture_smear);
                $("#bhi").val(JSON.parse(data).bhi);
                $("#resultlc").val(JSON.parse(data).result);
            },
            error: function() {
              console.log("err")
          }
        });

      $('#lc_editResult').modal('toggle');

    }
    else if(service==1 || service==2){

      $("#enrollIdmicro").val(enroll_id);
      $("#sampleidmicro").val(sample_label);
      $("#service_micro").val(service);
      $.ajax({
            url: "{{url('edit_result_micro')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                $("#result_microscopy").val(JSON.parse(data).result);
            },
            error: function() {
              console.log("err")
          }
        });
      $('#microscopy_editResult').modal('toggle');

    }
    else if(service==4){
      $("#enrollId").val(enroll_id);
      $("#sampleid").val(sample_label);

      $.ajax({
            url: "{{url('editResultCbnaat')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                $("#mtb").val(JSON.parse(data).result_MTB);
                $("#error_cbnaat").val(JSON.parse(data).error);
                $("#rif").val(JSON.parse(data).result_RIF);
            },
            error: function() {
              console.log("err")
          }
        });
      $("#mtb").change(function(){
          var _sample = $("#mtb").val();
          if(_sample == 'Error'){
              document.getElementById("rif").value = "";
              document.getElementById("rif").setAttribute("disabled","disabled");
          }else{
              document.getElementById("rif").removeAttribute("disabled","disabled");
          }
      });
      // $("#confirm").change(function(){
      //     var _sample = $("#mtb").val();
      //     if(_sample == 'Error'){
      //         document.getElementById("rif").removeAttribute("disabled","disabled");
      //     }
      // });
      $("#mtb").change(function(){

        	if($( "#mtb option:selected" ).text()=='Error'){
        		$('#error').removeClass('hide');
        	}
        	else{
        		$('#error').addClass('hide');
        	}
      	});
        $('#cbnaatresultpopupDiv').modal('toggle');
    }
    else if(service==15){

      $("#enrollIdlpa").val(enroll_id);
      $("#sampleidlpa").val(sample_label);
        $("#tag").val(tag);

      var _sample = $("#tag").val();

      if(_sample=='1st line LPA'){

        $("#secondLPA").addClass("hide");
        $(".slpa"). removeAttr("required");
        document.getElementById("quinolone").value = "";
        document.getElementById("quinolone").setAttribute("disabled","disabled");
        document.getElementById("slid").value = "";
        document.getElementById("slid").setAttribute("disabled","disabled");
        $("#mtb_result").change(function(){
                var _sample = $("#mtb_result").val();
                if(_sample=='Invalid'){
                  document.getElementById("inh").value = "";
                  document.getElementById("inh").setAttribute("disabled","disabled");
                  document.getElementById("rif").value = "";
                  document.getElementById("rif").setAttribute("disabled","disabled");
                }else{
                        document.getElementById("inh").removeAttribute("disabled","disabled");
                        document.getElementById("rif").removeAttribute("disabled","disabled");
                }
          });
      }
      else if(_sample=='2st line LPA')
      {
        $("#firstLPA").addClass("hide");
        $(".flpa"). removeAttr("required");
        document.getElementById("rif").value = "";
        document.getElementById("rif").removeAttribute("required");
        document.getElementById("rif").setAttribute("disabled","disabled");
        document.getElementById("inh").value = "";
        document.getElementById("inh").removeAttribute("required");
        document.getElementById("inh").setAttribute("disabled","disabled");
        $("#mtb_result").change(function(){
                var _sample = $("#mtb_result").val();
                if(_sample=='Invalid'){
                  document.getElementById("quinolone").value = "";
                  document.getElementById("quinolone").setAttribute("disabled","disabled");
                  document.getElementById("slid").value = "";
                  document.getElementById("slid").setAttribute("disabled","disabled");
                }else{
                        document.getElementById("slid").removeAttribute("disabled","disabled");
                        document.getElementById("quinolone").removeAttribute("disabled","disabled");
                }
          });
      }
      else {
        $("#mtb_result").change(function(){
                var _sample = $("#mtb_result").val();
                if(_sample=='Invalid'){

                  document.getElementById("quinolone").value = "";
                  document.getElementById("slid").value = "";
                  document.getElementById("inh").value = "";
                  document.getElementById("quinolone").setAttribute("dserviceisabled","disabled");
                  document.getElementById("inh").setAttribute("disabled","disabled");
                  document.getElementById("slid").setAttribute("disabled","disabled");
                  document.getElementById("rif").value = "";
                  document.getElementById("rif").setAttribute("disabled","disabled");


                }else{
                        document.getElementById("quinolone").removeAttribute("disabled","disabled");
                        document.getElementById("inh").removeAttribute("disabled","disabled");
                        document.getElementById("slid").removeAttribute("disabled","disabled");
                        document.getElementById("rif").removeAttribute("disabled","disabled");
                }
            });
      }

      $.ajax({
            url: "{{url('editResultLpa')}}"+'/'+sample_id,
            type:"GET",
            processData: false,
            contentType: false,
            dataType : "html",
            success: function(data) {
                $("#RpoB").val(JSON.parse(data).RpoB);
                $("#wt1").val(JSON.parse(data).wt1);
                $("#wt2").val(JSON.parse(data).wt2);
                $("#wt3").val(JSON.parse(data).wt3);
                $("#wt4").val(JSON.parse(data).wt4);
                $("#wt5").val(JSON.parse(data).wt5);
                $("#wt6").val(JSON.parse(data).wt6);
                $("#wt7").val(JSON.parse(data).wt7);
                $("#wt8").val(JSON.parse(data).wt8);
                $("#mut1DS16V").val(JSON.parse(data).mut1DS16V);
                $("#mut2aH526Y").val(JSON.parse(data).mut2aH526Y);
                $("#mut2bH526D").val(JSON.parse(data).mut2bH526D);
                $("#mut3S531L").val(JSON.parse(data).mut3S531L);
                $("#katg").val(JSON.parse(data).katg);
                $("#wt1315").val(JSON.parse(data).wt1315);
                $("#mut1S315T1").val(JSON.parse(data).mut1S315T1);
                $("#mut2S315T2").val(JSON.parse(data).mut2S315T2);
                $("#inha").val(JSON.parse(data).inha);
                $("#wt1516").val(JSON.parse(data).wt1516);
                $("#wt28").val(JSON.parse(data).wt28);
                $("#mut1C15T").val(JSON.parse(data).mut1C15T);
                $("#mut2A16G").val(JSON.parse(data).mut2A16G);
                $("#mut3aT8C").val(JSON.parse(data).mut3aT8C);
                $("#mut3bT8A").val(JSON.parse(data).mut3bT8A);

                $("#gyra").val(JSON.parse(data).gyra);
                $("#wt18590").val(JSON.parse(data).wt18590);
                $("#wt28993").val(JSON.parse(data).wt28993);
                $("#wt39297").val(JSON.parse(data).wt39297);
                $("#mut1A90V").val(JSON.parse(data).mut1A90V);
                $("#mut2S91P").val(JSON.parse(data).mut2S91P);
                $("#mut3aD94A").val(JSON.parse(data).mut3aD94A);
                $("#mut3bD94N").val(JSON.parse(data).mut3bD94N);
                $("#mut3cD94G").val(JSON.parse(data).mut3cD94G);
                $("#mut3dD94H").val(JSON.parse(data).mut3dD94H);
                $("#gyrb").val(JSON.parse(data).gyrb);
                $("#wt1536541").val(JSON.parse(data).wt1536541);
                $("#mut1N538D").val(JSON.parse(data).mut1N538D);
                $("#mut2E540V").val(JSON.parse(data).mut2E540V);
                $("#rrs").val(JSON.parse(data).rrs);
                $("#wt1140102").val(JSON.parse(data).wt1140102);
                $("#wt21484").val(JSON.parse(data).wt21484);
                $("#mut1A1401G").val(JSON.parse(data).mut1A1401G);
                $("#mut2G1484T").val(JSON.parse(data).mut2G1484T);
                $("#eis").val(JSON.parse(data).eis);
                $("#wt137").val(JSON.parse(data).wt137);
                $("#wt2141210").val(JSON.parse(data).wt2141210);
                $("#wt32").val(JSON.parse(data).wt32);
                $("#mut1c14t").val(JSON.parse(data).mut1c14t);

                $("#type").val(JSON.parse(data).type);
                $("#type_direct").val(JSON.parse(data).type_direct);
                $("#type_indirect").val(JSON.parse(data).type_indirect);
                $("#mtb_result").val(JSON.parse(data).mtb_result);
                $("#rif").val(JSON.parse(data).rif);
                $("#inh").val(JSON.parse(data).inh);
                $("#quinolone").val(JSON.parse(data).quinolone);
                $("#slid").val(JSON.parse(data).slid);
            },
            error: function() {
              console.log("err")
          }
        });
      $('#lparesultpopupDiv').modal('toggle');
    }

  });
}

$(function(){

  $('#nextStep').on('change', function (e) {
    var service = $("#nextStep").val();
    var no_sample = $("#no_sample").val();
    var service1 = $("#service1").val();

    if(service=='Request for Retest' && no_sample=='0' && service1=='26'){
      alert("standby sample not available");
      $("#nextStep").val('');
    }
   });

  $('#example1').DataTable( {
       "order": [[ 0, "desc" ]],
       dom: 'Bfrtip',
        buttons: [
           // 'excel', 'pdf',
           'excel'
        ]
   } );




  $(".resultbtn").click(function(){
      $('#sample_id').val($(this).val());
    });

    $('#confirmDelete').on('show.bs.modal', function (e) {

    // Pass form reference to modal for submission on yes/ok
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-footer #confirm').data('form', form);
  });

  /* Form confirm (yes/ok) handler, submits form*/
  $('#confirm1').click( function(){

    var form = $(document).find('form#cbnaat_result1');
      form.submit();
    // console.log( $('#cbnaat_result').serialize() );
    // var data = $('#cbnaat_result').serialize();
    // $.post(window.location.replace("{{ url('/PCR') }}"), data);
    //form.submit();
  });

  $('#confirmdrugs').click( function(){

    var form = $(document).find('form#drugform');
      form.submit();
  });
  // $('#confirm').click( function(){
  //   alert('sgds');
  //   var form = $(document).find('form#cbnaat_result');
  //     form.submit();
  //
  // });
});


 function openCbnaatForm1(enroll_id, sample_id, service, sample_ids){
    //console.log("sample_ids", sample_ids.split(','));
    $("#enrollId").val(enroll_id);
    $("#service").val(service);
    $("#sample-id").val(sample_ids);

    // var sampleArray = sample_ids.split(',');
    // $('#sampleid option').remove();
    // $.each(sampleArray, function (i, item) {
    //     $('#sampleid').append($('<option>', {
    //         text : item
    //     }));
    // });


    //load data
    var details = service + '/' + sample_id + '/' + enroll_id;

    $.ajax({
          url: "{{url('result')}}"+'/'+details,
          type:"GET",
          processData: false,
          contentType: false,
          dataType : "html",
          success: function(data) {

              //console.log('data',data);
              $("#resultData").html(data);
          },
          error: function() {
            console.log("err")
        }
      });


    $('#myModal1').modal('toggle');
 }
 function openCbnaatFormDrug(enroll_id, sample_id, service, sample_ids){
    $("#enrollIddrug").val(enroll_id);
    $("#servicedrug").val(service);
    $("#sample-iddrugs").val(sample_ids);
    $('.service_array').click(function(){
      if($(this).is(':checked')){

        // var index = dst_id.indexOf(parseInt($(this).val()));
        // if(index >= 0){
          console.log("dst checked",$(this).val());
          $(".dst_drugs").show();
        // }
      }else{

        // var index = dst_id.indexOf(parseInt($(this).val()));
        // if(index >= 0){
          console.log("dst unchecked",$(this).val());
          $(".dst_drugs").hide();
        // }
      }
    });
    $('#myModal_drug').modal('toggle');
 }


 function openCbnaatForm(enroll_id, sample_ids, service, sample, bwm_status, no){
  //console.log(enroll_id, sample_ids, service);

  $("#enrollId1").val(enroll_id);
  $("#service1").val(service);
  $("#bwm_status").val(bwm_status);
  $("#sampleID").val(sample);
  $('#no_sample').val(no);


  var sampleArray = sample_ids.split(',');
  $('#sampleid option').remove();
  $.each(sampleArray, function (i, item) {
      $('#sampleid1').append($('<option>', {
          text : item
      }));
  });

  if(service=='15')
  {
    $('#nextStep_div').show();
  }
  else{
    $('#nextStep_div').hide();
  }

  if(service=='3')
  {
    $('#nextStep option').remove();

    $('#nextStep').append($('<option>', {
          text : 'Print 15A form',
          value : 'Print 15A form'
      }));
    $('#nextStep').append($('<option>', {
          text : 'Request for Retest',
          value : 'Request for Retest'
      }));

    $('#nextStep').append($('<option>', {
          text : 'culture inoculation LC',
          value : 17
      }));
    $('#nextStep').append($('<option>', {
          text : 'culture inoculation LJ',
          value : 20
      }));
    $('#nextStep').append($('<option>', {
          text : 'DNA Extraction',
          value : 8
      }));
  }

  if(service=='1')
  {
    $('#nextStep option').remove();

     $('#nextStep').append($('<option>', {
          text : 'Print 15A form',
          value : 'Print 15A form'
      }));
    $('#nextStep').append($('<option>', {
          text : 'Request for Retest',
          value : 'Request for Retest'
      }));

    $('#nextStep').append($('<option>', {
          text : 'Decontamination DNA extraction',
          value : 3
      }));
    $('#nextStep').append($('<option>', {
          text : 'Decontamination for AFB Culture Liquid',
          value : 3
      }));
     $('#nextStep').append($('<option>', {
          text : 'Decontamination for AFB Culture Solid',
          value : 3
      }));
     $('#nextStep').append($('<option>', {
          text : 'Decontamination for Culture LC and LJ both',
          value : 3
      }));
      $('#nextStep').append($('<option>', {
          text : 'CBNAAT',
          value : 4
      }));
       $('#nextStep').append($('<option>', {
          text : 'Sent for storage',
          value : 11
      }));
  }

  if(service=='17')
  {
    $('#nextStep option').remove();

     $('#nextStep').append($('<option>', {
          text : 'Print 15A form',
          value : 'Print 15A form'
      }));
    $('#nextStep').append($('<option>', {
          text : 'Request for Retest',
          value : 'Request for Retest'
      }));

    $('#nextStep').append($('<option>', {
          text : 'DNA Extraction LPA First Line',
          value : 6
      }));
    $('#nextStep').append($('<option>', {
          text : 'DNA Extraction LPA Second Line',
          value : 7
      }));
     $('#nextStep').append($('<option>', {
          text : 'LC DST Inoculation',
          value : 21
      }));


  }
  if(service=='20')
  {
    $('#nextStep option').remove();

     $('#nextStep').append($('<option>', {
          text : 'Print 15A form',
          value : 'Print 15A form'
      }));
    $('#nextStep').append($('<option>', {
          text : 'Request for Retest',
          value : 'Request for Retest'
      }));

    $('#nextStep').append($('<option>', {
          text : 'DNA Extraction LPA First Line',
          value : 6
      }));
    $('#nextStep').append($('<option>', {
          text : 'DNA Extraction LPA Second Line',
          value : 7
      }));

     $('#nextStep').append($('<option>', {
          text : 'LJ DST First Line',
          value : 22
      }));
     $('#nextStep').append($('<option>', {
          text : 'LJ DST Second Line',
          value : 23
      }));


  }

  $('#myModal').modal('toggle');
  if($('#bwm_status').val()=='1'){
    $("#reason").removeClass("hide");
  }
  else {
    $("#reason").addClass("hide");
  }

  $("#reason_bwm").change(function(){
      var _sample = $(this).find(":selected").val();
      if(_sample=='Other'){
        $("#reason_other").removeClass("hide");
        document.getElementById("#reason_other").setAttribute("required","required");
      }else{
        $("#reason_other").addClass("hide");
        document.getElementById("#reason_other").removeAttribute("required","required");
      }
  });

  $("#confirmok").click(function(){
   var next = $("#print15A").val();
     if($('input[name=print15A]:checked').length>0){
       // $("#nextStep").removeAttribute("required");
        var sample = $("#sampleID").val();
        var url = '{{ url("/pdfview", "id") }}';
        // url = url.replace('id', sample+'/1');
        url = url.replace('id', sample);
        // url = url.replace('type', '1');
         window.open(url, '_blank');

     }
     var form = $(document).find('form#cbnaat_result');
       form.submit();
   });


 }

</script>


@endsection
