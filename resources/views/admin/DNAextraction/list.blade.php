@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">DNA Extraction</h3>

                  </div>
                  <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/DNA/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
              @include('admin/DNAextraction/nextsteppopup')
              @include('admin/DNAextraction/extractionpopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Sample ID</th>
                                              <!-- <th>LPA test requested</th> -->
                                              <th>LPA test requested</th>
                                              <th>Samples submitted</th>
                                              <th>Date of Decontamination</th>
                                              <th>Microscopy result</th>
                                              <th>Date of Extraction</th>
                                              <th>Next step</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                          @foreach ($data['sample'] as $key=> $samples)
                                          <tr>
                                            <td class="hide">{{$samples->ID}}</td>
                                            <td>{{$samples->samples}}</td>
                                           <!--  <td></td> -->
                                            <td>
                                              @if($samples->tag == 'LPA1')
                                                LPA 1st Line
                                              @elseif($samples->tag == 'LPA2')
                                                LPA 2st Line
                                              @else
                                              Pending
                                              @endif
                                            </td>
                                            <td>{{$samples->no_of_samples}}</td>
                                            <td>
                                              @if($samples->decontamination_date)
                                              {{$samples->decontamination_date}}</td>
                                              @endif
                                            <td>{{$samples->result}}</td>
                                            <td>
                                              @if(!$samples->extraction_date)
                                              <!-- <button onclick="openForm('{{$samples->samples}}', {{$samples->log_id}})",  value="" type="button" class = "btn btn-default btn-sm resultbtn">Submit</button> -->
                                              Pending
                                              @else
                                              <?php echo date('d/m/Y', strtotime($samples->extraction_date)); ?>
                                              @endif
                                            </td>
                                            <td>
                                              @if($samples->status==0)
                                              Done
                                              @else
                                              <button onclick="openNextForm('{{$samples->samples}}', {{$samples->log_id}},{{$samples->enroll_id}}, '{{ $samples->tag }}','{{$samples->no_sample}}')" type="button" class = "btn btn-default btn-sm  nextbtn">Next</button>
                                              @endif

                                            </td>
                                          </tr>
                                          @endforeach

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>





<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Barcodes</h4>
      </div>
      <div class="modal-body" id="printCode">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
$(function(){


});
function openForm(sample_label, log_id){
  console.log(sample_label);
  $('#sample').text(sample_label);
  $('#log_id').val(log_id);
  $('#extractionpopupDiv').modal('toggle');
}
function openNextForm(sample_label, log_id, enroll_id, tag, no){
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#next_enroll_id').val(enroll_id);
  $('#spantag').text(tag);
  $('#no_sample').val(no);
  $('#nextpopupDiv').modal('toggle');
}
</script>


@endsection
