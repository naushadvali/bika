@extends('admin.layout.app')
@section('content')

        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"> Enrollment</h3>

                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                       <p  class="pull-right ">Enrollment ID : {{$data['label']}}</p>
                   </div>
                </div>

               <!--  <form action="{{ url('/enroll') }}" class="form-horizontal form-material" method="post" enctype='multipart/form-data'>
 -->
                @if($data['patient']->id>0)
                   <form id="createForm" action="{{ url('/enroll/patient/'.$data['patient']->id) }}" method="post">
                     <input name="_method" type="hidden" value="patch">
                   @else
                   <form id="createForm" action="{{ url('/enroll/patient') }}" method="post" role="alert">
                 @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-block">

                                    <div class="row">
                                        <div class="col ">
                                           <label class="col-md-12">Nikshay ID </label>
                                            <div class="col-md-12">
                                               <input type="text"  name="nikshay_id" value="{{ old('nikshay_id', $data['patient']->nikshay_id) }}" class="form-control form-control-line" >
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12"> Health Establishment ID </label>
                                            <div class="col-md-12">
                                               <input type="text" name="health_establish_id" value="{{ old('health_establish_id', $data['patient']->health_establish_id) }}"  class="form-control form-control-line">
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col ">
                                            <label class="col-sm-12">State  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="state" class="form-control form-control-line" id="state" required>
                                                        <option value="">Select</option>
                                                        @foreach ($data['state'] as $key=> $state)
                                                                <option value="{{$state['STOCode']}}"
                                                                @if($data['patient']->state==$state['STOCode'])
                                                                    selected="selected"
                                                                @endif
                                                                >{{$state['name']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>

                                        <div class="col ">
                                            <label class="col-sm-12">District  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="district" class="form-control form-control-line"  id="district" required>
                                                        <option value="">Select</option>
                                                        <!-- @foreach ($data['district'] as $key=> $district)
                                                                @if($data['patient']->district==$district->id)
                                                                 <option value="{{$district['id']}}"  selected="selected">{{$district['name']}}</option>>
                                                                @endif

                                                        @endforeach -->
                                                        @foreach ($data['district'] as $key=> $facility)
                                                          <option
                                                            value="{{$facility['DTOCode']}}"
                                                            @if(isset($data['patient']->district) && $data['patient']->district == $facility['DTOCode'])
                                                              selected
                                                            @endif
                                                          >
                                                          {{$facility['name']}}
                                                          </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <label class="col-sm-12">TB Unit  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="tb" id="tb" class="form-control form-control-line" required>
                                                        <option>--Select--</option>
                                                         <!-- @foreach ($data['tb'] as $key=> $tb)

                                                                @if($data['patient']->tb==$tb->id)
                                                                 <option value="{{$tb->id}}"  selected="selected">{{$tb->name}}</option>

                                                                @endif
                                                                <option value="{{$tb->id}}" >{{$tb->name}}</option>

                                                        @endforeach -->

                                                        @foreach ($data['tb'] as $key=> $facility)
                                                          <option
                                                            value="{{$facility['TBUnitCode']}}"
                                                            @if(isset($data['patient']->tb) && $data['patient']->tb == $facility['TBUnitCode'])
                                                              selected
                                                            @endif
                                                          >
                                                          {{$facility['TBUnitName']}}
                                                          </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">PHI  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="phi" id="phi" class="form-control form-control-line" required>

                                                         <option>--Select--</option>
                                                         <!-- @foreach ($data['phi'] as $key=> $phi)

                                                                @if($data['patient']->phi==$phi->id)
                                                                 <option value="{{$phi->id}}"  selected="selected">{{$phi->name}}</option>

                                                                @endif
                                                                <option value="{{$phi->id}}" >{{$phi->name}}</option>

                                                        @endforeach -->
                                                        @foreach ($data['phi'] as $key=> $facility)
                                                          <option
                                                            value="{{$facility['id']}}"
                                                            @if(isset($data['patient']->phi) && $data['patient']->phi == $facility['id'])
                                                              selected
                                                            @endif
                                                          >
                                                          {{$facility['DMC_PHI_Name']}}
                                                          </option>

                                                        @endforeach


                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Patient's Name  <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="name" value="{{ $data['name']}}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Age (in years)  <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="number"  name="age"  value="{{ old('age', $data['patient']->age) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <label class="col-sm-12">Gender <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="gender" class="form-control form-control-line"  value="{{ old('gender', $data['patient']->gender) }}" required>
                                                         <option >--Select--</option>
                                                        <option value="M"
                                                         @if($data['patient']->gender=="M")
                                                            selected="selected"
                                                             @endif
                                                        >Male</option>
                                                        <option value="F"
                                                         @if($data['patient']->gender=="F")
                                                            selected="selected"
                                                             @endif
                                                        >Female</option>
                                                        <option value="T"
                                                        @if($data['patient']->gender=="T")
                                                            selected="selected"
                                                             @endif
                                                        >Transgender</option>

                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">Mobile Number/Other Number </label>
                                                <div class="col-md-12">
                                               <input type="number" name="mobile_number"  pattern="^[0-9]$" onKeyPress="if(this.value.length==12) return false;" value="{{ old('mobile_number', $data['patient']->mobile_number) }}" class="form-control form-control-line" >
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Aadhar Number </label>
                                            <div class="col-md-12">
                                               <input type="number"  name="adhar_no" onKeyPress="if(this.value.length==12) return false;" placeholder="{{ old('adhar_no', $data['adhaar_no']->adhar_no) }}" value="{{ old('adhar_no', $data['adhaar_no']->adhar_no) }}" class="form-control form-control-line " maxlength="12" >
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Bank Name</label>
                                            <div class="col-md-12">
                                              <!--  <input type="text"  name="bank_name" value="{{ old('bank_name', $data['patient']->bank_name) }}" class="form-control form-control-line"> -->
                                               <select name="bank_name" id="bank_name" class="form-control form-control-line"  value="{{ old('gender', $data['patient']->gender) }}" >
                                                         <option ></option>
                                                        <option value="Allahabad Bank"
                                                         @if($data['patient']->bank_name=="Allahabad Bank")
                                                            selected="selected"
                                                             @endif
                                                        >Allahabad Bank</option>
                                                        <option value="Andhra Bank"
                                                         @if($data['patient']->bank_name=="Andhra Bank")
                                                            selected="selected"
                                                             @endif
                                                        >Andhra Bank</option>
                                                        <option value="Bank of Baroda"
                                                        @if($data['patient']->bank_name=="Bank of Baroda")
                                                            selected="selected"
                                                             @endif
                                                        >Bank of Baroda</option>
                                                        <option value="Bank of India"
                                                         @if($data['patient']->bank_name=="Bank of India")
                                                            selected="selected"
                                                             @endif
                                                        >Bank of India</option>
                                                        <option value="Bank of Maharashtra"
                                                         @if($data['patient']->bank_name=="Bank of Maharashtra")
                                                            selected="selected"
                                                             @endif
                                                        >Bank of Maharashtra</option>
                                                        <option value="Canara Bank"
                                                        @if($data['patient']->bank_name=="Canara Bank")
                                                            selected="selected"
                                                             @endif
                                                        >Canara Bank</option>
                                                        <option value="ICICI Bank"
                                                         @if($data['patient']->bank_name=="ICICI Bank")
                                                            selected="selected"
                                                             @endif
                                                        >ICICI Bank</option>
                                                        <option value="Axis Bank"
                                                         @if($data['patient']->bank_name=="Axis Bank")
                                                            selected="selected"
                                                             @endif
                                                        >Axis Bank</option>
                                                        <option value="Other"
                                                        @if($data['patient']->bank_name=="Other")
                                                            selected="selected"
                                                             @endif
                                                        >Other</option>

                                                </select>
                                           </div>
                                        </div>
                                        <div class="col hide" id="bankname_other">
                                            <label class="col-md-6">Bank Name Other </label>
                                            <div class="col-md-6">
                                               <input type="text"  value="{{ old('bankname_other', $data['patient']->bankname_other) }}" class="form-control form-control-line" name="bankname_other">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Account Number </label>
                                            <div class="col-md-12">
                                               <input type="text"  name="account_no" value="{{ old('account_no', $data['patient']->account_no) }}" class="form-control form-control-line">
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">IFSC Code </label>
                                            <div class="col-md-12">
                                               <input type="text" name="ifsc_code" value="{{ old('ifsc_code', $data['patient']->ifsc_code) }}" class="form-control form-control-line">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col">
                                        <h4>Patient Address with Landmark:</h4>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">House Number <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="house_no" value="{{ old('house_no', $data['patient']->house_no) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Street <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="street" value="{{ old('street', $data['patient']->street) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <label class="col-md-12">Ward Number <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="ward" value="{{ old('ward', $data['patient']->ward) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-sm-12">Town/City/Village <span class="red">*</span></label>
                                                <div class="col-md-12">
                                               <input type="text"  name="city" value="{{ old('city', $data['patient']->city) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Taluk/Mandal <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="taluka" value="{{ old('taluka', $data['patient']->taluka) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Landmark <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="landmark"  value="{{ old('landmark', $data['patient']->landmark) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                            <label class="col-sm-12">State <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="landmark_state" id="landmark_state" class="form-control form-control-line" value="{{ old('landmark_state', $data['patient']->landmark_state) }}" required>
                                                         <option value="">Select</option>

                                                        @foreach ($data['state'] as $key=> $state)
                                                                <option value="{{$state['STOCode']}}"
                                                                @if($data['patient']->landmark_state==$state['STOCode'])
                                                                    selected="selected"
                                                                @endif
                                                                >{{$state['name']}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col ">
                                            <label class="col-sm-12">District  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="landmark_district" class="form-control form-control-line" id="landmark_district"  required>
                                                         <option value="">Select</option>
                                                        @foreach ($data['district2'] as $key=> $facility)
                                                          <option
                                                            value="{{$facility['DTOCode']}}"
                                                            @if($data['patient']->landmark_district == $facility['DTOCode'])
                                                              selected
                                                            @endif
                                                          >
                                                          {{$facility['name']}}
                                                          </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col" id="Pincode">
                                            <label class="col-md-12" id="caterralert">Pincode <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="number"  id="pincode" name="pincode"  onKeyPress="if(this.value.length==6) return false;"  value="{{ old('pincode', $data['patient']->pincode) }}" class="form-control form-control-line" min="100000" max="999999" oninvalid="setCustomValidity('Plz enter valid pincode')"
                                              onchange="try{setCustomValidity('')}catch(e){}" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">Area <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text"  name="area" value="{{ old('area', $data['patient']->area) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                         <div class="col ">
                                            <label class="col-sm-12">HIV Status <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="hiv_test" value="{{ old('hiv_test', $data['patient']->hiv_test) }}" class="form-control form-control-line" required>
                                                         <option value="">--Select--</option>
                                                         <option value="Pos"
                                                        @if($data['patient']->hiv_test=="Pos")
                                                            selected="selected"
                                                             @endif
                                                        >Pos</option>
                                                         <option value="Neg"
                                                        @if($data['patient']->hiv_test=="Neg")
                                                            selected="selected"
                                                             @endif
                                                        >Neg</option>
                                                        <option value="Unknown"
                                                         @if($data['patient']->hiv_test=="Unknown")
                                                            selected="selected"
                                                             @endif
                                                        >Unknown</option>



                                                    </select>
                                                </div>
                                        </div>




                                        <div class="col ">
                                            <label class="col-sm-12">Maritial Status  <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="marital_state" id="marital_state" value="{{ old('marital_state', $data['patient']->marital_state) }}" class="form-control form-control-line" required>
                                                         <option value="">Select</option>
                                                        <option value="1"
                                                         @if($data['patient']->marital_state=="1")
                                                            selected="selected"
                                                             @endif
                                                        >Married</option>
                                                        <option value="0"
                                                        @if($data['patient']->marital_state=="0")
                                                            selected="selected"
                                                             @endif
                                                        >Single</option>
                                                        <option value="2"
                                                        @if($data['patient']->marital_state=="2")
                                                            selected="selected"
                                                             @endif
                                                        >Other</option>

                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col hide" id="maritalstatus_other">
                                            <label class="col-md-6">Maritial Status Other </label>
                                            <div class="col-md-6">
                                               <input type="text"  value="{{ old('maritalstatus_other', $data['patient']->maritalstatus_other) }}" class="form-control form-control-line" name="maritalstatus_other">
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col ">
                                            <label class="col-sm-12">Occupation <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="occupation"  class="form-control form-control-line" required>
                                                         <option value="">Select</option>
                                                        @foreach ($data['occupation'] as $key=> $occupation)

                                                                <option value="{{$occupation['id']}}"
                                                                @if($data['patient']->occupation==$occupation['id'])
                                                                    selected="selected"
                                                                @endif
                                                                >{{$occupation['name']}}</option>

                                                        @endforeach


                                                    </select>
                                                </div>
                                        </div>

                                        <div class="col ">
                                            <label class="col-sm-12">Socioeconomic Status <span class="red">*</span></label>
                                                <div class="col-sm-12">
                                                    <select name="socioeconomy_status" value="{{ old('socioeconomy_status', $data['patient']->socioeconomy_status) }}" class="form-control form-control-line" required>
                                                         <option value="">Select</option>
                                                        <option value="NA"
                                                        @if($data['patient']->socioeconomy_status=="NA")
                                                            selected="selected"
                                                             @endif
                                                        >NA</option>
                                                        <option value="APL"
                                                         @if($data['patient']->socioeconomy_status=="APL")
                                                            selected="selected"
                                                             @endif
                                                        >APL</option>
                                                        <option value="BPL"
                                                         @if($data['patient']->socioeconomy_status=="BPL")
                                                            selected="selected"
                                                             @endif
                                                        >BPL</option>

                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Contact Person Name <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="cp_name" value="{{ old('cp_name', $data['patient']->cp_name) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col">
                                            <label class="col-md-12">CP Mobile Number <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="number"  pattern="^[0-9]$" name="cp_phn_no" onKeyPress="if(this.value.length==12) return false;" value="{{ old('cp_phn_no', $data['patient']->cp_phn_no) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Address <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="cp_address" value="{{ old('cp_address', $data['patient']->cp_address) }}" class="form-control form-control-line" required>
                                           </div>
                                        </div>
                                        <div class="col ">
                                            <label class="col-sm-12">Key Population</label>
                                                <div class="col-sm-12">
                                                    <select name="key_population" id="key_population" value="{{ old('key_population', $data['patient']->key_population) }}" class="form-control form-control-line" required>
                                                         <option value="">Select</option>
                                                        <option value="Contact of known TB Patient"
                                                        @if($data['patient']->key_population=="Contact of known TB Patient")
                                                            selected="selected"
                                                             @endif
                                                        >Contact of known TB Patient</option>
                                                        <option value="Diabetes"
                                                        @if($data['patient']->key_population=="Diabetes")
                                                            selected="selected"
                                                             @endif
                                                        >Diabetes</option>
                                                        <!-- <option value="Other"
                                                        @if($data['patient']->key_population=="Other")
                                                            selected="selected"
                                                             @endif
                                                        >Other</option> -->
                                                        <option value="Tobacco"
                                                        @if($data['patient']->key_population=="Tobacco")
                                                            selected="selected"
                                                             @endif
                                                        >Tobacco</option>
                                                        <option value="Prison"
                                                        @if($data['patient']->key_population=="Prison")
                                                            selected="selected"
                                                             @endif
                                                        >Prison</option>
                                                        <option value="Miner"
                                                        @if($data['patient']->key_population=="Miner")
                                                            selected="selected"
                                                             @endif
                                                        >Miner</option>
                                                        <option value="Migrant"
                                                        @if($data['patient']->key_population=="Migrant")
                                                            selected="selected"
                                                             @endif
                                                        >Migrant</option>
                                                        <option value="Refugee"
                                                        @if($data['patient']->key_population=="Refugee")
                                                            selected="selected"
                                                             @endif
                                                        >Refugee</option>
                                                        <option value="Urban slum"
                                                        @if($data['patient']->key_population=="Urban slum")
                                                            selected="selected"
                                                             @endif
                                                        >Urban slum</option>
                                                        <option value="Health-care worker"
                                                        @if($data['patient']->key_population=="Health-care worker")
                                                            selected="selected"
                                                             @endif
                                                        >Health-care worker</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col hide" id="population_other">
                                            <label class="col-md-6">Key Population Other </label>
                                            <div class="col-md-6">
                                               <input type="text"  value="{{ old('population_other', $data['patient']->population_other) }}" class="form-control form-control-line" name="population_other">
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row ">


                                        <div class="col">
                                            <label class="col-md-12">Registered Date <span class="red">*</span></label>
                                            <div class="col-md-12">
                                               <input type="text" name="regr_date" value="{{ old('regr_date', $data['patient']->regr_date) }}" max="<?php echo date("Y-m-d");?>"  placeholder="dd-mm-yy" class="form-control form-control-line datepicker" required>
                                           </div>
                                        </div>

                                        <div class="col ">
                                                  <label class="col-md-12">Sample Collection Date <span class="red">*</span></label>
                                                  <div class="col-md-12">
                                                     <input type="text"  value="{{ old('collection_date', $data['patient']->collection_date) }}" max="<?php echo date("Y-m-d");?>"  placeholder="dd-mm-yy" class="form-control form-control-line datepicker" name="collection_date" required>


                                                 </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="col-12">

                            <button class="btn btn-info">Save</button>
                            <a class="btn btn-warning" href="{{url('/enroll')}}">Cancel</a>
                            <!-- <button class="btn btn-success">Preview</button>
                             <button class="btn ">Print</button> -->

                        </div>

                    </div>
                </form>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>

<script>
    $(document).ready(function() {

        // $('#pincode').on('change', function(evt) {
        //     if($(this).value.length!=6)
        //         return false;
        //  });
        var _sample = $('#key_population').find(":selected").val();
        if(_sample=='Other'){
          $("#population_other").removeClass("hide");
        }else{
          $("#population_other").addClass("hide");
        }

        $("#key_population").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample=='Other'){
              $("#population_other").removeClass("hide");
              document.getElementById("#population_other").setAttribute("required","required");
            }else{
              $("#population_other").addClass("hide");
              document.getElementById("#population_other").removeAttribute("required","required");
            }
        });

        var _sample = $('#bank_name').find(":selected").val();
        if(_sample=='Other'){
          $("#bankname_other").removeClass("hide");
        }else{
          $("#bankname_other").addClass("hide");
        }

        $("#bank_name").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample=='Other'){
              $("#bankname_other").removeClass("hide");
              document.getElementById("#bankname_other").setAttribute("required","required");
            }else{
              $("#bankname_other").addClass("hide");
              document.getElementById("#bankname_other").removeAttribute("required","required");
            }
        });

        var _sample = $('#marital_state').find(":selected").val();
        if(_sample==2){
          $("#maritalstatus_other").removeClass("hide");
        }else{
          $("#maritalstatus_other").addClass("hide");
        }

        $("#marital_state").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample==2){
              $("#maritalstatus_other").removeClass("hide");
              document.getElementById("#maritalstatus_other").setAttribute("required","required");
            }else{
              $("#maritalstatus_other").addClass("hide");
              document.getElementById("#maritalstatus_other").removeAttribute("required","required");
            }
        });


        $("#state").change( function() {
            var state = $(this).val();

            $.ajax({
                url: "{{url('district')}}"+'/'+state,
                type:"GET",
                processData: false,
                contentType: false,
                dataType : "json",
                success: function(items) {

                    //$('#district').html();
                    $("#district option").remove();
                    $('#district').append($('<option>', {
                        value: '',
                        text : 'Select'
                    }))
                    $.each(items.district, function (i, item) {
                        $('#district').append($('<option>', {
                            value: item.DTOCode,
                            text : item.name
                        }));
                    });
                },
                error: function() {
                  console.log("err")
              }
            });
        });

        $("#district").change( function() {
            var district = $(this).val();
            var state = $('#state').val();
            $.ajax({
                url: "{{url('tbunit')}}"+'/'+state+'/'+district,
                type:"GET",
                processData: false,
                contentType: false,
                dataType : "json",
                success: function(items) {

                    //$('#district').html();
                    $("#tb option").remove();
                    $('#tb').append($('<option>', {
                        value: '',
                        text : 'Select'
                    }))
                    $.each(items.tbunit, function (i, item) {
                        $('#tb').append($('<option>', {
                            value: item.TBUnitCode,
                            text : item.TBUnitName
                        }));
                    });
                },
                error: function() {
                  console.log("err")
              }
            });
        });

        $("#tb").change( function() {
            var tbu = $(this).val();
            var state = $('#state').val();
            var district = $('#district').val();
            $.ajax({
                url: "{{url('phi')}}"+'/'+state+'/'+tbu+'/'+district,
                type:"GET",
                processData: false,
                contentType: false,
                dataType : "json",
                success: function(items) {
                    $("#phi option").remove();
                    $('#phi').append($('<option>', {
                        value: '',
                        text : 'Select'
                    }))
                    $.each(items.phi, function (i, item) {
                        $('#phi').append($('<option>', {
                            value: item.id,
                            text : item.DMC_PHI_Name
                        }));
                    });
                },
                error: function() {
                  console.log("err")
              }
            });
        });

        $("#landmark_state").change( function() {
            var state = $(this).val();

            $.ajax({
                url: "{{url('district')}}"+'/'+state,
                type:"GET",
                processData: false,
                contentType: false,
                dataType : "json",
                success: function(items) {
                    console.log(items);
                    //$('#district').html();
                    $("#landmark_district option").remove();
                    $('#landmark_district').append($('<option>', {
                        value: '',
                        text : 'Select'
                    }))
                    $.each(items.district, function (i, item) {
                        $('#landmark_district').append($('<option>', {
                            value: item.DTOCode,
                            text : item.name
                        }));
                    });
                },
                error: function() {
                  console.log("err")
              }
            });
        });
    });

</script>

@endsection
