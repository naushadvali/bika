@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Liquid Culture Result Review</h3>

                  </div>
                   <div class="col-md-7 col-4 align-self-center">
                    <form action="{{ url('/lcflagreview/print') }}" method="post" >
                    <!--   <input type ="hidden" name="enroll" value = "1"> -->
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="pull-right btn-sm btn-info" >Print</a>
                    </form>
                 </div>
              </div>
              @include('admin/lc_result_review/nextstepspopup')
                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                  <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                      <thead>
                                        <tr>
                                          <th>Lab Enrolment ID</th>
                                          <th>Sample ID</th>
                                          <th>DX/FU/EQA</th>
                                          <th>Follow up month</th>
                                          <th>Samples submitted</th>
                                          <th>Type of sample</th>
                                          <th>LPA Test requested</th>
                                          <th>Culture method S/L/Both</th>
                                          <th>Final result</th>
                                          <th>Date of Result </th>
                                          <th>Next Steps</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach ($data['sample'] as $key=> $samples)
                                        <tr>
                                          <td>{{$samples->enroll_label}}</td>
                                          <td>{{$samples->samples}}</td>
                                          <td>{{$samples->reason}}</td>
                                          <td>{{$samples->fu_month}}</td>
                                          <td>{{$samples->no_of_samples}}</td>
                                          <td>{{$samples->sample_type}}</td>
                                          <td>{{$samples->lpa_method}}</td>
                                          <td>{{$samples->lpa_type}}</td>
                                          <td>{{$samples->result}}</td>
                                          <td>{{$samples->result_date}}</td>

                                          <td>

                                            @if($samples->status == 0)
                                            Done
                                            @elseif($samples->status == 2)
                                            <button onclick="openNextForm('{{$samples->samples}}',{{$samples->log_id}},'{{$samples->no_sample}}')",  value="" type="button" class = "btn btn-default btn-sm resultbtn">Submit</button>
                                            @endif
                                          </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                      </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>

<script>
function openNextForm(sample_label, log_id, no){
  $('#no_sample').val(no);
  $('#next_sample_id').val(sample_label);
  $('#next_log_id').val(log_id);
  $('#nextpopupDiv').modal('toggle');
}
</script>


@endsection
