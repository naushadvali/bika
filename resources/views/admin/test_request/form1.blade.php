@extends('admin.layout.app')
@section('content')

        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Test Request</h3>
                        @if($data['enroll_label'])
                        <h5>Enrollment ID: {{$data['enroll_label']}}</h5>
                        @endif
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                       <!-- <p  class="pull-right ">Enrollment ID : {{ str_pad($data['enroll_id'], 10, "0", STR_PAD_LEFT) }}</p> -->
                   </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->


                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                @if($data['reason']=='DX')

                                    <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-expanded="true">DX TB</a>
                                    </li>
                                    <!-- <li class="nav-item hide">
                                        <a class="nav-link " data-toggle="tab" href="#tab2" role="tab" aria-expanded="false">FU TB</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab3" role="tab" aria-expanded="false">DX DR TB</a>
                                    </li>
                                   <!--  <li class="nav-item hide">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-expanded="false">FU DR TB</a>
                                    </li> -->

                                @elseif($data['reason']=='FU')

                                   <!--  <li class="nav-item hide">
                                    <a class="nav-link" data-toggle="tab" href="#tab1" role="tab" aria-expanded="false">DX TB</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab2" role="tab" aria-expanded="true">FU TB</a>
                                    </li>
                                    <!-- <li class="nav-item hide">
                                        <a class="nav-link" data-toggle="tab" href="#tab3" role="tab" aria-expanded="false">DX DR TB</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-expanded="false">FU DR TB</a>
                                    </li>

                                @endif
                               <!--  <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-expanded="true">Diagnosis TB</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#tab2" role="tab" aria-expanded="false">Follow Up (smear)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab3" role="tab">DSTB</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab4" role="tab">DRTB</a>
                                </li> -->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                 @if($data['reason']=='DX')
                                <div class="tab-pane active" id="tab1" role="tabpanel" >
                                    <div class="card-block">

                                        <form action="{{ url('/test_request') }}" method="post" class="form-horizontal form-material">
                                            <h6>Name and type of referring facility</h6>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="enroll_id" value="{{$data['enroll_id']}}">
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Facility Type <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line facility_type" name="facility_type" required>

                                                                <option value="">--Select--</option>
                                                                <!-- @foreach ($data['facility_type'] as $key=> $facility_type)
                                                                  <option
                                                                    value="{{$facility_type['facility_type_id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility_type['facility_type_id'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$facility_type['name']}}
                                                                  </option>

                                                                @endforeach -->
                                                                @foreach ($data['facility_types'] as $key=> $facility)
                                                                  <option
                                                                    value="{{$facility['id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility['id'])
                                                                      selected
                                                                    @endif
                                                                  >
                                                                  {{$facility['name']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>


                                                </div>
                                                <div class="col hide facility_type_other">
                                                    <label class="col-md-6">Facility Type Other </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  value="{{ old('facility_type_other', $data['testrequestservices']->facility_type_other) }}" class="form-control form-control-line" name="facility_type_other">
                                                   </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">State  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="state" id="state3" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['state'] as $key=> $state)
                                                                      <option
                                                                        value="{{$state['STOCode']}}"
                                                                        @if(isset($data['testrequest']->state) && $data['testrequest']->state == $state['STOCode'])
                                                                        selected
                                                                        @endif
                                                                      >
                                                                        {{$state['name']}}
                                                                      </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">District  <span class="red">*</span>  </label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="district" id="district3" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['district'] as $key=> $district)
                                                                <option
                                                                  value="{{$district['DTOCode']}}"
                                                                  @if(isset($data['testrequest']->district) && $data['testrequest']->district == $district['DTOCode'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$district['name']}}
                                                                </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">TBU  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="tbu" id="tbu3"required>

                                                                <option value="">--Select--</option>
                                                                @foreach ($data['tbunit'] as $key=> $tbunit)
                                                                  <option
                                                                    value="{{$tbunit['TBUnitCode']}}"
                                                                    @if(isset($data['testrequest']->tbu) && $data['testrequest']->tbu == $tbunit['TBUnitCode'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$tbunit['TBUnitName']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Name of Facility  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="facility_id" id="facility_id3" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['facility'] as $key=> $facility)
                                                                <option
                                                                  value="{{$facility['id']}}"
                                                                  @if(isset($data['testrequest']->facility_id) && $data['testrequest']->facility_id == $facility['id'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$facility['DMC_PHI_Name']}}
                                                                </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">H/O anti TB Rx for > 1 month  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="ho_anti_tb" required>
                                                                <option value="yes"
                                                                @if($data['testrequestservices']->ho_anti_tb=="yes")
                                                                   selected="selected"
                                                                @endif
                                                                >yes</option>
                                                                <option value="no"
                                                                @if($data['testrequestservices']->ho_anti_tb=="no")
                                                                   selected="selected"
                                                                @endif
                                                                >no</option>
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Diagnosis of TB  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">

                                                            <select class="form-control form-control-line" name="diagnosis[]" multiple required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['diagnosis'] as $key=> $diagnosis)
                                                                <option
                                                                  value="{{$diagnosis['diagnosis_id']}}"
                                                                  @if(in_array($diagnosis['diagnosis_id'],$data['diagnosis_value']))
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$diagnosis['name']}}
                                                                </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">Predominant Symptom  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="predmnnt_symptoms" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['predominan_symptom'] as $key=> $predominan_symptom)
                                                                <option
                                                                  value="{{$predominan_symptom['symptom_id']}}"
                                                                  @if(isset($data['testrequest']->predmnnt_symptoms) && $data['testrequest']->predmnnt_symptoms == $predominan_symptom['symptom_id'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$predominan_symptom['name']}}
                                                                </option>

                                                              @endforeach

                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-md-6">Duration in days <span class="red">*</span>  </label>
                                                    <div class="col-md-6">
                                                       <input type="number" value="{{ old('duration', $data['testrequest'] ? $data['testrequest']->duration : '') }}" class="form-control form-control-line" name="duration" required>
                                                   </div>
                                                </div>
                                                <div class="col hide">
                                                    <label class="col-md-6">type <span class="red">*</span> </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  class="form-control form-control-line" name="req_test_type" value="1" required>
                                                   </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                @foreach ($data['services'] as $key=> $services)
                                                <div class="col-md-4 top5px">
                                                  <input class="service_array"
                                                    value="{{$services['id']}}"
                                                    @if(isset($data['_reqservices']) && is_array($data['_reqservices']) && in_array($services['id'], $data['_reqservices']))
                                                      checked=""
                                                    @endif
                                                    name="services[]"
                                                    type="checkbox">{{$services['name']}}
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="dst_drugs hide">
                                              <div class="row  ">
                                                <label class="col-md-12">DST Drugs</label>

                                                @foreach ($data['dstdrugs'] as $key=> $drugs)
                                                <div class="col-md-4 ">
                                                  <input class="drugs_array"
                                                    value="{{$drugs['id']}}"

                                                    name="drugs[]"
                                                    type="checkbox">{{$drugs['name']}}
                                                </div>
                                                @endforeach

                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Requestor Name   </label>
                                                  <div class="col-md-12">
                                                     <input type="text"  class="form-control form-control-line" name="requestor_name" value="{{ old('requestor_name', $data['testrequest'] ? $data['testrequest']->requestor_name : '') }}">
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Designation   </label>
                                                  <div class="col-md-12">
                                                     <select  class="form-control form-control-line" name="designation" >
                                                     <option value="">--Select--</option>
                                                     @foreach ($data['designations'] as $key=> $designations)
                                                       <option
                                                         value="{{$designations['designation_id']}}"
                                                         @if(isset($data['testrequest']->designation) && $data['testrequest']->designation == $designations['designation_id'])
                                                           selected
                                                         @endif
                                                       >
                                                       {{$designations['name']}}
                                                       </option>

                                                     @endforeach
                                                   </select>
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Contact Number   </label>
                                                  <div class="col-md-12">
                                                     <input type="number"  class="form-control form-control-line" name="contact_no" pattern="^[0-9]$" onKeyPress="if(this.value.length==10) return false;" value="{{ old('contact_no', $data['testrequest'] ? $data['testrequest']->contact_no : '') }}" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Email Id   </label>
                                                  <div class="col-md-12">
                                                     <input type="email"  class="form-control form-control-line" value="{{ old('email_id', $data['testrequest'] ? $data['testrequest']->email_id : '') }}" name="email_id" >
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-6">Request Date  <span class="red">*</span> </label>
                                                  <div class="col-md-6">
                                                     <input type="text" value="{{ old('request_date', $data['testrequest'] ? $data['testrequest']->request_date : '') }}" class="form-control form-control-line datepicker"  max="<?php echo date("Y-m-d");?>" name="request_date" required>
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-info">Save</button>
                                                    <a class="btn btn-warning" href="{{url('/test_request')}}">Cancel</a>
                                                    <!-- <button class="btn btn-success">Preview</button>
                                                    <button class="btn ">Print</button> -->
                                                </div>

                                            </div>
                                        </form>


                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3" role="tabpanel" >
                                    <div class="card-block">
                                        <form action="{{ url('/test_request') }}" method="post" class="form-horizontal form-material">
                                            <h6>Name and type of referring facility</h6>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="enroll_id" value="{{$data['enroll_id']}}">
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Facility Type <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line facility_type" name="facility_type" required>

                                                                <option value="">--Select--</option>
                                                                <!-- @foreach ($data['facility_type'] as $key=> $facility_type)
                                                                  <option
                                                                    value="{{$facility_type['facility_type_id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility_type['facility_type_id'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$facility_type['name']}}
                                                                  </option>

                                                                @endforeach -->
                                                                @foreach ($data['facility_types'] as $key=> $facility)
                                                                  <option
                                                                    value="{{$facility['id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility['id'])
                                                                      selected
                                                                    @endif
                                                                  >
                                                                  {{$facility['name']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>

                                                </div>
                                                <div class="col hide facility_type_other">
                                                    <label class="col-md-6">Facility Type Other </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  value="{{ old('facility_type_other', $data['testrequestservices']->facility_type_other) }}" class="form-control form-control-line" name="facility_type_other">
                                                   </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">State <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="state" id="state2" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['state'] as $key=> $state)
                                                                      <option
                                                                        value="{{$state['STOCode']}}"
                                                                        @if(isset($data['testrequest']->state) && $data['testrequest']->state == $state['STOCode'])
                                                                        selected
                                                                        @endif
                                                                      >
                                                                        {{$state['name']}}
                                                                      </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">District <span class="red">*</span></label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="district" id="district2" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['district'] as $key=> $district)
                                                                <option
                                                                  value="{{$district['DTOCode']}}"
                                                                  @if(isset($data['testrequest']->district) && $data['testrequest']->district == $district['DTOCode'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$district['name']}}
                                                                </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">TBU <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="tbu" id="tbu2"required>

                                                                <option value="">--Select--</option>
                                                                @foreach ($data['tbunit'] as $key=> $tbunit)
                                                                  <option
                                                                    value="{{$tbunit['TBUnitCode']}}"
                                                                    @if(isset($data['testrequest']->tbu) && $data['testrequest']->tbu == $tbunit['TBUnitCode'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$tbunit['TBUnitName']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">Name of Facility <span class="red">*</span></label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="facility_id" id="facility_id2" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['facility'] as $key=> $facility)
                                                              <option
                                                                value="{{$facility['id']}}"
                                                                @if(isset($data['testrequest']->facility_id) && $data['testrequest']->facility_id == $facility['id'])
                                                                  selected
                                                                @endif
                                                              >
                                                              {{$facility['DMC_PHI_Name']}}
                                                              </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>

                                            </div>
                                            <br>
                                            <h6>Drug Susceptibility Testing DST :</h6>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Regimen <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="regimen[]" multiple required>
                                                                <option value="New"
                                                                @if(in_array("New",$data['regimen']))
                                                                   selected="selected"
                                                                @endif
                                                                >New</option>
                                                                <option value="Previously Treated"
                                                                @if(in_array("Previously Treated",$data['regimen']))
                                                                   selected="selected"
                                                                @endif
                                                                >Previously Treated</option>

                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">Presumptive MDR TB<span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="type_of_prsmptv_drtb[]" multiple required>
                                                                <option value="At diagnosis"
                                                                @if(in_array("At diagnosis",$data['type_of_prsmptv_drtb']))
                                                                   selected="selected"
                                                                @endif
                                                                >At diagnosis</option>
                                                                <option value="Contact of MDR/RR TB"
                                                                @if(in_array("Contact of MDR/RR TB",$data['type_of_prsmptv_drtb']))
                                                                   selected="selected"
                                                                @endif
                                                                >Contact of MDR/RR TB</option>
                                                                <option value="Follow up Sm+ve"
                                                                @if(in_array("Follow up Sm+ve",$data['type_of_prsmptv_drtb']))
                                                                   selected="selected"
                                                                @endif
                                                                >Follow up Sm+ve</option>
                                                                <option value="Private referall"
                                                                @if(in_array("Private referall",$data['type_of_prsmptv_drtb']))
                                                                   selected="selected"
                                                                @endif
                                                                >Private referall</option>
                                                                <option value="Discordance resolution"
                                                                @if(in_array("Discordance resolution",$data['type_of_prsmptv_drtb']))
                                                                   selected="selected"
                                                                @endif
                                                                >Discordance resolution</option>


                                                            </select>
                                                        </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col ">
                                                     <label class="col-sm-12">Presumptive H<span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="presumptive_h" required>
                                                                <option value="Mono"
                                                                @if($data['testrequestservices']->presumptive_h=="Mono")
                                                                   selected="selected"
                                                                @endif
                                                                >Mono</option>
                                                                <option value="Poly"
                                                                @if($data['testrequestservices']->presumptive_h=="Poly")
                                                                   selected="selected"
                                                                @endif
                                                                >Poly</option>



                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">Presumptive XDR TB <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="prsmptv_xdrtv[]" multiple required>


                                                                <option value="MDR/RR TB at Diagnosis"
                                                                @if(in_array("MDR/RR TB at Diagnosis",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >MDR/RR TB at Diagnosis</option>
                                                                <option value=">= 4 months culture positive"
                                                                  @if(in_array(">= 4 months culture positive",$data['prsmptv_xdrtv']))
                                                                     selected="selected"
                                                                  @endif
                                                                >>= 4 months culture positive</option>
                                                                <option value="3 monthly for persistent culture positives"
                                                                @if(in_array("3 monthly for persistent culture positives",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >3 monthly for persistent culture positives</option>
                                                                <option value="Culture reversion"
                                                                @if(in_array("Culture reversion",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >Culture reversion</option>
                                                                <option value="Failure of MDR/RR-TB regimen"
                                                                @if(in_array("MDR/RR TB at Diagnosis",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >Failure of MDR/RR-TB regimen</option>
                                                                <option value="Recurrant case of Second Line Treatment"
                                                                @if(in_array("MDR/RR TB at Diagnosis",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >Recurrant case of Second Line Treatment</option>
                                                                <option value="Discordance resolution"
                                                                @if(in_array("MDR/RR TB at Diagnosis",$data['prsmptv_xdrtv']))
                                                                   selected="selected"
                                                                @endif
                                                                >Discordance resolution</option>


                                                            </select>
                                                        </div>
                                                </div>

                                            </div>

                                            <div class="row hide">
                                                <div class="col ">
                                                    <label class="col-md-6">type <span class="red">*</span></label>
                                                    <div class="col-md-6">
                                                       <input type="text"  class="form-control form-control-line" name="req_test_type" value="3" required>
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                              @foreach ($data['services'] as $key=> $services)
                                              <div class="col-md-4 top5px">
                                                <input class="service_array"
                                                  value="{{$services['id']}}"
                                                  @if(isset($data['_reqservices']) && is_array($data['_reqservices']) && in_array($services['id'], $data['_reqservices']))
                                                    checked=""
                                                  @endif
                                                  name="services[]"
                                                  type="checkbox">{{$services['name']}}
                                              </div>
                                              @endforeach


                                            </div>
                                            <div class="dst_drugs hide">
                                              <div class="row  ">

                                                <label class="col-md-12">DST Drugs</label>
                                                @foreach ($data['dstdrugs'] as $key=> $drugs)
                                                <div class="col-md-4 ">
                                                  <input class="drugs_array"
                                                    value="{{$drugs['id']}}"

                                                    name="drugs[]"
                                                    type="checkbox">{{$drugs['name']}}
                                                </div>
                                                @endforeach

                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Requestor Name </label>
                                                  <div class="col-md-12">
                                                     <input type="text"  class="form-control form-control-line" value="{{ old('requestor_name', $data['testrequest'] ? $data['testrequest']->requestor_name : '') }}" name="requestor_name" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Designation </label>
                                                  <div class="col-md-12">
                                                     <select  class="form-control form-control-line" name="designation" >
                                                     <option value="">--Select--</option>
                                                     @foreach ($data['designations'] as $key=> $designations)
                                                       <option
                                                         value="{{$designations['designation_id']}}"
                                                         @if(isset($data['testrequest']->designation) && $data['testrequest']->designation == $designations['designation_id'])
                                                           selected
                                                         @endif
                                                       >
                                                       {{$designations['name']}}
                                                       </option>

                                                     @endforeach
                                                   </select>
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Contact Number </label>
                                                  <div class="col-md-12">
                                                     <input type="number" pattern="^[0-9]$" onKeyPress="if(this.value.length==12) return false;" value="{{ old('contact_no', $data['testrequest'] ? $data['testrequest']->contact_no : '') }}" class="form-control form-control-line" name="contact_no" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Email Id </label>
                                                  <div class="col-md-12">
                                                     <input type="email" value="{{ old('email_id', $data['testrequest'] ? $data['testrequest']->email_id : '') }}" class="form-control form-control-line" name="email_id" >
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-6">Request Date <span class="red">*</span></label>
                                                  <div class="col-md-6">
                                                     <input type="text" value="{{ old('request_date', $data['testrequest'] ? $data['testrequest']->request_date : '') }}" class="form-control form-control-line datepicker" name="request_date" max="<?php echo date("Y-m-d");?>" required>


                                                 </div>
                                              </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-info">Save</button>
                                                    <a class="btn btn-warning" href="{{url('/test_request')}}">Cancel</a>
                                                    <!-- <button class="btn btn-success">Preview</button>
                                                    <button class="btn ">Print</button> -->
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                                @endif
                                @if($data['reason']=='FU')
                                <!--second tab-->
                                <div class="tab-pane active" id="tab2" role="tabpanel" >
                                    <div class="card-block">
                                        <form action="{{ url('/test_request') }}" method="post" class="form-horizontal form-material">
                                            <h6>Name and type of referring facility</h6>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="enroll_id" value="{{$data['enroll_id']}}">
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Facility Type  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line facility_type" name="facility_type" required>

                                                                <option value="">--Select--</option>
                                                                <!-- @foreach ($data['facility_type'] as $key=> $facility_type)
                                                                  <option
                                                                    value="{{$facility_type['facility_type_id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility_type['facility_type_id'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$facility_type['name']}}
                                                                  </option>

                                                                @endforeach -->
                                                                @foreach ($data['facility_types'] as $key=> $facility)
                                                                  <option
                                                                    value="{{$facility['id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility['id'])
                                                                      selected
                                                                    @endif
                                                                  >
                                                                  {{$facility['name']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col hide facility_type_other">
                                                    <label class="col-md-6">Facility Type Other </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  value="{{ old('facility_type_other', $data['testrequestservices']->facility_type_other) }}" class="form-control form-control-line" name="facility_type_other">
                                                   </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">State  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="state" id="state" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['state'] as $key=> $state)
                                                                      <option
                                                                        value="{{$state['STOCode']}}"
                                                                        @if(isset($data['testrequest']->state) && $data['testrequest']->state == $state['STOCode'])
                                                                        selected
                                                                        @endif
                                                                      >
                                                                        {{$state['name']}}
                                                                      </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">District  <span class="red">*</span> </label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="district" id="district" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['district'] as $key=> $district)
                                                                <option
                                                                  value="{{$district['DTOCode']}}"
                                                                  @if(isset($data['testrequest']->district) && $data['testrequest']->district == $district['DTOCode'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$district['name']}}
                                                                </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">TBU  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="tbu" id="tbu" required>

                                                                <option value="">--Select--</option>
                                                                @foreach ($data['tbunit'] as $key=> $tbunit)
                                                                  <option
                                                                    value="{{$tbunit['TBUnitCode']}}"
                                                                    @if(isset($data['testrequest']->tbu) && $data['testrequest']->tbu == $tbunit['TBUnitCode'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$tbunit['TBUnitName']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">Name of Facility  <span class="red">*</span> </label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" id="facility_id" name="facility_id" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['facility'] as $key=> $facility)
                                                              <option
                                                                value="{{$facility['id']}}"
                                                                @if(isset($data['testrequest']->facility_id) && $data['testrequest']->facility_id == $facility['id'])
                                                                  selected
                                                                @endif
                                                              >
                                                              {{$facility['DMC_PHI_Name']}}
                                                              </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>
                                              <div class="col ">

                                                      <label class="col-md-12">RNTCP TB Reg No x  </label>
                                                      <div class="col-md-12">
                                                         <input type="number"  class="form-control form-control-line" value="{{ old('rntcp_reg_no', $data['testrequestservices']->rntcp_reg_no) }}" name="rntcp_reg_no" >

                                                  </div>
                                              </div>
                                            </div>
                                             <br>
                                            <h6>Follow Up (Smear and Culture) :</h6>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Regimen  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="regimen" required>
                                                                <option value="New"
                                                                @if($data['testrequestservices']->regimen=="New")
                                                                   selected="selected"
                                                                @endif
                                                                >New</option>
                                                                <option value="Previously Treated"
                                                                @if($data['testrequestservices']->regimen=="Previously Treated")
                                                                   selected="selected"
                                                                @endif
                                                                >Previously Treated</option>

                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">Reason  <span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="reason" required>
                                                                <option value="End IP"
                                                                @if($data['testrequestservices']->reason=="End IP")
                                                                   selected="selected"
                                                                @endif
                                                                >End IP</option>
                                                                <option value="End CP"
                                                                @if($data['testrequestservices']->reason=="End CP")
                                                                   selected="selected"
                                                                @endif
                                                                >End CP</option>


                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">Post treatment<span class="red">*</span> </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" id="post_treatment" name="post_treatment" required>

                                                                <option value="6 M"
                                                                @if($data['testrequestservices']->post_treatment=="6 M")
                                                                   selected="selected"
                                                                @endif
                                                                >6 M</option>
                                                                <option value="12 M"
                                                                @if($data['testrequestservices']->post_treatment=="12 M")
                                                                   selected="selected"
                                                                @endif
                                                                >12 M</option>
                                                                <option value="18 M"
                                                                @if($data['testrequestservices']->post_treatment=="18 M")
                                                                   selected="selected"
                                                                @endif
                                                                >18 M</option>
                                                                <option value="24 M"
                                                                @if($data['testrequestservices']->post_treatment=="24 M")
                                                                   selected="selected"
                                                                @endif
                                                                >24 M</option>
                                                                <option value="Other"
                                                                @if($data['testrequestservices']->post_treatment=="Other")
                                                                   selected="selected"
                                                                @endif
                                                                >Other</option>
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col hide" id="other_post_treatment">
                                                    <label class="col-sm-12">Other</label>
                                                        <div class="col-sm-12">
                                                          <input type="text"  class="form-control form-control-line" name="other_post_treatment">
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row hide">
                                                <div class="col ">
                                                    <label class="col-md-6">type <span class="red">*</span> </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  class="form-control form-control-line" name="req_test_type" value="2" required>
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                @foreach ($data['services'] as $key=> $services)
                                                <div class="col-md-4 top5px">
                                                  <input class="service_array"
                                                    value="{{$services['id']}}"
                                                    @if(isset($data['_reqservices']) && is_array($data['_reqservices']) && in_array($services['id'], $data['_reqservices']))
                                                      checked=""
                                                    @endif
                                                    name="services[]"
                                                    type="checkbox">{{$services['name']}}
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="dst_drugs hide">
                                              <div class="row  ">
                                                <label class="col-md-12">DST Drugs</label>
                                                @foreach ($data['dstdrugs'] as $key=> $drugs)
                                                <div class="col-md-4 ">
                                                  <input class="drugs_array"
                                                    value="{{$drugs['id']}}"
                                                    name="drugs[]"
                                                    type="checkbox">{{$drugs['name']}}
                                                </div>
                                                @endforeach

                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Requestor Name </label>
                                                  <div class="col-md-12">
                                                     <input type="text"  class="form-control form-control-line" name="requestor_name" value="{{ old('requestor_name', $data['testrequest'] ? $data['testrequest']->requestor_name : '') }}" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Designation </label>
                                                  <div class="col-md-12">
                                                     <select  class="form-control form-control-line" name="designation" >
                                                     <option value="">--Select--</option>
                                                     @foreach ($data['designations'] as $key=> $designations)
                                                       <option
                                                         value="{{$designations['designation_id']}}"
                                                         @if(isset($data['testrequest']->designation) && $data['testrequest']->designation == $designations['designation_id'])
                                                           selected
                                                         @endif
                                                       >
                                                       {{$designations['name']}}
                                                       </option>

                                                     @endforeach
                                                   </select>
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Contact Number </label>
                                                  <div class="col-md-12">
                                                     <input type="number" pattern="^[0-9]$" onKeyPress="if(this.value.length==12) return false;" value="{{ old('contact_no', $data['testrequest'] ? $data['testrequest']->contact_no : '') }}" class="form-control form-control-line" name="contact_no" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Email Id </label>
                                                  <div class="col-md-12">
                                                     <input type="email" value="{{ old('email_id', $data['testrequest'] ? $data['testrequest']->email_id : '') }}"  class="form-control form-control-line" name="email_id" >
                                                 </div>
                                              </div>

                                            </div>

                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-6">Request Date <span class="red">*</span></label>
                                                  <div class="col-md-6">
                                                     <input type="text" value="{{ old('request_date', $data['testrequest'] ? $data['testrequest']->request_date : '') }}"  max="<?php echo date("Y-m-d");?>" class="form-control form-control-line datepicker" name="request_date" required>
                                                 </div>
                                              </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-info">Save</button>
                                                    <a class="btn btn-warning" href="{{url('/test_request')}}">Cancel</a>
                                                    <!-- <button class="btn btn-success">Preview</button>
                                                    <button class="btn ">Print</button> -->
                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>
                                <div class="tab-pane" id="tab4" role="tabpanel" >
                                    <div class="card-block">

                                        <form action="{{ url('/test_request') }}" method="post" class="form-horizontal form-material">
                                            <h6>Name and type of referring facility</h6>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="enroll_id" value="{{$data['enroll_id']}}">
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-12">Facility Type <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line facility_type" name="facility_type" required>

                                                                <option value="">--Select--</option>
                                                                <!-- @foreach ($data['facility_type'] as $key=> $facility_type)
                                                                  <option
                                                                    value="{{$facility_type['facility_type_id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility_type['facility_type_id'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$facility_type['name']}}
                                                                  </option>

                                                                @endforeach -->
                                                                @foreach ($data['facility_types'] as $key=> $facility)
                                                                  <option
                                                                    value="{{$facility['id']}}"
                                                                    @if(isset($data['testrequest']->facility_type) && $data['testrequest']->facility_type == $facility['id'])
                                                                      selected
                                                                    @endif
                                                                  >
                                                                  {{$facility['name']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col hide facility_type_other">
                                                    <label class="col-md-6">Facility Type Other </label>
                                                    <div class="col-md-6">
                                                       <input type="text"  value="{{ old('facility_type_other', $data['testrequestservices']->facility_type_other) }}" class="form-control form-control-line" name="facility_type_other">
                                                   </div>
                                                </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">State <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="state" id="state4" required>
                                                              <option value="">--Select--</option>
                                                              @foreach ($data['state'] as $key=> $state)
                                                                      <option
                                                                        value="{{$state['STOCode']}}"
                                                                        @if(isset($data['testrequest']->state) && $data['testrequest']->state == $state['STOCode'])
                                                                        selected
                                                                        @endif
                                                                      >
                                                                        {{$state['name']}}
                                                                      </option>

                                                              @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">District <span class="red">*</span></label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="district" id="district4" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['district'] as $key=> $district)
                                                                <option
                                                                  value="{{$district['DTOCode']}}"
                                                                  @if(isset($data['testrequest']->district) && $data['testrequest']->district == $district['DTOCode'])
                                                                    selected
                                                                  @endif
                                                                >
                                                                {{$district['name']}}
                                                                </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>
                                                <div class="col ">
                                                    <label class="col-sm-12">TBU <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="tbu" id="tbu4"required>

                                                                <option value="">--Select--</option>
                                                                @foreach ($data['tbunit'] as $key=> $tbunit)
                                                                  <option
                                                                    value="{{$tbunit['TBUnitCode']}}"
                                                                    @if(isset($data['testrequest']->tbu) && $data['testrequest']->tbu == $tbunit['TBUnitCode'])
                                                                    selected
                                                                    @endif
                                                                  >
                                                                    {{$tbunit['TBUnitName']}}
                                                                  </option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-sm-12">Name of Facility <span class="red">*</span></label>
                                                      <div class="col-sm-12">
                                                          <select class="form-control form-control-line" name="facility_id" id="facility_id4" required>
                                                            <option value="">--Select--</option>
                                                            @foreach ($data['facility'] as $key=> $facility)
                                                              <option
                                                                value="{{$facility['id']}}"
                                                                @if(isset($data['testrequest']->facility_id) && $data['testrequest']->facility_id == $facility['id'])
                                                                  selected
                                                                @endif
                                                              >
                                                              {{$facility['DMC_PHI_Name']}}
                                                              </option>

                                                            @endforeach
                                                          </select>
                                                      </div>
                                              </div>

                                            </div>
                                            <br>
                                            <h6>Follow Up (Culture) :</h6>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-6">PMDT TB No. <span class="red">*</span></label>
                                                        <div class="col-md-6">
                                               <input type="number" class="form-control form-control-line" value="{{ old('pmdt_tb_no', $data['testrequestservices']->pmdt_tb_no) }}" name="pmdt_tb_no" required>
                                           </div>
                                                </div>
                                                <div class="col hide">
                                                    <label class="col-sm-12">Presumptive XDR TB <span class="red">*</span></label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control form-control-line" name="prsmptv_xdr_tb" required>
                                                                <option>At diagnosis</option>
                                                                <option>Contact of MDR/RR TB</option>
                                                                <option>Follow up SM+VE at  END IP</option>
                                                                <option>Private referall</option>
                                                                <option>Presumptive H mono/poly</option>
                                                                <option>MDR/RR TB at Diagnosis</option>
                                                                <option>>= 4 months culture positive</option>
                                                                <option>3 monthly for persistent culture positives</option>
                                                                <option>Culture reversion</option>
                                                                <option>Failure of MDR/RR-TB regimen</option>
                                                                <option>Recurrant case of Second Line Treatment</option>


                                                            </select>
                                                        </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col ">
                                                    <label class="col-sm-6">Treatment (Month/Week) <span class="red">*</span></label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control form-control-line" name="month_week" required>
                                                                <option value="Month"
                                                                @if($data['testrequestservices']->month_week=="Month")
                                                                   selected="selected"
                                                                @endif
                                                                >Month</option>
                                                                <option value="Week"
                                                                @if($data['testrequestservices']->month_week=="Week")
                                                                   selected="selected"
                                                                @endif
                                                                >Week</option>

                                                            </select>

                                                        </div>
                                                        <label class="col-sm-6">Month/Week No. <span class="red">*</span></label>
                                                            <div class="col-sm-6">

                                                                 <input type="text" class="form-control form-control-line" value="{{ old('treatment', $data['testrequestservices']->treatment) }}" name="treatment" required>
                                                            </div>
                                                </div>

                                                <div class="col ">
                                                    <label class="col-sm-6">Regimen <span class="red">*</span></label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control form-control-line" name="regimen_fu" id="regimen_fu" required>
                                                                <option value="Regimen for INH mono/poly resistant TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen for INH mono/poly resistant TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen for INH mono/poly resistant TB</option>
                                                                <option value="Regimen for MDR/RR TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen for MDR/RR TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen for MDR/RR TB</option>
                                                                <option value="Modified Regimen for MDR/RR-TB + FQ/SLI resistance"
                                                                @if($data['testrequestservices']->regimen_fu=="Modified Regimen for MDR/RR-TB + FQ/SLI resistance")
                                                                   selected="selected"
                                                                @endif
                                                                >Modified Regimen for MDR/RR-TB + FQ/SLI resistance</option>
                                                                <option value="Regimen for XDR TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen for XDR TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen for XDR TB</option>
                                                                <option value="Modified Regimen for mixed pattern resistance"
                                                                @if($data['testrequestservices']->regimen_fu=="Modified Regimen for mixed pattern resistance")
                                                                   selected="selected"
                                                                @endif
                                                                >Modified Regimen for mixed pattern resistance</option>
                                                                <option value="Regimen for Bedaquiline for MDR-TB Regimen +FQ/SLI resiatnce"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen for Bedaquiline for MDR-TB Regimen +FQ/SLI resiatnce")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen for Bedaquiline for MDR-TB Regimen +FQ/SLI resiatnce</option>
                                                                <option value="Regimen with Bedaquiline for XDR-TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen with Bedaquiline for XDR-TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen with Bedaquiline for XDR-TB</option>
                                                                <option value="Regimen with Bedaquiline for failures of regimenfor MDR- TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen with Bedaquiline for failures of regimenfor MDR- TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen with Bedaquiline for failures of regimenfor MDR- TB</option>
                                                                <option value="Regimen withBedaquiline for failures of regimenfor XDR- TB"
                                                                @if($data['testrequestservices']->regimen_fu=="Regimen with Bedaquiline for failures of regimenfor MDR- TBRegimen with Bedaquiline for failures of regimenfor MDR- TB")
                                                                   selected="selected"
                                                                @endif
                                                                >Regimen withBedaquiline for failures of regimenfor XDR- TB</option>
                                                                <option value="Other"
                                                                @if($data['testrequestservices']->regimen_fu=="Other")
                                                                   selected="selected"
                                                                @endif
                                                                >Other</option>

                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="col hide" id="fudrtb_regimen_other">
                                                  <label class="col-sm-6">Regimen Other</label>
                                                      <div class="col-sm-6">
                                                           <input type="text" class="form-control form-control-line" value="{{ old('fudrtb_regimen_other', $data['testrequestservices']->fudrtb_regimen_other) }}" name="fudrtb_regimen_other">
                                                      </div>
                                                </div>


                                                <div class="col hide">
                                                    <label class="col-md-6">type <span class="red">*</span></label>
                                                    <div class="col-md-6">
                                                       <input type="text"  class="form-control form-control-line" name="req_test_type" value="4" required>
                                                   </div>
                                                </div>

                                            </div>
                                            <br>
                                            <div class="row">
                                                @foreach ($data['services'] as $key=> $services)
                                                <div class="col-md-4 top5px">
                                                  <input id="{{$services['id']}}" class="service_array"
                                                    value="{{$services['id']}}"
                                                    @if(isset($data['_reqservices']) && !in_array($services['id'],$data['lpa_id_not']) && is_array($data['_reqservices']) && in_array($services['id'], $data['_reqservices']))
                                                      checked=""
                                                    @endif
                                                    name="services[]"
                                                    type="checkbox">{{$services['name']}}
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="dst_drugs hide">
                                              <div class="row  ">

                                                <label class="col-md-12">DST Drugs</label>
                                                @foreach ($data['dstdrugs'] as $key=> $drugs)
                                                <div class="col-md-4 ">
                                                  <input class="drugs_array"
                                                    value="{{$drugs['id']}}"
                                                    name="drugs[]"
                                                    type="checkbox">{{$drugs['name']}}
                                                </div>
                                                @endforeach

                                              </div>
                                            </div>

                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Requestor Name </label>
                                                  <div class="col-md-12">
                                                     <input type="text"  class="form-control form-control-line" name="requestor_name" value="{{ old('requestor_name', $data['testrequest'] ? $data['testrequest']->requestor_name : '') }}" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Designation </label>
                                                  <div class="col-md-12">
                                                     <select  class="form-control form-control-line" name="designation" >
                                                     <option value="">--Select--</option>
                                                     @foreach ($data['designations'] as $key=> $designations)
                                                       <option
                                                         value="{{$designations['designation_id']}}"
                                                         @if(isset($data['testrequest']->designation) && $data['testrequest']->designation == $designations['designation_id'])
                                                           selected
                                                         @endif
                                                       >
                                                       {{$designations['name']}}
                                                       </option>

                                                     @endforeach
                                                   </select>
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-12">Contact Number </label>
                                                  <div class="col-md-12">
                                                     <input type="number" pattern="^[0-9]$" onKeyPress="if(this.value.length==12) return false;" value="{{ old('contact_no', $data['testrequest'] ? $data['testrequest']->contact_no : '') }}" class="form-control form-control-line" name="contact_no" >
                                                 </div>
                                              </div>
                                              <div class="col ">
                                                  <label class="col-md-12">Email Id </label>
                                                  <div class="col-md-12">
                                                     <input type="email" value="{{ old('email_id', $data['testrequest'] ? $data['testrequest']->email_id : '') }}" class="form-control form-control-line" name="email_id" >
                                                 </div>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col ">
                                                  <label class="col-md-6">Request Date <span class="red">*</span></label>
                                                  <div class="col-md-6">
                                                     <input type="text"  value="{{ old('request_date', $data['testrequest'] ? $data['testrequest']->request_date : '') }}" max="<?php echo date("Y-m-d");?>" class="form-control form-control-line datepicker" name="request_date" required>


                                                 </div>
                                              </div>

                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-info">Save</button>
                                                    <a class="btn btn-warning" href="{{url('/test_request')}}">Cancel</a>
                                                    <!-- <button class="btn btn-success">Preview</button>
                                                    <button class="btn ">Print</button> -->
                                                </div>

                                            </div>

                                        </form>


                                    </div>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>




                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <script>
        @if(isset($data['testrequest']->req_test_type))
          activaTab('tab{{$data['testrequest']->req_test_type}}')
        @endif
        var dst_id = [{{$data['dst_id']}}];
        var lpa_id = [{{$data['lpa_id']}}];
        function activaTab(tab){
          $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
            $(document).ready(function() {

              var _sample = $('#regimen_fu').find(":selected").val();
              if(_sample=='Other'){
                $("#fudrtb_regimen_other").removeClass("hide");
              }else{
                $("#fudrtb_regimen_other").addClass("hide");
              }

              var _sample = $('.facility_type').find(":selected").val();
              if(_sample==13){
                $(".facility_type_other").removeClass("hide");
              }else{
                $(".facility_type_other").addClass("hide");
              }

              $('.service_array').click(function(){
                if($(this).is(':checked')){
                  var index = dst_id.indexOf(parseInt($(this).val()));
                  if(index >= 0){
                    console.log("dst checked",$(this).val(),index);
                    $(".dst_drugs").show();
                  }
                  var index = lpa_id.indexOf(parseInt($(this).val()));
                  if(index >= 0){
                    console.log("lpa checked",$(this).val(),index);
                    //lpa_id = [index];
                    // lpa_id.splice(index, 1);
                    $data['lpa_id_not'] = lpa_id.splice(index, 1);
                    console.log("lpa_id",lpa_id);
                  }

                }else{
                  var index = dst_id.indexOf(parseInt($(this).val()));
                  if(index >= 0){
                    console.log("dst unchecked",$(this).val(),index);
                    $(".dst_drugs").hide();
                  }
                  var index = lpa_id.indexOf(parseInt($(this).val()));
                  if(index >= 0){
                    console.log("dst unchecked",$(this).val(),index);
                    //lpa_id = [{{$data['lpa_id']}}];
                  }
                }
              });

              $("#regimen_fu").change(function(){
                  var _sample = $(this).find(":selected").val();
                  if(_sample=='Other'){
                    $("#fudrtb_regimen_other").removeClass("hide");
                    document.getElementById("fudrtb_regimen_other").setAttribute("required","required");
                  }else{
                    $("#fudrtb_regimen_other").addClass("hide");
                    document.getElementById("fudrtb_regimen_other").removeAttribute("required","required");
                  }
              });

              $(".facility_type").change(function(){
                  var _sample = $(this).find(":selected").val();
                  if(_sample==13){
                    $(".facility_type_other").removeClass("hide");
                    document.getElementById("facility_type_other").setAttribute("required","required");
                  }else{
                    $(".facility_type_other").addClass("hide");
                    document.getElementById("facility_type_other").removeAttribute("required","required");
                  }
              });

              $("#post_treatment").change(function(){
                  var _sample = $(this).find(":selected").val();
                  if(_sample=='Other'){
                    $("#other_post_treatment").removeClass("hide");
                    document.getElementById("other_post_treatment").setAttribute("required","required");
                  }else{
                    $("#other_post_treatment").addClass("hide");
                    document.getElementById("other_post_treatment").removeAttribute("required","required");
                  }
              });

                $("#state").change( function() {
                    var state = $(this).val();

                    $.ajax({
                        url: "{{url('district')}}"+'/'+state,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#district option").remove();
                            $('#district').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.district, function (i, item) {
                                $('#district').append($('<option>', {
                                    value: item.DTOCode,
                                    text : item.name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });
                $("#district3").change( function() {
                    var district = $(this).val();
                    var state = $('#state3').val();"facility_type"
                    // $.ajax({
                    //     url: "{{url('phi')}}"+'/'+state+'/'+district,
                    //     type:"GET",
                    //     processData: false,
                    //     contentType: false,
                    //     dataType : "json",
                    //     success: function(items) {
                    //
                    //         //$('#district').html();
                    //         $("#facility_id3 option").remove();
                    //         $('#facility_id3').append($('<option>', {
                    //             value: '',
                    //             text : 'Select'
                    //         }))
                    //         $.each(items.phi, function (i, item) {
                    //             $('#facility_id3').append($('<option>', {
                    //                 value: item.id,
                    //                 text : item.DMC_PHI_Name
                    //             }));
                    //         });
                    //     },
                    //     error: function() {
                    //       console.log("err")
                    //   }
                    // });
                    $.ajax({
                        url: "{{url('tbunit')}}"+'/'+state+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#tbu3 option").remove();
                            $('#tbu3').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.tbunit, function (i, item) {
                                $('#tbu3').append($('<option>', {
                                    value: item.TBUnitCode,
                                    text : item.TBUnitName
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });

                });

                $("#tbu3").change( function() {
                    var tbu = $(this).val();
                    var state = $('#state3').val();
                    var district = $('#district3').val();

                    //alert(district,state);

                    $.ajax({
                        url: "{{url('phi')}}"+'/'+state+'/'+tbu+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#facility_id3 option").remove();
                            $('#facility_id3').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.phi, function (i, item) {
                                $('#facility_id3').append($('<option>', {
                                    value: item.id,
                                    text : item.DMC_PHI_Name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });

                $("#district").change( function() {
                    var district = $(this).val();
                    var state = $('#state').val();
                    // $.ajax({
                    //     url: "{{url('phi')}}"+'/'+state+'/'+district,
                    //     type:"GET",
                    //     processData: false,
                    //     contentType: false,
                    //     dataType : "json",
                    //     success: function(items) {
                    //
                    //         //$('#district').html();
                    //         $("#facility_id option").remove();
                    //         $('#facility_id').append($('<option>', {
                    //             value: '',
                    //             text : 'Select'
                    //         }))
                    //         $.each(items.phi, function (i, item) {
                    //             $('#facility_id').append($('<option>', {
                    //                 value: item.id,
                    //                 text : item.DMC_PHI_Name
                    //             }));
                    //         });
                    //     },
                    //     error: function() {
                    //       console.log("err")
                    //   }
                    // });

                    $.ajax({
                        url: "{{url('tbunit')}}"+'/'+state+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#tbu option").remove();
                            $('#tbu').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.tbunit, function (i, item) {
                                $('#tbu').append($('<option>', {
                                    value: item.TBUnitCode,
                                    text : item.TBUnitName
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });

                $("#tbu").change( function() {
                    var tbu = $(this).val();
                    var state = $('#state').val();
                    var district = $('#district').val();
                    //alert(district,state);

                    $.ajax({
                        url: "{{url('phi')}}"+'/'+state+'/'+tbu+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#facility_id option").remove();
                            $('#facility_id').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.phi, function (i, item) {
                                $('#facility_id').append($('<option>', {
                                    value: item.id,
                                    text : item.DMC_PHI_Name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });

                $("#district4").change( function() {
                    var district = $(this).val();
                    var state = $('#state4').val();
                    // $.ajax({
                    //     url: "{{url('phi')}}"+'/'+state+'/'+district,
                    //     type:"GET",
                    //     processData: false,
                    //     contentType: false,
                    //     dataType : "json",
                    //     success: function(items) {
                    //
                    //         //$('#district').html();
                    //         $("#facility_id4 option").remove();
                    //         $('#facility_id4').append($('<option>', {
                    //             value: '',
                    //             text : 'Select'
                    //         }))
                    //         $.each(items.phi, function (i, item) {
                    //             $('#facility_id4').append($('<option>', {
                    //                 value: item.id,
                    //                 text : item.DMC_PHI_Name
                    //             }));
                    //         });
                    //     },
                    //     error: function() {
                    //       console.log("err")
                    //   }
                    // });

                    $.ajax({
                        url: "{{url('tbunit')}}"+'/'+state+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#tbu4 option").remove();
                            $('#tbu4').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.tbunit, function (i, item) {
                                $('#tbu4').append($('<option>', {
                                    value: item.TBUnitCode,
                                    text : item.TBUnitName
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });

                $("#tbu4").change( function() {
                    var tbu = $(this).val();
                    var state = $('#state4').val();
                    var district = $('#district4').val();

                    //alert(district,state);

                    $.ajax({
                        url: "{{url('phi')}}"+'/'+state+'/'+tbu+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#facility_id4 option").remove();
                            $('#facility_id4').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.phi, function (i, item) {
                                $('#facility_id4').append($('<option>', {
                                    value: item.id,
                                    text : item.DMC_PHI_Name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });


                $("#district2").change( function() {
                    var district = $(this).val();
                    var state = $('#state2').val();
                    // $.ajax({
                    //     url: "{{url('phi')}}"+'/'+state+'/'+district,
                    //     type:"GET",
                    //     processData: false,
                    //     contentType: false,
                    //     dataType : "json",
                    //     success: function(items) {
                    //
                    //         //$('#district').html();
                    //         $("#facility_id2 option").remove();
                    //         $('#facility_id2').append($('<option>', {
                    //             value: '',
                    //             text : 'Select'
                    //         }))
                    //         $.each(items.phi, function (i, item) {
                    //             $('#facility_id2').append($('<option>', {
                    //                 value: item.id,
                    //                 text : item.DMC_PHI_Name
                    //             }));
                    //         });
                    //     },
                    //     error: function() {
                    //       console.log("err")
                    //   }
                    // });

                    $.ajax({
                        url: "{{url('tbunit')}}"+'/'+state+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#tbu2 option").remove();
                            $('#tbu2').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.tbunit, function (i, item) {
                                $('#tbu2').append($('<option>', {
                                    value: item.TBUnitCode,
                                    text : item.TBUnitName
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });


                $("#tbu2").change( function() {
                    var tbu = $(this).val();
                    var state = $('#state2').val();
                    var district = $('#district2').val();
                    //alert(district,state);

                    $.ajax({
                        url: "{{url('phi')}}"+'/'+state+'/'+tbu+'/'+district,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#facility_id2 option").remove();
                            $('#facility_id2').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.phi, function (i, item) {
                                $('#facility_id2').append($('<option>', {
                                    value: item.id,
                                    text : item.DMC_PHI_Name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });


                $("#state4").change( function() {
                    var state = $(this).val();

                    $.ajax({
                        url: "{{url('district')}}"+'/'+state,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#district4 option").remove();
                            $('#district4').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.district, function (i, item) {
                                $('#district4').append($('<option>', {
                                    value: item.DTOCode,
                                    text : item.name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });
                $("#state2").change( function() {
                    var state = $(this).val();

                    $.ajax({
                        url: "{{url('district')}}"+'/'+state,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#district2 option").remove();
                            $('#district2').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.district, function (i, item) {
                                $('#district2').append($('<option>', {
                                    value: item.DTOCode,
                                    text : item.name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });
                $("#state3").change( function() {
                    var state = $(this).val();

                    $.ajax({
                        url: "{{url('district')}}"+'/'+state,
                        type:"GET",
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        success: function(items) {

                            //$('#district').html();
                            $("#district3 option").remove();
                            $('#district3').append($('<option>', {
                                value: '',
                                text : 'Select'
                            }))
                            $.each(items.district, function (i, item) {
                                $('#district3').append($('<option>', {
                                    value: item.DTOCode,
                                    text : item.name
                                }));
                            });
                        },
                        error: function() {
                          console.log("err")
                      }
                    });
                });

            });
        </script>
@endsection
