<!-- Modal Dialog -->
<div class="modal fade" id="resultpopupDiv" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">LC DST Inoculation Result</h4>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p></p>
          <form class="form-horizontal form-material" action="" method="post" id="nxtpopup">
          <input name="_method" type="hidden" value="patch">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="service_log_id" id="next_log_id" value="">
          <input type="hidden" name="lc_dst_tr_id" id="lc_dst_tr_id" value="">
          <div class="row">
            <div class="col ">
                <label class="col-md-12">Sample ID</label>
                <div class="col-md-12">
                   <input type="text" name="sample_id" class="form-control form-control-line sampleId" value="" id="next_sample_id" disabled>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
               <label class="col-md-12">Date of Result</label>
               <div class="col-md-12">
                 <input type="text" placeholder="dd-mm-yy" name="result_date"  class="form-control datepicker" required="required">
                  <!-- <input type="date" max="<?php echo date("Y-m-d");?>" name="result_date" class="form-control form-control-line" required> -->
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
               <label class="col-md-12">Next step</label>
               <div class="col-md-12">
                 <select name="next_step" class="form-control form-control-line" required>
                   <option value="">--Select--</option>
                   <option value="0">Results Finalization</option>
                    <option value="1">Repeat DST</option>
                    <option value="2">Interim Report</option>
                 </select>
              </div>
            </div>
          </div>

          <div class="row" id="drug_names">

          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Cancel</button>
        <button type="submit" class="pull-right btn btn-primary btn-md" id="nxtconfirm">Ok</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
$(function(){
  $('#resultpopupDiv').on('show.bs.modal', function (e) {

       // Pass form reference to modal for submission on yes/ok
       var form = $(e.relatedTarget).closest('form');
       $(this).find('.modal-footer #confirm').data('form', form);
   });


});
</script>
