<!-- Modal Dialog -->
<div class="modal fade" id="extractionpopupDiv" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
        <h4 class="modal-title">LC DST Innoculation</h4>
      </div>
      <div class="modal-body">
        <p></p>
        <form class="form-horizontal form-material" action="{{ url('/lc_dst_inoculation') }}" method="post" id="extractionpopup">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="log_id" id="log_id" value="">
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">Sample ID</label>
                   <div class="col-md-12">
                      <input type="text" name="sample_id" class="form-control form-control-line sampleId" value="" id="sample_id" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">Positive MGIT sequence ID</label>
                   <div class="col-md-12">
                      <input type="text" name="mgit_seq_id" class="form-control form-control-line" value="" id="mgit_seq_id" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">DST carrier set ID  1</label>
                   <div class="col-md-12">
                      <input type="text" name="dst_c_id1" class="form-control form-control-line" value="" id="dst_c_id1" required>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">DST carrier set ID 2</label>
                   <div class="col-md-12">
                      <input type="text" name="dst_c_id2" class="form-control form-control-line" value="" id="dst_c_id2" >
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">DST carrier set ID 3</label>
                   <div class="col-md-12">
                      <input type="text" name="dst_c_id3" class="form-control form-control-line" value="" id="dst_c_id3" >
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="col-md-12">
                   <label class="col-md-12">Date of DST inoculation</label>
                   <div class="col-md-12">
                      <input type="text" name="inoculation_date" max="<?php echo date("Y-m-d");?>" class="form-control form-control-line datepicker" required>
                  </div>
               </div>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Cancel</button>
        <button type="submit" class="pull-right btn btn-primary btn-md" id="submit">Ok</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
$('#extractionpopupDiv').on('show.bs.modal', function (e) {

     // Pass form reference to modal for submission on yes/ok
     var form = $(e.relatedTarget).closest('form');
     $(this).find('.modal-footer #confirm').data('form', form);
 });

</script>
