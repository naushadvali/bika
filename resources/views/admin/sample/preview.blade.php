@extends('admin.layout.app')
@section('content')
 <div class="page-wrapper">
            <div class="container-fluid">
              <div class="row page-titles">
                  <div class="col-md-5 col-8 align-self-center">
                      <h3 class="text-themecolor m-b-0 m-t-0">Sample Opening Preview</h3>

                  </div>


                 </div>
              </div>

              <div class="row page-titles">

                  <div class="col-md-7 col-4 align-self-center">
                    <div style="padding-left:30px;">
                      <p>No. of samples created : {{$data['sample_detail']->no_of_samples}}</p>
                      <p>Enrollment Number : {{$data['enroll']->enroll}}</p>
                      <p>Name : {{$data['sample_detail']->name}}</p>
                   </div>

                 </div>
              </div>

                <div class="row">

                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" style="width: auto;overflow-y: scroll;">

                                    <table id="example" class="table table-striped table-bordered responsive col-xlg-12" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="hide">ID</th>
                                              <th>Sample ID</th>
                                              <th>Date of Receipt</th>
                                              <th>Sample type</th>
                                              <th>Other Sample type</th>
                                              <th>Sample Accepted/Rejected</th>
                                              <th>Rejection reason</th>

                                              <th>Sample Quality</th>
                                              <th>Reason for Test</th>

                                              <th>Follow up month</th>
                                              <th>Sample sent to</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                          @foreach ($data['sample'] as $key=> $samples)
                                          <tr>
                                            <td class="hide">{{$samples->enroll_id}}</td>
                                            <td>{{$samples->sample_label}}</td>
                                            <td>{{$samples->receive_date}}</td>
                                            <td>{{$samples->sample_type}}</td>
                                            <td>
                                              @if($samples->sample_type=="Other" || $samples->sample_type=="Others")
                                              {{$samples->others_type}}
                                              @endif
                                            </td>
                                            <td>{{$samples->is_accepted}}</td>
                                            <td>{{$samples->rejection}}</td>

                                            <td>{{$samples->sample_quality}}</td>
                                            <td>{{$samples->test_reason}}</td>

                                            <td>{{$samples->fu_month}}</td>
                                            <td>
                                            	@if($samples->service_id==1)
                                            	ZN Microscopy
                                            	@elseif($samples->service_id==2)
                                            	FM Microscopy
                                            	@elseif($samples->service_id==3)
                                            	Decontamination
                                            	@elseif($samples->service_id==4)
                                            	CBNAAT MTB/RIF
                                            	@elseif($samples->service_id==5)
                                            	AFB Culture
                                            	@elseif($samples->service_id==6)
                                            	LPA 1st line
                                            	@elseif($samples->service_id==7)
                                            	LPA 2nd line
                                              @elseif($samples->service_id==8)
                                            	DNA Extraction
                                              @elseif($samples->service_id==12)
                                            	PCR
                                              @elseif($samples->service_id==13)
                                            	LPA Both Line
                                              @elseif($samples->service_id==14)
                                            	Hybridization
                                              @elseif($samples->service_id==15)
                                            	LPA interpretation
                                              @elseif($samples->service_id==16)
                                            	Culture inoculation
                                              @elseif($samples->service_id==17)
                                            	LC inoculation
                                              @elseif($samples->service_id==18)
                                            	LC culture reporting
                                              @elseif($samples->service_id==20)
                                            	LJ culture reporting
                                            	@elseif($samples->service_id==11)
                                            	Storage
                                            	@elseif($samples->service_id==26)
                                            	Rejected
                                            	@endif

                                            </td>


                                          </tr>
                                          @endforeach

                                      </tbody>
                                        </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
        </div>



@endsection
