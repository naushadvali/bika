@extends('admin.layout.app')
@section('content')

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css" />

        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Sample Opening</h3>

                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                       <!-- <p  class="pull-right ">Enrollment ID : {{ str_pad($data['enroll_id'], 10, "0", STR_PAD_LEFT) }}</p> -->
                   </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <form class="form-horizontal form-material" action="{{ url('/sample') }}" method="post" enctype='multipart/form-data'>
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-block">
                                  <div class="row">
                                    <div class="col hide" id="noOfSampleDiv">
                                      <label class="col-md-12">No. of samples submitted <span class="red">*</span> </label>
                                      <div class="col-md-12">
                                          <select  class="form-control form-control-line" id="noOfSample" name="no_of_samples" required>
                                              <option>1</option>
                                              <option>2</option>
                                              <!-- <option>3</option>
                                              <option>4</option> -->
                                          </select>
                                      </div>
                                    </div>
                                    <div class="col">
                                      <label class="col-md-6">Enrollment Number </label>
                                      <div class="col-md-6">
                                        <!-- <input type="text" name="enroll_label" class="form-control form-control-line" value="{{ str_pad($data['enroll_id'], 10, "0", STR_PAD_LEFT) }}" id="enroll_label" required> -->
                                          <input type="text" name="enroll_label" class="form-control form-control-line" onKeyPress="if(this.value.length==11) return false;" id="enroll_label" required>
                                      </div>
                                    </div>
                                  </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Name <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <input type="hidden" name="enroll_id" value="{{ str_pad($data['enroll_id'], 10, "0", STR_PAD_LEFT) }}" id="enrollId">
                                               <input type="text" name="name" style="text-transform:capitalize;" class="form-control form-control-line" onkeyup="nospaces(this)" required>
                                           </div>
                                        </div>
                                        <div class="col ">
                                            <!-- <label class="col-md-12">Nikshay ID</label>
                                            <div class="col-md-12">
                                               <input type="number" name="nikshay_id" class="form-control form-control-line" required>
                                           </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row sampleForm" id="sampleForm">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-block">
                                    <h4>Sample </h4>
                                    <div class="row">
                                        <div class="col ">
                                            <label class="col-md-12">Sample ID <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                               <!-- <input type="text" name="sample_id[]" class="form-control form-control-line sampleId" value="{{ str_pad($data['enroll_id'], 10, "0", STR_PAD_LEFT) }}A" id="sampleId" required> -->
                                               <input type="text" name="sample_id[]" class="form-control form-control-line sampleId" value="" id="sampleId" required>
                                           </div>
                                        </div>
                                        <div class="col ">
                                            <label class="col-md-12">Date of Receipt </label>
                                            <div class="col-md-12">

                                               <input type="text" name="receive_date[]" value="{{$data['today']}}" class="form-control form-control-line"  required>

                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Sample type <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                              <select name="sample_type[]" class="form-control form-control-line sample_type" id="sample_typeA" required>
                                                <option value="">--Select--</option>
                                                <option value="Sputum">Sputum</option>
                                                <option value="Other">Other</option>
                                              </select>
                                           </div>
                                        </div>
                                        <div class="col hide other_sample_type" id="other_sample_typeA">
                                            <label class="col-md-12">Other sample type <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                              <select id="other_sample_type" name="other_sample_type[]" class="form-control form-control-line">
                                                <option value="">--Select--</option>
                                                <option value="BAL">BAL</option>
                                                <option value="Pus">Pus</option>
                                                <option value="CSF">CSF</option>
                                                <option value="GA">GA</option>
                                                <option value="Pericardial fluid">Pericardial fluid</option>
                                                <option value="EB tissue">EB tissue</option>
                                                <option value="Urine">Urine</option>
                                                <option value="AFB MTB positive culture (LJ or LC)">AFB MTB positive culture (LJ or LC)</option>
                                                <option value="Pleural fluid">Pleural fluid</option>
                                                <option value="FNAC">FNAC</option>
                                                <option value="Others">Others</option>
                                              </select>
                                           </div>
                                        </div>
                                        <div class="col" >
                                        <div class="col hide others" id="othersA">
                                            <label class="col-md-12">Other</label>
                                            <div class="col-md-12">
                                                 <input type="text" id="others_typeA" name="others_type[]" class="form-control form-control-line others_type" >
                                           </div>
                                        </div>
                                      </div>


                                    </div>

                                    <div class="row">
                                       <div class="col">
                                            <label class="col-md-12">Sample Accepted/Rejected <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                              <select name="is_accepted[]" id="sample_statusA" class="form-control form-control-line sample_status" required>
                                                <option value="">--Select--</option>
                                                <option value="Accepted">Accepted</option>
                                                <option value="Rejected">Rejected</option>
                                              </select>
                                           </div>
                                      </div>
                                      <div class="col hide reject" id="rejectA">
                                            <label class="col-md-12">Reason of Rejection <span class="red">*</span> </label>
                                            <div class="col-md-12">

                                              <select  class="form-control form-control-line rejection" id="rejectionA" name="rejection">
                                                <option value="">--Select--</option>
                                              <option value="Sample leakage in the box">Sample leakage in the box</option>
                                              <option value="Wrong forms">Wrong forms</option>
                                              <option value="Insufficient quantity of sample">Insufficient quantity of sample</option>
                                              <option value="Wrong labelling">Wrong labelling</option>
                                              <option value="Sample transport with out cold chain">Sample transport with out cold chain</option>
                                              <option value="Sample received outside of the TAT time">Sample received outside of the TAT time</option>
                                              <option value="Other reason">Other reason</option>
                                          </select>
                                           </div>
                                        </div>
                                        <div class="col" >
                                        <div class="col hide other" id="otherA">
                                            <label class="col-md-12">Mention Reason of Rejection</label>
                                            <div class="col-md-12">
                                                 <input type="text" id="otherRA" name="reason_reject" class="form-control form-control-line" >
                                           </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">

                                            <label class="col-md-12">Sample Quality <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                               <!-- <input type="text" name="sample_quality[]" class="form-control form-control-line" required> -->
                                               <select name="sample_quality[]" class="form-control form-control-line sample_quality" id="sample_qualityA" required>
                                                 <option value="">--Select--</option>
                                                 <option value="Blood-stained purulent">Blood-stained purulent</option>
                                                 <option value="Thick mucoid">Thick mucoid</option>
                                                 <option value="Mucopurulent">Mucopurulent</option>
                                                 <option value="Saliva">Saliva</option>
                                                 <option value="Food particles">Food particles</option>
                                                 <option value="NA">NA</option>
                                               </select>
                                           </div>


                                        </div>
                                        <!-- <div class="col hide other_sample_type" id="other_sample_typeA">
                                            <label class="col-md-12">Other sample type <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                              <select name="other_sample_type[]" class="form-control form-control-line">
                                                <option value="">--Select--</option>
                                                <option value="BAL">BAL</option>
                                                <option value="Pus">Pus</option>
                                                <option value="CSF">CSF</option>
                                                <option value="GA">GA</option>
                                                <option value="Pericardial fluid">Pericardial fluid</option>
                                                <option value="EB tissue">EB tissue</option>
                                                <option value="Urine">Urine</option>
                                                <option value="AFB MTB positive culture (LJ or LC)">AFB MTB positive culture (LJ or LC)</option>
                                                <option value="Pleural fluid">Pleural fluid</option>
                                                <option value="FNAC">FNAC</option>
                                                <option value="Others">Others</option>
                                              </select>
                                           </div>
                                        </div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Reason for Test <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                               <!-- <textarea rows="5" name="test_reason[]" class="form-control form-control-line" required></textarea> -->
                                               <select name="test_reason[]" class="form-control form-control-line test_reason" id="test_reasonA" required>
                                                 <option value="">--Select--</option>
                                                 <option value="DX">Diagnosis</option>
                                                 <option value="FU">Follow up</option>
                                                 <option value="EQA">EQA</option>
                                               </select>
                                           </div>
                                        </div>
                                        <div class="col hide fu_month" id="fu_monthA">
                                            <label class="col-md-12">Follow up month <span class="red">*</span></label>
                                            <div class="col-md-12">
                                              <select id="fu_month_valueA" name="fu_month[]" class="form-control form-control-line fu_month_value month" >
                                                <option value="">--Select--</option>
                                                <option value="End IP">End IP</option>
                                                <option value="End CP">End CP</option>

                                                <option value="6 M">6 M</option>
                                                <option value="12 M">12 M</option>
                                                <option value="18 M">18 M</option>
                                                <option value="24 M">24 M</option>

                                                <option value="Other">Other</option>
                                              </select>
                                           </div>
                                        </div>
                                        <div class="col hide followup-other" id="followup-otherA">
                                            <label class="col-md-12">Other </label>
                                            <div class="col-md-12">
                                               <input type="text" id="followup_otherA" name="followup_other[]" class="form-control form-control-line followup_other" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Sample Sent to <span class="red">*</span> </label>
                                            <div class="col-md-12">
                                               <!-- <textarea rows="5" name="test_reason[]" class="form-control form-control-line" required></textarea> -->
                                               <select name="service_id[]" class="form-control form-control-line test_reason next_test" id="next_testA" required>
                                                 <option value="">--Select--</option>
                                                 <!-- @foreach ($data['services'] as $key => $service)
                                                  <option value="{{$service->id}}">{{$service->name}}</option>
                                                 @endforeach -->
                                                <option value="1">ZN Microscopy</option>
                                                <option value="2">FM Microscopy</option>
                                                <option value="3">Decontamination</option>
                                                <option value="4">CBNAAT MTB/RIF</option>
                                                <option value="16">AFB Culture Innoculation</option>
                                                <option value="8F">LPA 1st line</option>
                                                <option value="8S">LPA 2nd line</option>
                                                <option value="11">Storage</option>
                                                <!-- <option value="24">BMW</option> -->
                                               </select>
                                           </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="moreSample"></div>


                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-info" id="submit_sample">Save</button>
                            <!-- <button class="btn btn-success">Preview</button>
                            <button class="btn ">Print</button> -->
                            <!-- <a class="btn btn-default btn-sm" href="/sample_preview/{{$data['enroll_id']}}">Preview </a> -->
                        </div>

                    </div>
                </form>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>


<script>
var alpha = ["A","B","C","D","E"];
    $(function(){
        $("#enroll_label").keyup(function(){
          var _sample = $(this).val()+"A";
          $("#sampleId").val(_sample);
          $("#noOfSampleDiv").removeClass("hide");
        });
        $("#noOfSample").change(function(){
            $("#moreSample").html('');
            var _noOfSample = $(this).val();

            for(var i = 1; i < _noOfSample; i++) {
                $( "#sampleForm" ).clone().appendTo( "#moreSample" );

            }
            var enroll_id = $("#enroll_label").val();
            $(".sampleForm").each(function(i){
              $(this).find(".sampleId").val(enroll_id+alpha[i]);
              $(this).find(".sample_type").attr('id', "sample_type"+alpha[i]);
              $(this).find(".other_sample_type").attr('id', "other_sample_type"+alpha[i]);
              $(this).find(".test_reason").attr('id', "test_reason"+alpha[i]);
              $(this).find(".next_test").attr('id', "next_test"+alpha[i]);
              $(this).find(".followup-other").attr('id', "followup-other"+alpha[i]);
              $(this).find(".followup_other").attr('id', "followup_other"+alpha[i]);
              $(this).find(".sample_quality").attr('id', "sample_quality"+alpha[i]);
              $(this).find(".fu_month").attr('id', "fu_month"+alpha[i]);
              $(this).find(".fu_month_value").attr('id', "fu_month_value"+alpha[i]);
              $(this).find(".sample_status").attr('id', "sample_status"+alpha[i]);
              $(this).find(".rejection").attr('id', "rejection"+alpha[i]);
              $(this).find(".reject").attr('id', "reject"+alpha[i]);
              $(this).find(".other").attr('id', "other"+alpha[i]);
              $(this).find(".others").attr('id', "others"+alpha[i]);
              $(this).find(".others_type").attr('id', "others_type"+alpha[i]);
            })
        });
        $(".sampleId").change(function(){
          var sample = $(this).val().slice(0,-1);
          $("#enroll_label").val(sample);

          $(".sampleForm:not(:first)").each(function(i){
            $(this).find(".sampleId").val(sample+alpha[i+1]);
          })
        });
        $("#sample_typeA").change(function(){
            var _sample = $(this).val();
            var _noOfSample = $(this).val();
            $(".sampleForm").each(function(i){
              $(this).find(".sample_type").val(_sample);
            })

        });

        //  $("#sample_typeA").change(function(){
        //     var _sample = $("#sample_typeA").val();
        //       document.getElementById("sample_typeB").value = _sample;
        //       document.getElementById("sample_typeC").value = _sample;
        //       document.getElementById("sample_typeD").value = _sample;

        // });

        $("#other_sample_type").change(function(){
            var _sample = $(this).find(":selected").val();
            if(_sample=='Others'){
              $("#othersA").removeClass("hide");
              $("#othersB").removeClass("hide");
              $("#othersC").removeClass("hide");
              $("#othersD").removeClass("hide");
              document.getElementById("other_type").setAttribute("required","required");
            }else{
              $("#othersA").addClass("hide");
              $("#othersB").addClass("hide");
              $("#othersC").addClass("hide");
              $("#othersD").addClass("hide");
            }
        });

        $("#sample_typeA").change(function(){
            var _sample = $("#sample_typeA").val();
            if(_sample=='Other'){
              $("#other_sample_typeA").removeClass("hide");
              $("#other_sample_typeB").removeClass("hide");
              $("#other_sample_typeC").removeClass("hide");
              $("#other_sample_typeD").removeClass("hide");
              document.getElementById("other_sample_type").setAttribute("required","required");
            }else{
              $("#other_sample_typeA").addClass("hide");
              $("#other_sample_typeB").addClass("hide");
              $("#other_sample_typeC").addClass("hide");
              $("#other_sample_typeD").addClass("hide");
              document.getElementById("other_sample_type").removeAttribute("required","required");
            }
        });

        $("#other_sample_typeA").change(function(){
            var _sample = $(this).find(":selected").val();
              $("#other_sample_typeB select").val(_sample).change();
              $("#other_sample_typeC select").val(_sample).change();
              $("#other_sample_typeD select").val(_sample).change();


        });

        $("#others_typeA").change(function(){
            var _sample = $(this).val();
              $("#others_typeB").val(_sample).change();
              $("#others_typeC").val(_sample).change();
              $("#others_typeD").val(_sample).change();


        });

        $("#fu_monthA").change(function(){
            var _sample = $(this).find(":selected").val();

              $("#fu_monthB select").val(_sample).change();
              $("#fu_monthC select").val(_sample).change();
              $("#fu_monthD select").val(_sample).change();


        });



        $("#test_reasonA").change(function(){
            var _sample = $(this).val();
            var _noOfSample = $(this).val();
            $(".sampleForm").each(function(i){
              $(this).find(".test_reason").val(_sample);
            })

        });
        $("#sample_qualityA").change(function(){
            var _sample = $(this).val();
            var _noOfSample = $(this).val();
            $(".sampleForm").each(function(i){
              $(this).find(".sample_quality").val(_sample);
            })

        });

        $("#fu_monthA").change(function(){
          var _sample = $("#fu_monthA").find(":selected").val();

          if(_sample=='Other'){
            $("#followup-otherA").removeClass("hide");
            $("#followup-otherA").attr("required","required");
            $("#followup-otherB").removeClass("hide");
            $("#followup-otherB").attr("required","required");
            $("#followup-otherC").removeClass("hide");
            $("#followup-otherC").attr("required","required");
            $("#followup-otherD").removeClass("hide");
            $("#followup-otherD").attr("required","required");
          }
          else {
            $("#followup-otherA").addClass("hide");
            $("#followup-otherB").addClass("hide");
            $("#followup-otherC").addClass("hide");
            $("#followup-otherD").addClass("hide");
          }

        });



        $("#test_reasonA").change(function(){
            var _sample = $("#test_reasonA").val();
            if(_sample=='FU'){
              $("#fu_monthA").removeClass("hide");
              document.getElementById("fu_month_valueA").setAttribute("required","required");
              $("#fu_monthB").removeClass("hide");
              document.getElementById("fu_month_valueB").setAttribute("required","required");
              $("#fu_monthC").removeClass("hide");
              document.getElementById("fu_month_valueC").setAttribute("required","required");
              $("#fu_monthD").removeClass("hide");
              document.getElementById("fu_month_valueD").setAttribute("required","required");

              document.getElementById(".month").setAttribute("required","required");
            }else{
              $("#fu_monthA").addClass("hide");
              $("#fu_monthB").addClass("hide");
              $("#fu_monthC").addClass("hide");
              $("#fu_monthD").addClass("hide");
            }
        });

        $("#test_reasonB").change(function(){
            var _sample = $("#test_reasonB").val();
            if(_sample=='FU'){
              $("#fu_monthA").removeClass("hide");
              document.getElementById("fu_month_valueA").setAttribute("required","required");
              $("#fu_monthB").removeClass("hide");
              document.getElementById("fu_month_valueB").setAttribute("required","required");
              $("#fu_monthC").removeClass("hide");
              document.getElementById("fu_month_valueC").setAttribute("required","required");
              $("#fu_monthD").removeClass("hide");
              document.getElementById("fu_month_valueD").setAttribute("required","required");

              document.getElementById(".month").setAttribute("required","required");
            }else{
              $("#fu_monthA").addClass("hide");
              $("#fu_monthB").addClass("hide");
              $("#fu_monthC").addClass("hide");
              $("#fu_monthD").addClass("hide");
            }
        });

          var _sample = $("#test_reasonB").val();
          if(_sample=='FU'){
            $("#fu_monthB").removeClass("hide");
            document.getElementById("fu_month_valueB").setAttribute("required","required");
            document.getElementById(".month").setAttribute("required","required");
          }else{
            $("#fu_monthB").addClass("hide");
          }

        $("#followup_otherA").change(function(){
            var _sample = $("#followup_otherA").val();
            // alert(_sample);
              document.getElementById("followup_otherB").value = _sample;
              document.getElementById("followup_otherC").value = _sample;
              document.getElementById("followup_otherD").value = _sample;

        });


        $("#test_reasonA").change(function(){
            var _sample = $("#test_reasonA").val();
              document.getElementById("test_reasonB").value = _sample;

              document.getElementById("test_reasonC").value = _sample;
              document.getElementById("test_reasonD").value = _sample;

        });


        $("#sample_statusA").change(function(){
            var _sample = $("#sample_statusA").val();
            if(_sample=='Rejected'){
              $('#next_testA').append($("<option></option>").attr("value",24).text('BWM'));
              $("#rejectA").removeClass("hide");
              document.getElementById("test_reasonA").value = "";
              document.getElementById("sample_qualityA").value = "";
              document.getElementById("test_reasonA").setAttribute("disabled","disabled");
              document.getElementById("next_testA").value = "24";
              document.getElementById("next_testA").setAttribute("disabled","disabled");
              document.getElementById("sample_qualityA").setAttribute("disabled","disabled");
              document.getElementById("rejectionA").setAttribute("required","required");
              document.getElementById("test_reasonA").removeAttribute("required","required");
              document.getElementById("next_testA").removeAttribute("required","required");
              document.getElementById("sample_qualityA").removeAttribute("required","required");

            }else{
              $("#next_testA option[value='24']").remove();
              document.getElementById("rejectionA").removeAttribute("required","required");
              $("#test_reasonA").removeAttr("disabled");
              $("#sample_qualityA").removeAttr("disabled");
              $("#next_testA").removeAttr("disabled");
              $("#rejectA").addClass("hide");
            }
        });

        $("#rejectA").change(function(){
            var _sample = $("#rejectionA").val();
            if(_sample=='Other reason'){
              $("#otherA").removeClass("hide");
              document.getElementById("otherRA").setAttribute("required","required");

            }else{
              $("#otherA").addClass("hide");
              document.getElementById("otherRA").removeAttribute("required","required");
            }
        });
        $(document).on("change", "#sample_statusB" , function() {
          var _sample = $(this).val();
          if(_sample=='Rejected'){
            $("#rejectB").removeClass("hide");
            $('#next_testB').append($("<option></option>").attr("value",24).text('BWM'));
            document.getElementById("test_reasonB").value = "";
            document.getElementById("sample_qualityB").value = "";
            document.getElementById("test_reasonB").setAttribute("disabled","disabled");
            document.getElementById("next_testB").value = "24";
            document.getElementById("next_testB").setAttribute("disabled","disabled");
            document.getElementById("sample_qualityB").setAttribute("disabled","disabled");
            document.getElementById("rejectionB").setAttribute("required","required");
            document.getElementById("test_reasonB").removeAttribute("required","required");
            document.getElementById("next_testB").removeAttribute("required","required");
            document.getElementById("sample_qualityB").removeAttribute("required","required");
          }else{
            //$("#otherB").removeClass("hide");
            $("#next_testB option[value='24']").remove();
            document.getElementById("rejectionB").removeAttribute("required","required");
            $("#test_reasonB").removeAttr("disabled");
            $("#sample_qualityB").removeAttr("disabled");
            $("#next_testB").removeAttr("disabled");
            $(".other").addClass("hide");
            $("#rejectB").addClass("hide");
          }
        });
        $(document).on("change", "#rejectionB" , function() {
          var _sample = $(this).val();
          console.log(_sample);
          if(_sample=='Other reason'){
            $("#otherB").removeClass("hide");
            document.getElementById("otherRB").setAttribute("required","required");
          }else{
            $("#otherB").addClass("hide");
            document.getElementById("otherRB").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#sample_statusC" , function() {
          var _sample = $(this).val();
          if(_sample=='Rejected'){
            $("#rejectC").removeClass("hide");
            $('#next_testC').append($("<option></option>").attr("value",24).text('BWM'));
            document.getElementById("test_reasonC").value = "";
            document.getElementById("sample_qualityC").value = "";
            document.getElementById("test_reasonC").setAttribute("disabled","disabled");
            document.getElementById("next_testC").value = "24";
            document.getElementById("next_testC").setAttribute("disabled","disabled");

            document.getElementById("sample_qualityC").setAttribute("disabled","disabled");
            document.getElementById("rejectionC").setAttribute("required","required");
            document.getElementById("test_reasonC").removeAttribute("required","required");
            document.getElementById("next_testC").removeAttribute("required","required");
            document.getElementById("sample_qualityC").removeAttribute("required","required");

          }else{
            $(".other").addClass("hide");
            $("#next_testC option[value='24']").remove();
            document.getElementById("rejectionC").removeAttribute("required","required");
            $("#test_reasonC").removeAttr("disabled");
            $("#sample_qualityC").removeAttr("disabled");
            $("#next_testC").removeAttr("disabled");
            $(".other").addClass("hide");
            //$("#otherC").removeClass("hide");
            $("#rejectC").addClass("hide");
          }
        });
        $(document).on("change", "#rejectionC" , function() {
          var _sample = $(this).val();
          console.log(_sample);
          if(_sample=='Other reason'){
            $("#otherC").removeClass("hide");
            document.getElementById("otherRC").setAttribute("required","required");
          }else{
            $("#otherC").addClass("hide");
            document.getElementById("otherRC").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#sample_statusD" , function() {
          var _sample = $(this).val();
          if(_sample=='Rejected'){
            $("#rejectD").removeClass("hide");
            document.getElementById("test_reasonD").value = "";
            $('#next_testD').append($("<option></option>").attr("value",24).text('BWM'));
            document.getElementById("sample_qualityD").value = "";
            document.getElementById("test_reasonD").setAttribute("disabled","disabled");
            document.getElementById("next_testD").value = "24";
            document.getElementById("next_testD").setAttribute("disabled","disabled");
            document.getElementById("sample_qualityD").setAttribute("disabled","disabled");
            document.getElementById("rejectD").setAttribute("required","required");
            document.getElementById("test_reasonD").removeAttribute("required","required");
            document.getElementById("next_testD").removeAttribute("required","required");
            document.getElementById("sample_qualityD").removeAttribute("required","required");
          }else{
            $(".other").addClass("hide");
            $("#next_testD option[value='24']").remove();
            document.getElementById("rejectionD").removeAttribute("required","required");
            $("#test_reasonD").removeAttr("disabled");
            $("#sample_qualityD").removeAttr("disabled");
            $("#next_testD").removeAttr("disabled");
            $(".other").addClass("hide");
            //$("#otherD").removeClass("hide");
            $("#rejectD").addClass("hide");
          }
        });
        $(document).on("change", "#rejectionD" , function() {
          var _sample = $(this).val();
          console.log(_sample);
          if(_sample=='Other reason'){
            $("#otherD").removeClass("hide");
            document.getElementById("otherRD").setAttribute("required","required");
          }else{
            $("#otherD").addClass("hide");
            document.getElementById("otherRD").removeAttribute("required","required");
          }
        });



        $(document).on("change", "#sample_typeB" , function() {
          var _sample = $(this).val();
          if(_sample=='Other'){
            $("#other_sample_typeB").removeClass("hide");
            document.getElementById("other_sample_typeB").setAttribute("required","required");
          }else{
            $("#other_sample_typeB").addClass("hide");
            document.getElementById("other_sample_typeB").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#sample_typeC" , function() {
          var _sample = $(this).val();
          if(_sample=='Other'){
            $("#other_sample_typeC").removeClass("hide");
            document.getElementById("other_sample_typeC").setAttribute("required","required");
          }else{
            $("#other_sample_typeC").addClass("hide");
            document.getElementById("other_sample_typeC").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#sample_typeD" , function() {
          var _sample = $(this).val();
          if(_sample=='Other'){
            $("#other_sample_typeD").removeClass("hide");
            document.getElementById("other_sample_typeD").setAttribute("required","required");
          }else{
            $("#other_sample_typeD").addClass("hide");
            document.getElementById("other_sample_typeD").removeAttribute("required","required");
          }
        });

        $(document).on("change", "#test_reasonB" , function() {
          var _sample = $(this).val();
          if(_sample=='FU'){
            $("#fu_monthB").removeClass("hide");
            document.getElementById("fu_month_valueB").setAttribute("required","required");
          }else{
            $("#fu_monthB").addClass("hide");
            document.getElementById("fu_month_valueB").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#test_reasonC" , function() {
          var _sample = $(this).val();
          if(_sample=='FU'){
            $("#fu_monthC").removeClass("hide");
            document.getElementById("fu_monthC").setAttribute("required","required");
          }else{
            $("#fu_monthC").addClass("hide");
              document.getElementById("fu_monthC").removeAttribute("required","required");
          }
        });
        $(document).on("change", "#test_reasonD" , function() {
          var _sample = $(this).val();
          if(_sample=='FU'){
            $("#fu_monthD").removeClass("hide");
            document.getElementById("fu_monthD").setAttribute("required","required");
          }else{
            $("#fu_monthD").addClass("hide");
              document.getElementById("fu_monthD").removeAttribute("required","required");
          }
        });
        $("#datep").datepicker({
            dateFormat: "dd/mm/yyyy"
        }).datepicker("setDate", "0");

        $("#submit_sample").on('click',function(){
          document.getElementById("test_reasonA").removeAttribute("disabled","disabled");
          document.getElementById("next_testA").removeAttribute("disabled","disabled");
          document.getElementById("sample_qualityA").removeAttribute("disabled","disabled");
          document.getElementById("test_reasonB").removeAttribute("disabled","disabled");
          document.getElementById("next_testB").removeAttribute("disabled","disabled");
          document.getElementById("sample_qualityB").removeAttribute("disabled","disabled");
          document.getElementById("test_reasonC").removeAttribute("disabled","disabled");
          document.getElementById("next_testC").removeAttribute("disabled","disabled");
          document.getElementById("sample_qualityC").removeAttribute("disabled","disabled");

        });

    });

    function nospaces(t){
    if(t.value.match(/\s/g)){
    alert('Sorry, you are not allowed to enter any spaces');
    t.value=t.value.replace(/\s/g,'');
    }
    }
</script>
@endsection
