@extends('admin.layout.app')
@section('content')
<!-- <link href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ url('https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css') }}" rel="stylesheet">
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js') }}"></script> -->
        <div class="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                              <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                <div class="card" >
                                    <div class="card-block">
                                      <form method="post" action="{{ url('/report/quality_indicator') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                          <div class="col-sm-1">
                                            From:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="from_date" id="from_date" value="{{$data['from_date']}}" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-1">
                                            To:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="to_date" id="to_date" value="{{$data['to_date']}}" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-12">
                                            <button type="submit" style="padding: 5px 15px; border-radius: 4px; background: #009efb; color: #ffffff; margin-right: 3px;">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h6>Quality Indicator test wise  </h6>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  .card-block {
                                                        width: inherit;
                                                        overflow-x: auto;
                                                    }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="lc_culture">
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b></b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="3"><b>LC Culture</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b></b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                          <tr>
                                                            <td></td>
                                                            <td>Positive</td>
                                                            <td>Negative</td>
                                                            <td>Contaminated</td>
                                                            <td>Total</td>

                                                          </tr>
                                                          <tr>
                                                           <td>Microscopy Smear +ve</td>
                                                           <td>{{$data['lc_m_pos']->fmf_pos?$data['lc_m_pos']->fmf_pos:0}}</td>
                                                           <td>{{$data['lc_m_pos']->fmf_neg?$data['lc_m_pos']->fmf_neg:0}}</td>
                                                           <td>{{$data['lc_m_pos']->fmf_con?$data['lc_m_pos']->fmf_con:0}}</td>
                                                           <td>{{$data['lc_m_pos_total']?$data['lc_m_pos_total']:0}}</td>

                                                         </tr>
                                                         <tr>
                                                          <td>Microscopy Smear -ve</td>
                                                          <td>{{$data['lc_m_neg']->fmf_pos?$data['lc_m_neg']->fmf_pos:0}}</td>
                                                          <td>{{$data['lc_m_neg']->fmf_neg?$data['lc_m_neg']->fmf_neg:0}}</td>
                                                          <td>{{$data['lc_m_neg']->fmf_con?$data['lc_m_neg']->fmf_con:0}}</td>
                                                          <td>{{$data['lc_m_neg_total']}}</td>

                                                        </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="solid_culture">
                                                    <thead>
                                                      <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                        <th style="text-align: center!important; font-weight: 600;"><b></b></th>
                                                        <th style="text-align: center!important; font-weight: 600;" colspan="3"><b>Solid Culture</b></th>
                                                        <th style="text-align: center!important; font-weight: 600;"><b></b></th>

                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td></td>
                                                        <td>Positive</td>
                                                        <td>Negative</td>
                                                        <td>Contaminated</td>
                                                        <td>Total</td>

                                                      </tr>
                                                      <tr>
                                                       <td>Microscopy Smear +ve</td>
                                                       <td>{{$data['lj_m_pos']->lj_pos?$data['lj_m_pos']->lj_pos:0}}</td>
                                                       <td>{{$data['lj_m_pos']->lj_neg?$data['lj_m_pos']->lj_neg:0}}</td>
                                                       <td>{{$data['lj_m_pos']->lj_con?$data['lj_m_pos']->lj_con:0}}</td>
                                                       <td>{{$data['lj_m_pos_total']}}</td>

                                                     </tr>
                                                     <tr>
                                                      <td>Microscopy Smear -ve</td>
                                                      <td>{{$data['lj_m_neg']->lj_pos?$data['lj_m_neg']->lj_pos:0}}</td>
                                                      <td>{{$data['lj_m_neg']->lj_neg?$data['lj_m_neg']->lj_neg:0}}</td>
                                                      <td>{{$data['lj_m_neg']->lj_con?$data['lj_m_neg']->lj_con:0}}</td>
                                                      <td>{{$data['lj_m_neg_total']}}</td>

                                                    </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LPA FLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="lpa_fld" >
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total H+R Sens</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total H+R Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total H only Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total R only Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Denominator</b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                          <tr>
                                                            <td>{{$data['lpa_fld']->h_r_sens?$data['lpa_fld']->h_r_sens:0}}</td>
                                                            <td>{{$data['lpa_fld']->h_r_res?$data['lpa_fld']->h_r_res:0}}</td>
                                                            <td>{{$data['lpa_fld']->h_res?$data['lpa_fld']->h_res:0}}</td>
                                                            <td>{{$data['lpa_fld']->r_res?$data['lpa_fld']->r_res:0}}</td>
                                                            <td></td>

                                                          </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LPA SLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="lpa_sld">
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total FLQ+SLID+Low KM Sen </b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>FLQ+SLID Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total FLQ only Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total SLID only Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Low KM+SLID sen</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Low KM+SLID Res</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Total Only Low KM Res </b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Denominator </b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                          <tr>
                                                            <td>{{$data['lpa_sld']->q_s_lkm_sens?$data['lpa_sld']->q_s_lkm_sens:0}}</td>
                                                            <td>{{$data['lpa_sld']->q_s_res?$data['lpa_sld']->q_s_res:0}}</td>
                                                            <td>{{$data['lpa_sld']->q_res?$data['lpa_sld']->q_res:0}}</td>
                                                            <td>{{$data['lpa_sld']->s_res?$data['lpa_sld']->s_res:0}}</td>
                                                            <td>{{$data['lpa_sld']->s_lkm_sens?$data['lpa_sld']->s_lkm_sens:0}}</td>
                                                            <td>{{$data['lpa_sld']->s_lkm_res?$data['lpa_sld']->s_lkm_res:0}}</td>
                                                            <td>{{$data['lpa_sld']->lkm_res?$data['lpa_sld']->lkm_res:0}}</td>
                                                            <td></td>

                                                          </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LC DST FLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table  style="width:100%; text-align: center!important;" id="lcdstfld">
                                                  <thead>
                                                    <tr  style="text-align: center!important; font-weight: 600; color: #6495ed;">
                                                      <th style="text-align: center!important; font-weight: 600;">Total H+R Sens</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total H+R Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total H only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total R only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">All Drugs Sen</th>
                                                      <th style="text-align: center!important; font-weight: 600;">All Drugs Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">PZA (Z)Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Denominator</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>{{$data['r_h_s']}}</td>
                                                      <td>{{$data['r_h_r']}}</td>
                                                      <td>{{$data['h_r']}}</td>
                                                      <td>{{$data['lc_dst_fld']->r_res?$data['lc_dst_fld']->r_res:0}}</td>
                                                      <td>{{$data['lc_dst_fld']->all_sens?$data['lc_dst_fld']->all_sens:0}}</td>
                                                      <td>{{$data['lc_dst_fld']->all_res?$data['lc_dst_fld']->all_res:0}}</td>
                                                      <td>{{$data['lc_dst_fld']->z_res?$data['lc_dst_fld']->z_res:0}}</td>
                                                      <td></td>
                                                    </tr>
                                                  </tbody>
                                                  </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LJ DST FLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table  style="width:100%; text-align: center!important;" id="lj_dst_fld">
                                                  <thead>
                                                    <tr  style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <th style="text-align: center!important; font-weight: 600;">Total H+R Sens</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total H+R Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total H only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total R only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">All Drugs Sen</th>
                                                      <th style="text-align: center!important; font-weight: 600;">All Drugs Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">PZA (Z)Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Denominator</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>{{$data['lj_h_r_sens']}}</td>
                                                      <td>{{$data['lj_h_r_res']}}</td>
                                                      <td>{{$data['lj_h_res']}}</td>
                                                      <td>{{$data['lj_r_res']}}</td>
                                                      <td>0</td>
                                                      <td>0</td>
                                                      <td>{{$data['lj_z_res']}}</td>
                                                      <td></td>
                                                    </tr>
                                                  </tbody>
                                                  </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LC DST SLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table  style="width:100%; text-align: center!important;" id="lc_dst_sld">
                                                    <thead>
                                                    <tr  style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <th style="text-align: center!important; font-weight: 600;">Total MX + KM + CAP + AM Sens </th>
                                                      <th style="text-align: center!important; font-weight: 600;">MX + KM + CAP + AM Res </th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total FLQ (MX (0.5) + MX(2) OR LFX Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total SLID only Res (any)KM/CAP/AM OR ALL</th>
                                                      <th style="text-align: center!important; font-weight: 600;">PAS only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Lzd Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Cfz Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Eto Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Cla Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Azi Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Others Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Denominator</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>{{$data['mx_am_sens']}}</td>
                                                      <td>{{$data['mx_am_res']}}</td>
                                                      <td>{{$data['flq_flx_res']}}</td>
                                                      <td>{{$data['lc_dst_sld_all']}}</td>
                                                      <td>{{$data['lc_dst_sld']->pas_res?$data['lc_dst_sld']->pas_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->lzd_res?$data['lc_dst_sld']->lzd_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->cfz_res?$data['lc_dst_sld']->cfz_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->eto_res?$data['lc_dst_sld']->eto_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->cla_res?$data['lc_dst_sld']->cla_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->azi_res?$data['lc_dst_sld']->azi_res:0}}</td>
                                                      <td>{{$data['lc_dst_sld']->others_res?$data['lc_dst_sld']->others_res:0}}</td>
                                                      <td></td>
                                                    </tr>
                                                  </tbody>
                                                  </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>LJ DST SLD </h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table  style="width:100%; text-align: center!important;" id="lj_dst_sld">
                                                    <thead>
                                                    <tr  style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <th style="text-align: center!important; font-weight: 600;">Total MX + KM + CAP + AM Sens </th>
                                                      <th style="text-align: center!important; font-weight: 600;">MX + KM + CAP + AM Res </th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total FLQ (MX (0.5) + MX(2) OR LFX Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Total SLID only Res (any)KM/CAP/AM OR ALL</th>
                                                      <th style="text-align: center!important; font-weight: 600;">PAS only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Lzd Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Cfz Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Eto Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Cla Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Azi Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Others Only Res</th>
                                                      <th style="text-align: center!important; font-weight: 600;">Denominator</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>{{$data['lj_sld_1']}}</td>
                                                      <td>{{$data['lj_sld_2']}}</td>
                                                      <td>{{$data['lj_sld_3']}}</td>
                                                      <td>0</td>
                                                      <td>{{$data['lj_pas_res']}}</td>
                                                      <td>{{$data['lj_lzd_res']}}</td>
                                                      <td>{{$data['lj_cfz_res']}}</td>
                                                      <td>{{$data['lj_eto_res']}}</td>
                                                      <td>{{$data['lj_cla_res']}}</td>
                                                      <td>{{$data['lj_azi_res']}}</td>
                                                      <td>{{$data['lj_others_res']}}</td>
                                                      <td></td>
                                                    </tr>
                                                  </tbody>
                                                  </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h4>CBNAAT Indicator</h4>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="cbnaat_indicator">
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b>Sample type </b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="2"><b>Mtb detected, Rif detected</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="2"><b>Mtb detected ,Rif Not detected</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="2"><b>Mtb Not detected</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="2"><b>Invalid</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;" colspan="2"><b>Indeterminate</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Denominator</b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td></td>
                                                        <td>No</td>
                                                        <td>%</td>
                                                        <td>No</td>
                                                        <td>%</td>
                                                        <td>No</td>
                                                        <td>%</td>
                                                        <td>No</td>
                                                        <td>%</td>
                                                        <td>No</td>
                                                        <td>%</td>
                                                        <td>No of each <br />sample type <br />received </td>

                                                      </tr>
                                                          <tr>
                                                            <td>Sputum</td>
                                                            <td>{{$data['cbn_sputum']->mtb_ref_det?$data['cbn_sputum']->mtb_ref_det:0}}</td>
                                                            <td>{{$data['cbn_sputum']->total?number_format($data['cbn_sputum']->mtb_ref_det/$data['cbn_sputum']->total*100, 2, '.', ','):0}}</td>
                                                            <td>{{$data['cbn_sputum']->mtb_det_ref_ndet?$data['cbn_sputum']->mtb_det_ref_ndet:0}}</td>
                                                            <td>{{$data['cbn_sputum']->total?number_format($data['cbn_sputum']->mtb_det_ref_ndet/$data['cbn_sputum']->total*100, 2, '.', ','):0}}</td>
                                                            <td>{{$data['cbn_sputum']->mtb_ndet?$data['cbn_sputum']->mtb_ndet:0}}</td>
                                                            <td>{{$data['cbn_sputum']->total?number_format($data['cbn_sputum']->mtb_ndet/$data['cbn_sputum']->total*100, 2, '.', ','):0}}</td>
                                                            <td>{{$data['cbn_sputum']->mtb_invalid?$data['cbn_sputum']->mtb_invalid:0}}</td>
                                                            <td>{{$data['cbn_sputum']->total?number_format($data['cbn_sputum']->mtb_invalid/$data['cbn_sputum']->total*100, 2, '.', ','):0}}</td>
                                                            <td>{{$data['cbn_sputum']->rif_indeterminate?$data['cbn_sputum']->rif_indeterminate:0}}</td>
                                                            <td>{{$data['cbn_sputum']->total?number_format($data['cbn_sputum']->rif_indeterminate/$data['cbn_sputum']->total*100, 2, '.', ','):0}}</td>
                                                            <td>No of each <br />sample type <br />received </td>

                                                          </tr>
                                                          <tr>
                                                           <td>CSF</td>
                                                           <td>{{$data['cbn_csf']->mtb_ref_det?$data['cbn_csf']->mtb_ref_det:0}}</td>
                                                           <td>{{$data['cbn_csf']->total?number_format($data['cbn_csf']->mtb_ref_det/$data['cbn_csf']->total*100, 2, '.', ','):0}}</td>
                                                           <td>{{$data['cbn_csf']->mtb_det_ref_ndet?$data['cbn_csf']->mtb_det_ref_ndet:0.00}}</td>
                                                           <td>{{$data['cbn_csf']->total?number_format($data['cbn_csf']->mtb_det_ref_ndet/$data['cbn_csf']->total*100, 2, '.', ','):0.00}}</td>
                                                           <td>{{$data['cbn_csf']->mtb_ndet?$data['cbn_csf']->mtb_ndet:0}}</td>
                                                           <td>{{$data['cbn_csf']->total?number_format($data['cbn_csf']->mtb_ndet/$data['cbn_csf']->total*100, 2, '.', ','):0.00}}</td>
                                                           <td>{{$data['cbn_csf']->mtb_invalid?$data['cbn_csf']->mtb_invalid:0}}</td>
                                                           <td>{{$data['cbn_csf']->total?number_format($data['cbn_csf']->mtb_invalid/$data['cbn_csf']->total*100, 2, '.', ','):0.00}}</td>
                                                           <td>{{$data['cbn_csf']->rif_indeterminate?$data['cbn_csf']->rif_indeterminate:0}}</td>
                                                           <td>{{$data['cbn_csf']->total?number_format($data['cbn_csf']->rif_indeterminate/$data['cbn_csf']->total*100, 2, '.', ','):0.00}}</td>
                                                           <td>No of each <br />sample type <br />received </td>

                                                         </tr>
                                                         <tr>
                                                          <td>GA</td>
                                                          <td>{{$data['cbn_ga']->mtb_ref_det?$data['cbn_ga']->mtb_ref_det:0}}</td>
                                                          <td>{{$data['cbn_ga']->total?number_format($data['cbn_ga']->mtb_ref_det/$data['cbn_ga']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_ga']->mtb_det_ref_ndet?$data['cbn_ga']->mtb_det_ref_ndet:0}}</td>
                                                          <td>{{$data['cbn_ga']->total?number_format($data['cbn_ga']->mtb_det_ref_ndet/$data['cbn_ga']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_ga']->mtb_ndet?$data['cbn_ga']->mtb_ndet:0}}</td>
                                                          <td>{{$data['cbn_ga']->total?number_format($data['cbn_ga']->mtb_ndet/$data['cbn_ga']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_ga']->mtb_invalid?$data['cbn_ga']->mtb_invalid:0}}</td>
                                                          <td>{{$data['cbn_ga']->total?number_format($data['cbn_ga']->mtb_invalid/$data['cbn_ga']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_ga']->rif_indeterminate?$data['cbn_ga']->rif_indeterminate:0}}</td>
                                                          <td>{{$data['cbn_ga']->total?number_format($data['cbn_ga']->rif_indeterminate/$data['cbn_ga']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>No of each <br />sample type <br />received </td>

                                                        </tr>
                                                        <tr>
                                                          <td>PUS</td>
                                                          <td>{{$data['cbn_pus']->mtb_ref_det?$data['cbn_pus']->mtb_ref_det:0}}</td>
                                                          <td>{{$data['cbn_pus']->total?number_format($data['cbn_pus']->mtb_ref_det/$data['cbn_pus']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_pus']->mtb_det_ref_ndet?$data['cbn_pus']->mtb_det_ref_ndet:0}}</td>
                                                          <td>{{$data['cbn_pus']->total?number_format($data['cbn_pus']->mtb_det_ref_ndet/$data['cbn_pus']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_pus']->mtb_ndet?$data['cbn_pus']->mtb_ndet:0}}</td>
                                                          <td>{{$data['cbn_pus']->total?number_format($data['cbn_pus']->mtb_ndet/$data['cbn_pus']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_pus']->mtb_invalid?$data['cbn_pus']->mtb_invalid:0}}</td>
                                                          <td>{{$data['cbn_pus']->total?number_format($data['cbn_pus']->mtb_invalid/$data['cbn_pus']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>{{$data['cbn_pus']->rif_indeterminate?$data['cbn_pus']->rif_indeterminate:0}}</td>
                                                          <td>{{$data['cbn_pus']->total?number_format($data['cbn_pus']->rif_indeterminate/$data['cbn_pus']->total*100, 2, '.', ','):0.00}}</td>
                                                          <td>No of each <br />sample type <br />received </td>

                                                        </tr>
                                                        <tr>
                                                         <td>BAL</td>
                                                         <td>{{$data['cbn_bal']->mtb_ref_det?$data['cbn_bal']->mtb_ref_det:0}}</td>
                                                         <td>{{$data['cbn_bal']->total?number_format($data['cbn_bal']->mtb_ref_det/$data['cbn_bal']->total*100, 2, '.', ','):0.00}}</td>
                                                         <td>{{$data['cbn_bal']->mtb_det_ref_ndet?$data['cbn_bal']->mtb_det_ref_ndet:0}}</td>
                                                         <td>{{$data['cbn_bal']->total?number_format($data['cbn_bal']->mtb_det_ref_ndet/$data['cbn_bal']->total*100, 2, '.', ','):0.00}}</td>
                                                         <td>{{$data['cbn_bal']->mtb_ndet?$data['cbn_bal']->mtb_ndet:0}}</td>
                                                         <td>{{$data['cbn_bal']->total?number_format($data['cbn_bal']->mtb_ndet/$data['cbn_bal']->total*100, 2, '.', ','):0.00}}</td>
                                                         <td>{{$data['cbn_bal']->mtb_invalid?$data['cbn_bal']->mtb_invalid:0}}</td>
                                                         <td>{{$data['cbn_bal']->total?number_format($data['cbn_bal']->mtb_invalid/$data['cbn_bal']->total*100, 2, '.', ','):0.00}}</td>
                                                         <td>{{$data['cbn_bal']->rif_indeterminate?$data['cbn_bal']->rif_indeterminate:0}}</td>
                                                         <td>{{$data['cbn_bal']->total?number_format($data['cbn_bal']->rif_indeterminate/$data['cbn_bal']->total*100, 2, '.', ','):0.00}}</td>
                                                         <td>No of each <br />sample type <br />received </td>

                                                       </tr>
                                                       <tr>
                                                        <td>PF</td>
                                                        <td>{{$data['cbn_pf']->mtb_ref_det?$data['cbn_pf']->mtb_ref_det:0}}</td>
                                                        <td>{{$data['cbn_pf']->total?number_format($data['cbn_pf']->mtb_ref_det/$data['cbn_pf']->total*100, 2, '.', ','):0.00}}</td>
                                                        <td>{{$data['cbn_pf']->mtb_det_ref_ndet?$data['cbn_pf']->mtb_det_ref_ndet:0}}</td>
                                                        <td>{{$data['cbn_pf']->total?number_format($data['cbn_pf']->mtb_det_ref_ndet/$data['cbn_pf']->total*100, 2, '.', ','):0.00}}</td>
                                                        <td>{{$data['cbn_pf']->mtb_ndet?$data['cbn_pf']->mtb_ndet:0}}</td>
                                                        <td>{{$data['cbn_pf']->total?number_format($data['cbn_pf']->mtb_ndet/$data['cbn_pf']->total*100, 2, '.', ','):0.00}}</td>
                                                        <td>{{$data['cbn_pf']->mtb_invalid?$data['cbn_pf']->mtb_invalid:0}}</td>
                                                        <td>{{$data['cbn_pf']->total?number_format($data['cbn_pf']->mtb_invalid/$data['cbn_pf']->total*100, 2, '.', ','):0.00}}</td>
                                                        <td>{{$data['cbn_pf']->rif_indeterminate?$data['cbn_pf']->rif_indeterminate:0}}</td>
                                                        <td>{{$data['cbn_pf']->total?number_format($data['cbn_pf']->rif_indeterminate/$data['cbn_pf']->total*100, 2, '.', ','):0.00}}</td>
                                                        <td>No of each <br />sample type <br />received </td>

                                                      </tr>
                                                      <tr>
                                                       <td>HIV positive (All sample type)</td>
                                                       <td>{{$data['cbn_hiv']->mtb_ref_det?$data['cbn_hiv']->mtb_ref_det:0}}</td>
                                                       <td>{{$data['cbn_hiv']->total?number_format($data['cbn_hiv']->mtb_ref_det/$data['cbn_hiv']->total*100, 2, '.', ','):0.00}}</td>
                                                       <td>{{$data['cbn_hiv']->mtb_det_ref_ndet?$data['cbn_hiv']->mtb_det_ref_ndet:0}}</td>
                                                       <td>{{$data['cbn_hiv']->total?number_format($data['cbn_hiv']->mtb_det_ref_ndet/$data['cbn_hiv']->total*100, 2, '.', ','):0.00}}</td>
                                                       <td>{{$data['cbn_hiv']->mtb_ndet?$data['cbn_hiv']->mtb_ndet:0}}</td>
                                                       <td>{{$data['cbn_hiv']->total?number_format($data['cbn_hiv']->mtb_ndet/$data['cbn_hiv']->total*100, 2, '.', ','):0.00}}</td>
                                                       <td>{{$data['cbn_hiv']->mtb_invalid?$data['cbn_hiv']->mtb_invalid:0}}</td>
                                                       <td>{{$data['cbn_hiv']->total?number_format($data['cbn_hiv']->mtb_invalid/$data['cbn_hiv']->total*100, 2, '.', ','):0.00}}</td>
                                                       <td>{{$data['cbn_hiv']->rif_indeterminate?$data['cbn_hiv']->rif_indeterminate:0}}</td>
                                                       <td>{{$data['cbn_hiv']->total?number_format($data['cbn_hiv']->rif_indeterminate/$data['cbn_hiv']->total*100, 2, '.', ','):0.00}}</td>
                                                       <td>No of each <br />sample type <br />received </td>

                                                     </tr>
                                                     <tr>
                                                      <td>Others</td>
                                                      <td>{{$data['cbn_others']->mtb_ref_det?$data['cbn_others']->mtb_ref_det:0}}</td>
                                                      <td>{{$data['cbn_others']->total?number_format($data['cbn_others']->mtb_ref_det/$data['cbn_others']->total*100, 2, '.', ','):0.00}}</td>
                                                      <td>{{$data['cbn_others']->mtb_det_ref_ndet?$data['cbn_others']->mtb_det_ref_ndet:0}}</td>
                                                      <td>{{$data['cbn_others']->total?number_format($data['cbn_others']->mtb_det_ref_ndet/$data['cbn_others']->total*100, 2, '.', ','):0.00}}</td>
                                                      <td>{{$data['cbn_others']->mtb_ndet?$data['cbn_others']->mtb_ndet:0}}</td>
                                                      <td>{{$data['cbn_others']->total?number_format($data['cbn_others']->mtb_ndet/$data['cbn_others']->total*100, 2, '.', ','):0.00}}</td>
                                                      <td>{{$data['cbn_others']->mtb_invalid?$data['cbn_others']->mtb_invalid:0}}</td>
                                                      <td>{{$data['cbn_others']->total?number_format($data['cbn_others']->mtb_invalid/$data['cbn_others']->total*100, 2, '.', ','):0.00}}</td>
                                                      <td>{{$data['cbn_others']->rif_indeterminate?$data['cbn_others']->rif_indeterminate:0}}</td>
                                                      <td>{{$data['cbn_others']->total?number_format($data['cbn_others']->rif_indeterminate/$data['cbn_others']->total*100, 2, '.', ','):0.00}}</td>
                                                      <td>All others <br />except above <br />sample types</td>

                                                    </tr>
                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>

        </div>
        <script>
        $(function() {
          $('#lc_culture').DataTable({
            searching: false,
            paging: false,
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
               {
                   extend: 'excelHtml5',
                   title: 'Quality Indicator test wise',
                   messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

               },
               {
                   extend: 'pdfHtml5',
                   title: 'Quality Indicator test wise',
                    messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

               }
          ],
          });



        } );
        </script>
        <script src="{{ url('/js/solid_culture.js') }}"></script>
        <script src="{{ url('/js/lpa_fld.js') }}"></script>
        <script src="{{ url('/js/lpa_sld.js') }}"></script>
        <script src="{{ url('/js/cbnaat_indicator.js') }}"></script>
        <script src="{{ url('/js/lj_dst_sld.js') }}"></script>
        <script src="{{ url('/js/lj_dst_fld.js') }}"></script>
        <script src="{{ url('/js/lc_dst_sld.js') }}"></script>
        <script src="{{ url('/js/lc_dst_fld.js') }}"></script>

    @endsection
