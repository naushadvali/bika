@extends('admin.layout.app')
@section('content')
<link href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ url('https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css') }}" rel="stylesheet">
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js') }}"></script>
        <div class="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                              <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                <div class="card" >
                                    <div class="card-block">
                                      <form method="post" action="{{ url('/report/performance_indicator') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                          <div class="col-sm-1">
                                            From:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="from_date"  value="{{$data['from_date']}}" id="from_date" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-1">
                                            To:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="to_date" id="to_date" value="{{ $data['to_date'] }}" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-12">
                                            <button type="submit" style="padding: 5px 15px; border-radius: 4px; background: #009efb; color: #ffffff; margin-right: 3px;">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h6>Performance Indicator   </h6>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  .card-block {
                                                        width: inherit;
                                                        overflow-x: auto;
                                                    }
                                                  </style>
                                                  <table class="tg"  style="width:100%; text-align: center!important; overflow-x:auto;" id ="example1" >
                                                    <thead>
                                                    <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <th class="tg-031e" rowspan="3">S.No</th>
                                                      <th class="tg-yw4l" rowspan="3">Name of the Culture &amp; DST Laboratory</th>
                                                      <th class="tg-yw4l" colspan="2" rowspan="2">Culture workload,(from culture register)</th>
                                                      <th class="tg-yw4l" colspan="7">DST workload and results (from DST register)</th>
                                                      <th class="tg-031e" colspan="6">Laboratory Quality Indicators</th>
                                                      <th class="tg-031e" colspan="24"></th>
                                                    </tr>
                                                    <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <td class="tg-yw4l" colspan="7">[DST results summary combined all methods]</td>
                                                      <td class="tg-yw4l" colspan="2">Specimens (all) received within 7 days of sputum collection (with CPC)</td>
                                                      <td class="tg-yw4l" colspan="2">Specimens (all) received within 72 hours of sputum collection in 4-8 C (without CPC*)</td>
                                                      <td class="tg-yw4l" colspan="2">Number of specimen rejected at the lab due to various reason(eg. Leakage, inadequate quantity, etc)</td>
                                                      <td class="tg-yw4l" colspan="2">Specimens (all) with cultures reported as Mtb. complex</td>
                                                      <td class="tg-yw4l" colspan="2">Propotion of smear positive diagnositic specimens reported as culture positive</td>
                                                      <td class="tg-yw4l" colspan="2">Proportion of all specimens with culture 'contaminated' results ( Solid)</td>
                                                      <td class="tg-yw4l" colspan="2">Specimens (all) with culture-contaminated results (by liquid,culture )</td>
                                                      <td class="tg-yw4l" colspan="2">Specimens (all) with culture results reported as NTM</td>
                                                      <td class="tg-yw4l" colspan="2">Patients (with diagnostic specimens) with DST completed within the benchmark turn-around time (by Solid Culture)</td>
                                                      <td class="tg-yw4l" colspan="2">Patients (with diagnostic specimens) with DST completed within the benchmark turn-around time (by Liquid Culture)</td>
                                                      <td class="tg-yw4l" colspan="2">Patients (with diagnostic specimens) with DST completed within the benchmark turn-around time (by LPA )</td>
                                                      <td class="tg-yw4l" colspan="2">Patients (all) with final culture results reported to providers within 1 days of declaration of result</td>
                                                      <td class="tg-yw4l" colspan="2">Patients with final DST results reported to providers within 1 days of declaration of result</td>
                                                      <td class="tg-yw4l" colspan="2">Number and Percentage of invalid LPA results</td>
                                                      <td class="tg-yw4l" colspan="2">Number of events of LPA contamination in the quarter</td>
                                                    </tr>
                                                    <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">
                                                      <td class="tg-yw4l">Diagnostic Sputum SPECIMENS inoculated</td>
                                                      <td class="tg-yw4l">Follow-Up SPECIMENS inoculated</td>
                                                      <td class="tg-yw4l">Solid DST Processed</td>
                                                      <td class="tg-yw4l">LPADST done</td>
                                                      <td class="tg-yw4l">Liquid DST Done</td>
                                                      <td class="tg-yw4l">Total H+R Sens</td>
                                                      <td class="tg-yw4l">Total H+R Res</td>
                                                      <td class="tg-031e">Total H only Res</td>
                                                      <td class="tg-031e">Total R only Res</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                      <td class="tg-031e">Number</td>
                                                      <td class="tg-031e">%</td>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td class="tg-031e">1</td>
                                                      <td class="tg-yw4l">5</td>
                                                      <td class="tg-yw4l">{{$data['c_workload']->d_s_s_inoculated?$data['c_workload']->d_s_s_inoculated:0}}</td>
                                                      <td class="tg-yw4l">{{$data['c_workload']->f_s_inoculated? $data['c_workload']->f_s_inoculated:0}}</td>
                                                      <td class="tg-yw4l">{{$data['solid_dst_processed']? $data['solid_dst_processed']:0}}</td>
                                                      <td class="tg-yw4l">{{$data['lpa_done']?$data['lpa_done']:0}}</td>
                                                      <td class="tg-yw4l">{{$data['lc_dst_done']?$data['lc_dst_done']:0}}</td>
                                                      <td class="tg-yw4l">{{$data['lpa_fld']->h_r_sens?$data['lpa_fld']->h_r_sens:0}}</td>
                                                      <td class="tg-yw4l">{{$data['lpa_fld']->h_r_res?$data['lpa_fld']->h_r_res:0}}</td>
                                                      <td class="tg-031e">{{$data['lpa_fld']->h_res?$data['lpa_fld']->h_res:0}}</td>
                                                      <td class="tg-031e">{{$data['lpa_fld']->r_res?$data['lpa_fld']->r_res:0}}</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">{{$data['lpa1_in_tat']?$data['lpa1_in_tat']:0}}</td>
                                                      <td class="tg-031e">{{number_format($data['t_total']?$data['lpa1_in_tat']/$data['t_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['sample_rejected']->rej?$data['sample_rejected']->rej:0}}</td>
                                                      <td class="tg-031e">{{number_format($data['sample_rejected']->alle?$data['sample_rejected']->rej/$data['sample_rejected']->alle*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">{{$data['culture_smear_positive']}}</td>
                                                      <td class="tg-031e">{{number_format($data['culture_positive_total']?$data['culture_smear_positive']/$data['culture_positive_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['culture_lj_contaminated']}}</td>
                                                      <td class="tg-031e">{{number_format($data['culture_lj_total']?$data['culture_lj_contaminated']/$data['culture_lj_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['culture_lc_contaminated']}}</td>
                                                      <td class="tg-031e">{{number_format($data['culture_lc_total']?$data['culture_lc_contaminated']/$data['culture_lc_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['culture_all_ntm']}}</td>
                                                      <td class="tg-031e">{{number_format($data['culture_lj_total']||$data['culture_lc_total']?$data['culture_all_ntm']/($data['culture_lj_total']+$data['culture_lc_total'])*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['lj1_in_tat']+$data['lj2_in_tat']}}</td>
                                                      <td class="tg-031e">{{number_format($data['lj_total']?($data['lj1_in_tat']+$data['lj2_in_tat'])/$data['lj_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">{{$data['lpa2_in_tat']}}</td>
                                                      <td class="tg-031e">{{number_format($data['lpa2_total']?$data['lpa2_in_tat']/$data['lpa2_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['p_culture_in_tat']}}</td>
                                                      <td class="tg-031e">{{number_format($data['p_culture_total']?$data['p_culture_in_tat']/$data['p_culture_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['p_lj_in_tat']}}</td>
                                                      <td class="tg-031e">{{number_format($data['p_lj_total']?$data['p_lj_in_tat']/$data['p_lj_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">{{$data['lpa_invalid']}}</td>
                                                      <td class="tg-031e">{{number_format($data['lpa_total']?$data['lpa_invalid']/$data['lpa_total']*100:0, 2, '.', '')}}</td>
                                                      <td class="tg-031e">--</td>
                                                      <td class="tg-031e">--</td>
                                                    </tr>
                                                  </tbody>
                                                  </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>

        </div>
        <script>
        $(document).ready(function() {
            $('#example1').DataTable({
              searching: false,
              paging: false,
              ordering: false,
              dom: 'Bfrtip',
              buttons: [
                 {
                     extend: 'excelHtml5',
                     title: 'Performance Indicator',
                     messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 },
                 {
                     extend: 'pdfHtml5',
                     title: 'Performance Indicator',
                      messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 }
            ],
            });
        } );
        </script>
    @endsection
