@extends('admin.layout.app')
@section('content')
<link href="{{ url('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ url('https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css') }}" rel="stylesheet">
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
<script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js') }}"></script>
        <div class="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                              <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                <div class="card" >
                                    <div class="card-block">
                                      <form method="post" action="{{ url('/report/lqcIndicator_tat') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                          <div class="col-sm-1">
                                            From:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="from_date" value="{{$data['from_date']}}" id="from_date" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-1">
                                            To:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="to_date" id="to_date" value="{{ $data['to_date'] }}" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-12">
                                            <button type="submit" style="padding: 5px 15px; border-radius: 4px; background: #009efb; color: #ffffff; margin-right: 3px;">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h6>Lab QC indicator Turn Around Time    </h6>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >
                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  .card-block {
                                                        width: inherit;
                                                        overflow-x: auto;
                                                    }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="example1">
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b></b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>TAT in guidelines `(In days)</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>With in TAT(%)</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Out of TAT(%)</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Denominator</b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                          <td>Microscopy</td>
                                                          <td ></td>
                                                          <td ></td>
                                                          <td ></td>
                                                          <td>NR</td>

                                                        </tr>
                                                         <tr>
                                                           <td>Culture</td>
                                                           <td ></td>
                                                           <td ></td>
                                                           <td ></td>
                                                           <td>NR</td>
                                                          </tr>
                                                          <tr>
                                                            <td>DST First Line(LPA)</td>
                                                            <td>3</td>
                                                            <td>{{$data['lpa1_total']?$data['lpa1_in_tat']/$data['lpa1_total']*100:0}}%</td>
                                                            <td>{{$data['lpa1_total']?$data['lpa1_out_tat']/$data['lpa1_total']*100:0}}%</td>
                                                            <td>From the date of receiption</td>

                                                          </tr>
                                                          <tr>
                                                           <td>DST Second Line(LPA)</td>
                                                           <td>3</td>
                                                           <td>{{$data['lpa2_total']?$data['lpa2_in_tat']/$data['lpa2_total']*100:0}}%</td>
                                                           <td>{{$data['lpa2_total']?$data['lpa2_out_tat']/$data['lpa2_total']*100:0}}%</td>
                                                           <td>From the date of receiption</td>

                                                         </tr>
                                                         <tr>
                                                          <td>DST First Line(LC)</td>
                                                          <td>28</td>
                                                          <td>{{$data['lc_dst_fld_total']?$data['lc_dst_fld_in_tat']/$data['lc_dst_fld_total']*100:0}}%</td>
                                                          <td>{{$data['lc_dst_fld_total']?$data['lc_dst_fld_out_tat']/$data['lc_dst_fld_total']*100:0}}%</td>
                                                          <td>From the date of receiption</td>

                                                        </tr>
                                                        <tr>
                                                         <td>DST Second Line(LC)</td>
                                                         <td>28</td>
                                                         <td>{{$data['lc_dst_sld_total']?$data['lc_dst_sld_in_tat']/$data['lc_dst_sld_total']*100:0}}%</td>
                                                         <td>{{$data['lc_dst_sld_total']?$data['lc_dst_sld_out_tat']/$data['lc_dst_sld_total']*100:0}}%</td>
                                                         <td>From the date of receiption</td>

                                                         </tr>
                                                         <tr>
                                                          <td>DST First Line(Solid)</td>
                                                          <td>84</td>
                                                          <td>{{$data['lj1_total']?$data['lj1_in_tat']/$data['lj1_total']*100:0}}%</td>
                                                          <td>{{$data['lj1_total']?$data['lj1_out_tat']/$data['lj1_total']*100:0}}%</td>
                                                          <td>From the date of receiption</td>
                                                        </tr>
                                                        <tr>
                                                         <td>DST Second Line(Solid)</td>
                                                         <td>84</td>
                                                         <td>{{$data['lj2_total']?$data['lj2_in_tat']/$data['lj2_total']*100:0}}%</td>
                                                         <td>{{$data['lj2_total']?$data['lj2_out_tat']/$data['lj2_total']*100:0}}%</td>
                                                         <td>From the date of receiption</td>
                                                       </tr>
                                                       <tr>
                                                        <td>CBNAAT</td>
                                                        <td>1</td>
                                                        <td>{{$data['cbnaat_total']?$data['cbnaat_in_tat']/$data['cbnaat_total']*100:0}}%</td>
                                                        <td>{{$data['cbnaat_total']?$data['cbnaat_out_tat']/$data['cbnaat_total']*100:0}}%</td>
                                                        <td>From the date of receiption</td>
                                                      </tr>
                                                      <tr>
                                                       <td>Patients (all) with Final <br />Culture results reported <br />to providers with 1 <br />days of declaration of result</td>
                                                       <td>1</td>
                                                       <td>{{$data['p_culture_total']?$data['p_culture_in_tat']/$data['p_culture_total']*100:0}}%</td>
                                                       <td>{{$data['p_culture_total']?$data['p_culture_out_tat']/$data['p_culture_total']*100:0}}%</td>
                                                       <td>From the date of culture result</td>
                                                     </tr>
                                                     <tr>
                                                      <td>Patients (all) with Final <br />DST results reported <br />to providers with 1 <br />days of declaration of result</td>
                                                      <td>1</td>
                                                      <td>{{$data['p_lj_total']?$data['p_lj_in_tat']/$data['p_lj_total']*100:0}}%</td>
                                                      <td>{{$data['p_lj_total']?$data['p_lj_out_tat']/$data['p_lj_total']*100:0}}%</td>
                                                      <td>From the date of DST result</td>
                                                    </tr>
                                                    <tr>
                                                     <td>Patients (all) with Final <br />CBNAAT results reported <br />to providers with 1 <br />days of declaration of result</td>
                                                     <td>1</td>
                                                     <td></td>
                                                     <td></td>
                                                     <td>From the date of CBNAAT result</td>
                                                   </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>

        </div>
        <script>
        $(document).ready(function() {
            $('#example1').DataTable({
              searching: false,
              paging: false,
              ordering: false,
              dom: 'Bfrtip',
              buttons: [
                 {
                     extend: 'excelHtml5',
                     title: 'Lab QC indicator Turn Around Time',
                     messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 },
                 {
                     extend: 'pdfHtml5',
                     title: 'Lab QC indicator Turn Around Time',
                      messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 }
            ],
            });
        } );
        </script>
    @endsection
