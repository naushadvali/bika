@extends('admin.layout.app')
@section('content')
        <div class="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" style="margin-top: 16px;">
                        <div class="card" style="border: none;">
                            <div class="card-block">
                              <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                <div class="card" >
                                    <div class="card-block">
                                      <form method="post" action="{{ url('/report/lqc_indicator') }}" >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                          <div class="col-sm-1">
                                            From:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="from_date"  value="{{$data['from_date']}}" id="from_date" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-1">
                                            To:
                                          </div>
                                          <div class="col-sm-11">
                                            <input type="text" name="to_date" id="to_date" value="{{ $data['to_date'] }}" class="datepicker" max="<?php echo date("Y-m-d");?>" required>
                                          </div>
                                          <div class="col-sm-12">
                                            <button type="submit" style="padding: 5px 15px; border-radius: 4px; background: #009efb; color: #ffffff; margin-right: 3px;">Submit</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12" >
                                    <div class="card" >
                                        <div class="card-block">
                                            <h6>LAB QC indicator  </h6>
                                            <div class="col-lg-12 col-xlg-12 col-md-12 col-sm-12 col-sm-12" >


                                                  <style>
                                                  table, th, td {
                                                      border: 1px solid black;
                                                  }
                                                  .card-block {
                                                        width: inherit;
                                                        overflow-x: auto;
                                                    }
                                                  </style>
                                                  <table style="width:100%; text-align: center!important;" id="example1">
                                                    <!-- <tr>
                                                      <td>fgdgd</td>
                                                      <td></td>
                                                      <td>fgdgd</td>
                                                      <td></td>
                                                      <td>fgdgd</td>
                                                      <td></td>
                                                    </tr> -->
                                                    <thead>
                                                        <tr style="text-align: center!important; font-weight: 600;     color: #6495ed;">

                                                          <th style="text-align: center!important; font-weight: 600;"><b>Indicator</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Diagnosis</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>%</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Followup</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>%</b></th>
                                                          <th style="text-align: center!important; font-weight: 600;"><b>Denominator</b></th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>

                                                        <td>Microscopy Positive</td>
                                                        <td>{{$data['m_positive_dg']}}</td>
                                                        <td>{{number_format((float)$data['m_positive_dg_per'], 2, '.', '')}}%</td>
                                                        <td>{{$data['m_positive_fu']}}</td>
                                                        <td>{{number_format((float)$data['m_positive_fu_per'], 2, '.', '')}}%</td>
                                                        <td>Total no. of Sample received for perticular<br /> period for Diagnosis and Followup</td>

                                                       </tr>
                                                       <tr>
                                                         <td>Microscopy Negative</td>
                                                         <td>{{$data['m_negative_dg']}}</td>
                                                         <td>{{number_format((float)$data['m_negative_dg_per'], 2, '.', '')}}%</td>
                                                         <td>{{$data['m_negative_fu']}}</td>
                                                         <td>{{number_format((float)$data['m_negative_fu_per'], 2, '.', '')}}%</td>
                                                         <td>Total no. of Sample received for perticular<br /> period for Diagnosis and Followup</td>

                                                        </tr>
                                                        <tr>
                                                          <td>LPA First Line Valid</td>
                                                          <td>{{$data['lpa1_diagnosis']?$data['lpa1_diagnosis']:0}}</td>
                                                          <td>{{$data['lpa1_total']?number_format($data['lpa1_diagnosis']/$data['lpa1_total']*100, 2, '.', ','):0.00}}%</td>
                                                          <td>{{$data['lpa1_fu']?$data['lpa1_fu']:0}}</td>
                                                          <td>{{$data['lpa1_total']?number_format($data['lpa1_fu']/$data['lpa1_total']*100, 2, '.', ','):0.00}}%</td>
                                                          <td>Total no. of Sample received <br />for perticular period</td>

                                                        </tr>
                                                         <tr>
                                                           <td>LPA First Line Invalid/Indeterminate</td>
                                                           <td>{{$data['lpa1_d_invalid']?$data['lpa1_d_invalid']:0}}</td>
                                                           <td>{{$data['lpa1_d_invalid_total']?number_format($data['lpa1_d_invalid']/$data['lpa1_d_invalid_total']*100, 2, '.', ','):0.00}}%</td>
                                                           <td>{{$data['lpa1_fu_invalid']?$data['lpa1_fu_invalid']:0}}</td>
                                                           <td>{{$data['lpa1_d_invalid_total']?number_format($data['lpa1_fu_invalid']/$data['lpa1_d_invalid_total']*100, 2, '.', ','):0.00}}%</td>
                                                           <td>Total no. of Sample received <br />for perticular period</td>
                                                          </tr>
                                                          <tr>
                                                            <td>LPA FLD Retest</td>
                                                            <td>{{$data['lpa_fld_retest']->dx?$data['lpa_fld_retest']->dx:0}}</td>
                                                            <td>{{$data['lpa_total']->dx?number_format($data['lpa_fld_retest']->dx/$data['lpa_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                            <td>{{$data['lpa_fld_retest']->fu?$data['lpa_fld_retest']->fu:0}}</td>
                                                            <td>{{$data['lpa_total']->fu?number_format($data['lpa_fld_retest']->fu/$data['lpa_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                            <td>Total no. of Sample received <br /> Invalid/Indeterminate</td>

                                                          </tr>
                                                          <tr>
                                                           <td>LPA Second Line Valid</td>
                                                           <td>{{$data['lpa2_diagnosis']?$data['lpa2_diagnosis']:0}}</td>
                                                           <td>{{$data['lpa2_total']?number_format($data['lpa2_diagnosis']/$data['lpa2_total']*100, 2, '.', ','):0.00}}%</td>
                                                           <td>{{$data['lpa2_fu']?$data['lpa2_fu']:0}}</td>
                                                           <td>{{$data['lpa2_total']?number_format($data['lpa2_fu']/$data['lpa2_total']*100, 2, '.', ','):0.00}}%</td>
                                                           <td>Total no. of Sample received <br />for perticular period</td>

                                                         </tr>
                                                         <tr>
                                                          <td>LPA Second Line Invalid/Indeterminate</td>
                                                          <td>{{$data['lpa2_d_invalid']?$data['lpa2_d_invalid']:0}}</td>
                                                          <td>{{$data['lpa2_d_invalid_total']?number_format($data['lpa2_d_invalid']/$data['lpa2_d_invalid_total']*100, 2, '.', ','):0.00}}%</td>
                                                          <td>{{$data['lpa2_fu_invalid']?$data['lpa2_fu_invalid']:0}}</td>
                                                          <td>{{$data['lpa2_d_invalid_total']?number_format($data['lpa2_fu_invalid']/$data['lpa2_d_invalid_total']*100, 2, '.', ','):0.00}}%</td>
                                                          <td>Total no. of Sample received <br />for perticular period</td>

                                                        </tr>
                                                        <tr>
                                                         <td>LPA SLD Retest</td>
                                                         <td>{{$data['lpa_sld_retest']->dx?$data['lpa_sld_retest']->dx:0}}</td>
                                                         <td>{{$data['lpa_sld_total']->dx?number_format($data['lpa_sld_retest']->dx/$data['lpa_sld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                         <td>{{$data['lpa_sld_retest']->fu?$data['lpa_sld_retest']->fu:0}}</td>
                                                         <td>{{$data['lpa_sld_total']->fu?number_format($data['lpa_sld_retest']->fu/$data['lpa_sld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                         <td>Total no. of Sample received <br /> Invalid/Indeterminate</td>

                                                         </tr>
                                                         <tr>
                                                          <td>LC DST FLD Valid</td>
                                                          <td>{{$data['lc_dst_fld_d']->valid?$data['lc_dst_fld_d']->valid:0}}</td>
                                                          <td>{{$data['lc_dst_fld_total']->dx?number_format($data['lc_dst_fld_d']->valid/$data['lc_dst_fld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                          <td>{{$data['lc_dst_fld_fu']->valid?$data['lc_dst_fld_fu']->valid:0}}</td>
                                                          <td>{{$data['lc_dst_fld_total']->fu?number_format($data['lc_dst_fld_fu']->valid/$data['lc_dst_fld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                          <td>Total no. of Sample received <br />for perticular period</td>
                                                        </tr>
                                                        <tr>
                                                         <td>LC DST FLD Invalid</td>
                                                         <td>{{$data['lc_dst_fld_d']->invalid?$data['lc_dst_fld_d']->invalid:0}}</td>
                                                         <td>{{$data['lc_dst_fld_total']->dx?number_format($data['lc_dst_fld_d']->invalid/$data['lc_dst_fld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                         <td>{{$data['lc_dst_fld_fu']->invalid?$data['lc_dst_fld_fu']->invalid:0}}</td>
                                                         <td>{{$data['lc_dst_fld_total']->fu?number_format($data['lc_dst_fld_fu']->invalid/$data['lc_dst_fld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                         <td>Total no. of Sample received <br />for perticular period</td>
                                                       </tr>
                                                       <tr>
                                                        <td>LC DST FLD Retest</td>
                                                        <td>{{$data['lc_dst_fld_retest']->dx?$data['lc_dst_fld_retest']->dx:0}}</td>
                                                        <td>{{$data['lc_dst_fld_total']->dx?number_format($data['lc_dst_fld_retest']->dx/$data['lc_dst_fld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                        <td>{{$data['lc_dst_fld_retest']->fu?$data['lc_dst_fld_retest']->fu:0}}</td>
                                                        <td>{{$data['lc_dst_fld_total']->fu?number_format($data['lc_dst_fld_retest']->fu/$data['lc_dst_fld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                        <td>Total no. of Sample received <br /> Invalid</td>
                                                      </tr>
                                                      <tr>
                                                       <td>LC DST SLD Valid</td>
                                                       <td>{{$data['lc_dst_sld_d']->valid?$data['lc_dst_sld_d']->valid:0}}</td>
                                                       <td>{{$data['lc_dst_sld_total']->dx?number_format($data['lc_dst_sld_d']->valid/$data['lc_dst_sld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                       <td>{{$data['lc_dst_sld_fu']->valid?$data['lc_dst_sld_fu']->valid:0}}</td>
                                                       <td>{{$data['lc_dst_sld_total']->fu?number_format($data['lc_dst_sld_fu']->valid/$data['lc_dst_sld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                       <td>Total no. of Sample received <br />for perticular period</td>
                                                     </tr>
                                                     <tr>
                                                      <td>LC DST SLD Invalid</td>
                                                      <td>{{$data['lc_dst_sld_d']->invalid?$data['lc_dst_sld_d']->invalid:0}}</td>
                                                      <td>{{$data['lc_dst_sld_total']->dx?number_format($data['lc_dst_sld_d']->invalid/$data['lc_dst_sld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                      <td>{{$data['lc_dst_sld_fu']->invalid?$data['lc_dst_sld_fu']->invalid:0}}</td>
                                                      <td>{{$data['lc_dst_sld_total']->fu?number_format($data['lc_dst_sld_fu']->invalid/$data['lc_dst_sld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                      <td>Total no. of Sample received <br />for perticular period</td>
                                                    </tr>
                                                    <tr>
                                                     <td>LC DST SLD Retest</td>
                                                     <td>{{$data['lc_dst_sld_retest']->dx?$data['lc_dst_sld_retest']->dx:0}}</td>
                                                     <td>{{$data['lc_dst_sld_total']->dx?number_format($data['lc_dst_sld_retest']->dx/$data['lc_dst_sld_total']->dx*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['lc_dst_sld_retest']->fu?$data['lc_dst_sld_retest']->fu:0}}</td>
                                                     <td>{{$data['lc_dst_sld_total']->fu?number_format($data['lc_dst_sld_retest']->fu/$data['lc_dst_sld_total']->fu*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of Sample received <br /> Invalid</td>
                                                   </tr>
                                                   <tr>
                                                    <td>AFB Culture by LC negative</td>
                                                    <td>{{$data['afb_lc_dx']->neg?$data['afb_lc_dx']->neg:0}}</td>
                                                    <td>{{number_format($data['afb_total']?$data['afb_lc_dx']->neg/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                    <td>{{$data['afb_lc_fu']->neg?$data['afb_lc_fu']->neg:0}}</td>
                                                    <td>{{number_format($data['afb_total']?$data['afb_lc_fu']->neg/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                    <td>Total no. of Sample received <br />for perticular period for Diagnosis and followup</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by LC positive</td>
                                                     <td>{{$data['afb_lc_dx']->pos?$data['afb_lc_dx']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_dx']->pos/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_fu']->pos?$data['afb_lc_fu']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_fu']->pos/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of AFB culture LC positive</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by LC positive and NTM positive</td>
                                                     <td>{{$data['afb_lc_dx']->pos_ntm?$data['afb_lc_dx']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_dx']->pos_ntm/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_fu']->pos_ntm?$data['afb_lc_fu']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_fu']->pos_ntm/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of Sample received <br />for perticular period for Diagnosis and followup</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by LC contaminated</td>
                                                     <td>{{$data['afb_lc_dx']->con?$data['afb_lc_dx']->con:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_dx']->con/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_fu']->con?$data['afb_lc_fu']->con:0}}</td>
                                                     <td>{{number_format($data['afb_total']?$data['afb_lc_fu']->con/$data['afb_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of Sample received <br />for perticular period for Diagnosis and followup</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by Solid negative</td>
                                                     <td>{{$data['afb_lj_dx']->neg?$data['afb_lj_dx']->neg:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_dx']->neg/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lj_fu']->neg?$data['afb_lj_fu']->neg:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_fu']->neg/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of sample only inoculated in Solid</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFData exportB Culture by Solid positive</td>
                                                     <td>{{$data['afb_lj_dx']->pos?$data['afb_lj_dx']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_dx']->pos/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lj_fu']->pos?$data['afb_lj_fu']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_fu']->pos/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of sample only inoculated in Solid</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by Solid positive and NTM positive</td>
                                                     <td>{{$data['afb_lj_dx']->pos_ntm?$data['afb_lj_dx']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_dx']->pos_ntm/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lj_fu']->pos_ntm?$data['afb_lj_fu']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_fu']->pos_ntm/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of AFB culture LC positive</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture by Solid contaminated</td>
                                                     <td>{{$data['afb_lj_dx']->con?$data['afb_lj_dx']->con:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_dx']->con/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lj_fu']->con?$data['afb_lj_fu']->con:0}}</td>
                                                     <td>{{number_format($data['afb_lj_total']?$data['afb_lj_fu']->con/$data['afb_lj_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of AFB culture LC positive</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture negative by Both LC and LJ</td>
                                                     <td>{{$data['afb_lc_lj_dx']->neg?$data['afb_lc_lj_dx']->neg:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_dx']->neg/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_lj_fu']->neg?$data['afb_lc_lj_fu']->neg:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_fu']->neg/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of sample only inoculated in Solid</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture positive by Both LC and LJ</td>
                                                     <td>{{$data['afb_lc_lj_dx']->pos?$data['afb_lc_lj_dx']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_dx']->pos/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_lj_fu']->pos?$data['afb_lc_lj_fu']->pos:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_fu']->pos/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of sample only inoculated in Solid</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture positive by Both LC and LJ and NTM positive</td>
                                                     <td>{{$data['afb_lc_lj_dx']->pos_ntm?$data['afb_lc_lj_dx']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_dx']->pos_ntm/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_lj_fu']->pos_ntm?$data['afb_lc_lj_fu']->pos_ntm:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_fu']->pos_ntm/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of sample only inoculated in Solid</td>
                                                    </tr>
                                                    <tr>
                                                     <td>AFB Culture contaminated</td>
                                                     <td>{{$data['afb_lc_lj_dx']->con?$data['afb_lc_lj_dx']->con:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_dx']->con/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>{{$data['afb_lc_lj_fu']->con?$data['afb_lc_lj_fu']->con:0}}</td>
                                                     <td>{{number_format($data['afb_lj_lc_total']?$data['afb_lc_lj_fu']->con/$data['afb_lj_lc_total']*100:0, 2, '.', ',')}}%</td>
                                                     <td>Total no. of AFB culture LC positive</td>
                                                    </tr>
                                                    <tr>
                                                     <td>CBNAAT MTB detected RIF Not detected</td>
                                                     <td>{{$data['cbnaat_diagnosis']->mtb_det_ref_ndet?$data['cbnaat_diagnosis']->mtb_det_ref_ndet:0}}</td>
                                                     <td>{{$data['cbnaat_diagnosis']->total?number_format($data['cbnaat_diagnosis']->mtb_det_ref_ndet/$data['cbnaat_diagnosis']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['cbnaat_fu']->mtb_det_ref_ndet?$data['cbnaat_fu']->mtb_det_ref_ndet:0}}</td>
                                                     <td>{{$data['cbnaat_fu']->total?number_format($data['cbnaat_fu']->mtb_det_ref_ndet/$data['cbnaat_fu']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of sample received for CBNAAT testing</td>
                                                    </tr>
                                                    <tr>
                                                     <td>CBNAAT MTB detected RIF detected</td>
                                                     <td>{{$data['cbnaat_diagnosis']->mtb_ref_det?$data['cbnaat_diagnosis']->mtb_ref_det:0}}</td>
                                                     <td>{{$data['cbnaat_diagnosis']->total?number_format($data['cbnaat_diagnosis']->mtb_ref_det/$data['cbnaat_diagnosis']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['cbnaat_fu']->mtb_ref_det?$data['cbnaat_fu']->mtb_ref_det:0}}</td>
                                                     <td>{{$data['cbnaat_fu']->total?number_format($data['cbnaat_fu']->mtb_ref_det/$data['cbnaat_fu']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of sample received for CBNAAT testing</td>
                                                    </tr>
                                                    <tr>
                                                     <td>CBNAAT MTB Invalid</td>
                                                     <td>{{$data['cbnaat_diagnosis']->mtb_invalid?$data['cbnaat_diagnosis']->mtb_invalid:0}}</td>
                                                     <td>{{$data['cbnaat_diagnosis']->total?number_format($data['cbnaat_diagnosis']->mtb_invalid/$data['cbnaat_diagnosis']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['cbnaat_fu']->mtb_invalid?$data['cbnaat_fu']->mtb_invalid:0}}</td>
                                                     <td>{{$data['cbnaat_fu']->total?number_format($data['cbnaat_fu']->mtb_invalid/$data['cbnaat_fu']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of sample received for CBNAAT testing</td>
                                                    </tr>
                                                    <tr>
                                                     <td>CBNAAT MTB Not detected</td>
                                                     <td>{{$data['cbnaat_diagnosis']->mtb_ndet?$data['cbnaat_diagnosis']->mtb_ndet:0}}</td>
                                                     <td>{{$data['cbnaat_diagnosis']->total?number_format($data['cbnaat_diagnosis']->mtb_ndet/$data['cbnaat_diagnosis']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['cbnaat_fu']->mtb_ndet?$data['cbnaat_fu']->mtb_ndet:0}}</td>
                                                     <td>{{$data['cbnaat_fu']->total?number_format($data['cbnaat_fu']->mtb_ndet/$data['cbnaat_fu']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of sample received for CBNAAT testing</td>
                                                    </tr>
                                                    <tr>
                                                     <td>CBNAAT Retest</td>
                                                     <td>{{$data['cbnaat_retest']->dx?$data['cbnaat_retest']->dx:0}}</td>
                                                     <td>{{$data['cbnaat_diagnosis']->total?number_format($data['cbnaat_retest']->dx/$data['cbnaat_diagnosis']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>{{$data['cbnaat_retest']->fu?$data['cbnaat_retest']->fu:0}}</td>
                                                     <td>{{$data['cbnaat_fu']->total?number_format($data['cbnaat_retest']->fu/$data['cbnaat_fu']->total*100, 2, '.', ','):0.00}}%</td>
                                                     <td>Total no. of CBNAAT invalid</td>
                                                    </tr>

                                                    </tbody>
                                                    </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>

        </div>
        <script>
        $(document).ready(function() {
            $('#example1').DataTable({
              searching: false,
              paging: false,
              ordering: false,
              dom: 'Bfrtip',
              buttons: [
                 {
                     extend: 'excelHtml5',
                     title: 'LAB QC indicator',
                     messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 },
                 {
                     extend: 'pdfHtml5',
                     title: 'LAB QC indicator',
                      messageTop: 'From Date: ' + $("#from_date").val() + '    To Date:' + $("#to_date").val()

                 }
            ],

            });
        } );

         var _customizeExcelOptions = function (xlsx) {
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            var numrows = 5;
            var clR = $('row', sheet);

            $('row c ', sheet).each(function () {
                var attr = $(this).attr('r');
                var pre = attr.substring(0, 1);
                var ind = parseInt(attr.substring(1, attr.length));
                ind = ind + numrows;
                $(this).attr("r", pre + ind);
            });

          }
        </script>
    @endsection
