<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::resource('/hello', 'Web\Admin\HelloController');


 //Route::get('/admin_login','Web\Admin\UserController@admin_login')->name('admin_login');

 //Route::post('/login', 'Auth\LoginController@webAuthenticate')->name('webAuthenticate');
Route::group(['middleware'=>'auth:web'], function(){
 Route::get('/dashboard', 'Web\Admin\DashboardController@index');

 Route::resource('/microbiologist', 'Web\Admin\MicroController');

 Route::get('/logout', 'Web\Admin\DashboardController@logout');

 Route::resource('/enroll/patient', 'Web\Admin\PatientController');

 Route::resource('/lab', 'Web\Admin\LabController');

 Route::resource('/sample', 'Web\Admin\SampleController');
 Route::get('/sample/print/{sample_str}', 'Web\Admin\SampleController@barCodePrint');



 Route::get('/pdfview/{id}/{pdf?}', 'Web\Admin\SampleController@pdfview');
 Route::get('/annexurek', 'Web\Admin\SampleController@annexurek');
 Route::get('/annexurel', 'Web\Admin\SampleController@annexurel');

 Route::get('/sample/create/{id}', 'Web\Admin\SampleController@create');

 Route::get('test_request/create/{id}', [
    'as' => 'test_request.create',
    'uses' => 'Web\Admin\TestRequestController@create'
 ]);
 Route::resource('/test_request', 'Web\Admin\TestRequestController',['except' => [
    'create',
 ]]);

 Route::get('/result/{serviceId}/{sampleId}/{enrollmentId}','Web\Admin\MicroController@result')->name('result');

 Route::resource('/adduser', 'Web\Admin\AddUserController');

 Route::group(['middleware'=>['role:cbnaat,can_view']], function(){
   Route::resource('/cbnaat', 'Web\Admin\CbnaatController');
 });

 Route::group(['middleware'=>['role:microscopy,can_view']], function(){
   Route::resource('/microscopy', 'Web\Admin\MicroscopyController');
 });

 Route::group(['middleware'=>['role:microscopy_review,can_view']], function(){
   Route::resource('/review_microscopy', 'Web\Admin\MicroscopyReviewController');
 });

 Route::resource('/serviceLog', 'Web\Admin\ServiceLogController');

 Route::group(['middleware'=>['role:dna_extraction,can_view']], function(){
   Route::resource('/DNAextraction', 'Web\Admin\DNAextractionController');
 });

 Route::group(['middleware'=>['role:hybridization,can_view']], function(){
   Route::resource('/hybridization', 'Web\Admin\HybridizationController');
 });

 Route::group(['middleware'=>['role:decontamination_review,can_view']], function(){
   Route::resource('/decontamination', 'Web\Admin\DecontaminationController');
 });

 Route::group(['middleware'=>['role:decontamination,can_view']], function(){
   Route::resource('/dash_decontamination', 'Web\Admin\DashboardDecontaminationController');
 });

 Route::get('/cbnaat/submit/{id}', 'Web\Admin\CbnaatController@cbnaat_submit');

 Route::group(['middleware'=>['role:enroller,can_view']], function(){
   Route::resource('/enroll', 'Web\Admin\EnrollController');
 });

 Route::group(['middleware'=>['role:pcr,can_view']], function(){
   Route::resource('/PCR', 'Web\Admin\PCRController');
 });

 Route::group(['middleware'=>['role:lpa_interpretation,can_view']], function(){
   Route::resource('/lpa_interpretation', 'Web\Admin\LPAController');

 });

 Route::group(['middleware'=>['role:culture_inoculation,can_view']], function(){
   Route::resource('/culture_inoculation', 'Web\Admin\CultureInoculationController');
 });

 Route::group(['middleware'=>['role:lc_flagged_mgit,can_view']], function(){
   Route::resource('/lc_flagged_mgit', 'Web\Admin\LCFlaggedMGITController');
 });

 Route::group(['middleware'=>['role:lc_flagged_mgit_further,can_view']], function(){
   Route::resource('/further_lc_flagged_mgit', 'Web\Admin\LCFlaggedMGITFurtherController');
 });

 Route::group(['middleware'=>['role:lc_result_review,can_view']], function(){
   Route::resource('/lc_result_review', 'Web\Admin\LCResultReviewController');
 });

 Route::group(['middleware'=>['role:lj,can_view']], function(){
   Route::resource('/dashboardlj', 'Web\Admin\LJController');
 });

 Route::group(['middleware'=>['role:lj_review,can_view']], function(){
   Route::resource('/reviewlj', 'Web\Admin\LJReviewController');
 });

 Route::group(['middleware'=>['role:lc_dst_inoculation,can_view']], function(){
   Route::resource('/lc_dst_inoculation', 'Web\Admin\LCDSTInoculationController');
 });

 Route::group(['middleware'=>['role:lj_dst_1st_line,can_view']], function(){
   Route::resource('/lj_dst_ln1', 'Web\Admin\LJDST1Controller');
 });

 Route::post('/DNANext', 'Web\Admin\DNAextractionController@DNANext');

 Route::post('/lj_dst_ln1/inoculation', 'Web\Admin\LJDST1Controller@inoculation');
 Route::post('/lj_dst_ln1/reading', 'Web\Admin\LJDST1Controller@reading');
Route::post('/lj_dst_ln1/readingReview', 'Web\Admin\LJDST1Controller@reading_review');
 Route::get('/lj_dst_ln1/detail/{id}/{week}', 'Web\Admin\LJDST1Controller@detail');


 Route::group(['middleware'=>['role:lj_dst_2st_line,can_view']], function(){
   Route::resource('/lj_dst_ln2', 'Web\Admin\LJDST2Controller');
 });

Route::get('/district/{state}','Web\Admin\PatientController@district_collect')->name('district');

Route::get('/phi/{state}/{tbu}/{district}','Web\Admin\TestRequestController@phi_collect')->name('phi');

Route::get('/tbunit/{state}/{district}','Web\Admin\TestRequestController@tbunit_collect')->name('tbunit');

Route::get('/pcr','Web\Admin\TestController@pcr')->name('pcr');

Route::get('/Hybridization','Web\Admin\TestController@Hybridization')->name('Hybridization');

Route::get('/culture_inoc','Web\Admin\TestController@culture_inoc')->name('culture_inoc');

Route::get('/culture_inoc_flag','Web\Admin\TestController@culture_inoc_flag')->name('culture_inoc_flag');

Route::get('/', 'Web\Admin\DashboardController@index');

Route::resource('/hr','Web\Admin\HRController');
Route::post('/hr/yearDesignationFilter','Web\Admin\HRController@yearFilter');
Route::post('/hr/yearOrganizationFilter','Web\Admin\HRController@yearOrganizationFilter');
Route::post('/hr/yearTypeFilter','Web\Admin\HRController@yearTypeFilter');
Route::get('/hr/{id}/delete_hr','Web\Admin\HRController@delete_hr');

Route::resource('/equipment','Web\Admin\EquipmentController');
Route::post('/equipment/downtimeAnalysis','Web\Admin\EquipmentController@downtimeAnalysis');
Route::post('/equipment/downtimeAnalysisDatefilfer','Web\Admin\EquipmentController@downtimeAnalysisDatefilfer');
Route::post('/equipment/freqdowntimeAnalysis','Web\Admin\EquipmentController@freqdowntimeAnalysis');
Route::get('/equipment/{id}/delete_equipment','Web\Admin\EquipmentController@delete_equipment');
Route::post('/equipment/addbreakdown','Web\Admin\EquipmentController@addbreakdown');

Route::resource('/user_role','Web\Admin\RoleController');



Route::resource('/barcodes', 'Web\Admin\BarcodeController');

Route::post('/barcodes/print', 'Web\Admin\BarcodeController@printBarcodes');

Route::post('/enroll/print', 'Web\Admin\EnrollController@enrollPrint');

Route::post('/test_request/print', 'Web\Admin\TestRequestController@testrequestPrint');

Route::post('/cbnaat/print', 'Web\Admin\CbnaatController@cbnaatPrint');

Route::post('/microscopy/print', 'Web\Admin\MicroscopyController@microscopyPrint');

Route::post('/review_microscopy/print', 'Web\Admin\MicroscopyReviewController@mreviewPrint');

Route::post('/dashboardDecontamination/print', 'Web\Admin\DashboardDecontaminationController@dashdecontPrint');

Route::post('/Decontamination/print', 'Web\Admin\DecontaminationController@decontaminationPrint');

Route::post('/DNA/print', 'Web\Admin\DNAextractionController@dnaextractprint');

Route::post('/pcr/print', 'Web\Admin\PCRController@PCRprint');

Route::post('/hybrid/print', 'Web\Admin\HybridizationController@Hybridizationprint');

Route::post('/lpa/print', 'Web\Admin\LPAController@Lpaprint');

Route::post('/cultureInno/print', 'Web\Admin\CultureInoculationController@cultureInnoprint');

Route::post('/cultureInno/printBarcode', 'Web\Admin\CultureInoculationController@printBarcode');

Route::post('/lcflag/print', 'Web\Admin\LCFlaggedMGITController@lcflagprint');

Route::post('/lcflagfurther/print', 'Web\Admin\LCFlaggedMGITFurtherController@lcflagprint');

Route::post('/lcflagreview/print', 'Web\Admin\LCResultReviewController@lcflagprint');

Route::post('/dashboardlj/print', 'Web\Admin\LJController@ljprint');

Route::post('/ljreview/print', 'Web\Admin\LJReviewController@ljreviewprint');

Route::post('/lcdstinoculation/print', 'Web\Admin\LCDSTInoculationController@lcdstinoculationprint');

Route::post('/ljdstfirst/print', 'Web\Admin\LJDST1Controller@ljdstfirstprint');

Route::post('/ljdstsecond/print', 'Web\Admin\LJDST2Controller@ljdstsecondprint');

Route::post('/ljdstsecond/print', 'Web\Admin\LJDST2Controller@ljdstsecondprint');

Route::post('/microbiologist/print', 'Web\Admin\MicroController@microbiologistprint');

Route::post('/hr/print', 'Web\Admin\HRController@hrprint');

Route::post('/equipment/print', 'Web\Admin\EquipmentController@equipmentprint');

Route::resource('/laboratory', 'Web\Admin\LaboratoryController');

Route::resource('/passwordReset', 'Web\Admin\PasswordController');

Route::post('/laboratory/edit_config', 'Web\Admin\LaboratoryController@edit_config');

Route::post('/laboratory/view', 'Web\Admin\LaboratoryController@view_form');

Route::post('/adduser/{id}/updateform', 'Web\Admin\AddUserController@update_form');

Route::get('/samplePreview/{id}', 'Web\Admin\SampleController@sample_preview');

Route::get('/report/lqc_indicator', 'Web\Admin\ReportController@lqc_indicator');
Route::post('/report/lqc_indicator', 'Web\Admin\ReportController@lqc_indicator');
Route::get('/report/lqcIndicator_tat', 'Web\Admin\ReportController@lqc_indicator_tat');
Route::post('/report/lqcIndicator_tat', 'Web\Admin\ReportController@lqc_indicator_tat');
Route::get('/report/quality_indicator', 'Web\Admin\ReportController@quality_indicator');
Route::post('/report/quality_indicator', 'Web\Admin\ReportController@quality_indicator');
Route::get('/report/referral_indicator', 'Web\Admin\ReportController@referral_indicator');
Route::post('/report/referral_indicator', 'Web\Admin\ReportController@referral_indicator');
Route::get('/report/cbnaat_indicator', 'Web\Admin\ReportController@cbnaat_indicator');
Route::post('/report/cbnaat_indicator', 'Web\Admin\ReportController@cbnaat_indicator');
Route::get('/report/performance_indicator', 'Web\Admin\ReportController@performance_indicator');
Route::post('/report/performance_indicator', 'Web\Admin\ReportController@performance_indicator');
Route::get('/report/cbnaat_monthly_report', 'Web\Admin\ReportController@cbnaat_monthly_report');
Route::post('/report/cbnaat_monthly_report/submit', 'Web\Admin\ReportController@cbnaat_monthly_report_submit');
Route::get('/report/referral_indicator', 'Web\Admin\ReportController@referral_indicator');
Route::post('/report/referral_indicator', 'Web\Admin\ReportController@referral_indicator');
Route::get('/report/result_edit', 'Web\Admin\ReportController@result_edit');

Route::resource('/report/workload', 'Web\Admin\ReportController');

Route::resource('/bioWaste', 'Web\Admin\BioWasteController');
Route::resource('/bioWaste/print', 'Web\Admin\BioWasteController@biowasteprint');

Route::resource('/calendar', 'Web\Admin\CalenderController');

Route::get('/edit_result_micro/{sample}', 'Web\Admin\EditResultController@edit_result_micro');
Route::get('/editResultCbnaat/{sample}', 'Web\Admin\EditResultController@edit_result_cbnaat');
Route::get('/edit_result_lc/{sample}', 'Web\Admin\EditResultController@edit_result_lc');
Route::get('/edit_result_lj/{sample}', 'Web\Admin\EditResultController@edit_result_lj');
Route::get('/edit_result_lj_dst1/{sample}', 'Web\Admin\EditResultController@edit_result_lj_dst1');
Route::get('/edit_result_lj_dst2/{sample}', 'Web\Admin\EditResultController@edit_result_lj_dst2');
Route::get('/edit_result_lc_dst/{sample}', 'Web\Admin\EditResultController@edit_result_lc_dst');
Route::get('/editResultLpa/{sample}', 'Web\Admin\EditResultController@editResultLpa');

});




Auth::routes();

Route::get('/home', 'HomeController@index');
